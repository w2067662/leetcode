//https://leetcode.com/problems/lucky-numbers-in-a-matrix/description/

class Solution {
public:
    bool isMinInRow(int i,int j,vector<vector<int>>& matrix){   //check is min number in row or not
        for(int y=0;y<matrix[i].size();y++){
            if(matrix[i][y]<matrix[i][j] && y!=j) return false;
        }
        return true;
    }

    bool isMaxInCol(int i,int j,vector<vector<int>>& matrix){   //check is max number in column or not
        for(int x=0;x<matrix.size();x++){
            if(matrix[x][j]>matrix[i][j] && x!=i) return false;
        }
        return true;
    }

    vector<int> luckyNumbers (vector<vector<int>>& matrix) {
        vector<int> luckyNum;
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){
                //if is min in row AND is max in column, then found lucky number
                if(isMinInRow(i, j, matrix) && isMaxInCol(i, j, matrix))luckyNum.push_back(matrix[i][j]);
            }
        }
        return luckyNum;
    }
};
