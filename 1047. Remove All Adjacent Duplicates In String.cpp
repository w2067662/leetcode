//https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/description/

class Solution {
public:
    string removeDuplicates(string s) {
        for(int i=1;i<s.length();){
            if(s[i-1] == s[i]){
                s.erase(i-1, 2);        //remove all the consecutive duplicates
                i -= i>1 ? 1 : 0;
            }else i++;
        }

        return s;
    }
};
