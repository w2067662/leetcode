//https://leetcode.com/problems/maximum-value-of-an-ordered-triplet-i/description/

class Solution {
public:
    long long maximumTripletValue(vector<int>& nums) {
        long long ans = 0, temp;

        for(int i=0;i<nums.size()-2;i++){
            for(int j=i+1;j<nums.size()-1;j++){
                if(nums[i] > nums[j]){
                    for(int k=j+1;k<nums.size();k++){                                 //find a triplet (i,j,k) that 
                        temp = (long long)(nums[i] - nums[j]) * (long long)(nums[k]); // i<j<k  AND  value = (nums[i] - nums[j]) * nums[k]
                        ans = max(temp, ans);                                         //store the max value of triplet (i,j,k)
                    }
                }
            }
        }

        return ans;
    }
};
