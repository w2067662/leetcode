//https://leetcode.com/problems/delete-nodes-and-return-forest/description/

//ex:  root = [1,2,3,4,5,6,7], to_delete = [3,5,1]
//
//     root =      1         ; set = [1,3,5]
//               2   3                        (Step 1. Insert the delete value from vector to set)
//              4 5 6 7
//
//     root =        -1       
//                2      -1                   (Step 2. Traverse the tree and mark all the delete value as -1)
//              4  -1   6   7
//
//   forest = [   2   ,  6  ,  7  ]           (Step 3. Split the trees by deleted node (value = -1) into forest)
//              4  -1
//
//   forest = [   2   ,  6  ,  7  ]           (Step 4. Trim all the trees in forest)
//              4  

class Solution {
public:
    void traverse(TreeNode* root, set<int> &set){ //traverse the tree to find delete nodes
        if(!root) return;

        if(set.find(root->val) != set.end()) root->val = -1;

        traverse(root->left , set);
        traverse(root->right, set);
        return;
    }

    void split(TreeNode* root, vector<TreeNode*> &forest){ //split the tree into forest
        if(!root) return;

        if(root->val == -1){                                              //IF   current node is deleted
            if(root->left){                                               //     IF   left child exist
                if(root->left->val  != -1) forest.push_back(root->left);  //          IF   not deleted
            }                                                             //               -> push subtree's root into forest
            if(root->right){                                              //     IF   right child exist
                if(root->right->val != -1) forest.push_back(root->right); //          IF   not deleted
            }                                                             //               -> push subtree's root into forest
        }

        split(root->left , forest);
        split(root->right, forest);
        return;
    }

    TreeNode* trim(TreeNode* root){
        if(!root) return NULL;
        if(root->val == -1) return NULL;  //IF  current node is deleted -> cut the subtree (return NULL)

        root->left  = trim(root->left);   //trim left  subtree
        root->right = trim(root->right);  //trim right subtree
        return root;
    }

    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& to_delete) {
        vector<TreeNode*> forest;
        set<int> set;

        //Step 1. Insert the delete value from vector to set
        for(const auto val: to_delete) set.insert(val);

        //Step 2. Traverse the tree and mark all the delete value as -1
        traverse(root, set);

        //Step 3. Split the trees by deleted node (value = -1) into forest
        if(root){
            if(root->val != -1) forest.push_back(root);
        }
        split(root, forest);

        //Step 4. Trim all the trees in forest
        for(auto& tree: forest) tree = trim(tree);

        return forest;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
