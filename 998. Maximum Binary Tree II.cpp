
class Solution {
public:
    int getMaxNumberIndex(const vector<int>& nums, int left, int right){ //get the max number's index within the range = {left, right}
        int maxNumIndex = 0, maxNum = -1;

        for(int i=left; i<=right; i++){
            if(nums[i] > maxNum){
                maxNum = nums[i];
                maxNumIndex = i;
            }
        }

        return maxNumIndex;
    }

    TreeNode* buildTree(const vector<int>& nums, int left, int right){ //build the tree
        if(left > right) return NULL;

        int pivot = getMaxNumberIndex(nums, left, right);   //get the max number's index as pivot

        return new TreeNode(                                //create a new node with
            nums[pivot],                                    //      -> value of pivot index number in vector
            buildTree(nums,    left, pivot-1),              //      -> left  subtree built by  left subarray
            buildTree(nums, pivot+1,   right)               //      -> right subtree built by right subarray
        );
    }

    void traverse(TreeNode* root, vector<int> &inorder){ //traverse the tree and get the node values inorder
        if(!root) return;

        traverse(root->left , inorder);
        inorder.push_back(root->val);
        traverse(root->right, inorder);

        return;
    }

    TreeNode* insertIntoMaxTree(TreeNode* root, int val) {
        vector<int> inorder;

        traverse(root, inorder);                         //traverse the tree and get the node values inorder
        inorder.push_back(val);                          //push the value into inorder vector's end

        return buildTree(inorder, 0, inorder.size()-1);  //build the tree with inorder vector
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
