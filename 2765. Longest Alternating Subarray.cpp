//https://leetcode.com/problems/longest-alternating-subarray/description/

class Solution {
public:
    int alternatingSubarray(vector<int>& nums) {
        int ans = -1;                                //no possible answer -> return -1
        for(int i=0, x=0, count=1; i<nums.size()-1; i++){
            if(nums[i+1]-nums[i] == pow(-1,x)){      //check alternating
                x++;
                count++;
                ans = count > ans ? count : ans;
            } else {                                 //if number[i] and number[i+1] is not alternating
                x=0;                                 //     reset x
                count = 1;                           //     reset count
                if(nums[i+1]-nums[i] == pow(-1,x)){  //     restart checking alternating
                    x++;
                    count++;
                    ans = count > ans ? count : ans;
                }
            }
        }
        return ans;
    }
};
