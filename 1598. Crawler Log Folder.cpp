//https://leetcode.com/problems/crawler-log-folder/description/

class Solution {
public:
    int minOperations(vector<string>& logs) {
        int depth = 0;
        for(int i=0; i<logs.size(); i++){
            if(logs[i] == "../"){              //if encounter "../", go back to upper folder
                depth -= depth == 0 ? 0 : 1;   //   if depth == 0 -> no upper folder -> current depth = 0
                continue;
            }
            if(logs[i] != "./"){               //if encounter "x/", go to inner folder
                depth++;
                continue;
            }
        }

        return depth;
    }
};
