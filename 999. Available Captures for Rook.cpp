//https://leetcode.com/problems/available-captures-for-rook/description/

class Solution {
public:
    int availableCaptures(vector<vector<char>>& board, int i, int j){ //count attackable pawns
        int count = 0;
        int x, y;

        for(int x=j-1; x>=0; x--){              //count from left (<)
            if(board[i][x] == 'p'){
                count++;
                break;
            }else if(board[i][x] == 'B') break;
        }

        for(int x=j+1; x<board.size(); x++){    //count from right (>)
            if(board[i][x] == 'p'){
                count++;
                break;
            }else if(board[i][x] == 'B') break;
        }

        for(int y=i-1; y>=0; y--){              //count from up (^)
            if(board[y][j] == 'p'){
                count++;
                break;
            }else if(board[y][j] == 'B') break;
        }

        for(int y=i+1; y<board.size(); y++){    //count from down (v)
            if(board[y][j] == 'p'){
                count++;
                break;
            }else if(board[y][j] == 'B') break;
        }
        return count;
    }

    int numRookCaptures(vector<vector<char>>& board) {
        for(int i=0; i<board.size();i++){
            for(int j=0; j<board[i].size();j++){
                if(board[i][j] == 'R'){                    //when found Rook
                    return availableCaptures(board, i, j); //count for attckatable pawns
                }
            }
        }

        return -1;
    }
};
