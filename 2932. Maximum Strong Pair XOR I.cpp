//https://leetcode.com/problems/maximum-strong-pair-xor-i/description/

class Solution {
public:
    int maximumStrongPairXor(vector<int>& nums) {
        int maxXor = 0, XOR;

        for(const auto a: nums){
            for(const auto b: nums){
                if(abs(a-b) <= min(a, b)){      //IF   (a, b) is strong part (|x - y| <= min(x, y))
                    XOR = a ^ b;
                    maxXor = max(maxXor, XOR);  //     -> store the max XOR
                }
            }
        }

        return maxXor;
    }
};
