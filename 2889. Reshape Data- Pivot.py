#https://leetcode.com/problems/reshape-data-pivot/description/

import pandas as pd

def pivotTable(weather: pd.DataFrame) -> pd.DataFrame:
    return weather.pivot(index='month', columns='city', values='temperature')  
    
#+----------+--------+--------------+
#| month    | ElPaso | Jacksonville | < (columns)
#+----------+--------+--------------+
#| April    | 2      | 5            |
#| February | 6      | 23           |
#| January  | 20     | 13           |
#| March    | 26     | 38  (values) |
#| May      | 43     | 34           |
#+----------+--------+--------------+
#  ^ (index)
