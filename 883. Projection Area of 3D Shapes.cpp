//https://leetcode.com/problems/projection-area-of-3d-shapes/description/

class Solution {
public:
    int projectionArea(vector<vector<int>>& grid) {
        int area=0;

        for(int i=0;i<grid.size();i++){     //get xy-plane projection area
            for(int j=0;j<grid[i].size();j++){
                if(grid[i][j]>0)area++;
            }
        }

        for(int i=0;i<grid.size();i++){     //get xz-plane projection area
            int xzMax=0;
            for(int j=0;j<grid[i].size();j++){
                xzMax = grid[i][j]>xzMax ? grid[i][j]:xzMax;
            }
            area+=xzMax;
        }

        for(int i=0;i<grid.size();i++){     //get yz-plane projection area
            int yzMax=0;
            for(int j=0;j<grid[i].size();j++){
                yzMax = grid[j][i]>yzMax ? grid[j][i]:yzMax;
            }
            area+=yzMax;
        } 

        return area;
    }
};
