//https://leetcode.com/problems/generate-a-string-with-characters-that-have-odd-counts/description/

class Solution {
public:
    string generateTheString(int n) {
        string ans="";
        int countA = 0;
        int countB = 0;

        if(n%2==0){         //if even, 
            countA = n-1;   //append n-1 'a' to answer
            countB = 1;     //append   1 'b' to answer
        }else{              //if odd,
            countA = n;     //append   n 'a' to answer
        }                   //append   0 'b' to answer

        for(int i=0;i<countA;i++)ans+='a';
        for(int i=0;i<countB;i++)ans+='b';

        return ans;
    }
};
