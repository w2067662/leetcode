//https://leetcode.com/problems/delete-greatest-value-in-each-row/description/

class Solution {
public:
    int deleteGreatestValue(vector<vector<int>>& grid) {
        int ans=0;

        for(int i=0;i<grid.size();i++){             //sort all the rows ascending order
            sort(grid[i].begin(), grid[i].end());
        }

        for(int j=0;j<grid[0].size();j++){          //find max number in each column
            int max=-1;
            for(int i=0;i<grid.size();i++){
                if(grid[i][j]>max) max=grid[i][j];
            }
            ans+=max;                               //add up sum
        }
        
        return ans;
    }
};
