//https://leetcode.com/problems/time-needed-to-inform-all-employees/description/

class Node{ //define node
public:
    int val;
    vector<Node*> children;
    Node(int n):val(n){};
};

class Solution {
public:
    bool isLeaf(Node* node){ //check current node is leaf node or not
        return node->children.size() == 0;
    }

    void traverse(Node* node, int currMinutes, int &maxMinutes, vector<int>& informTime){
        if(!node) return;

        if(isLeaf(node)) maxMinutes = max(maxMinutes, currMinutes);                                  //IF   current node is leaf node -> store the max minutes for informing

        for(int i=0;i<node->children.size();i++){
            traverse(node->children[i], currMinutes + informTime[node->val], maxMinutes, informTime);//add up inform time of current manager and traverse children nodes
        }

        return;
    }

    int numOfMinutes(int n, int headID, vector<int>& manager, vector<int>& informTime) {
        vector<Node*> nodes;
        Node* head = NULL;
        int maxMinutes = 0;

        for(int i=0;i<n;i++) nodes.push_back(new Node(i));                        //create nodes and push into vector

        head = nodes[headID];                                                     //set the head node by headID

        for(int i=0;i<manager.size();i++) {
            if(manager[i] != -1) nodes[manager[i]]->children.push_back(nodes[i]); //connect the manager to employee
        }

        traverse(head, 0, maxMinutes, informTime);                                //traverse the tree to get the maxMinutes for informing

        return maxMinutes;
    }
};
