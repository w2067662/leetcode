#https://leetcode.com/problems/reshape-data-concatenate/description/

import pandas as pd

def concatenateTables(df1: pd.DataFrame, df2: pd.DataFrame) -> pd.DataFrame:
    return pd.concat([df1, df2])

    #df= pd.merge(df1,df2,how='outer')
    #return df

#concat() : concat the tables
#merge() : merge the tables 
    
