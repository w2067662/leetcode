//https://leetcode.com/problems/find-the-distance-value-between-two-arrays/description/

class Solution {
public:
    int findTheDistanceValue(vector<int>& arr1, vector<int>& arr2, int d) {
        int ans=0;
        for(int i=0;i<arr1.size();i++){
            bool inDistanceValue = false;
            for(int j=0;j<arr2.size();j++){     //traverse array2
                if(abs(arr1[i]-arr2[j])<=d){    //if find number's value in distance
                    inDistanceValue = true;     //set inDistanceValue = true
                    break;                      //end the loop
                }
            }
            if(!inDistanceValue) ans++;         //current nubmer in array1 has no inDistanceValue_number in array2
        }
        return ans;
    }
};
