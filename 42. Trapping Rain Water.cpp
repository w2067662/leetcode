//https://leetcode.com/problems/trapping-rain-water/description/

class Solution {
public:
    //ex1:             height = [0,1,0,2,1,0,1,3,2,1,2,1]
    //                                      
    //                                      
    //                                         X
    //                                 X       X X   X
    //                             X   X X   X X X X X X
    //                      ---------------------------------
    //                   left = [0,1,1,2,2,2,2,3,3,3,3,3]
    //                  right = [3,3,3,3,3,3,3,3,2,2,2,1]
    // min(left,right)-height = [0,0,1,0,1,2,1,0,0,1,0,0] => 0+0+1+0+1+2+1+0+0+1+0+0 = 6

    //ex2:             height = [4,2,0,3,2,5]
    //                                     X
    //                           X         X
    //                           X     X   X
    //                           X X   X X X       
    //                           X X   X X X
    //                      ---------------------
    //                   left = [4,4,4,4,4,5]
    //                  right = [5,5,5,5,5,5]
    // min(left,right)-height = [0,2,4,1,2,0] => 0+2+4+1+2+0 = 9

    int trap(vector<int>& height) {
        vector<int> left(height.size(), 0), right(height.size(), 0);
        int traps = 0;

        for(int i=0; i<height.size(); i++){
            left[i] = (i == 0) ?                         // IF   i = 0   -> left[0] = height[0]
                height[i] : max(left[i-1], height[i]);   // ELSE         -> left[i] = max(left[i-1], height[i])
        }

        for(int i=height.size()-1; i>=0; i--){
            right[i] = (i == height.size()-1) ?          // IF   i = 0   -> right[0] = height[0]
                height[i] : max(right[i+1], height[i]);  // ELSE         -> right[i] = max(right[i+1], height[i])
        }

        for(int i=0; i<height.size(); i++){
            traps += min(left[i], right[i]) - height[i]; // Add up current traps
        }
        
        return traps;
    }
};