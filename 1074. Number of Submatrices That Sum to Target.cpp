//https://leetcode.com/problems/number-of-submatrices-that-sum-to-target/description/

class Solution {
public:
    //ex:  matrix = [[1,-1],   ;  target = 0
    //               [-1,1]] 
    //
    //    matrix' = [[1,0],
    //               [0,0]] 
    //
    // submatices = [[1]], [[0]], [[0]], [[0]], [[1,0]], [[0,0]], [[1], [[0], [[1,0],
    //                                                             [0]]  [0]]  [0,0]]
    //
    //        sum =   1      0      0      0       1        0       1     0      1      (5 submatices has sum = target) -> answer = 5
    //
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target) { 
        int count = 0;

        for(int i=0; i<matrix.size(); i++){                              // Traverse the matrix and calculate the sum from (0,0) to (i,j)
            for(int j=0; j<matrix[i].size(); j++){
                if(i > 0) matrix[i][j] += matrix[i-1][j];
                if(j > 0) matrix[i][j] += matrix[i][j-1];
                if(i > 0 && j > 0) matrix[i][j] -= matrix[i-1][j-1];
            }
        }

        for(int x1=0; x1<matrix.size(); x1++){                           // For start = (x1,y1) from (0,0) to (columnSize, rowSize)
            for(int y1=0; y1<matrix[x1].size(); y1++){
                for(int x2=x1; x2<matrix.size(); x2++){                  //     For end = (x2,y2) from (x1,y1) to  (columnSize, rowSize)
                    for(int y2=y1; y2<matrix[x2].size(); y2++){
                        int sum = matrix[x2][y2];

                        if(x1 > 0) sum -= matrix[x1-1][y2];              //         -> Calculate the sum of submatrix from (x1,y1) to (x2,y2)
                        if(y1 > 0) sum -= matrix[x2][y1-1];
                        if(x1 > 0 && y1 > 0) sum += matrix[x1-1][y1-1];

                        if(sum == target) count++;                       //         IF sum = target -> count+1
                    }
                }
            }
        }

        return count;
    }
};
