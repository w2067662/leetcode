//https://leetcode.com/problems/sort-the-matrix-diagonally/description/

class Solution {
public:
    //ex:  mat = [[3,3,1,1],
    //            [2,2,1,2],
    //            [1,1,1,2]]
    //
    //   lists = [[1],         ->   lists' =  [[1],      (sorted)
    //            [1,2],                       [1,2],
    //            [3,1,2],                     [1,2,3],
    //            [3,2,1],                     [1,2,3],
    //            [2,1],                       [1,2],
    //            [1]]                         [1]]
    //
    //    mat' = [[1,1,1,1],   (diagonal sorted)
    //            [1,2,2,2],
    //            [1,2,3,3]]
    //
    bool isValid(vector<vector<int>>& mat, int x, int y){ // Check current position is valid in the range of matrix
        return 0 <= x && x < mat.size() && 0 <= y && y < mat[0].size();
    }

    void storeToLists(pair<int, int> start, vector<vector<int>>& mat, vector<vector<int>>& lists){ // Store the values from matrix to lists
        vector<int> temp;
        int x=start.first, y=start.second; // Start point = (x, y)

        while(isValid(mat, x, y)){         // Go diagonal (x, y) -> (x+1, y+1)
            temp.push_back(mat[x][y]);
            x++;
            y++;
        }

        lists.push_back(temp);
    }

    void storeToMatrix(pair<int, int> start, vector<vector<int>>& mat, vector<vector<int>>& lists, int row){ // Store the values from lists to matrix
        int col = 0;
        int x=start.first, y=start.second; // Start point = (x, y)

        while(isValid(mat, x, y)){         // Go diagonal (x, y) -> (x+1, y+1)
            mat[x][y] = lists[row][col++];
            x++;
            y++;
        }
    }

    vector<vector<int>> diagonalSort(vector<vector<int>>& mat) {
        vector<vector<int>> lists;
        int index = 0;

        // Step1: store the values from matrix to lists
        for(int j=mat[0].size()-1; j>=0        ; j--) storeToLists({0, j}, mat, lists);  // Start storing from (0, n) to (0, 0)   (matrix to lists)
        for(int i=1              ; i<mat.size(); i++) storeToLists({i, 0}, mat, lists);  // Start storing from (1, 0) to (m, 0)   (matrix to lists)

        // Step2: sort all the lists in ascending order
        for(auto& list: lists){
            sort(list.begin(), list.end());
        }

        // Step3: store back to matrix
        for(int j=mat[0].size()-1; j>=0        ; j--) storeToMatrix({0, j}, mat, lists, index++);    // Start storing from (0, n) to (0, 0)   (lists to matrix)
        for(int i=1              ; i<mat.size(); i++) storeToMatrix({i, 0}, mat, lists, index++);    // Start storing from (1, 0) to (m, 0)   (lists to matrix)

        return mat;
    }
};
