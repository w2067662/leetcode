//https://leetcode.com/problems/minimum-increment-to-make-array-unique/description/

class Solution {
public:
    int minIncrementForUnique(vector<int>& nums) {
        sort(nums.begin(), nums.end());    // Sort the numbers in ascending order

        int curr = 0, moves = 0;

        for(const auto& num: nums){        // Traverse the numbers
            if(num >= curr){               //       IF  number >= current value
                curr = num + 1;            //           -> set (number + 1) as current value
            } else {                       //       IF  number <  current value
                moves += curr - num;       //           -> needs (curr - num) moves to make current number unique
                curr = curr + 1;           //           -> set (current value + 1) as current value
            }
        }

        return moves;
    }
};