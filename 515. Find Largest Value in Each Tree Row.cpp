//https://leetcode.com/problems/find-largest-value-in-each-tree-row/description/

class Solution {
public:
    void traverse(TreeNode* root, int level, vector<int> &ans){
        if(!root) return;

        if(level >= ans.size()) ans.push_back(root->val); //IF   no current level's max value -> push current node value to answer
        else ans[level] = max(ans[level], root->val);     //ELSE compare and store the current level's max value in answer

        traverse(root->left , level+1, ans); //traverse  left subtree
        traverse(root->right, level+1, ans); //traverse right subtree
        return;
    }

    vector<int> largestValues(TreeNode* root) { 
        vector<int> ans;
        traverse(root, 0, ans); //traverse the tree
        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
