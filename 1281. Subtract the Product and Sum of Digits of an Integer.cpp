//https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/description/

class Solution {
public:
    int subtractProductAndSum(int n) {
        string s = to_string(n);       //ex: n = 123 -> s = "123"
        int Product=1;
        int Sum =0;
        for(int i=0;i<s.length();i++){ 
            Product *= s[i]-'0';       //Product = 1x2x3 = 6
            Sum += s[i]-'0';           //Sum     = 1+2+3 = 6
        }
        return Product-Sum;  //Product-Sum = 6-6 = 0
    }
};
