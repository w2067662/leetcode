//https://leetcode.com/problems/minimum-time-to-type-word-using-special-typewriter/description/

class Solution {
public:
    int minTimeToType(string word) {
        int ans = 0;
        int move = 0;

        move = abs(word[0]-'a');                 //initial pointer point at 'a'
        ans += move > 26/2 ? 26-move : move;     //count for min moves from 'a' to word[0]

        for(int i=0; i<word.length()-1; i++){
            move = abs(word[i]-word[i+1]);
            ans += move > 26/2 ? 26-move : move; //count for min moves from word[i] to word[i+1]
        }

        return ans + word.length(); //each letter in word cost 1 second to be typed, total add up [word.length] seconds
        
    }
};
