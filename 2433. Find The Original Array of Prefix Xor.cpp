//https://leetcode.com/problems/find-the-original-array-of-prefix-xor/description/

class Solution {
public:
    //ex:  pref = [5,2,0,3,1]
    //                         num ^ curr = ?  ;  curr = 0   ;   ans = []
    //             ^             5 ^  0   = 5  ;  curr = 5   ;   ans = [5]
    //               ^           2 ^  5   = 7  ;  curr = 2   ;   ans = [5,7]
    //                 ^         0 ^  2   = 2  ;  curr = 0   ;   ans = [5,7,2]
    //                   ^       3 ^  0   = 3  ;  curr = 3   ;   ans = [5,7,2,3]
    //                     ^     1 ^  3   = 2  ;  curr = 1   ;   ans = [5,7,2,3,2]
    //
    vector<int> findArray(vector<int>& pref) {
        vector<int> ans;
        int curr = 0;

        for(const auto num: pref){
            ans.push_back(num ^ curr);  //push (num ^ curr) into answer
            curr = num;                 //set curr as current number
        }

        return ans;
    }
};
