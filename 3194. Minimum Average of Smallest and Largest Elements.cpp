//https://leetcode.com/problems/minimum-average-of-smallest-and-largest-elements/description/

class Solution {
public:
    double minimumAverage(vector<int>& nums) {
        double ans = DBL_MAX;

        sort(nums.begin(), nums.end());                                           // Sort the number in ascending order

        for(int i=0; i<nums.size(); i++){
            double avg = ((double)nums[i] + (double)nums[nums.size()-i-1]) / 2;   // Calculate the average

            ans = min(avg, ans);                                                  // Store the min average
        }

        return ans;
    }
};