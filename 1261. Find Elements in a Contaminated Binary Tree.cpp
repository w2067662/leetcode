//https://leetcode.com/problems/find-elements-in-a-contaminated-binary-tree/description/

class FindElements {
private:
    TreeNode* root;
public:
    FindElements(TreeNode* root) {
        this->root = root;
        if(root){
            this->root->val = 0;    //set root value as 0
            recover(this->root);    //recover tree
        }
    }
    
    bool find(int target) {
        return findTarget(this->root, target); //find the target from the tree
    }

    //-----------------------
    //  supporting functions
    //-----------------------
    void recover(TreeNode* root){ //recover the tree
        if(!root) return;

        if(root->left ) root->left->val  = root->val*2 + 1;
        if(root->right) root->right->val = root->val*2 + 2;

        recover(root->left );
        recover(root->right);
        return;
    }

    bool findTarget(TreeNode* root, const int &target){ //find the target from current subtree
        return !root ? false : findTarget(root->left , target) ||
                               findTarget(root->right, target) ||
                               root->val == target;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

/**
 * Your FindElements object will be instantiated and called as such:
 * FindElements* obj = new FindElements(root);
 * bool param_1 = obj->find(target);
 */
