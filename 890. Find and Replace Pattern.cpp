//https://leetcode.com/problems/find-and-replace-pattern/description/

class Solution {
public:
    bool match(const string &word, const string &pattern){
        map<char, char> map;
        set<char> set;

        for(int i=0;i<pattern.length();i++){
            if(map.find(pattern[i]) == map.end()){               //IF   pattern[i] not match to word[i] yet
                if(set.find(word[i]) != set.end()) return false; //     IF   word[i] already match by other letters -> FALSE

                map[pattern[i]] = word[i];                       //     ELSE -> match pattern[i] to word[i]
                set.insert(word[i]);                             //          -> mark word[i] is matched
            } 
            else if(map[pattern[i]] != word[i]) return false;    //IF   pattern[i] do not match word[i] -> FALSE
        }

        return true;
    }

    vector<string> findAndReplacePattern(vector<string>& words, string pattern) {
        vector<string> ans;

        for(const auto word: words){
            if(match(word, pattern)) ans.push_back(word); //IF   current word matches patter -> push into answer
        }

        return ans;
    }
};
