//https://leetcode.com/problems/minimum-number-game/description/

class Solution {
public:
    vector<int> numberGame(vector<int>& nums) {
        vector<int> Alice, Bob, ans;

        sort(nums.begin(), nums.end());       //sort the vector in ascending order

        for(int i=0; i<nums.size(); i+=2){
            Alice.push_back(nums[i]);         //Alice remove the smallest element from vector first
            Bob.push_back(nums[i+1]);         //  Bob remove the smallest element from vector next
        }

        for(int i=0; i<Alice.size(); i++){
            ans.push_back(Bob[i]);            //  Bob append the element first
            ans.push_back(Alice[i]);          //Alice append the element next
        }

        return ans;
    }
};
