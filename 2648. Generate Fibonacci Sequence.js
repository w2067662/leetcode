//https://leetcode.com/problems/generate-fibonacci-sequence/description/

/**
 * @return {Generator<number>}
 */
var fibGenerator = function*() {
    let fib = [0, 1];
    let secondLast = fib[0];
    let Last = fib[1];

    yield fib[0];   //yield fib[0] -> yield.next().value = 0
    yield fib[1];   //yield fib[1] -> yield.next().value = 1

    while(true){
        let next = secondLast + Last;
        yield next; //yield next -> yield.next().value = fib[N-2] + fib[N-1]
        secondLast = Last;
        Last = next;
    }
};

/**
 * const gen = fibGenerator();
 * gen.next().value; // 0
 * gen.next().value; // 1
 */
