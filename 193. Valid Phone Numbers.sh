#https://leetcode.com/problems/valid-phone-numbers/description/

grep -P '^(\d{3}-|\(\d{3}\) )\d{3}-\d{4}$' file.txt

#formats: (xxx) xxx-xxxx or xxx-xxx-xxxx

#grep : word finding command

#\d :digit
#{n}:n chars
#\( : (
#\) : )
#|  : or
#^  : find the front word
#$  : find the end word
#^$ : find the word with same syntax between ^ and $

#Regexp selection and interpretation:
#  -E, --extended-regexp     PATTERN is an extended regular expression (ERE)
#  -F, --fixed-strings       PATTERN is a set of newline-separated fixed strings
#  -G, --basic-regexp        PATTERN is a basic regular expression (BRE)
#  -P, --perl-regexp         PATTERN is a Perl regular expression
#  -e, --regexp=PATTERN      use PATTERN for matching
#  -f, --file=FILE           obtain PATTERN from FILE
#  -i, --ignore-case         ignore case distinctions
#  -w, --word-regexp         force PATTERN to match only whole words
#  -x, --line-regexp         force PATTERN to match only whole lines
#  -z, --null-data           a data line ends in 0 byte, not newline
