//https://leetcode.com/problems/unique-binary-search-trees/description/

class Solution {
public:
    //ex:
    //  n = 1   0  0  ->  1 *  1  ->   1   ->  1
    //  n = 2   0  1  ->  1 *  1  ->   1
    //          1  0  ->  1 *  1  ->   1   ->  1+1 = 2
    //  n = 3   0  2  ->  1 *  2  ->   2
    //          1  1  ->  1 *  1  ->   1
    //          2  0  ->  2 *  1  ->   2   ->  2+1+2 = 5
    //  n = 4   0  3  ->  1 *  5  ->   5
    //          1  2  ->  1 *  2  ->   2
    //          2  1  ->  2 *  1  ->   2
    //          3  0  ->  5 *  1  ->   5   ->  5+2+2+5 = 14
    //  n = 5   0  4  ->  1 * 14  ->  14
    //          1  3  ->  1 *  5  ->   5
    //          2  2  ->  2 *  2  ->   4
    //          3  1  ->  5 *  1  ->   5
    //          4  0  -> 14 *  1  ->  14   ->  14+5+4+5+14 = 42
    //  n = 6   0  5  ->  1 * 42  ->  42
    //          1  4  ->  1 * 14  ->  14
    //          2  3  ->  2 *  5  ->  10
    //          3  2  ->  5 *  2  ->  10
    //          4  1  -> 14 *  1  ->  14
    //          5  0  -> 42 *  1  ->  42   ->  42+14+10+10+14+42 = 132
    int numTrees(int n) {
        vector<int> vec = {1};
        int total;

        for(int i=1; i<=n; i++){
            total = 0;
            for(int j=0; j<i; j++){         //j start from 0 to i-1
                total += vec[j]*vec[i-j-1]; //calculate total
            }
            vec.push_back(total);           //set vec[i] = total
        }

        return vec[n];
    }
};
