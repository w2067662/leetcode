//https://leetcode.com/problems/evaluate-boolean-binary-tree/description/

class Solution {
public:
    bool evaluateTree(TreeNode* root) {
        switch(root->val){
            case 0: return false;   // FALSE
            case 1: return true;    // TRUE
            case 2: return evaluateTree(root->left) || evaluateTree(root->right); // OR
            case 3: return evaluateTree(root->left) && evaluateTree(root->right); // AND
        }

        return false;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
