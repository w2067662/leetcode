//https://leetcode.com/problems/delete-leaves-with-a-given-value/

class Solution {
public:
    bool isLeaf(TreeNode* root){    // Check is current node is leaf or not
        return !root->left && !root->right;
    }

    bool deleteLeafNodesByValue(TreeNode* root, const int target){  // Delete leaf node by value
        if(!root) return false;

        bool deleted = false;

        if(root->left){                                             // IF  left child exist
            if(isLeaf(root->left) && root->left->val == target){    //     IF   left child is leaf node with value = target
                root->left = NULL;                                  //          -> delete it
                deleted = true;
            }
        }

        if(root->right){                                            // IF  right child exist
             if(isLeaf(root->right) && root->right->val == target){ //     IF   right child is leaf node with value = target
                root->right = NULL;                                 //          -> delete it
                deleted = true;
            }
        }

        return deleteLeafNodesByValue(root->left , target) ||       // Delete from left  subtree
               deleteLeafNodesByValue(root->right, target) ||       // Delete from right subtree
               deleted;
    }

    TreeNode* removeLeafNodes(TreeNode* root, int target) {
        while(deleteLeafNodesByValue(root, target)){}             // IF   there are nodes being deleted 
                                                                  //      -> re-check the tree again for deletion

        return isLeaf(root) && root->val == target ? NULL : root; // IF   current root is leaf node AND value = target
    }                                                             //      -> return NULL
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
