//https://leetcode.com/problems/relative-sort-array/description/

class Solution {
public:
    vector<int> relativeSortArray(vector<int>& arr1, vector<int>& arr2) {
        map<int, int> map;
        vector<int> vec;

        for(int i=0; i<arr1.size(); i++){ 
            map[arr1[i]]++;                                // Store the frequency of arr1 elements into map
        }

        for(int i=0; i<arr2.size(); i++){                  // For  i from 0 to arr2.size
            for(int times=0; times<map[arr2[i]]; times++){
                vec.push_back(arr2[i]);                    //      -> store current number for its frequency times
            }

            map.erase(arr2[i]);                            //      -> erase the key of current number from map
        }

        for(auto it:map){                                  // For  all the elements within map
            for(int times=0; times<it.second; times++){    
                vec.push_back(it.first);                   //      -> store the element' value for its frequency times
            }
        }

        return vec;
    }
};
