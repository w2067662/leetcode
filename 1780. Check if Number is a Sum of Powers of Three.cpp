//https://leetcode.com/problems/check-if-number-is-a-sum-of-powers-of-three/description/

class Solution {
public:
    bool checkPowersOfThree(int n) {
        double exp = log(n)/log(3);           // Get the started exponential number of current number with base 3

        for(int i=(int)exp; i>=0; i--){       // For i from started exponential number (integar) to 0 
            int powerNum = pow(3, i);
            if(n >= powerNum) n -= powerNum;  //    IF n >= power(3, i) -> n deduct power(3, i)
        }

        return n == 0;                        // Check n is sum of powers of 3
    }
};
