//https://leetcode.com/problems/find-if-path-exists-in-graph/description/

class DisjointSet { // Class of Disjoint set 
private:
    vector<int> node;
public:
    // Initialize Disjoint Set
    DisjointSet(int n){ 
        for(int i=0; i<n; i++) node.push_back(i);
    }
    // Find the belonged set of the node
    int find(int x){
	    return x == node[x] ? x : (node[x] = find(node[x]));
    }
    // Check 2 nodes are in the same set or not
    bool equivalence(int x, int y){
	    return find(x) == find(y);
    }
    // Merge 2 sets
    void merge(int x, int y){
	    node[find(x)] = find(y);
    }
};

class Solution {
public:
    bool validPath(int n, vector<vector<int>>& edges, int source, int destination) {
        DisjointSet djSet(n);                           // Create new Disjoint Set

        for(const auto edge: edges){
            djSet.merge(edge[0], edge[1]);              // Draw the edge of 2 nodes
        }

        return djSet.equivalence(source, destination);  // Check the path from source to destination exist or not
    }
};
