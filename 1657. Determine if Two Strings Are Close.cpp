//https://leetcode.com/problems/determine-if-two-strings-are-close/description/

class Solution {
public:
    bool closeStrings(string word1, string word2) {
        map<int, int> map1, map2, diff;

        for(const auto ch: word1) map1[ch]++;         // Store chars appear times of word1 to map1
        for(const auto ch: word2) map2[ch]++;         // Store chars appear times of word2 to map2

        for(int ch='a'; ch<='z'; ch++){                                                // For 'a' to 'z'
            if((map1[ch] == 0 && map2[ch] != 0) || (map1[ch] != 0 && map2[ch] == 0)){  // IF exist 1 char only appear in 1 of the words
                return false;                                                          //    -> FALSE
            }
        }

        for(const auto it: map1) diff[it.second]++;   // Add up the appear times frequency from word1
        for(const auto it: map2) diff[it.second]--;   // Deduct the appear times frequency from word2

        for(const auto it: diff) {
            if(it.second != 0) return false;          // IF exist appear times has none-0 frequency
        }                                             //    -> word1 and word2 are not close strings
                                                      //    -> FALSE
        return true;
    }
};
