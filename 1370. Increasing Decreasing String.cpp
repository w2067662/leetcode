//https://leetcode.com/problems/increasing-decreasing-string/description/

class Solution {
public:
    string sortString(string s) {
        map<char, int>map;
        bool inOrder = true; //true: 'a'-'z'; false: 'z'-'a'
        vector<int> vec;     //for storing map['char'] is 0
        string ans="";

        for(int i=0;i<s.length();i++){
            map[s[i]]++;     //store {[char]:[appear times]} into map
        }

        while(map.size()>0){ //end the loop until map is empty
            if(inOrder){                                        
                for(auto it=map.begin();it!=map.end();it++){    //[Ascending traversal]
                    ans+=it->first;                             //append current char to answer
                    it->second--;                               //appear time of current char -1
                    if(it->second==0) vec.push_back(it->first); //if map['char'] is 0, store current char to vector
                }
                inOrder=false;                                  //change to [Descending traversal]
            }else{                                              
                for(auto it=map.rbegin();it!=map.rend();it++){  //[Descending traversal]
                    ans+=it->first;                             //append current char to answer
                    it->second--;                               //appear time of current char -1
                    if(it->second==0) vec.push_back(it->first); //if map['char'] is 0, store current char to vector
                }
                inOrder=true;                                   //change to [Ascending traversal]
            }
            for(auto it:vec) map.erase(it);                     //delete stored chars that map['char'] is 0
            vec.clear();                                        //clear the vector
        }

        return ans;
    }
};
