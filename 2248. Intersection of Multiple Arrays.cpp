//https://leetcode.com/problems/intersection-of-multiple-arrays/description/

class Solution {
public:
    vector<int> intersection(vector<vector<int>>& nums) {
        set<int> set1, set2;
        vector<int> vec;

        for(const auto num: nums[0])set1.insert(num);              //insert all the number from vector nums[0] into set1

        for(const auto numVec: nums){
            set2.clear();                                          //clear set2
            for(const auto num: numVec){
                if(set1.find(num) != set1.end()) set2.insert(num); //IF  current number from vector nums[i] exist in set1 -> insert current number into set2
            }
            swap(set1, set2);                                      //swap set1 and set2
        }

        for(const auto num: set1) vec.push_back(num);              //push the intersection numbers from set1 into answer

        return vec;
    }
};
