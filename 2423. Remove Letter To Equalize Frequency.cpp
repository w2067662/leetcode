//https://leetcode.com/problems/remove-letter-to-equalize-frequency/description/

class Solution {
public:
    void deepCopy(map<char, int>& copied, map<char, int>& origin){ //deep copy the map from origin
        copied.clear();
        for(const auto it: origin){
            copied[it.first] = it.second;
        }
    }

    bool isFrequencyEqual(map<char, int>& map){ //check every letter's appear times are equal
        set<int> set;
        for(const auto it:map) set.insert(it.second);

        return set.size() == 1;
    }

    bool equalFrequency(string word) {
        map<char, int> map, copied;

        for(const auto ch: word)map[ch]++;      //store the appear times of chars into map 

        for(const auto it: map){
            deepCopy(copied, map);                           //deep copy a map from original
            copied[it.first]--;                              //remove 1 letter from string
            if(copied[it.first] == 0) copied.erase(it.first);

            if(isFrequencyEqual(copied)) return true;        //check current string is frequency equal
        }

        return false;
    }
};
