//https://leetcode.com/problems/find-mode-in-binary-search-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, unordered_map<int, int> &umap){ //traverse the tree
        if(!root) return;

        umap[root->val]++;           //store the value's frequency into umap

        traverse(root->left , umap); //traverse left  subtree
        traverse(root->right, umap); //traverse right subtree
        return;
    }

    vector<int> findMode(TreeNode* root) {
        unordered_map<int, int> umap;
        map<int, vector<int>, greater<int>> map; //sort the map in descending order

        traverse(root, umap);                    //traverse the tree

        for(const auto it: umap){
            map[it.second].push_back(it.first);  //insert values and frequencies from umap to map
        }

        return map.begin()->second;              //return the vector with values that have largest frequency
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
