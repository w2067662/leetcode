//https://leetcode.com/problems/spiral-matrix-ii/description/

class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        //right->down->left->up->right...
        //orientation = [right:0];[down:1];[left:2];[up:3];

        if(n==1)return {{1}};   //if n=1 will cause runtime error, so return matrix directly
        
        vector<vector<int>> matrix (n, vector<int>(n));       //initialize matrix
        int directions[4][2] = {{1,0},{0,1},{-1,0},{0,-1}};   //initialize direction
        int number = 0;                                       //initial number as 0
        int x = 0;                                            //initial (x, y) as (0,0)
        int y = 0;
        int orientation = 0;                                  //initial orientation as right

        while(matrix[y][x] == 0){                             //while current position's value in matrix is not 0, then end the loop
            number++;                                         //add the number,
            matrix[y][x] = number;                            //and set current position's value as number

            if(x+directions[orientation][0]>=n || x+directions[orientation][0] <0 ||        //if next step x out of range OR
               y+directions[orientation][1]>=n || y+directions[orientation][1] <0 ||        //   next step y out of range OR
               matrix[y+directions[orientation][1]][x+directions[orientation][0]] !=0){     //   next position's value isn't zero,
                   orientation++;                                                           //then change to next orientation
                   if(orientation>3)orientation=0;                                          //(next orientation of UP will be RIGHT)
            }
            x+=directions[orientation][0];                                                  //goto next position
            y+=directions[orientation][1];
        }

        return matrix;
    }
};
