//https://leetcode.com/problems/find-the-difference/description/

class Solution {
public:
    char findTheDifference(string s, string t) {
        map<char, pair<int, int>>map;
        for(int i=0;i<s.length();i++){  //count for appear times of each char in strings s
            if(map.find(s[i])==map.end()) map.insert({s[i],{1, 0}} );
            else map[s[i]].first++;
        }
        for(int i=0;i<t.length();i++){  //count for appear times of each char in strings t
            if(map.find(t[i])==map.end()) map.insert({t[i],{0, 1}} );
            else map[t[i]].second++;
        }
        for(const auto& element: map){  //if the char in string s and t have different appear times,  return the char
            if(element.second.first != element.second.second) return element.first;
        }
        return ' ';
    }
};
