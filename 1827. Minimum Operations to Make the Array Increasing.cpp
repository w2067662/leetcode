//https://leetcode.com/problems/minimum-operations-to-make-the-array-increasing/description/

class Solution {
public:
    int minOperations(vector<int>& nums) {
        int last = nums[0];
        int ans = 0;
        for(int i=1; i<nums.size(); i++){
            if(last >= nums[i]){           //IF   current number is smaller than last number
                ans += last+1 - nums[i];   //     -> do the operations AND answer add up operation times
                last = last+1;             //     -> set last increasing 1
            } else last = nums[i];         //ELSE -> set last as current number
        }

        return ans;
    }
};
