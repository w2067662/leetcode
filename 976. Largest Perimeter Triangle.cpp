//https://leetcode.com/problems/largest-perimeter-triangle/description/

class Solution {
public:
    //ex: nums = [1,2,1,5,4,3,2,10]
    //  sorted = [1,1,2,2,3,4,5,10]
    //                      ^ ^ ^   (4,5,10) -> can not form triangle
    //                    ^   ^ ^   (3,5,10) -> can not form triangle
    //                    ^ ^   ^   (3,4,10) -> can not form triangle
    //                    ^ ^ ^     (3,4,5)  -> can form triangle       -> answer = 3+4+5 = 12 (largest-perimeter)
    bool canFormTriangle(int a, int b, int c){  //check if triplet (a,b,c) that a<=b<=c can form a triangle
        return a + b > c;
    }

    int largestPerimeter(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int curr = 0, size = nums.size();
        vector<int> vec = {size-3, size-2, size-1};

        while(vec[0]>=0 && vec[1]>=1 && vec[2] >= 2){
            if(canFormTriangle(nums[vec[0]], nums[vec[1]], nums[vec[2]])){ //check current triplet is able to form a triangle or not
                return nums[vec[0]]+nums[vec[1]]+nums[vec[2]];
            }
                
            vec[curr]--;                        //goto next triplet
            curr++;
            if(curr >= vec.size()) curr = 0;
        }

        return 0; //no possible answer
    }
};
