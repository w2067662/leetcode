//https://leetcode.com/problems/distance-between-bus-stops/description/

class Solution {
public:
    int distanceBetweenBusStops(vector<int>& distance, int start, int destination) {
        int goRight = start;
        int goLeft  = start;
        int rightSum = 0;
        int leftSum  = 0;

        while(goRight!=destination){               //[go clockwise]
            rightSum+=distance[goRight];           //add up the distance
            goRight++;                             //go next
            if(goRight>=distance.size())goRight=0; //if out of range, set index to the other side as a cycle
        }

        while(goLeft!=destination){                //[go counter-clockwise]
            goLeft--;                              //go next
            if(goLeft<0)goLeft=distance.size()-1;  //if out of range, set index to the other side as a cycle
            leftSum+=distance[goLeft];             //add up the distance
        }

        return min(rightSum, leftSum);             //return min distance from left sum and right sum
    }
};
