//https://leetcode.com/problems/left-and-right-sum-differences/description/

class Solution {
public:
    vector<int> leftRightDifference(vector<int>& nums) {
        int SumFromLeft = 0;
        int SumFromRight = 0;
        vector<int> leftSum (nums.size(), 0);
        vector<int> rightSum (nums.size(), 0);

        for(int i=0; i<nums.size(); i++){    //calculate sum from left
            leftSum[i] = SumFromLeft;
            SumFromLeft += nums[i];
        }

        for(int i=nums.size()-1; i>=0; i--){ //calculate sum from right
            rightSum[i] = SumFromRight;
            SumFromRight += nums[i];
        }

        for(int i=0; i<nums.size(); i++){
            nums[i] = abs(leftSum[i]-rightSum[i]); //calculate the left and right difference
        }

        return nums;
    }
};
