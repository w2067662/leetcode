//https://leetcode.com/problems/the-number-of-full-rounds-you-have-played/description/

class Solution {
public:
    int toIntTime(const pair<int, int>& time){ // Convert {[Hour],[Minute]} to integar time value
        return time.first*60 + time.second;
    }

    pair<int, int> toRoundStartTime(const pair<int, int>& time){ // Convert [Minute] to ⌈ Minute ⌉ = min { n ∈ Z ∣ x = 0, 15, 30, 45, 60 }
        for(int i=0; i<4; i++){
            if(i*15+1 <= time.second && time.second <= i*15+15){
                return {time.first, i*15+15};
            }
        }
        return time;
    }

    pair<int, int> toRoundEndTime(const pair<int, int>& time){   // Convert [Minute] to ⌊ Minute ⌋ = max { n ∈ Z ∣ x = 0, 15, 30, 45, 60 }
        for(int i=0; i<4; i++){
            if(i*15 <= time.second && time.second <= i*15+14){
                return {time.first, i*15};
            }
        }
        return time;
    }

    pair<int, int> toPairIntTime(const string& time){ // Convert "XX[Hour]:OO[Minute]" string to {[Hour], [Minute]}
        return {(time[0]-'0')*10 + (time[1]-'0'),
                (time[3]-'0')*10 + (time[4]-'0')};
    }

    int numberOfRounds(string loginTime, string logoutTime) {
        int roundStart = toIntTime(toRoundStartTime(toPairIntTime(loginTime)));
        int roundEnd   = toIntTime(toRoundEndTime  (toPairIntTime(logoutTime)));

        return toIntTime(toPairIntTime(logoutTime)) >= toIntTime(toPairIntTime(loginTime)) ? // IF   logout time >= login time
                    ( (roundEnd-roundStart) >= 0 ? (roundEnd-roundStart)/15 : 0 ) :          //      -> rounds = (round end time - round start time)/15 (can't be negative)
                    ( (24*60 - (roundStart-roundEnd))/15 );                                  // ELSE -> rounds = (24 (hour) x 60 (minutes) - (round start time - round end time))/15
    }
};
