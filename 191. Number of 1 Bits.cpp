//https://leetcode.com/problems/number-of-1-bits/description/

class Solution {
public:
    //ex:  n = 00000000000000000000000000001011
    //         1011
    //       & 1010
    //       = 1010     count = 0+1 = 1
    //
    //         1010
    //       & 1001
    //       = 1000     count = 1+1 = 2
    //
    //         1000
    //       & 0111
    //       = 0000     count = 2+1 = 3
    //
    int hammingWeight(uint32_t n) {
        int count = 0;

        for(; n>0; n&=(n-1)) count++; //count '1' bits

        return count;
    }
};
