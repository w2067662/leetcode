//https://leetcode.com/problems/missing-number/description/

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        sort(nums.begin(), nums.end());                     // Sort the numbers in ascending order

        for(int i=0, prev=-1; i<nums.size(); i++, prev++){  // For i from 0 to nums.size
                                                            // Set the previous as -1
            if(nums[i]-prev != 1){                          //      IF current number is not previous+1
                return prev+1;                              //         -> return previous+1 as missing number
            }
        }

        return nums[nums.size()-1]+1;                       // IF missing number not found return N+1 (N = last number of vector)
    }
};
