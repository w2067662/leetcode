//https://leetcode.com/problems/harshad-number/description/

class Solution {
public:
    int sumOfTheDigitsOfHarshadNumber(int x) {
        int digitSum = 0, n = x;

        while(n > 0){                                // Calculate digit sum
            digitSum += n%10;
            n /= 10;
        }

        return (x % digitSum == 0) ? digitSum : -1;  // IF    X can be divided by X's digit sum -> Return X's digit sum as answer
                                                     // ELSE                                    -> Return -1
    }
};