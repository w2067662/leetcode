//https://leetcode.com/problems/decompress-run-length-encoded-list/description/

//ex:nums = [1,2,3,4]
//[1,2] -> freq = 1 ; val = 2 -> ans = [2]
//[3,4] -> freq = 3 ; val = 4 -> ans = [2,4,4,4]
class Solution {
public:
    vector<int> decompressRLElist(vector<int>& nums) {
        int freq, val;
        vector<int> ans;
        for(int i=0;i<nums.size();i+=2){
            freq = nums[i];
            val = nums[i+1];
            for(int j=0;j<freq;j++) ans.push_back(val);
        }
        return ans;
    }
};
