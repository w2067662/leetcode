//https://leetcode.com/problems/distribute-candies-among-children-i/description/

class Solution {
public:
    int distributeCandies(int n, int limit) {
        int count = 0;

        for (int i = 0; i <= min(n, limit); i++) {               //first  child get i candies (i = 0~n    , i <= limit)
            for (int j = 0; j <= min(n-i, limit); j++) {         //second child get j candies (j = 0~n-i  , j <= limit)
                                                                 //third  child get k candies (k = 0~n-i-j, k <= limit)
                count += (n-i-j <= limit) ? 1 : 0;               //IF K is valid -> count+1     
            }
        }

        return count;
    }
};
