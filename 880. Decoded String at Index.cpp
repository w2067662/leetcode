//https://leetcode.com/problems/decoded-string-at-index/description/

class Solution {
public:
    //ex: s = "leet2code3", k = 80
    //    s = "l  e  e  t  2  c  o  d  e  3"
    // size = [1, 2, 3, 4, 8, 9,10,11,12,36]
    //                                    ^     k = 26 mod 36 = 26
    //                                 ^        k = 26 mod 12 =  2
    //                              ^           k =  2 mod 11 =  2
    //                           ^              k =  2 mod 10 =  2
    //                        ^                 k =  2 mod  9 =  2
    //                     ^                    k =  2 mod  8 =  2
    //                  ^                       k =  2 mod  4 =  2
    //               ^                          k =  2 mod  3 =  2
    //            ^                             k =  2 mod  2 =  0   -> is lower case letter -> answer = "e"  
    string decodeAtIndex(string s, int k) {
        long long size = 0;

        for(const auto ch: s){                               //calculate the size of current char
            if(isdigit(ch)) size *= ch-'0';
            else if(islower(ch)) size++;
        }

        for(int i=s.length()-1; i>=0; i--){
            k = k % size;
            if(k == 0 && islower(s[i])) return s.substr(i,1);//check if current char is the answer or not

            if(isdigit(s[i]))size /= s[i] -'0';              //calculate the size of next char
            else size--;
        }

        return "";
    }
};
