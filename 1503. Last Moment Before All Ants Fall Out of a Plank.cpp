//https://leetcode.com/problems/last-moment-before-all-ants-fall-out-of-a-plank/description/

class Solution {
public:
    int getLastMoment(int n, vector<int>& left, vector<int>& right) {
        sort( left.begin(),  left.end());   // Sort the vector in ascending order
        sort(right.begin(), right.end());   // Sort the vector in ascending order

        if(left.size() > 0 && right.size() > 0) return max(n-right[0], left[left.size()-1]-0); // IF  left and right are not empty -> get the max time of LastMoment
        else if (left.size() > 0 && right.size() == 0) return left[left.size()-1]-0;           // IF  only right is empty          -> get the max time from left
        else return n-right[0];                                                                // IF  only left  is empty          -> get the max time from right
    }
};
