//https://leetcode.com/problems/time-needed-to-buy-tickets/description/

class Solution {
public:
    //ex: tickets = [84,49,5,24,70,77,87,8] ; k = 3  -> tickets[3] = 24
    //      count = [24,24,5,24,23,23,23,8]          -> answer = 154 (sum of count)
    int timeRequiredToBuy(vector<int>& tickets, int k) {
        int ans = 0;

        for(int i=0; i<tickets.size(); i++){
            if(tickets[i] < tickets[k]) ans += tickets[i];  // IF   line i has tickets smaller than line k  -> add up tickets   in line i
            else ans += i > k ? tickets[k]-1 : tickets[k];  // IF   line i has tickets larger than or equal to line k
        }                                                   //      IF   i >  k (index of i behind k)       -> add up tickets-1 in line k
                                                            //      IF   i <= k (index of i before or is k) -> add up tickets   in line k
        return ans;
    }
};
