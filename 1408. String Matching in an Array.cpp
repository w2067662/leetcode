//https://leetcode.com/problems/string-matching-in-an-array/description/

//ex: words = ["leetcode","et","code"]
//    map = { 2: {"et"}, 4: {"code"}, 8: {"leetcode"}} ; ans = []
//
//[check Match]
//stringA = "et" ; stringB = "code"         -> not matched
//stringA = "et" ; stringB = "leetcode"     -> matched       -> ans = ["et"]
//stringA = "code" ; stringB = "leetcode"   -> matched       -> ans = ["et", "code"]

class Solution {
public:
    bool isMatched(string& a, string& b){   //check string a is string b's substring
        for(int i=0; i<b.length()-a.length()+1; i++){
            if(a == b.substr(i, a.length())) return true;
        }
        return false;
    }

    vector<string> stringMatching(vector<string>& words) {
        map<int, vector<string>> map;   //map = { [length of the words] : vector{[word1], [word2], ...} }
        vector<string> ans;
        bool wordChecked = false;

        for(int i=0;i<words.size();i++){
            map[words[i].length()].push_back(words[i]); //store the words into map 
        }

        for(auto it=map.begin(); it!=map.end(); it++){  //traverse the map by key = [length of the words] to map's end
            for(int i=0;i<it->second.size();i++){       //traverse the vector that contain words that are [length of the words]          
                wordChecked = false;    //reset wordChecked as FALSE of current word

                auto another=it;
                another++;
                for(; another!=map.end(); another++){           //traverse the map by key = [length of the words]+1 to map's end
                    for(int j=0;j<another->second.size();j++){  //traverse the vector that contain words that are [length of the words]+1
                        if(isMatched(it->second[i], another->second[j])) {
                            ans.push_back(it->second[i]);
                            wordChecked = true; //mark current word as checked 
                                                //(!!!If not checked, same word might appear more than one time in the answer)
                            break;
                        } //check string a is string b's substring or not
                    }
                    if(wordChecked) break;
                }
                
            }
        }

        return ans;
    }
};
