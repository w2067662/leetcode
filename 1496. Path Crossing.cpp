//https://leetcode.com/problems/path-crossing/description/

class Solution {
public:
    bool isPathCrossing(string path) {
        set<string> visited;
        int x = 0, y = 0;
        string temp = "";

        visited.insert(to_string(x) + "," + to_string(y));      //insert the origin point into set

        for(int i=0; i<path.length(); i++){
            switch(path[i]){                                    //goto next position according to letters
                case 'N':
                    y += 1;
                    break;                
                case 'E':
                    x += 1;
                    break;
                case 'S':
                    y -= 1;
                    break;
                case 'W':
                    x -= 1;
                    break;
            }
            temp = to_string(x) + "," + to_string(y);           //convert current position to string ("x,y")

            if(visited.find(temp) != visited.end())return true; //if current position is visited -> return TRUE
            visited.insert(temp);                               //insert the visited 
        }

        return false;
    }
};
