//https://leetcode.com/problems/largest-divisible-subset/description/

class Solution {
public:
    //ex:  nums = [4,8,10,240]
    //    nums' = [4,8,10,240] (sorted)
    //                                                       -> DP = [{4},{8},{10},{240}]
    //             ^
    //             * ^                    8 % 4  = 0
    //                                                       -> DP = [{4},{4,8},{10},{240}]
    //               *  ^                10 % 8 != 0
    //                                                       -> DP = [{4},{4,8},{10},{240}] (unchanged)
    //             *    ^                10 % 4 != 0
    //                                                       -> DP = [{4},{4,8},{10},{240}] (unchanged)
    //                  *   ^           240 % 10 = 0
    //                                                       -> DP = [{4},{4,8},{10},{10,240}]
    //               *      ^           240 %  8 = 0
    //                                                       -> DP = [{4},{4,8},{10},{4,8,240}]
    //                                                                                ^------ answer
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        vector<vector<int>> DP;
        vector<int> ans;

        sort(nums.begin(), nums.end());            // Sort the numbers in ascending order

        for(const auto num: nums){
            DP.push_back({num});                   // Push the vector with current number into DP
        }

        for(int i=0; i<nums.size(); i++){          // For i from 0 to nums.size
            for(int j=i-1; j>=0; j--){             //     For j from i-1 to 0
                if(nums[i] % nums[j] == 0 &&       //         IF nums[i] is divisible by nums[j] AND
                   DP[j].size()+1 > DP[i].size()){ //            size of DP[j] is larger than size of DP[i]
                    DP[i] = DP[j];                 //            -> set subset of DP[i] as DP[j]
                    DP[i].push_back(nums[i]);      //            -> append nums[i] into DP[i]'s subset
                }
            }

            ans = (DP[i].size() >= ans.size()) ? DP[i] : ans; // Store the largest subset from DP
        }

        return ans;
    }
};
