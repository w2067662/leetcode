//https://leetcode.com/problems/minimum-number-of-operations-to-make-array-empty/description/

class Solution {
public:
    //    appear times :  1 2 3 4 5 6 7 8 9 10 11 12
    //  min operations :  x 1 1 2 2 2 3 3 3  4  4  4
    int minOperations(vector<int>& nums) {
        map<int, int> map;
        int minOper = 0;

        for(const auto num: nums) map[num]++;                                 //store the number and appear times into map

        for(const auto it: map){
            if(it.second == 1) return -1;                                     //IF   current number's appear time = 1 -> -1 (not possible to empty the array)
            else minOper += it.second%3 == 0 ? it.second/3 : it.second/3 + 1; //else calculate the min operation to empty current number
        }

        return minOper;
    }
};
