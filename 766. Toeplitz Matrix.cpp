//https://leetcode.com/problems/toeplitz-matrix/description/

class Solution {
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {     //tips: matrix[i][j] will be equal to matrix[i-deduct][j-deduct]
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){                            //traverse matrix
                int deduct = i < j ? i : j;                                 //get deduct = min(i, j)
                if(matrix[i][j]!= matrix[i-deduct][j-deduct]) return false; //matrix[i][j] != matrix[i-deduct][j-deduct] -> False
            }
        }
        return true;
    }
};
