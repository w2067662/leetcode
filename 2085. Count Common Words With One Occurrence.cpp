//https://leetcode.com/problems/count-common-words-with-one-occurrence/description/

class Solution {
public:
    typedef map<string, int> WordsCounter;  // define map<string, int> as  WordsCounter
    int countWords(vector<string>& words1, vector<string>& words2) {
        WordsCounter wc1;
        WordsCounter wc2;
        int ans=0;

        for(int i=0;i<words1.size();i++) wc1[words1[i]]++; //store from words1 to WordsCounter 1
        for(int i=0;i<words2.size();i++) wc2[words2[i]]++; //store from words2 to WordsCounter 2

        for(const auto it: wc1){
            //count for common words with exactly one occurence
            if(wc2.find(it.first)!=wc2.end() && it.second==1 && wc2[it.first]==1 )ans++; 
        }

        return ans;
    }
};
