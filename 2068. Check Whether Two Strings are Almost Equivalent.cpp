//https://leetcode.com/problems/check-whether-two-strings-are-almost-equivalent/description/

class Solution {
public:
    bool checkAlmostEquivalent(string word1, string word2) {
        map<char, int> map;

        for(const auto ch: word1)map[ch]++;       //calculate the difference of each letter from word1 and word2
        for(const auto ch: word2)map[ch]--;

        for(const auto it: map){
            if(abs(it.second-0) > 3)return false; //if difference of current letter is bigger than 3, not AlmostEquivalent
        }

        return true;
    }
};
