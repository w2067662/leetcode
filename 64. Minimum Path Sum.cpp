//https://leetcode.com/problems/minimum-path-sum/description/

class Solution {
public:
    //ex:  grid = [[1,3,1],
    //             [1,5,1],
    //             [4,2,1]]
    //
    //     grid = [[1,4,5],
    //             [2,7,6],
    //             [6,8,7]]
    //
    //   answer = 7
    //
    bool isValid(const vector<vector<int>>& grid, int i, int j){    //check position is valid or not
        return 0 <= i && 0 <= j && i < grid.size() && j < grid[0].size();
    }

    int minPathSum(vector<vector<int>>& grid) {
        for(int i=0; i<grid.size(); i++){                           //traverse the grid
            for(int j=0; j<grid[i].size(); j++){
                if(isValid(grid, i-1, j) && isValid(grid, i, j-1)){ //IF   up and left are valid
                    grid[i][j] += min(grid[i-1][j], grid[i][j-1]);  //     -> add up the min value from up and left
                } else if (isValid(grid, i-1, j)){                  //IF   only up is valid
                    grid[i][j] += grid[i-1][j];                     //     -> add up the value from up
                } else if (isValid(grid, i, j-1)){                  //IF   only left is valid
                    grid[i][j] += grid[i][j-1];                     //     -> add up the value from left
                }
            }
        }

        return grid[grid.size()-1][grid[grid.size()-1].size()-1];   //return the value at right buttom of the grid (min path sum)
    }
};
