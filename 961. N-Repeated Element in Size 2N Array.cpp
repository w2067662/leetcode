//https://leetcode.com/problems/n-repeated-element-in-size-2n-array/description/

class Solution {
public:
    int repeatedNTimes(vector<int>& nums) {
        map<int, int>map;
        unordered_map<int, int> umap;

        for(int i=0;i<nums.size();i++){ 
            map[nums[i]]++;             //store {[number]:[appear times]} into map
        }

        for(const auto it:map){
            umap[it.second] = it.first; //store {[appear times]:[number]} from map into umap
        }

        map.clear();                    //clear map

        return umap[nums.size()/2];     //return the number with appear times = vector_length/2
    }
};
