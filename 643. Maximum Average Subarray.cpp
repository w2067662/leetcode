//https://leetcode.com/problems/maximum-average-subarray-i/description/

class Solution {
public:
    double findMaxAverage(vector<int>& nums, int k) {
        double max=INT_MIN;
        double sum=0;

        for(int i=0;i<k;i++){   //get first sum,  window = range(0, k), sum = (num[0]+...+num[k])
            sum+=nums[i];
        }

        for(int i=0;i<nums.size()-k+1;i++){               //window size is k, and from 0 to len-k
            if(i!=0) sum = sum - nums[i-1] + nums[i+k-1]; //if not first sum, add up numbers in the window as sum
            if(sum/k > max) max = sum/k;                  //if current average(=sum/k) is bigger, set as max average
        }
        
        return max;
    }
};
