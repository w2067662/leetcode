//https://leetcode.com/problems/largest-submatrix-with-rearrangements/description/

class Solution {
public:
    //ex:  matrix = [[0, 0, 1],
    //               [1, 1, 1],
    //               [1, 0, 1]]
    //
    //    matrix' = [[0, 0, 1],
    //               [1, 1, 2],    1 |  add up height           (Step1: Add up previous row)
    //               [2, 0, 3]]    2 v
    //
    //   matrix'' = [[0, 0, 1],
    //               [1, 1, 2],    1 1 2                        (Step2: Sort the Row)
    //               [0, 2, 3]]    0 2 3
    //                             ---->  push to right to construct submatrix
    //
    //   matrix'' = [[0, 0, 1],    1 |          0 1 
    //               [1, 1, 2],    2 |          1 2 |           (Step3: Calculate area)
    //               [0, 2, 3]]    3 v          2 3 v
    //                             <-  area=3   <-- area = 4
    //
    // answer = max(
    //            max(0 x 3, 0 x 2, 1 x 1),
    //            max(1 x 3, 1 x 2, 2 x 1), 
    //            max(0 x 3, 2 x 2, 3 x 1)) = 4
    //
    int largestSubmatrix(vector<vector<int>>& matrix) {
        int ans = 0;

        // Step1
        for(int j=0; j<matrix[0].size(); j++){
            for(int i=1; i<matrix.size(); i++){
                matrix[i][j] += matrix[i][j] == 1 ? matrix[i-1][j] : 0;
            }
        }

        // Step2
        for(int i=0; i<matrix.size(); i++){
            sort(matrix[i].begin(), matrix[i].end());
        }

        // Step3
        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                ans = max(ans, matrix[i][j]*((int)matrix[i].size()-j));
            }
        }      

        return ans;
    }
};
