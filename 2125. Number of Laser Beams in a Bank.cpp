//https://leetcode.com/problems/number-of-laser-beams-in-a-bank/description/

class Solution {
public:
    bool isEmpty(const string &s){ //check current row is empty or not (no laser)
        for(const auto ch: s){
            if(ch == '1') return false;
        }
        return true;
    }

    int rowLasers(const string &s){ //count numbers of lasers in current row
        int count = 0;
        for(const auto ch: s){
            if(ch == '1') count++;
        }
        return count;
    }

    int numberOfBeams(vector<string>& bank) {
        int count = 0;

        for(int i=bank.size()-1; i>=0; i--){
            if(isEmpty(bank[i])){                               //IF  current row is empty
                bank.erase(bank.begin()+i);                     //    -> erase the row from bank
            }
        } 

        for(int i=1; i<bank.size(); i++){
            count += rowLasers(bank[i-1]) * rowLasers(bank[i]); //add up the laser beams from 2 consecutive rows
        }

        return count;
    }
};
