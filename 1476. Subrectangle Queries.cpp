//https://leetcode.com/problems/subrectangle-queries/description/

class SubrectangleQueries {
private:
    vector<vector<int>> Rectangle;
    
public:
    SubrectangleQueries(vector<vector<int>>& rectangle) {
        this->Rectangle = rectangle;                        // Initialize rectangle
    }
    
    void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        for(int i=row1; i<=row2; i++){
            for(int j=col1; j<=col2; j++){
                this->Rectangle[i][j] = newValue;           // Update rectangle value
            }
        }
    }
    
    int getValue(int row, int col) {
        if(0 <= row && row < this->Rectangle.size() &&      // IF   current position is in the range
           0 <= col && col < this->Rectangle[row].size())
            return this->Rectangle[row][col];               //      -> return value
        else return -1;                                     // ELSE -> return -1
    }
};

/**
 * Your SubrectangleQueries object will be instantiated and called as such:
 * SubrectangleQueries* obj = new SubrectangleQueries(rectangle);
 * obj->updateSubrectangle(row1,col1,row2,col2,newValue);
 * int param_2 = obj->getValue(row,col);
 */
