#https://leetcode.com/problems/drop-duplicate-rows/description/

import pandas as pd

def dropDuplicateEmails(customers: pd.DataFrame) -> pd.DataFrame:
    return customers.drop_duplicates("email")

#drop_duplicates() : drop the row if there are duplicates value certain column
