//https://leetcode.com/problems/path-in-zigzag-labelled-binary-tree/description/

class Solution {
public:
    //ex:  label = 14
    //tree:                                height = log2(14) = 3
    //                (1)               
    //          (3)          2
    //       (4)    5     6     7
    //     15 (14)13 12 11 10  9 8
    //
    //height = 3  ;  left = 2^(3-1) = 4  ; right = 2^3-1 = 7  -> label = 4 + (7 - 14/2) = 4
    //height = 2  ;  left = 2^(2-1) = 2  ; right = 2^2-1 = 3  -> label = 2 + (3 - 4/2)  = 3
    //height = 1  ;  left = 2^(1-1) = 1  ; right = 2^1-1 = 1  -> label = 1 + (1 - 3/2)  = 1
    //
    vector<int> pathInZigZagTree(int label) {
        vector<int> path;
        int height = log2(label), left, right;

        path.push_back(label);

        while(height > 0){
            left  = pow(2,height-1);          //calculate the  left range of current level
            right = pow(2,height)-1;          //calculate the right range of current level
            label = left + (right - label/2); //calculate the parent node

            path.push_back(label);
            height--;
        }
        
        reverse(path.begin(), path.end());    //reverse the vector
        return path;
    }
};
