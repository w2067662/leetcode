//https://leetcode.com/problems/construct-the-rectangle/description/

class Solution {
public:
    vector<int> constructRectangle(int area) {
        int length = area;
        int width = 1;
        for(int i=1;i<=sqrt(area);i++){ //length always >= width, width won't bigger than square root of area
                                //***{length, width} should be closest pair***
            if(area%i ==0){     //if {length, width} pair is valid, then store new answer
                width = i;
                length = area/width;
            }
        } 
        return {length, width};
    }
};
