//https://leetcode.com/problems/a-number-after-a-double-reversal/description/

class Solution {
public:
    bool isSameAfterReversals(int num) {
        string s = to_string(num);

        return s[0] != '0' && s[s.length()-1] != '0' || (num>=0 && num<=9); //IF   number contain only 1 digit               -> TRUE
    }                                                                       //IF   reversed number string has no leading '0' -> TRUE
};
