//https://leetcode.com/problems/intersection-of-two-arrays-ii/description/

class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        map<int, int> map;
        vector<int> ans;

        for(const auto& num: nums1){  // FOR  numbers within nums1
            map[num]++;               //      -> insert number into map
        }

        for(const auto& num: nums2){  // FOR  numbers within nums2
            if(map[num] > 0){         //      IF  frequency of number is > 0
                ans.push_back(num);   //          -> push number into answer
                map[num]--;           //          -> frequency - 1
            }
        }

        return ans;
    }
};