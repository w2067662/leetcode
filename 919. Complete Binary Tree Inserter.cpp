//https://leetcode.com/problems/complete-binary-tree-inserter/description/

class CBTInserter {
private:
    TreeNode* root;
    int Size;
public:
    //ex:  CBTInserter = [[1,2,3,4,5,6,7,8,9]] ; Size = 9
    //
    //     insert(10) 
    //                -> Size = 10 ; path = [10,5,2] ; curr = 1 (start at root)
    //
    //     path = [10,5,2]
    //                  ^   curr = 1 ; next = 2  (even) -> path.size != 1 
    //                                                  -> go root left
    //     path = [10,5]
    //                ^     curr = 2 ; next = 5  (odd)  -> path.size != 1
    //                                                  -> go root right
    //     path = [10]
    //              ^       curr = 5 ; next = 10 (even) -> path.size = 1 
    //                                                  -> insert at root left 
    //                                                  -> return current root value (5)
    CBTInserter(TreeNode* root) {
        this->root = root;              //set current tree's root as root
        this->Size = size(this->root);  //set Size as current tree's size
    }

    int insert(int val) {
        this->Size++;                            //insert 1 node -> Size+1

        vector<int> path;                        //create a vector for path
        int temp = this->Size;                   //set temp as Size (the index that node should insert at)

        while(temp > 1){
            path.push_back(temp);                //insert all the passed nodes from root to insert point into path
            temp /= 2;
        }

        return insertNum(this->root, path, val); //insert node with value into tree
    }
    
    TreeNode* get_root() {
        return this->root;
    }

    //----------------------
    // supporting functions
    //----------------------

    int size(TreeNode* root){   //get the size of current tree
        return !root ? 0 : size(root->left) + size(root->right) + 1;
    }

    int insertNum(TreeNode* root, vector<int> &path, const int num){
        if(path.size() == 1){                                    //IF  size of path = 1 (current node is parent node of insert point)
            if(path[0]%2 == 0) root->left = new TreeNode(num);   //    IF   next is even -> insert at left
            else root->right = new TreeNode(num);                //    IF   next is odd  -> insert at right
            return root->val;                                    //    -> return current node's value
        }

        int next = path[path.size()-1];                          //get next step's value
        path.pop_back();                                         //pop out next step from path

        if(next%2 == 0) return insertNum(root->left, path, num); //    IF   next is even -> go root left
        else return insertNum(root->right, path, num);           //    IF   next is odd  -> go root right
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter* obj = new CBTInserter(root);
 * int param_1 = obj->insert(val);
 * TreeNode* param_2 = obj->get_root();
 */
