//https://leetcode.com/problems/height-checker/description/

class Solution {
public:
    int heightChecker(vector<int>& heights) {
        vector<int> copy(heights);            // Create a copy of heights
        int count = 0;

        sort(copy.begin(), copy.end());       // Sort the copy in ascending order

        for(int i=0; i<heights.size(); i++){  // Compare heights with copy
            if(copy[i] != heights[i]){        //    IF not match
                count++;                      //       -> count + 1
            }
        }

        return count;
    }
};