//https://leetcode.com/problems/longest-palindrome/description/

class Solution {
public:
	int longestPalindrome(string s) {
        map<char, int> map;
        int count = 0;

        for(int i=0; i<s.length(); i++){
            map[s[i]]++;                  // Store the frequency of each char
        }
        
        for(const auto& i: map){          // FOR element within map
            if(i.second%2 != 0) count++;  //        IF  current char's frequency is odd -> count + 1
        }

        return (count > 1) ? s.length()-count+1 : s.length(); // Calculate the length of the longest palindrome
    }
};