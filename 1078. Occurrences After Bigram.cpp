//https://leetcode.com/problems/occurrences-after-bigram/description/

class Solution {
public:
    vector<string> findOcurrences(string text, string first, string second) {
        vector<string> words;
        vector<string> ans;
        string temp ="";
        int count =0;

        //store words form text into vector
        for(int i=0;i<text.length();i++){
            if(text[i] == ' '){
                words.push_back(temp);
                temp="";
            }else{
                temp += text[i];
            }
        }
        if(temp != "")words.push_back(temp);

        //find third words
        for(int i=0;i<words.size()-2;i++){
            if(words[i]==first && words[i+1]==second){  //if current word match first word and current+1 word match second word,
                ans.push_back(words[i+2]);              //then current+2 word is 1 possible answer
            }
        }

        return ans;
    }
};
