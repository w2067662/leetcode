//https://leetcode.com/problems/count-the-digits-that-divide-a-number/description/

class Solution {
public:
    int countDigits(int num) {
        int ans=0;
        string digits = to_string(num);

        for(int i=0;i<digits.length();i++){
            if(num%(digits[i]-'0')==0) ans++; //if number is divisable by its current digit, answer+1
        }
        return ans;
    }
};
