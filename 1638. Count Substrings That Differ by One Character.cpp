//https://leetcode.com/problems/count-substrings-that-differ-by-one-character/description/

class Solution {
public:
    bool deferExactlyOne(string a, string b){ //check 2 strings defer exactly 1 char or not
        int defer = 0;
        for(int i=0;i<a.length();i++){
            if(a[i] != b[i]) defer++;
            if(defer > 1) return false;
        }
        return defer == 1;
    }

    int countSubstrings(string s, string t) {
        int count = 0;

        for(int len=1; len <= min(s.length(), t.length()); len++){                  //length of substring start from 1 to shorter string from s & t
            for(int i=0; i+len-1 < s.length(); i++){
                for(int j=0; j+len-1 < t.length(); j++){
                    if(deferExactlyOne(s.substr(i, len), t.substr(j, len))) count++;//check 2 substrings defer exactly 1 char or not
                }
            }
        }

        return count;
    }
};
