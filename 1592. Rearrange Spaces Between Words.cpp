//https://leetcode.com/problems/rearrange-spaces-between-words/description/

class Solution {
public:
    string reorderSpaces(string text) {
        int spaceCount = 0;
        int avgSpace = 0;
        int lastSpace = 0;
        vector<string> words;   //for storing the words from sentence
        string temp = "";
        string ans  = "";

        //[step 1]: Store the words and count the spaces
        for(int i=0;i<text.length();i++){
            if(text[i]==' '){                //when encounter space
                spaceCount++;                //spaceCount +1
                if(temp != ""){              //if temp is not empty,
                    words.push_back(temp);   //push the word into vector
                    temp = "";               //clear temp
                }
            } else {
                temp += text[i];             //append current letter into temp
            }
        }
        if(temp != "") words.push_back(temp);//if temp is not empty, push last word into vector

        //[step 2]: Calculate avgSpace and lastSpace for rearrange spaces between words
        avgSpace = (words.size()-1) != 0 ? spaceCount / (words.size()-1) : 0; //calculate the avgSpace between words (!!!check words.size()-1 is not 0 -> might divide 0 and cause runtime error)
        lastSpace = spaceCount - avgSpace * (words.size()-1); //calculate the lefted spaces as lastSpace

        //[step 3]: Rearrange spaces between words
        for(int i=0, times;i<words.size();i++){              //start from first word to last word
            ans += words[i];                                 //append current word to answer

            if(i!=words.size()-1) times = avgSpace;          //if current word is not the last one, print [avgSpace] times ' '
            else times = lastSpace;                          //if current word is the last one, print [lastSpace] times ' '

            for(int time=0; time < times; time++) ans += ' ';//print ' ' for N times
        }

        return ans;
    }
};
