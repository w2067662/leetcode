//https://leetcode.com/problems/product-of-array-except-self/description/

class Solution {
public:
    //ex:      nums = [ 1, 2, 3, 4]
    // ProductLeft  = [ 1, 1, 2, 6]
    // ProductRight = [24,12, 4, 1]
    //          ans = [24,12, 8, 6] (ProductLeft x ProductRight)    
    vector<int> productExceptSelf(vector<int>& nums) {
        vector<int> ProductLeft(nums.size(), 1), ProductRight(nums.size(), 1);
        vector<int> ans;

        for(int i=0; i<nums.size(); i++){                               // Calculate left products
            switch(i){
                case 0: break;
                case 1: ProductLeft[i] = nums[i-1]; break;
                default: ProductLeft[i] = ProductLeft[i-1]*nums[i-1];
            }
        }
        
        for(int i=nums.size()-1; i>=0; i--){                            // Calculate right products
            switch(nums.size()-1-i){
                case 0: break;
                case 1: ProductRight[i] = nums[i+1]; break;
                default: ProductRight[i] = ProductRight[i+1]*nums[i+1];
            }
        }

        for(int i=0; i<nums.size(); i++){
            ans.push_back(ProductLeft[i] * ProductRight[i]);            // Calculate answers
        }

        return ans;
    }
};