#https://leetcode.com/problems/group-sold-products-by-the-date/description/

# Write your MySQL query statement below
SELECT 
    sell_date,
    COUNT(distinct product) AS num_sold,
    GROUP_CONCAT(distinct product) AS products
FROM Activities
GROUP BY sell_date

# GROUP_CONCAT(): multiple datas will be merged into one
