//https://leetcode.com/problems/two-furthest-houses-with-different-colors/description/

class Solution {
public:
    int maxDistance(vector<int>& colors) {
        int ans = -1;

        for(int i=0;i<colors.size()-1;i++){
            for(int j=colors.size()-1;j>i;j--){ 
                if(colors[j] != colors[i]){     //IF   2 houses (i, j) are different color
                    ans = max(ans, j-i);        //     -> store the max distance of (i, j)
                    break;
                }
            }
        }

        return ans;
    }
};
