//https://leetcode.com/problems/sum-of-root-to-leaf-binary-numbers/description/

class Solution {
public:
    int binaryToIntegar(string s){  // Convert binary string to integar
        int res=0;
        for(int i=0;i<s.length();i++){
            res = res*2 + (s[i]-'0');
        }
        return res;
    }

    bool isLeaf(TreeNode* root){    // Check is leaf node or not
        return !root->left && !root->right;
    }

    void traverse(TreeNode* root, int& sum, string binary){  // Traverse the root
        if(!root) return;

        binary += root->val + '0';                           // Append value of current node to binary string
        if(isLeaf(root)) sum += binaryToIntegar(binary);     // IF  found leaf node -> convert binary string to integar

        traverse(root->left , sum, binary);
        traverse(root->right, sum, binary);
        return;
    }

    int sumRootToLeaf(TreeNode* root) {
        int sum=0;
        traverse(root, sum, "");    // Traverse the tree
        return sum;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
