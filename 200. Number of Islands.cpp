//https://leetcode.com/problems/number-of-islands/description/

class Solution {
public:
    bool isValid(const vector<vector<char>>& grid, const int& m, const int& n){ // Check current position is valid
        return 0 <= m && m < grid.size() && 0 <= n && n < grid[m].size();
    }

    void DFS(vector<vector<char>>& grid, int m, int n){ // DFS to mark "1" -> "-"
        if(!isValid(grid, m, n)) return;
        if(grid[m][n] != '1') return;
        
        grid[m][n] = '-';
        
        DFS(grid, m+1, n);
        DFS(grid, m-1, n);
        DFS(grid, m, n+1);
        DFS(grid, m, n-1);
    }
    
    int numIslands(vector<vector<char>>& grid) {
        int ans = 0;
        
        for(int m=0; m<grid.size(); m++){         // Traverse the grid
            for(int n=0; n<grid[0].size(); n++){
                if(grid[m][n] == '1'){            //    IF current position is land ('1')
                    ans++;                        //       -> answer + 1
                    DFS(grid,m,n);                //       -> DFS to mark current land as '-'
                }
            }
        }
        
        return ans;
    }
};