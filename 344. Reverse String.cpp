//https://leetcode.com/problems/reverse-string/description/

class Solution {
public:
    void reverseString(vector<char>& s) {
        for(int i=0; i<s.size()/2; i++){    // FOR  i from 0 to s.size/2-1
            swap(s[i], s[s.size()-i-1]);    //      -> Swap element (i, s.size-i-1)
        }
    }
};