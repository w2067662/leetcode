//https://leetcode.com/problems/reveal-cards-in-increasing-order/description/

class Solution {
public:
    //ex:  deck = [17,13,11,2,3,5,7]
    //     deck = [2,3,5,7,11,13,17] (sorted)
    //                           ^             ans = [17]
    //                        ^                ans = [13,17]
    //                     ^                   ans = [11,17,13]
    //                   ^                     ans = [7,13,11,17]
    //                 ^                       ans = [5,17,7,13,11]
    //               ^                         ans = [3,11,5,17,7,13]
    //             ^                           ans = [2,13,3,11,5,17,7] 
    vector<int> deckRevealedIncreasing(vector<int>& deck) {
        vector<int> ans;

        sort(deck.begin(), deck.end());            // Sort the deck in ascending order

        for(int i=deck.size()-1, temp; i>=0; i--){
            if(!ans.empty()){                      // IF  answer vector is not empty 
                temp = ans[ans.size()-1];          //     -> Get the last number from answer
                ans.pop_back();                    //     -> Pop out last number from answer
                ans.insert(ans.begin(), temp);     //     -> Insert last number into answer's front
            }

            ans.insert(ans.begin(), deck[i]);      // Insert current number from deck to answer's front
        }

        return ans;
    }
};