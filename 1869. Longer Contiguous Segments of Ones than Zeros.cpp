//https://leetcode.com/problems/longer-contiguous-segments-of-ones-than-zeros/description/

class Solution {
public:
    //ex: s = "110100010"
    //vec = ["11", "0", "1", "000", "1", "0"]
    //        ^                                 maxLenOne = 2  ;  maxLenZero = 0;
    //              ^                           maxLenOne = 2  ;  maxLenZero = 1;
    //                   ^                      maxLenOne = 2  ;  maxLenZero = 1;
    //                        ^                 maxLenOne = 2  ;  maxLenZero = 3;
    //                               ^          maxLenOne = 2  ;  maxLenZero = 3;
    //                                    ^     maxLenOne = 2  ;  maxLenZero = 3;
    // 2 = maxLenOne < maxLenZero = 3 -> FALSE
    //
    bool checkZeroOnes(string s) {
        vector<string> vec;
        string temp = "";
        int maxLenOne = 0, maxLenZero = 0;

        temp += s[0];

        for(int i=1; i<s.length();i++){     //split string by 0 and 1 into substrings and store into vector
            if(s[i] != s[i-1]){
                vec.push_back(temp);
                temp = "";
                temp += s[i];
            }else temp += s[i];
        }
        if(temp != "")vec.push_back(temp);

        for(int i=0; i<vec.size();i++){     //get the max consecutive length of 0 and 1
            switch(vec[i][0]){
                case '1':
                    maxLenOne = vec[i].length() > maxLenOne ? vec[i].length() : maxLenOne;
                    break;
                case '0':
                    maxLenZero = vec[i].length() > maxLenZero ? vec[i].length() : maxLenZero;
                    break;
            }
        }

        return maxLenOne > maxLenZero;
    }
};
