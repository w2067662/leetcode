#https://leetcode.com/problems/department-highest-salary/description/

# Write your MySQL query statement below
SELECT d.name AS Department, e.name AS Employee, e.salary AS Salary
FROM Department AS d, Employee AS e
WHERE e.departmentId = d.id AND (e.departmentId, salary) IN
    (
        SELECT departmentId, MAX(salary) # Get the employee with hightest salary in each department
        FROM Employee 
        GROUP BY departmentId
    )
