//https://leetcode.com/problems/maximum-sum-with-exactly-k-elements/description/

class Solution {
public:
    int maximizeSum(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end());                                   //sort the vector in ascending order
        return (nums[nums.size()-1] + (nums[nums.size()-1]+k-1)) * k / 2; //add up from last number's value (assume N) to N+k-1 
    }
};
