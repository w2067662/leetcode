//https://leetcode.com/problems/sort-characters-by-frequency/description/

class Solution {
public:
    string frequencySort(string s) {
        unordered_map<int, int> umap;
        map<int, vector<char>, greater<int>> map;                    //sort map value in descending order
        string ans = "";

        for(const auto ch: s) umap[ch]++;                            //store the appear times of chars into umap                           

        for(const auto it: umap) map[it.second].push_back(it.first); //store the appear times from umap to map

        for(const auto it: map){
            for(const auto ch: it.second){
                for(int i=0;i<it.first;i++){
                    ans += ch;                                       //append the chars from map to answer
                }
            }
        }

        return ans;
    }
};
