//https://leetcode.com/problems/detect-pattern-of-length-m-repeated-k-or-more-times/description/

class Solution {
public:
    bool containsPattern(vector<int>& arr, int m, int k) {
        int count = 0;

        for(int i=0; i+m<arr.size(); i++){           //i start from 0 to arr.size-M (M = pattern's length)
            count = arr[i] != arr[i+m] ? 0: count+1; //IF  pattern not matched  ->  restart counting -> set count = 0
                                                     //IF  pattern     matched  ->  count+1
            if(count == (k-1)*m) return true;        //IF  there exist possible pattern with M length and repeat K times
        }                                            //                         ->  TRUE

        return false;
    }
};
