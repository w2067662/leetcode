//https://leetcode.com/problems/find-players-with-zero-or-one-losses/description/

class Solution {
public:
    vector<vector<int>> findWinners(vector<vector<int>>& matches) {
        map<int, int> map;                               // {Player number : Losses}
        vector<int> noLose, exactlyOneLose;

        for(const auto match: matches){                  // Traverse all the matches
            if(map.find(match[0]) == map.end()){
                map[match[0]] = 0;                       //     Set the winner with  0 lose   
            }
            map[match[1]]++;                             //     Set the loser  with +1 lose
        }

        for(const auto it: map){                         // Traverse all the players
            switch(it.second){
                case 0:                                  //     IF no lose
                    noLose.push_back(it.first);          //        -> push into noLose
                    break;
                case 1:                                  //     IF has exactly 1 lose
                    exactlyOneLose.push_back(it.first);  //        -> push into exactlyOneLose
                    break;
            }
        }

        return {noLose, exactlyOneLose};
    }
};
