//https://leetcode.com/problems/find-minimum-operations-to-make-all-elements-divisible-by-three/description/

class Solution {
public:
    int minimumOperations(vector<int>& nums) {
        int count = 0;

        for(const auto& num: nums){
            if(num%3 != 0){         // IF current number can be divisible by 3
                count++;            //    -> count + 1
            }
        }

        return count;
    }
};