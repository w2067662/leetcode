//https://leetcode.com/problems/existence-of-a-substring-in-a-string-and-its-reverse/description/

class Solution {
private:
    const int len = 2; // Find common substring with string length = 2

public:
    bool isSubstringPresent(string s) {
        set<string> set;
        string reversed = s;
        reverse(reversed.begin(), reversed.end());

        for(int i=0; i+len-1<s.length(); i++){
            set.insert(s.substr(i, len));                       // Insert all the substring of s into set
        }

        for(int i=0; i+len-1<s.length(); i++){
            if(set.find(reversed.substr(i, len)) != set.end()){ // IF  found common substring from reversed within set
                return true;                                    //     -> return TRUE
            }
        }

        return false;
    }
};