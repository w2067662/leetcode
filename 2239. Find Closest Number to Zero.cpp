//https://leetcode.com/problems/find-closest-number-to-zero/description/

class Solution {
public:
    int findClosestNumber(vector<int>& nums) {
        int closest = INT_MAX;
        vector<int> vec;

        for(const auto num: nums){
            closest = min(closest, abs(0-num));          //get the closest distance of 0 and num
        }

        for(const auto num: nums){
            if(closest == abs(0-num)) vec.push_back(num);//store all the possible answers with closest distance into vector
        }

        sort(vec.begin(), vec.end());                    //sort the vector in ascending order

        return vec[vec.size()-1];                        //return the answer with largest value
    }
};
