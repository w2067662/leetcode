//https://leetcode.com/problems/shortest-distance-to-a-character/description/

class Solution {
public:
    int shortest_count(string& s,char& c,int& current){        //count for shortest steps to char
        int min=s.length();                                    //set shortest steps as string's length
        for(int i=current, count=0;i>=0;i--,count++){          //go left
            if(s[i]==c) min = count<min ? count:min;           //if found char check it is shortest or not
        }
        for(int i=current, count=0;i<s.length();i++,count++){  //go right
            if(s[i]==c) min = count<min ? count:min;           //if found char check it is shortest or not
        }
        return min;                                            //return shortest steps
    }

    vector<int> shortestToChar(string s, char c) {
        vector<int> ans;
        for(int i=0;i<s.length();i++){                         //traverse the string
            ans.push_back(shortest_count(s, c, i));            //find shortest steps and push to answer
        }
        return ans;
    }
};
