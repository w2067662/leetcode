//https://leetcode.com/problems/get-maximum-in-generated-array/description/

class Solution {
public:
    int getMaximumGenerated(int n) {
        if(n == 0) return 0;
        if(n == 1) return 1;

        vector<int> vec;

        vec.push_back(0);
        vec.push_back(1);

        for(int i=2; i<=n;i++){                      //calculate vec[i]'s value
            if(i%2 == 0) vec.push_back(vec[i/2]);    //IF  i is even -> vec[i] = vec[i/2]
            else vec.push_back(vec[i/2]+vec[i/2+1]); //IF  i is  odd -> vec[i] = vec[i/2] + vec[i/2+1]
        }

        sort(vec.begin(), vec.end());                //sort the vector in ascending order

        return vec[vec.size()-1];                    //return the max value in vector
    }
};
