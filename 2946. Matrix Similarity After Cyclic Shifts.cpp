//https://leetcode.com/problems/matrix-similarity-after-cyclic-shifts/description/

class Solution {
public:
    bool areSimilar(vector<vector<int>>& mat, int k) {
        k %= mat[0].size();                                                                        //mod K to make K smaller than row size
        int leftStart = (mat[0].size()-k)%mat[0].size(), rightStart = k%mat[0].size(), currStart;  //calculate the left shift start index and right shift start index

        for(int i=0;i<mat.size();i++){
            currStart = i%2 == 0 ? leftStart : rightStart;                                         //IF  current row is  odd -> set right start index as current start index
                                                                                                   //IF  current row is even -> set  left start index as current start index
            for(int j=0;j<mat[i].size();j++){
                if(mat[i][j] != mat[i][(currStart+j)%mat[i].size()]) return false;                 //compare the value of origrinal and shifted matrix 
            }
        }

        return true;
    }
};
