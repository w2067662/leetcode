//https://leetcode.com/problems/house-robber-ii/description/

class Solution {
public:
    //ex:  nums = [1,2,3,1] (circular)
    //
    //    nums1 = [1,2,3,x] (not circular)
    //    nums2 = [x,2,3,1] (not circular)
    //
    //    nums1 = [1,2,4]  -> max possible robbing money = max(2,4) = 4
    //    nums2 = [2,3,3]  -> max possible robbing money = max(3,3) = 3
    //
    //   answer = max(4,3) = 4
    //
    int rob(vector<int>& nums) {
        switch(nums.size()){
            case 1: return nums[0];                                         // IF nums.size = 1 -> return nums[0]
            case 2:                                                         // IF nums.size = 2 OR 3
            case 3:
                    return *max_element(nums.begin(), nums.end());          //                  -> return max value of nums
        }

        vector<int> nums1(nums.begin(), nums.end()-1);                      // Deep copy 0 to N-1 from nums to nums1
        vector<int> nums2(nums.begin()+1, nums.end());                      // Deep copy 1 to N   from nums to nums2

                                                                            // Calculate nums1's possible robbing money
        for(int i=2; i<nums1.size();i++){                                   // For i to nums1.size
            nums1[i] += (i == 2)? nums1[i-2] : max(nums1[i-3], nums1[i-2]); //      IF i  = 2  ->  nums1[i] += nums1[i-2]
                                                                            //      IF i != 2  ->  nums1[i] += max(nums1[i-3], nums1[i-2])
        }
                                                                            // Calculate nums2's possible robbing money
        for(int i=2; i<nums2.size();i++){                                   // For i to nums2.size
            nums2[i] += (i == 2)? nums2[i-2] : max(nums2[i-3], nums2[i-2]); //      IF i  = 2  ->  nums2[i] += nums2[i-2]
                                                                            //      IF i != 2  ->  nums2[i] += max(nums2[i-3], nums2[i-2])
        }

        return max({nums1[nums1.size()-2], nums1[nums1.size()-1],           // Get the max possible robbing money from nums1 and nums2
                    nums2[nums2.size()-2], nums2[nums2.size()-1]});
    }
};
