//https://leetcode.com/problems/lexicographical-numbers/description/

class Solution {
public:
    int strToInt(string s){ //convert number string to integar
        int num = 0;
        for(const auto ch: s) num = num * 10 + ch - '0';
        return num;
    }

    vector<int> lexicalOrder(int n) {
        vector<string> numberStr;
        vector<int> ans;

        for(int i=1; i<=n; i++){
            numberStr.push_back(to_string(i));                              //convert integar number to string and push into vector
        }

        sort(numberStr.begin(), numberStr.end(), [&](string &a, string &b){ //sort the vector in Lexicographical order
            for(int i=0; i<min(a.length(), b.length()); i++){
                if(a[i] > b[i]) return false;
                else if(a[i] < b[i]) return true;
            }
            return a.length() < b.length();
        });

        for(const auto str: numberStr) ans.push_back(strToInt(str));        //convert the string number to integar from sorted vector to answer

        return ans;
    }
};
