//https://leetcode.com/problems/water-bottles/description/

class Solution {
public:
    int numWaterBottles(int numBottles, int numExchange) {
        int current = numBottles;
        int exchanged, lefted;
        int ans = numBottles;                   //consume initial bottles

        while(current >= numExchange){
            exchanged = current/numExchange;    //calculate exchanged bottles
            lefted = current%numExchange;       //calculate lefted bottles
            ans += exchanged;                   //consume exchanged bottles
            current = exchanged + lefted;       //calculate current empty bottles
        }

        return ans;
    }
};
