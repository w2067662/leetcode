//https://leetcode.com/problems/count-equal-and-divisible-pairs-in-an-array/description/

class Solution {
public:
    int countPairs(vector<int>& nums, int k) {
        map<int, vector<int>> map;
        int count =0;

        for(int i=0;i<nums.size();i++){
            map[nums[i]].push_back(i);                                  //store the index of current number into map
        }

        for(const auto it:map){
            if(it.second.size() >= 2){
                for(int i=0;i<it.second.size()-1;i++){
                    for(int j=i+1;j<it.second.size();j++){
                        if((it.second[i]*it.second[j])%k == 0) count++; //IF  any 2 index's product is divisible by k -> count+1
                    }
                }
            }
        }

        return count;
    }
};
