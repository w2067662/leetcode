//https://leetcode.com/problems/reverse-integer/description/

class Solution {
public:
    bool outOfRange(int ans, int temp, int x){    //check out of range or not
        return abs(ans) > 214748364 || abs(ans) == 214748364 && (x > 0 && temp > 7 || x < 0 && temp < -8);
    }
    
    int reverse(int x) {
        int ans=0, temp=0;
        
        while(x!=0){                              //reverse the int
            temp=x%10;
            if(outOfRange(ans, temp, x))return 0; //if out of range -> return 0
            ans=ans*10+temp;
            x/=10;
        }
        
        return ans;
    }
};
