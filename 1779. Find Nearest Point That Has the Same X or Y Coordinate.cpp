//https://leetcode.com/problems/find-nearest-point-that-has-the-same-x-or-y-coordinate/description/

class Solution {
public:
    int ManhattanDistance(const pair<int, int>& a, const pair<int, int>& b){        //count for Manhattan distance
        return abs(a.first-b.first) + abs(a.second-b.second);
    }

    int nearestValidPoint(int x, int y, vector<vector<int>>& points) {
        int minDis = INT_MAX, distance;
        int ans = -1;

        for(int i=0;i<points.size();i++){
            if(points[i][0] == x || points[i][1] == y){                             //IF  current points is valid
                distance = ManhattanDistance({points[i][0], points[i][1]}, {x, y}); //    -> count for manhattan distance
                if(distance < minDis){                                              //    IF   distance is smaller
                    minDis = distance;                                              //         -> store the index
                    ans = i;
                }
            }
        }

        return ans;
    }
};
