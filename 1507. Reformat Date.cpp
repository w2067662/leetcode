//https://leetcode.com/problems/reformat-date/description/

class Solution {
private:
    const vector<string> MonthString = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    const vector<string> MonthDigit = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
public:
    string reformatDay(string s){   //reformat day
        s = s.substr(0, s.length()-2);
        return s.length() == 1 ? "0"+s : s;
    }

    string reformatDate(string date) {
        map<string, string> map;
        vector<string> Date;   // Date = {[year], [month], [day]}
        string temp = "";

        for(int i=0; i<MonthString.size(); i++){    //mapping MonthString to MonthDigit
            map[MonthString[i]] = MonthDigit[i];
        }

        for(int i=0; i<date.length();i++){  //split day, month, year and store into vector
            if(date[i] == ' '){
                Date.push_back(temp);
                temp = "";
            }else{
                temp += date[i];
            }
        }
        if(temp != "")Date.push_back(temp);

        return Date[2] + "-" + map[Date[1]] + "-" + reformatDay(Date[0]); //return the reformated date
    }
};
