//https://leetcode.com/problems/determine-if-string-halves-are-alike/description/

class Solution {
public:
    bool isVowel(char ch){                            //check for vowels
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

    bool halvesAreAlike(string s) {
        int leftVowels = 0;
        int rightVowels = 0;

        for(int i=0; i<s.length()/2; i++){            //i start from 0 to s.length/2-1
            if(isVowel(tolower(s[i]))) leftVowels++;  //count for vowels in the left substring
        }
        
        for(int i=s.length()/2; i<s.length(); i++){   //i start from s.length/2 to s.length-1
            if(isVowel(tolower(s[i]))) rightVowels++; //count for vowels in the right substring
        }

        return leftVowels == rightVowels;             //check left and right substring are alike, if leftVowels = rightVowels
    }
};
