//https://leetcode.com/problems/power-of-four/description/

class Solution {
public:
    bool isPowerOfFour(int n) {
        if(n<=0)return false;   //negative numbers and 0 are not power of 4
        while(n>=4){
            if(n%4 !=0) return false;   //if n mod 4 is not 0, then is not power of 4
            n/=4;
        }
        return n%4 ==1; //ex: 16 -> 16/4=4 -> 4/4=1 (the end of division will be 1)
    }
};
