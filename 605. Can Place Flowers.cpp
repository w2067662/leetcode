//https://leetcode.com/problems/can-place-flowers/description/

//ex: flowerbed = [1,0,0,0,0,1] ; n = 2
//                   ^ !isValid
//    flowerbed = [1,0,0,0,0,1] ; n = 2
//                     ^ isValid
//    flowerbed = [1,0,1,0,0,1] ; n = 1
//                       ^ !isValid
//    flowerbed = [1,0,1,0,0,1] ; n = 1
//                         ^ !isValid
//                                n = 1 > 0 -> false
class Solution {
public:
    bool canPlaceFlowers(vector<int>& flowerbed, int n) {
        int left , right;
        bool isValid = true;
        for(int i=0;i<flowerbed.size();i++){
            isValid = true;
            if(flowerbed[i]==0){
                left  = i-1;
                right = i+1;
                if(left >=0 && isValid) isValid = (flowerbed[left] ==1)? false: true;               //check for left  flowerbed
                if(right<flowerbed.size() && isValid) isValid = (flowerbed[right]==1)? false: true; //check for right flowerbed
                
                if(isValid){    //if left and right flowerbed are 0, then is valid -> replace as 1 
                    n--;
                    flowerbed[i] = 1;
                }
            }
        }
        return n<=0;
    }
};
