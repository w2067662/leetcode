//https://leetcode.com/problems/make-two-arrays-equal-by-reversing-subarrays/description/

class Solution {
public:
    bool canBeEqual(vector<int>& target, vector<int>& arr) {
        sort(target.begin(), target.end());      //sort target in ascending order
        sort(arr.begin(), arr.end());            //sort arr    in ascending order

        for(int i=0; i<target.size(); i++){
            if(target[i] != arr[i]) return false;//campare two vectors
        }

        return true;
    }
};
