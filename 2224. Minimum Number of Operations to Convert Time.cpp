//https://leetcode.com/problems/minimum-number-of-operations-to-convert-time/description/

class Solution {
private:
    const vector<int> adjust = {1,5,15,60};
public:
    int timeToInt(string time){ //convert time to integar
        int hour   = (time[0]-'0')*10 + (time[1]-'0');
        int minute = (time[3]-'0')*10 + (time[4]-'0');

        return hour*60 + minute;
    }

    int convertTime(string current, string correct) {
        int duration = abs(timeToInt(current)-timeToInt(correct)); //calculate the duration
        int ans = 0;

        for(int i=adjust.size()-1;i>=0;i--){ 
            ans += duration/adjust[i];       //add up adjust times
            duration %= adjust[i];
        }
        
        return ans;
    }
};
