//https://leetcode.com/problems/sort-list/description/

class Solution {
private:
    vector<pair<int, ListNode*>> vec;
public:
    ListNode* sortList(ListNode* head) {
        if(!head) return NULL;
        if(!head->next) return head;

        while(head){
            vec.push_back({head->val, head});      //push nodes into vector
            head = head->next;
        }

        sort(vec.begin(), vec.end());              //sort the nodes by value in ascending order

        ListNode* newHead;

        for(int i=0;i<vec.size()-1;i++){
            if(i == 0) newHead = vec[i].second;    //set the new head
            vec[i].second->next = vec[i+1].second; //reconnect the the list in sorted order
        }

        vec[vec.size()-1].second->next = NULL;     //set the last node's next as NULL

        return newHead;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
