//https://leetcode.com/problems/implement-queue-using-stacks/description/

class MyQueue {
private:
    stack<int> s1;
    stack<int> s2;
public:
    MyQueue() {
        // No implementation
    }
    
    void push(int x) {
        s1.push(x);  // Push value into stack1
    }
    
    int pop() {
        int size = s1.size();

        for(int i=0; i<size-1; i++){ // For i from 0 to size-1
            s2.push(s1.top());       //     -> Push stack1's top to stack2
            s1.pop();                //     -> Pop top element from stack1
        }

        int value = s1.top();        // Get the bottom element in stack1
        s1.pop();                    // Pop the bottom element from stack1

        size = s2.size();

        for(int i=0; i<size; i++){   // For i from 0 to size
            s1.push(s2.top());       //     -> Push stack2's top back to stack1
            s2.pop();                //     -> Pop top element from stack2
        }

        return value;
    }
    
    int peek() {
        int size = s1.size();

        for(int i=0; i<size-1; i++){
            s2.push(s1.top());
            s1.pop();
        }

        int value = s1.top();        // Same as pop function, but don't pop the bottom element

        size = s2.size();

        for(int i=0; i<size; i++){
            s1.push(s2.top());
            s2.pop();
        }

        return value;
    }
    
    bool empty() {
        return s1.empty(); // Check is stack1 is empty or not
    }
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */
