//https://leetcode.com/problems/sort-the-students-by-their-kth-score/description/

class Solution {
public:
    vector<vector<int>> sortTheStudents(vector<vector<int>>& score, int k) {
        sort(score.begin(), score.end(), [&](vector<int> &a, vector<int> &b){  // Sort the vectors in decresing order by Kth element
            return a[k] > b[k];
        });

        return score;
    }
};
