//https://leetcode.com/problems/widest-vertical-area-between-two-points-containing-no-points/description/

class Solution {
public:
    int maxWidthOfVerticalArea(vector<vector<int>>& points) {
        int maxWidth = 0;

        sort(points.begin(), points.end(), [&](vector<int> &a, vector<int> &b){ //sort the vector by X in ascending order
            return a[0] < b[0];
        });

        for(int i=1; i<points.size(); i++){
            maxWidth = max(maxWidth, points[i][0]-points[i-1][0]);              //get the max width by X's difference of 2 points
        }

        return maxWidth;
    }
};
