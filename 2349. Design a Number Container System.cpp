//https://leetcode.com/problems/design-a-number-container-system/description/

class NumberContainers {
private:
    map<int, set<int>> container;
    unordered_map<int, int> indexToNum;
    
public:
    NumberContainers() {     // Initialize number container
        container.clear();
        indexToNum.clear();
    }
    
    void change(int index, int number) { // Change the index with number
        if(indexToNum.find(index) != indexToNum.end()){     // IF current index aleary assign a number
            container[indexToNum[index]].erase(index);      //    -> erase it from the assigned number's set

            if(container[indexToNum[index]].size() == 0) {  //    IF assigned number's set is empty after erase
                container.erase(indexToNum[index]);         //       -> erase it from container
            }

            indexToNum.erase(index);                        //    -> erase index to assigned number's mapping
        }

        container[number].insert(index);                    // Insert the index into set of current number
        indexToNum[index] = number;                         // Map the index to current number
    }
    
    int find(int number) {  // Find the number and return smallest index
        if(container.find(number) == container.end()){ // IF number not exist in container
            return -1;                                 //    -> return -1
        }

        return *container[number].begin();             // Return the smallest index of current number
    }
};

/**
 * Your NumberContainers object will be instantiated and called as such:
 * NumberContainers* obj = new NumberContainers();
 * obj->change(index,number);
 * int param_2 = obj->find(number);
 */
