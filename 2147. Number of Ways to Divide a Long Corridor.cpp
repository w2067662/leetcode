//https://leetcode.com/problems/number-of-ways-to-divide-a-long-corridor/description/

class Solution {
private:
    const int MOD = pow(10, 9)+7;
public:
    int numberOfWays(string corridor) {
        vector<int> divides;
        int count = 0, currSeats = 0, currDivide = 1;
        long long ways = 0;

        for(const auto ch: corridor){a
            if(ch == 'S') count++;                      //count for seats
        }

        if(count%2 != 0) return 0;                      //IF seats are not even -> no possible ways

        for(const auto ch: corridor){                   //traverse the corridor
            switch(ch){
                case 'S':                               //IF current is seats
                    currSeats++;                        //   -> current seats+1
                    if(currSeats > 2){                  //   IF current seats is more than 2
                        currSeats = 1;                  //      -> set current seats as 1
                        divides.push_back(currDivide);  //      -> push current divided count into divides
                        currDivide = 1;                 //      -> reset current divided count
                    } 
                break;
                case 'P':                               //IF current is plant
                    if(currSeats == 2) currDivide++;    //   IF current seats is equal to 2 -> current divided count +1
                break;
            }
        }
        if(currSeats == 2) divides.push_back(1);        //IF corridor contains only 2 seats
                                                        //   -> only 1 possible way
                                                        
        for(const auto divide: divides){
            if(ways == 0) ways = 1;
            ways = (ways * divide) % MOD;               //calculate the possible ways
        }

        return ways;
    }
};
