//https://leetcode.com/problems/check-if-word-equals-summation-of-two-words/description/

class Solution {
public:
    int toDecimal(string s){    //convert string to Decimal number
        int res=0;
        for(int i=0; i<s.length(); i++){
            res = res * 10 + s[i]- 'a';
        }
        return res;
    }

    bool isSumEqual(string firstWord, string secondWord, string targetWord) {
        return toDecimal(firstWord) + toDecimal(secondWord) == toDecimal(targetWord); //check sum equal
    }
};
