//https://leetcode.com/problems/palindrome-number/description/

class Solution {
public:
    bool isPalindrome(long long int x){
        long long int pop=0,rev=0;
        long long int temp=x;
    
        while(x>0){
            if(rev>INT_MAX||(rev==INT_MAX && pop>7)) return 0;  //out of range
            if(rev<INT_MIN||(rev==INT_MIN && pop<-8)) return 0; //out of range
            pop = x%10;
            x/=10;
            rev = rev*10+pop;
        }
    
        if (rev==temp) return true; //check number is palindrome or not
        else return false;
    }
};
