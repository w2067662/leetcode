//https://leetcode.com/problems/add-one-row-to-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, const int &val, const int &depth, int level){ // Traverse the tree
        if(!root) return;

        if(level == depth-1){                                   // IF   current level = depth-1
            root->left = new TreeNode(val, root->left, NULL);   //      -> Set current root left  as new node's left
            root->right = new TreeNode(val, NULL, root->right); //      -> Set current root right as new node's right
        } else {                                                // ELSE
            traverse(root->left , val, depth, level+1);         //      -> Traverse left  subtree
            traverse(root->right, val, depth, level+1);         //      -> Traverse right subtree
        }

        return;
    }

    TreeNode* addOneRow(TreeNode* root, int val, int depth) {
        if(depth == 1) root = new TreeNode(val, root, NULL); // IF    depth = 1 -> Set root as new root's left child
        else traverse(root, val, depth, 1);                  // ELSE            -> Traverse the tree to insert the row
        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
