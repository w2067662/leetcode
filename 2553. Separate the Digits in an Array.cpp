//https://leetcode.com/problems/separate-the-digits-in-an-array/description/

class Solution {
public:
    vector<int> separateDigits(vector<int>& nums) {
        vector<int> ans;
        string temp ="";

        for(int i=0;i<nums.size();i++){
            temp = to_string(nums[i]);          //convert integar to string
            for(int j=0; j<temp.length(); j++){
                ans.push_back(temp[j]-'0');     //push current digit from number string to answer
            }
        }

        return ans;
    }
};
