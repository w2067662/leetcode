//https://leetcode.com/problems/count-the-number-of-special-characters-i/description/

class Solution {
public:
    int numberOfSpecialChars(string word) {
        set<char> exist, specials;

        for(const auto& ch: word){                          // FOR  each char within a word
            exist.insert(ch);                               //      Insert current char into exist

            if( exist.find(tolower(ch)) != exist.end() &&   //      IF  exist lowercase of current char AND
                exist.find(toupper(ch)) != exist.end() ){   //          exist uppercase of current char 
                    specials.insert(tolower(ch));           //          -> insert lowercase of current char into specials
                }
        }

        return specials.size();
    }
};