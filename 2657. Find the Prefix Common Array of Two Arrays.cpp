//https://leetcode.com/problems/find-the-prefix-common-array-of-two-arrays/description/

class Solution {
public:
    vector<int> findThePrefixCommonArray(vector<int>& A, vector<int>& B) {
        map<int, int> map;
        vector<int> ans;
        set<int> common;

        for(int i=0;i<A.size();i++){
            map[A[i]]++;                              //store current number's appear times from list A
            map[B[i]]++;                              //store current number's appear times from list B

            if(map[A[i]] >= 2) common.insert(A[i]);   //IF current number from list A appear twice -> common number -> insert to set
            if(map[B[i]] >= 2) common.insert(B[i]);   //IF current number from list B appear twice -> common number -> insert to set

            ans.push_back(common.size());             //push the amount of common numbers into answer
        }

        return ans;
    }
};
