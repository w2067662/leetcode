//https://leetcode.com/problems/find-indices-with-index-and-value-difference-i/description/

class Solution {
public:
    bool isValid(const vector<int> &nums, int i){   //check current index is valid
        return 0 <= i && i < nums.size();
    }

    vector<int> findIndices(vector<int>& nums, int indexDifference, int valueDifference) {
        for(int i=0, j; i<nums.size(); i++){
            for(int j=i+indexDifference; j<=nums.size(); j++){                //traverse the pair index (i, j) that fullfill abs(i-j) >= indexDifference
                if(isValid(nums, j)){
                    if(abs(nums[i]-nums[j]) >= valueDifference) return {i, j};//IF   current pair index (i, j) fullfill abs(nums[i]-nums[j]) >= valueDifference
                }                                                             //     -> return (i, j)
            }
        }

        return {-1,-1};
    }
};
