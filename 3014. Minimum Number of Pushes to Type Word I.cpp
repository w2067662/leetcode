//https://leetcode.com/problems/minimum-number-of-pushes-to-type-word-i/description/

class Solution {
public:
    int minimumPushes(string word) {
        int ans = 0;

        for(int i=0; i<word.length(); i++){
            ans += ceil(i/8) + 1;     // IF      (i <= 7)             -> ans += 1;
                                      // ELSE IF (8  <= i && i <= 15) -> ans += 2;
                                      // ELSE IF (16 <= i && i <= 23) -> ans += 3;
                                      // ELSE                         -> ans += 4;
        }

        return ans;
    }
};
