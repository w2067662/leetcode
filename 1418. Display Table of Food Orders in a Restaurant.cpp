//https://leetcode.com/problems/display-table-of-food-orders-in-a-restaurant/description/

class Solution {
public:
    vector<vector<string>> displayTable(vector<vector<string>>& orders) {
        vector<vector<string>> ans;
        map<int, map<string, int>> table;
        set<string> foods;

        for(const auto vec: orders){      
            table[stoi(vec[1])][vec[2]]++;  //store the orders into map
            foods.insert(vec[2]);           //store the food types into set
        }

        ans.push_back({"Table"});                           //push the column title into answer
        for(const auto food: foods) ans[0].push_back(food); 

        for(auto it: table){
            vector<string> curr;
            curr.push_back(to_string(it.first));            //push table number into current vector

            for(auto food: foods){
                curr.push_back(to_string(it.second[food])); //push each food's order amounts into vector
            }

            ans.push_back(curr);
        }

        return ans;
    }
};
