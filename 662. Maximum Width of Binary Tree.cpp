//https://leetcode.com/problems/maximum-width-of-binary-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, int level, unsigned long long curr, vector<pair<unsigned long long, unsigned long long>> &levelRange){
        if(!root) return;

        if(level >= levelRange.size()) levelRange.push_back({curr, curr}); //IF   no current level -> push current range into current level
        else levelRange[level].second = curr;                              //ELSE                  -> update range right

        traverse(root->left , level+1, curr*2  , levelRange);              //traverse left  subtree
        traverse(root->right, level+1, curr*2+1, levelRange);              //traverse right subtree
        return;
    }

    int widthOfBinaryTree(TreeNode* root) {
        vector<pair<unsigned long long, unsigned long long>> levelRange; // vec = [{left0, right0}, {left1, right1}, ...]
        int maxWidth = 0;

        traverse(root, 0, 1, levelRange);          //traverse the tree

        for(const auto pair: levelRange){
            int width = pair.second-pair.first+1;
            maxWidth = max(maxWidth, width);       //store the max width from levelRange
        }

        return maxWidth;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
