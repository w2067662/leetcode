//https://leetcode.com/problems/valid-anagram/description/

class Solution {
public:
    bool isAnagram(string s, string t) {
        if(s.length()!=t.length())return false; //if string len no the same, return false

        map<int , int>map;

        for(int i=0;i<s.length();i++){
            if(map.find(s[i])==map.end()) map.insert({s[i], 1}); //count for appear times of letters in string s
            else map[s[i]]++;
        }
         for(int i=0;i<t.length();i++){
            if(map.find(t[i])==map.end()) map.insert({t[i], 1}); //minus 1 for appear times of letters in string t
            else map[t[i]]--;
        }
        //valid anagram should have all letters' value are 0 in map (the appear times of letters should be the same in s and t)
        for(const auto& element: map){
            if(element.second != 0)return false; 
        }
        return true;
    }
};
