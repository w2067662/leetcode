//https://leetcode.com/problems/longest-common-subsequence/description/

class Solution {
public:
    int solve(vector<vector<int>>& dp, const string& s1, const string& s2, int i1, int i2){ // Solve the longest common subsequence (bottom-up)
    
        if(i1 >= s1.length() || i2 >= s2.length()) return 0;                          // IF index1 OR index2 out of range
                                                                                      //    -> return 0

        if(dp[i1][i2] != -1) return dp[i1][i2];                                       // IF current (index1, index2) of DP memory is not empty
                                                                                      //    -> return the value from memory

        if(s1[i1] == s2[i2]) return dp[i1][i2] = solve(dp, s1, s2, i1+1, i2+1) + 1;   // IF index1 char of string1 = index2 char of string2 (found 1 common char)
                                                                                      //    -> store the maxLongestCommonSubsequenceLength +1 to DP memory
                                                                                      //    -> index1++  AND  index2++ (index1 and index2 goto next in deeper recursion) 

                                                                                      // Get the maxLongestCommonSubsequenceLength from the deeper recursion with
        return dp[i1][i2] = max( solve(dp, s1, s2, i1+1, i2),                         //                                                                           index1 +1 OR
                                 solve(dp, s1, s2, i1, i2+1) );                       //                                                                           index2 +1
    }

    int longestCommonSubsequence(string text1, string text2) {
        vector<vector<int>> dp (text1.length(), vector<int> (text2.length(), -1));   // Create 2D matrix for dynamic programming memory

        return solve(dp, text1, text2, 0, 0);
    }
};
