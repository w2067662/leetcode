//https://leetcode.com/problems/same-tree/description/

class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if(!p && !q) return true;                 // IF P AND Q are NULL           -> TRUE
        if((p && !q) || (!p && q)) return false;  // IF (!P AND Q)  OR  (P AND !Q) -> FALSE
        if(p->val != q->val) return false;        // IF P AND Q are not NULL  AND
                                                  //    P.val != Q.val             -> FALSE

        return isSameTree(p->left , q->left ) &&  // Check left  subtrees are the same AND
               isSameTree(p->right, q->right);    //       right subtrees are the same
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
