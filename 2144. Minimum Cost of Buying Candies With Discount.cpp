//https://leetcode.com/problems/minimum-cost-of-buying-candies-with-discount/description/

bool cmp(int& a, int& b){return a > b;}      //for sorting in decending order

class Solution {
public:
    int minimumCost(vector<int>& cost) {
        sort(cost.begin(), cost.end(), cmp); //sort in decending order

        int sum = 0, curr = 0;

        for(const auto candy: cost){
            if(curr != 2) sum += candy;      //IF   current candy is not third candy, buy it (only third candy can be free)
            curr++;
            if(curr >= 3) curr = 0;
        }

        return sum;
    }
};
