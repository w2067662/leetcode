//https://leetcode.com/problems/longest-unequal-adjacent-groups-subsequence-i/description/

class Solution {
public:
    vector<string> getWordsInLongestSubsequence(int n, vector<string>& words, vector<int>& groups) {
        vector<string> ans;

        ans.push_back(words[0]);                                  //push first word into answer

        for(int i=1; i<groups.size(); i++){
            if(groups[i] != groups[i-1]) ans.push_back(words[i]); //IF  current bit is not the same as previous bit
        }                                                         //    -> store current word into answer

        return ans;
    }
};
