//https://leetcode.com/problems/minimum-falling-path-sum-ii/description/

class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& grid) {
        int ans = INT_MAX;

        for(int i=1; i<grid.size(); i++){                                // Traverse the grid
            for(int j=0; j<grid[i].size(); j++){
                int minPathSum = INT_MAX;

                for(int index=0; index<grid[i-1].size(); index++){       //     For all the indexes
                    if(index != j){                                      //         IF  index shift more than 1 
                        minPathSum = min(minPathSum, grid[i-1][index]);  //             -> Get the min path sum of current row
                    } 
                }

                grid[i][j] += minPathSum;                                //     Add up the min path sum
            }
        }

        for(int i=0; i<grid[grid.size()-1].size(); i++) {
            ans = min(ans, grid[grid.size()-1][i]);                      // Find the min path sum from last row
        }   

        return ans;
    }
};
