//https://leetcode.com/problems/partition-array-into-three-parts-with-equal-sum/description/

class Solution {
public:
    bool canThreePartsEqualSum(vector<int>& arr) {
        int sum = 0, count = 0, total = 0;

        for(const auto num: arr){
            total += num;             // Sum up total
        }
        if(total%3 != 0){             // IF total is divisible by 3
            return false;             //    ->  FALSE
        }    

        for(const auto num: arr){     // Traverse all numbers within array
            if(count >= 2){           //     IF  there exist 3 subsequence with sum = total/3
                return true;          //         -> TRUE
            }

            sum += num;               //     Sum up current number

            if(sum == total/3){       //     IF  sum = total/3
                sum = 0;              //         -> reset sum for next subsequence
                count++;              //         -> count + 1
            }
        }

        return false;                 // No possible answer -> FALSE
    }
};
