//https://leetcode.com/problems/maximum-value-of-a-string-in-an-array/description/

class Solution {
public:
    bool only_digits(string s){                     //check if string is only digits or not
        bool isOnlyDigits=true;
        for(int i=0;i<s.length();i++){
            if(!isdigit(s[i])) isOnlyDigits=false;
        }
        return isOnlyDigits;
    }

    int count_for_value(const string& s){           //count the string value
        int val=0;
        for(int i=0;i<s.length();i++){
            val=val*10+(s[i]-'0');
        }
        return val;
    }

    int maximumValue(vector<string>& strs) {
        int maxValue=0;

        for(int i=0;i<strs.size();i++){             //traverse all the strings in vector
            int value = 0;
            if(!only_digits(strs[i])){              //if the string contain not only digits, 
                value = strs[i].length();           //then the string val = len(str)
            }    
            else{                                   //if the string contain only digits,
                value = count_for_value(strs[i]);   //count for string value
            }
            if(value>maxValue)maxValue=value;       //store the max value
        }
        return maxValue;
    }
};
