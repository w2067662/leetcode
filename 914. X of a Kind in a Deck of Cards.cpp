//https://leetcode.com/problems/x-of-a-kind-in-a-deck-of-cards/description/

class Solution {
public:
    //ex1: deck = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,4,4,4]
    //     map = {1:8, 2:6, 3:3, 4:3}
    //     set = {3,6,8}
    //     vec = [3,6,8]
    //
    //i start from 2 to 3 (vec[0]),  vec[j] mod i = 0?
    //     vec = [3,6,8]  ;  i = 2
    //            x 
    //     vec = [3,6,8]  ;  i = 3
    //            v v x
    //answer = FALSE 
    //
    //ex2: deck = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,4,4,4,4]
    //     map = {1:8, 2:6, 3:4, 4:4}
    //     set = {4,6,8}
    //     vec = [4,6,8]
    //
    //i start from 2 to 4 (vec[0]),  vec[j] mod i = 0?
    //     vec = [4,6,8]  ;  i = 2
    //            v v v
    //answer = TRUE
    //
    bool hasGroupsSizeX(vector<int>& deck) {
        map<int, int> map;
        set<int> set;
        vector<int> vec;
        bool isValid;

        for(const auto num: deck) map[num]++; //store appear times of each number into map

        for(const auto it: map){
            if(it.second == 1) return false;  //IF   any number's appear times = 1 -> FALSE
            set.insert(it.second);            //store the appear times into set (to decrease duplicate appear times)
        }

        for(const auto num: set){
            vec.push_back(num);               //store the appear times from set into vector
        }

        sort(vec.begin(), vec.end());         //sort in ascending order (in order to make vec[0] the smallest)

        for(int i=2;i<=vec[0];i++){           //i start from 2 to vec[0] (each group should contain at least 2)
            isValid = true;
            for(int j=0;j<vec.size();j++){    //check i is the common number for all the groups (all groups can have exactly i amounts of same number)
                if(vec[j]%i != 0){
                    isValid = false;
                    break;
                }
            }
            if(isValid) return true;
        }

        return false;
    }
};
