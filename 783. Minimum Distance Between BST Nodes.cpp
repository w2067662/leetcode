//https://leetcode.com/problems/minimum-distance-between-bst-nodes/description/

class Solution {
private:
    vector<int> value;
public:
    void traverse(TreeNode* root) {   //traverse tree and store all nodes value in vector
        if(!root)return;
        value.push_back(root->val);
        traverse(root->left );
        traverse(root->right);
        return;
    }
    int minDiffInBST(TreeNode* root) {
        int min=INT_MAX;                     //set min as INT_MAX

        traverse(root);                      //store all the values from tree into vector
        sort(value.begin(), value.end());    //sort the vector in ascending order

        for(int i=0, j=i+1;i<value.size()-1;i++, j++){
            min= abs(value[j]-value[i]) < min?abs(value[j]-value[i]) : min; //find the min difference from vector
        }

        return min;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
