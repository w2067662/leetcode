//https://leetcode.com/problems/substrings-of-size-three-with-distinct-characters/description/

class Solution {
public:
    int countGoodSubstrings(string s) {
        if(s.length()<3) return 0;  //the string contain less than 3 letters, no possible answer
        
        int ans = 0;
        for(int i=0; i<s.length()-3+1;i++){
            if(s[i] != s[i+1] && s[i] != s[i+2] && s[i+1] != s[i+2])ans++;  //find good substrings with length = 3 and 3 letters among are not equal to each other
        }
        return ans;
    }
};
