//https://leetcode.com/problems/design-an-ordered-stream/description/

class OrderedStream {
private:
    vector<string> stream;
    int pointer;
public:
    OrderedStream(int n) {
        vector<string> vec(n); //create vector
        stream = vec;          //set stream as new vector
        pointer = 0;           //set pointer as 0
    }
    
    vector<string> insert(int idKey, string value) {
        stream[idKey-1] = value;                                     //insert value

        vector<string> res;                                          //create vector for return
        for(; pointer<stream.size();pointer++){
            if(stream[pointer] != "")res.push_back(stream[pointer]); //if pointer point to not empty slot -> push value into return vector and goto next slot
            else break;                                              //if pointer point to empty slot -> end the loop
        }

        return res;
    }
};

/**
 * Your OrderedStream object will be instantiated and called as such:
 * OrderedStream* obj = new OrderedStream(n);
 * vector<string> param_1 = obj->insert(idKey,value);
 */
