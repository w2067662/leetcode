//https://leetcode.com/problems/final-prices-with-a-special-discount-in-a-shop/description/

class Solution {
public:
    vector<int> finalPrices(vector<int>& prices) {
        for(int i=0, discount; i<prices.size()-1; i++){
            discount = 0;
            for(int j=i+1; j<prices.size(); j++){       //find valid discount with min index
                if(prices[j] <= prices[i]) {
                    discount = prices[j];
                    break;
                }
            }
            if(discount) prices[i] -= discount;         //deduct the discount
        }

        return prices;
    }
};
