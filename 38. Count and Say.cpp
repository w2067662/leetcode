//https://leetcode.com/problems/count-and-say/description/

class Solution {
public:
    string reverse(string s){ //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }

    string Itoa(int num){   //int to string
        string s="";
        while(num>0){
            s+=num%10+'0';
            num/=10;
        }
        return reverse(s);
    }

    string say(string s){   //call say()-> "11" -> two 1's -> "21"
        int count=1;
        int num=s[0]-'0';
        string res="";
        for(int i=1;i<s.length();i++){
            if(s[i]-'0'==num){
                count++;
            }
            else{
                res=res+Itoa(count)+Itoa(num);
                num=s[i]-'0';
                count=1;
            }   
        }
        res=res+Itoa(count)+Itoa(num);
        return res;
    }

    string countAndSay(int n) {
        if(n==1)return "1"; //n==1 return str = "1"
        return say(countAndSay(n-1)); //recursion
    }
};
