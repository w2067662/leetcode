//https://leetcode.com/problems/redistribute-characters-to-make-all-strings-equal/description/

class Solution {
public:
    bool makeEqual(vector<string>& words) {
        map<char, int> map;                             //map = {[char]: [appear times]}

        for(const auto word: words){
            for(const auto letters: word){
                map[letters]++;                         //store all the letters into map
            }
        }

        for(const auto it:map){
            if(it.second%words.size() != 0)return false;//check current letter can distribute to all the words equally or not
        }

        return true;
    }
};
