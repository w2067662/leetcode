//https://leetcode.com/problems/minimum-number-of-operations-to-sort-a-binary-tree-by-level/description/

class Solution {
public:
    void traverse(TreeNode* root, vector<vector<int>> &levelList, int level){ // Traverse the tree and get the level list
        if(!root) return;

        if(level >= levelList.size()) levelList.push_back({root->val});       // IF current level out of range -> push new list with current node value into level lists
        else levelList[level].push_back(root->val);                           // ELSE  -> push current node value into list's back

        traverse(root->left , levelList, level+1);
        traverse(root->right, levelList, level+1);

        return;
    }

    //ex:  list = [5,3,1,2,4]
    //     copy = [1,2,3,4,5] (sorted)                                               ; count = 0
    //
    //     list = [5,3,1,2,4]
    //             ^          curr = 5 ; correct = 1 -> swap(5, map[5]) = swap(5, 4) ; count = 1 (+1)
    // ->  list = [4,3,1,2,5]
    //             ^          curr = 4 ; correct = 1 -> swap(4, map[4]) = swap(4, 2) ; count = 2 (+1)
    // ->  list = [2,3,1,4,5]
    //             ^          curr = 2 ; correct = 1 -> swap(2, map[2]) = swap(2, 3) ; count = 3 (+1)
    // ->  list = [3,2,1,4,5]
    //             ^          curr = 3 ; correct = 1 -> swap(3, map[3]) = swap(2, 3) ; count = 4 (+1)
    // ->  list = [1,2,3,4,5]
    //             ^          curr = 1 ; correct = 1
    //               ^        curr = 2 ; correct = 2
    //    ...
    //                     ^  curr = 5 ; correct = 5

    void countForMinOperations(vector<vector<int>> &levelList, int &count){ // Count the min operation
        for(auto list: levelList){
            map<int, int> map;

            vector<int> copy(list);                     // Copy current level's list
            sort(copy.begin(), copy.end());             // Sort it in ascending order

            for(int i=0; i<copy.size();i++){
                map[copy[i]] = i;                       // Store the index of the node value sorted copy
            } 

            for(int i=0; i<list.size(); i++){           // Traverse the unsorted original list
                if(map[list[i]] != i){                  // IF current node value not belongs to current index
                    swap(list[i], list[map[list[i]]]);  //    -> swap (current node value, the number at current node value's correct index)
                    count++;                            //    -> operation+1
                    i--;                                //    -> stay at current index to check current node value is set to corrent index
                }
            }
        }

        return;
    }

    int minimumOperations(TreeNode* root) {
        vector<vector<int>> levelList;
        int count = 0;

        traverse(root, levelList, 0);            // Traverse the tree to get level lists

        countForMinOperations(levelList, count); // Count for the min operations to make all the lists in same level strictly ascending

        return count;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
