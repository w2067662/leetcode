//https://leetcode.com/problems/longest-string-chain/description/

bool compare(string a, string b){   //compare function to sort the string vector
    return a.length()<b.length();
}

class Solution {
public:
    int longestStrChain(vector<string>& words) {
        sort(words.begin(), words.end(), compare);        //sort the string by length in ascending order

        unordered_map<string, int> umap;
        int ans = 0, longest;

        for(const auto word : words) {                    //traverse the words
            longest =0;
            for (int i=0; i < word.length(); i++) {
                string substring = word;
                substring.erase(i, 1);                    //get all the len-1 substrings of current word (ex: "bdc" -> ["dc", "bc", "bd"])
                longest = max(longest,umap[substring]+1); //find the substrings in umap and get the max length of  those substrings (find the longest chain for current word)
            }
            umap[word] = longest;                         //store the longest chain length of current word 
            ans = max(ans,longest);                       //store the longest chain length into answer
        }

        return ans;
    }
};
