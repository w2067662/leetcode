//https://leetcode.com/problems/132-pattern/description/

class Solution {
public:
    bool find132pattern(vector<int>& nums) {
        vector<int> vec;
        bool isNotDivergence = false;
        // delete consecutive numbers (ex: [1,0,1,1,0,0,1,1,0,0,1])
        for(int i=1;i<nums.size();i++){
            if(nums[i-1] == nums[i])vec.push_back(i);
        }

        for(int i=vec.size()-1;i>=0;i--){
            nums.erase(nums.begin()+vec[i]-1);
        }

        if(nums.size() < 3)return false;

        // check the numbers array is not divergence (ex: [0,-1,2,-3,4,-5])
        for(int i=1;i<nums.size();i++){
            if(abs(nums[i-1]-0) > abs(nums[i]-0))isNotDivergence = true;
        }

        if(!isNotDivergence)return false;
        
        // find a triplet that matches the condition (i < j < k AND nums[i] < nums[k] < nums[j])
        for(int i=0;i<nums.size()-2;i++){
            for(int j=i+1;j<nums.size()-1;j++){
                if(nums[i] < nums[j]){
                    for(int k=j+1;k<nums.size();k++){
                        if(nums[i] < nums[k] && nums[k] < nums[j]) return true;
                    }
                }
            }
        }
        
        return false;
    }
};
