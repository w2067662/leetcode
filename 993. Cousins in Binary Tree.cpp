//https://leetcode.com/problems/cousins-in-binary-tree/description/

class Solution {
private:
    TreeNode* Xparent;
    TreeNode* Yparent;
    int Xdepth;
    int Ydepth;

public:
    void traverse(TreeNode* root, int depth, int x, int y){
        if(root->left){                            //if left node exist
            if(root->left->val == x){              //   if left node value is x
                Xparent = root;                    //       set Xparent as root
                Xdepth = depth+1;                  //       set Xdepth as current depth+1
            }
            if(root->left->val == y){              //   if right node value is y
                Yparent = root;                    //       set Yparent as root
                Ydepth = depth+1;                  //       set Ydepth as current depth+1
            }
            traverse(root->left, depth+1, x, y);   //traverse left tree
        }
        if(root->right){                           //if right node exist
            if(root->right->val == x){             //   if left node value is x
                Xparent = root;                    //       set Xparent as root
                Xdepth = depth+1;                  //       set Xdepth as current depth+1
            }
            if(root->right->val == y){             //   if right node value is y
                Yparent = root;                    //       set Yparent as root
                Ydepth = depth+1;                  //       set Ydepth as current depth+1
            }
            traverse(root->right, depth+1, x, y);  //traverse right tree
        }

        return;
    }

    bool isCousins(TreeNode* root, int x, int y) {
        traverse(root, 0, x, y);
        return Xparent != Yparent && Xdepth == Ydepth; //is node x and node y is cousin -> TRUE
                                                       //(same depth but different parents)
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
