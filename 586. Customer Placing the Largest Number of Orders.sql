#https://leetcode.com/problems/customer-placing-the-largest-number-of-orders/description/

# Write your MySQL query statement below
SELECT customer_number
FROM Orders
GROUP BY customer_number  #group customer_number from small to big 
ORDER BY count(customer_number) desc limit 1; #count customer_number from big to small in descending order, only count once
                                              #the biggest customer_number will have the lastest order_number
