//https://leetcode.com/problems/keyboard-row/description/

class Solution {
private:
    const int rowSize = 3;
    const string keyboard[3] = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};  //{row1, row2, row3}
    int letterOnRow;
    bool isSameRow;
public:
    vector<string> findWords(vector<string>& words) {
        vector<string> ans;
        map<char, int> map;

        for(int i=0;i<rowSize;i++){
            for(int j=0;j<keyboard[i].length();j++){
                map.insert({keyboard[i][j], i+1});
                map.insert({toupper(keyboard[i][j]), i+1}); //upper case of keyboard are contained
            }
        }

        for(int i=0;i<words.size();i++){
            letterOnRow = map[words[i][0]]; //letter On Row 1 or 2 or 3
            isSameRow = true;
            for(int j=0;j<words[i].length();j++){
                if(map[words[i][j]] != letterOnRow){    //if other letters in words[i] aren't in same row
                    isSameRow = false;                  //letters of words[i] is NOT in the Same Row
                    break;
                }
            }
            if(isSameRow){
                ans.push_back(words[i]);    //if all letters of words[i] is in the Same Row, push to answer
            }
        }

        return ans;
    }
};
