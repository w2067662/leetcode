//https://leetcode.com/problems/largest-triangle-area/description/

class Solution {
public:
    double Heronformula(double a, double b, double c, double s){  // use Heron's formula for triangle area
        return (double)sqrt(s*(s-a)*(s-b)*(s-c));
    }

    double largestTriangleArea(vector<vector<int>>& points) {
        double max=0;
        for(int i=0;i<points.size()-2;i++){         //traverse all the possible combinations (3 points)
            for(int j=i+1;j<points.size()-1;j++){
                for(int k=j+1;k<points.size();k++){
                    //get length of 3 triangle edge
                    double a= sqrt(pow(abs((double)points[i][0]-(double)points[j][0]), 2)+pow(abs((double)points[i][1]-(double)points[j][1]), 2));
                    double b= sqrt(pow(abs((double)points[k][0]-(double)points[j][0]), 2)+pow(abs((double)points[k][1]-(double)points[j][1]), 2));
                    double c= sqrt(pow(abs((double)points[i][0]-(double)points[k][0]), 2)+pow(abs((double)points[i][1]-(double)points[k][1]), 2));
                    //get triange area by Heron's formula, if area is bigger than max area, then it become the largest area
                    if(Heronformula(a, b, c, (a+b+c)/2)>max) max = Heronformula(a, b, c, (a+b+c)/2);
                }
            }
        }
        return max;
    }
};
