//https://leetcode.com/problems/boats-to-save-people/description/

class Solution {
public:
    int numRescueBoats(vector<int>& people, int limit) {
        sort(people.begin(), people.end());                    // Sort the weight of people in ascending order

        int left = 0, right = people.size()-1;
        int boats = 0;

        while(left <= right){                                  // WHILE  left <= right (each boat)
            if(limit-people[right] >= people[left]) left++;    //    IF  limit - right person's weight >= left person's weight 
                                                               //        -> Left goto next (Take left person)
            right--;                                           //    Right goto next (Take right person)

            boats++;                                           //    Boat + 1          
        }

        return boats; 
    }
};