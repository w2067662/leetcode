//https://leetcode.com/problems/invert-binary-tree/description/

class Solution {
public:
    void recurrence(TreeNode* root){    //traverse all the nodes
        if(!root)return;
        swap(root->left, root->right);  //swap left and right
        recurrence(root-> left);
        recurrence(root->right);
        return;
    }
    TreeNode* invertTree(TreeNode* root) {
        recurrence(root);
        return root;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
