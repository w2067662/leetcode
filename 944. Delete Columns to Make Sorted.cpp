//https://leetcode.com/problems/delete-columns-to-make-sorted/description/

class Solution {
public:
    int minDeletionSize(vector<string>& strs) {
        int ans=0;
        for(int j=0;j<strs[0].length();j++){    //j -> from 0 to string's length
            for(int i=0;i<strs.size()-1;i++){   //i -> from 0 to vector's size
                if(strs[i][j]>strs[i+1][j]){    //if not sorted
                    ans++;
                    break;                      //end the loop
                }
            }
        }
        return ans;
    }
};
