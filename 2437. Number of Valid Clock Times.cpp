//https://leetcode.com/problems/number-of-valid-clock-times/description/

class Solution {
public:
    int countTime(string time) {
        int pHour = 1;
        int pMinute = 1; 
        if(time[0] == '?' && time[1] == '?') pHour = 24;        // if "??:"      -> possibility of hours = 24
        else if(time[0] != '?' && time[1] == '?'){              // if "-?:"
            if(time[0] == '2') pHour = 4;                       //      if "2?:" -> possibility of hours = 4 (0,1,2,3)
            else pHour = 10;                                    //      if "0?:" OR "1?:" -> possibility of hours = 10
        }   
        else if(time[0] == '?' && time[1] != '?'){              // if "?-:"
            if(time[1] >= '4') pHour = 2;                       //      if "-4:" -> possibility of hours = 2 (0,1)
            else pHour = 3;                                     //      if "-N:" N=0,1,2,3 -> possibility of hours = 3 (0,1,2)
        }

        if(time[3] == '?' && time[4] == '?') pMinute = 60;      // if ":??" -> possibility of minutes = 60
        else if(time[3] != '?' && time[4] == '?') pMinute = 10; // if ":-?" -> possibility of minutes = 10
        else if(time[3] == '?' && time[4] != '?') pMinute = 6;  // if ":?-" -> possibility of minutes = 6

        cout << pHour << " " << pMinute;

        return pHour * pMinute;
    }
};
