//https://leetcode.com/problems/maximum-number-of-integers-to-choose-from-a-range-i/description/

class Solution {
public:
    int maxCount(vector<int>& banned, int n, int maxSum) {
        set<int> set;
        int count = 0;

        for(const auto& num: banned){      // For all banned numbers
            set.insert(num);               //       Insert into set
        }

        for(int i=1; i<=n; i++){           // For i from 1 to N
            if(i > maxSum) break;          //       IF i > maxSum -> end the loop

            if(set.find(i) == set.end()){  //       IF i is banned
                count++;                   //          -> count + 1
                maxSum -= i;               //          -> maxSum - i
            }
        }

        return count;
    }
};