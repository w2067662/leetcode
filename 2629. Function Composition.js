//https://leetcode.com/problems/function-composition/description/

/**
 * @param {Function[]} functions
 * @return {Function}
 */
var compose = function(functions) {
    // if no functions return f(x) = x
	if (functions.length ===0) return function(x) { return x; };
    // else return f(preF, nextF) = nextF(preF(x))
    return functions.reduceRight(function(preF, nextF) { return function(x) {return nextF(preF(x));}})
    //return functions.reduce((curr, prev) => x => curr(prev(x)), x => x)
};

/**
 * const fn = compose([x => x + 1, x => 2 * x])
 * fn(4) // 9
 */
