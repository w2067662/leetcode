//https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/description/

class Solution {
public:
    vector<bool> kidsWithCandies(vector<int>& candies, int extraCandies) {
        vector<bool>ans;
        int max=0;

        for(int i=0;i<candies.size();i++){
            max = candies[i]>max ? candies[i] : max;              //find max
        }

        for(int i=0;i<candies.size();i++){
            if(candies[i]+extraCandies>=max) ans.push_back(true); //if current kid's candies + extra_candies >= max -> TRUE
            else ans.push_back(false);                            //if current kid's candies + extra_candies <  max -> FALSE
        }

        return ans;
    }
};
