//https://leetcode.com/problems/print-in-order/description/

//explain: 
//[process in order] printFirst() -> {firstDone} -> printSecond() -> {secondDone} -> printThird()
// initial Foo()                       [Locked]                        [Locked]
// call printSecond()                     ^ is locked                                               (x)
// call printThird()                                                      ^ is locked               (x)
// call printFirst()   ^ processed->  [Unlocked]                                                    (v)
// call printThird()                                                      ^ is locked               (x)       
// call printSecond()                  [Locked]  ->  ^ processed  ->  [Unlocked]                    (v)
// call printThird()                                                   [Locked]   ->  ^ processed   (v)

class Foo {
private:
    mutex firstDone, secondDone;
public:
    Foo() {
        firstDone.lock();
        secondDone.lock();
    }

    void first(function<void()> printFirst) {
        printFirst();
        firstDone.unlock();
    }

    void second(function<void()> printSecond) {
        firstDone.lock();
        printSecond();
        secondDone.unlock();
    }

    void third(function<void()> printThird) {
        secondDone.lock();
        printThird();
    }
};
