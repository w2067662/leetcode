//https://leetcode.com/problems/convert-a-number-to-hexadecimal/description/

class Solution {
private:
    const int str_max_len = 8;
public:
    string reverseString(string s){ //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    string positiveIntToHex(int num){
        string res="";
        while(num>=16){
            res+= (num%16 >= 10)? (num%16)-10+'a' : num%16+'0'; //decimal int number to hexal char digit, 10~15->'a'~'f'
            num/=16;
        }
        res+= (num%16 >= 10)? (num%16)-10+'a' : num%16+'0';
        return res;
    }
    string negativeIntToHex(int num){
        string res= positiveIntToHex(num);
        //***add reversed "80000000" string***
        if (res.length()<str_max_len){
            for(int i=res.length();i<str_max_len-1;i++)res +='0';       //ex: res = "1" -> "1000000"
            res +='8';                                                  //              -> "10000008"
        }
        else{
            int temp = res[str_max_len-1] - '0' + str_max_len;          //ex: res = "fffffff1" -> "fffffff9"
            res[str_max_len-1] = (temp >= 10)? temp-10+'a' : temp+'0';
        }

        return res;
    }
    string toHex(int num) {
        //-2147483648 ~         -1 -> "80000000" ~ "ffffffff"   (negative int to hex)
        //          0 ~ 2147483647 ->        "0" ~ "7fffffff"   (positive int to hex)
        return (num>=0)? reverseString(positiveIntToHex(num)): reverseString(negativeIntToHex(num-INT_MIN));
    }
};
