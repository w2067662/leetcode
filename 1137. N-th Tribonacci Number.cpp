//https://leetcode.com/problems/n-th-tribonacci-number/description/

class Solution {
public:
    int tribonacci(int n) {
        vector<int> tribonacciList = {0, 1, 1};                                  // Initialize tribonacciList

        for(int i=3; i<=n; i++){                                                 // For  i start from 3 to N
            tribonacciList.push_back(
                tribonacciList[i-1] + tribonacciList[i-2] + tribonacciList[i-3]  //      -> Calculate tribonacciList[i] and push into vector
            );
        }

        return tribonacciList[n];
    }
};