//https://leetcode.com/problems/sequential-digits/description/

class Solution {
private:
    const string sequentialStr = "123456789";

public:
    //ex:  low = 1000, high = 13000
    //
    //  lenRange = [4, 5]
    //
    //  i = 4  ;  "123456789"
    //             ^---            1000 < 1234 < 13000  -> answer = [1234]
    //              ^---           1000 < 2345 < 13000  -> answer = [1234,2345]
    //               ^---          1000 < 3456 < 13000  -> answer = [1234,2345,3456]
    //                ^---         1000 < 4567 < 13000  -> answer = [1234,2345,3456,4567]
    //                 ^---        1000 < 5678 < 13000  -> answer = [1234,2345,3456,4567,5678]
    //                  ^---       1000 < 6789 < 13000  -> answer = [1234,2345,3456,4567,5678,6789]
    //  i = 5  ;  "123456789"
    //             ^----           1000 < 12345 < 13000 -> answer = [1234,2345,3456,4567,5678,6789,12345]
    //
    vector<int> sequentialDigits(int low, int high) {
        pair<int, int> lenRange = {to_string(low).length(), to_string(high).length()}; // Length range = [low number string's length, high number string's length]
        vector<int> ans;

        for(int i=lenRange.first; i<=lenRange.second; i++){     // For length range from the shortest to the longest
            for(int j=0; j+i-1<sequentialStr.length(); j++){    //      For string index j from 0 to j+i-1 is within sequential string
                int num = stoi(sequentialStr.substr(j, i));     //          Get the current string number's value

                if(low <= num && num <= high ){                 //          IF current string number's value is within the range
                    ans.push_back(num);                         //          -> push into answer
                }
            }
        }

        return ans;
    }
};
