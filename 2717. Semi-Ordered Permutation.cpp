//https://leetcode.com/problems/semi-ordered-permutation/description/

class Solution {
public:
    int semiOrderedPermutation(vector<int>& nums) {
        int maxNumIndex = -1;
        int minNumIndex = -1;
        int ans;

        for(int i=0; i<nums.size(); i++){                       
            if(nums[i] == nums.size()) maxNumIndex = i;         //find index of N (= nums.size()) in vector
            if(nums[i] == 1) minNumIndex = i;                   //find index of 1 in vector
        }

        ans = (minNumIndex - 0) + (nums.size()-1 - maxNumIndex);//calculate the distance sum of 1's index to 0 and N's index to nums.size (last element)

        return maxNumIndex < minNumIndex ? ans-1 : ans;         //if N's index is smaller than 1's index, 1 swap is repeated -> answer-1
    }
};
