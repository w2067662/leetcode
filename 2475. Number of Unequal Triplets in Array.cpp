//https://leetcode.com/problems/number-of-unequal-triplets-in-array/description/

class Solution {
public:
    //nums = [4,4,2,4,3,5]
    //map  = [ [2:1], [3:1], [4:3], [5:1]], ans = 0
    //(2,3,4) -> 1x1x3=3 -> ans = 3 (+3)
    //(2,3,5) -> 1x1x1=1 -> ans = 4 (+1)
    //(2,4,5) -> 1x3x1=3 -> ans = 7 (+3)
    //(3,4,5) -> 1x3x1=3 -> ans =10 (+3)
    //ans = 10
    int unequalTriplets(vector<int>& nums) {
        map<int, int> map;              //[value: count]
        int ans=0;

        for(int i=0;i<nums.size();i++){ //store into map
            map[nums[i]]++;
        }

        if(map.size()<3)return 0;       //if no more than 3 unique numbers, no possible triplet exist

        for(auto a=map.begin(); a!=map.end();a++){
            auto b=a; b++;                              //b will be a's next
            if(b==map.end())break;                      //if b out of map's range, end the loop

            for(; b!=map.end();b++){
                auto c=b; c++;                          //c will be b's next
                if(c==map.end())break;                  //if c out of map's range, end the loop

                for(; c!=map.end();c++){
                    ans+=a->second*b->second*c->second; //add up the possible triplet amount
                }
            }
        }

        return ans;
    }
};
