//https://leetcode.com/problems/find-champion-i/description/

class Solution {
public:
    int findChampion(vector<vector<int>>& grid) {
        int count;

        for(int i=0;i<grid.size();i++){
            count = 0;
            for(int j=0;j<grid[i].size();j++){
                count += grid[i][j];                //add up the values in current row
            }
            if(count == grid[i].size()-1) return i; //IF  count = current row size-1 -> current row is Champion
        }

        return -1;
    }
};
