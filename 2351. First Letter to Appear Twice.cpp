//https://leetcode.com/problems/first-letter-to-appear-twice/description/

class Solution {
public:
    char repeatedCharacter(string s) {
        map<char, int> map;
        for(int i=0; i<s.length(); i++){
            map[s[i]]++;                    //add up char's appear time
            if(map[s[i]] >= 2)return s[i];  //if current char appear more than twice, return as answer
        }

        return ' ';
    }
};
