//https://leetcode.com/problems/maximum-repeating-substring/description/

class Solution {
public:
    int maxRepeating(string sequence, string word) {
        int ans = 0;

        for(int i=0, count=0; i<sequence.length(); ){
            count = 0;
            if(word == sequence.substr(i, word.length())){                           //if found first substring match the word
                for(int j=i; j+word.length()-1<sequence.length(); j+=word.length()){ //     -> count for repeats
                    if(word == sequence.substr(j, word.length()))count++;
                    else break;
                }
            }
            ans = max(count, ans);                                                   //store the max repeat times
            i++;
        }

        return ans;
    }
};
