//https://leetcode.com/problems/sort-array-by-increasing-frequency/description/

class Solution {
public:
    vector<int> frequencySort(vector<int>& nums) {
        map<int, vector<int>>map;
        unordered_map<int, int> umap;
        vector<int> order;
        vector<int> ans;

        for(int i=0;i<nums.size();i++){ //store {[number]:[appear times]} into umap
            umap[nums[i]]++;
        }

        for(const auto it: umap){
            map[it.second].push_back(it.first); //store from umap to map in structure {[appear times]: vector<int>{[number1], [number2]...}}
        }

        for(auto it: map){  //traverse the map
            sort(it.second.begin(), it.second.end(), greater<int>()); //sort the vector in descending order

            for(int i=0;i<it.second.size();i++){    //traverse the numbers in vector in descending order
                for(int times=0;times<it.first;times++)ans.push_back(it.second[i]); //push number into answer for [appear times] times
            }
        }

        return ans;
    }
};
