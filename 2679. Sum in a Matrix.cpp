//https://leetcode.com/problems/sum-in-a-matrix/description/

class Solution {
public:
    int matrixSum(vector<vector<int>>& nums) {
        int finalScore = 0;

        for(auto& list: nums){                     // For all the rows in matrix
            sort(list.begin(), list.end());        //       Sort each row in ascending order
        }

        for(int j=0; j<nums[0].size(); j++){       // Traverse the columns
            int maxNum = 0;

            for(int i=0; i<nums.size(); i++){
                maxNum = max(maxNum, nums[i][j]);  //       Pick the largest num in current column
            }

            finalScore += maxNum;                  //       Add up the largest num in current column to final score
        }

        return finalScore;
    }
};
