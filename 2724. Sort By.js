//https://leetcode.com/problems/sort-by/description/

/**
 * @param {Array} arr
 * @param {Function} fn
 * @return {Array}
 */
var sortBy = function(arr, fn) {
    return arr.sort((a, b) => fn(a) - fn(b));
};

//arr.sort([compareFunction])

//function compareNumbers(a, b) {
//  return a - b;                   //ascending order
//}
