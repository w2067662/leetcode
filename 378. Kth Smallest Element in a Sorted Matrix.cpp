//https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/description/

class Solution {
public:
    int kthSmallest(vector<vector<int>>& matrix, int k) {
        map<int, int> map;
        int curr = 0;

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                map[matrix[i][j]]++;                // Store number and appear times from matrix to map
            }
        }

        for(const auto it: map){
            curr += it.second;                      // Add up current appear times 
            if(curr >= k) return it.first;          // IF curr >= K (found Kth smallest) -> return the Kth smallest number in matrix
        }

        return -1;
    }
};
