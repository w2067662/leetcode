//https://leetcode.com/problems/reduction-operations-to-make-the-array-elements-equal/description/

class Solution {
public:
    int reductionOperations(vector<int>& nums) {
        sort(nums.begin(), nums.end());      //sort the vector in ascending order

        int lastNum = nums[0], operation = 0, ans = 0;

        for(const auto num: nums){
            if(num != lastNum) operation++;  //IF  current number != last number -> operation+1
            ans += operation;                //add up operations
            lastNum = num;                   //store current number as lastNum
        }

        return ans;
    }
};
