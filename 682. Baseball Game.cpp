//https://leetcode.com/problems/baseball-game/description/

class Solution {
public:
    int calPoints(vector<string>& operations) {
        vector<int> score;
        int sum=0;

        for(int i=0;i<operations.size();i++){
            if(operations[i]=="C"){
                score.pop_back();                                               //pop out last score from vector
            }else if(operations[i]=="D"){
                score.push_back(score[score.size()-1]*2);                       //record last score's double
            }else if(operations[i]=="+"){
                score.push_back(score[score.size()-1]+score[score.size()-2]);   //record last two scores' sum
            }else{
                score.push_back(stoi(operations[i]));                           //record current score
            }
        }

        for(int i=0;i<score.size();i++){
            sum+=score[i];                //add up sum from vector
        }

        return sum;
    }
};
