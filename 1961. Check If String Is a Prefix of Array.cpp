//https://leetcode.com/problems/check-if-string-is-a-prefix-of-array/description/

class Solution {
public:
    bool isPrefixString(string s, vector<string>& words) {
        for(const auto word: words){
            if(s.length()>=word.length()){
                if(word == s.substr(0, word.length())) s.erase(0, word.length()); //IF    current word matches the prefix of string -> erase current word as prefix from string
                else return false;                                                //ELSE  current word is not the prefix of string -> FALSE
            }else break;
        }

        return s == ""; //check the whole string can be construct by the words in vector
    }
};
