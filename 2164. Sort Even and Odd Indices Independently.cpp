//https://leetcode.com/problems/sort-even-and-odd-indices-independently/description/

bool cmp(int& a, int& b){return a > b;}

class Solution {
public:
    vector<int> sortEvenOdd(vector<int>& nums) {
        vector<int> even, odd, ans;

        for(int i=0;i<nums.size();i++){
            i%2 == 0 ? even.push_back(nums[i]) : odd.push_back(nums[i]); //store the numbers into vector by even or odd index
        }

        sort(even.begin(), even.end());       //sort the nubmer with even index in  ascending order
        sort( odd.begin(),  odd.end(), cmp);  //sort the nubmer with  odd index in descending order

        for(int i=0;i<odd.size();i++){        //append the numbers to answer
            ans.push_back(even[i]);           
            ans.push_back( odd[i]);
        }

        if(nums.size()%2 != 0) ans.push_back(even[even.size()-1]);

        return ans;
    }
};
