//https://leetcode.com/problems/range-sum-query-2d-immutable/description/

class NumMatrix {
private:
    vector<vector<int>> DP;
    
public:
    //ex: matrix = [[3, 0, 1, 4, 2],
    //              [5, 6, 3, 2, 1],
    //              [1, 2, 0, 1, 5],
    //              [4, 1, 0, 1, 7],
    //              [1, 0, 3, 0, 5]]
    //
    //        DP = [[ 3,  3,  4,  8, 10],
    //              [ 8, 14, 18, 24, 27],
    //              [ 9, 17, 21, 28, 36],
    //              [13, 22, 26, 34, 49],
    //              [14, 23, 30, 38, 58]]
    //
    //           . . . . .        . . . .     . . . .      . .      . .
    //           . . . . .        . . . .     . . .24      . .      .14
    //   get     . . 0 1 .  =>    . . . .  -           -   . .  +         =    . .
    //           . . 0 1 .        . . .34                  .22                 . 2
    //           . . . . .
    //
    //
    NumMatrix(vector<vector<int>>& matrix) { // Initialize the DP matrix for (i,j) = sum of (0,0) to (i,j)
        this->DP = matrix;

        for(int i=0; i<DP.size(); i++){
            for(int j=0; j<DP[i].size(); j++){
                if(i > 0) this->DP[i][j] += this->DP[i-1][j];
                if(j > 0) this->DP[i][j] += this->DP[i][j-1];
                if(i > 0 && j > 0) this->DP[i][j] -= this->DP[i-1][j-1];
            }
        }
    }
    
    int sumRegion(int row1, int col1, int row2, int col2) { // Calculate the sum of (row1, col1) to (row2, col2) in O(1) time complexity
        int sum = this->DP[row2][col2];

        if(row1 > 0) sum -= this->DP[row1-1][col2];
        if(col1 > 0) sum -= this->DP[row2][col1-1];
        if(row1 > 0 && col1 > 0) sum += this->DP[row1-1][col1-1]; 

        return sum;
    }
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */
