//https://leetcode.com/problems/count-items-matching-a-rule/description/

class Item{     //create Item class for items
public:
    string Type;
    string Color;
    string Name;
    Item():Type(""), Color(""), Name(""){}
    Item(string type, string color, string name):Type(type), Color(color), Name(name){}
};

class Solution {
public:
    int countMatches(vector<vector<string>>& items, string ruleKey, string ruleValue) {
        vector<Item> vec;
        int ans = 0;

        for(int i=0; i<items.size();i++){
            Item item(items[i][0], items[i][1], items[i][2]);   //create new item
            vec.push_back(item);                                //store new item into vector
        }

        if(ruleKey == "type"){                                  //if using type rule
            for(int i=0;i<vec.size();i++){                      
                if(vec[i].Type == ruleValue) ans++;             //  check type = rule value
            }
        }else if(ruleKey == "color"){                           //if using color rule
            for(int i=0;i<vec.size();i++){
                if(vec[i].Color == ruleValue) ans++;            //  check color = rule value
            }
        }else if(ruleKey == "name"){                            //if using name rule
            for(int i=0;i<vec.size();i++){
                if(vec[i].Name == ruleValue) ans++;             //  check name = rule value
            }
        }

        return ans;
    }
};
