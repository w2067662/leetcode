//https://leetcode.com/problems/maximum-number-of-pairs-in-array/description/

class Solution {
public:
    vector<int> numberOfPairs(vector<int>& nums) {
        map<int, int> map;          //map = {[number]: [appear times]}
        int pairs = 0, lefted = 0;

        for(int i=0;i<nums.size();i++){
            map[nums[i]]++;         //store number's appear times into map
        }

        for(const auto it:map){
            pairs += it.second/2;   //pair up 2 numbers with same value
            lefted += it.second%2;  //add up the lefted number
        }

        return {pairs, lefted};
    }
};
