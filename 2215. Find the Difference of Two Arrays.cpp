//https://leetcode.com/problems/find-the-difference-of-two-arrays/description/

class Solution {
public:
    vector<vector<int>> findDifference(vector<int>& nums1, vector<int>& nums2) {
        vector<vector<int>>ans;

        set<int> set1;
        set<int> set2;

        for(int i=0;i<nums1.size();i++){    //insert two vector's number to set
            set1.insert(nums1[i]);
        }
        for(int i=0;i<nums2.size();i++){
            set2.insert(nums2[i]);
        }

        vector<int> vec1;
        vector<int> vec2;

        for(const auto it :set1){           //find and push not differ numbers to vector
            if(set2.find(it)==set2.end()) vec1.push_back(it);
        }
        for(const auto it :set2){
            if(set1.find(it)==set1.end()) vec2.push_back(it);
        }

        ans.push_back(vec1);
        ans.push_back(vec2);
        return ans;
    }
};
