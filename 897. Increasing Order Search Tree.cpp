//https://leetcode.com/problems/increasing-order-search-tree/description/

class Solution {
public:
    map<int , int>map;

    void traverse(TreeNode* root){  //traverse and store node values into map
        if(!root)return;
        map[root->val]++;           //store into map
        traverse(root->left );
        traverse(root->right);
        return;
    }

    TreeNode* create_increasingBST(){       //create the increasing BST 
        TreeNode* root=new TreeNode;        //for new tree root (=root->right)
        TreeNode* temp=root;                //for setting parentNode->right
        for(const auto it:map){
            TreeNode* node=new TreeNode;    //create new node
            for(int i=0;i<it.second;i++){
                node->val=it.first;         //set value to new node
                temp->right=node;           //set parentNode->right=node
                temp=node;                  //set parentNode=node
            }
        }
        return root->right;                 //return new tree root
    }

    TreeNode* increasingBST(TreeNode* root) {
        if(!root)return NULL;
        traverse(root);
        return create_increasingBST();
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
