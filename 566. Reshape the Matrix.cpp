//https://leetcode.com/problems/reshape-the-matrix/description/

class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& mat, int r, int c) {
        if (r*c != mat.size()*mat[0].size()) return mat;    //return if container size invalid
        vector<vector<int>> ans(r, vector<int>(c));         //create new matrix

        for(int i=0, k=0, s=0;i<mat.size();i++){    //traverse all element in old matrix
            for(int j=0;j <mat[i].size();j++){
                ans[k][s]=mat[i][j];                //store into new matrix
                s++;
                if(s>=c){                           //if s is over column size c
                    k++;                            //go to next row
                    s=0;
                }
            }
        }
        return ans;
    }
};
