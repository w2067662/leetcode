//https://leetcode.com/problems/candy/description/

class Solution {
public:
    //ex: ratings = [1,2,5,3,2]
    //                              leftToRight = [1,1,1,1,1] ; rightToLeft = [1,1,1,1,1]
    //                 ^            leftToRight = [1,2,1,1,1] ; rightToLeft = [1,1,1,1,1]
    //                   ^          leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,1,1,1]
    //                     ^        leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,1,1,1]
    //                       ^      leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,1,1,1]
    //                     ^        leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,1,2,1]
    //                   ^          leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,3,2,1]
    //                 ^            leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,3,2,1]
    //               ^              leftToRight = [1,2,3,1,1] ; rightToLeft = [1,1,3,2,1]
    // leftToRight = [1,2,3,1,1]      (all children get high candies than their neighbors from left to right)
    // rightToLeft = [1,1,3,2,1]      (all children get high candies than their neighbors from right to left)
    //      answer =  1+2+3+2+1 = 9   (get the max value from 2 vectors, to get min sum of candies)
    //
    int candy(vector<int>& ratings) {
        vector<int> leftToRight(ratings.size(), 1);          //set vector leftTo
        vector<int> rightToLeft(ratings.size(), 1);
        int ans = 0;
       
        for(int i=1; i<ratings.size(); i++){                 //from left to right
            if(ratings[i] > ratings[i-1]){                   //IF current child's rating > current-1 child's rating
                leftToRight[i] = leftToRight[i-1] + 1;       //   -> set current child = current-1 child + 1
            }
        }
          for(int i=ratings.size()-2; i>=0; i--){            //from right to left
            if(ratings[i] > ratings[i+1]){                   //IF current child's rating > current+1 child's rating
                rightToLeft[i] = rightToLeft[i+1] + 1;       //   -> set current child = current+1 child + 1
            }
        }

        for(int i =0; i<ratings.size(); i++){
           ans = ans + max(rightToLeft[i], leftToRight[i]);  //store the largest value
        }

        return ans;
    }
};
