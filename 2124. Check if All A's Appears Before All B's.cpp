//https://leetcode.com/problems/check-if-all-as-appears-before-all-bs/description/

class Solution {
public:
    bool checkString(string s) {
        bool foundB = false;

        for(const auto ch: s){
            if(ch =='b' && !foundB) foundB = true; //IF   first 'b' is found -> set foundB as true
            if(ch =='a' &&  foundB) return false;  //IF   current char is 'a' and 'b' is found -> FALSE
        }

        return true;
    }
};
