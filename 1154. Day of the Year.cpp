//https://leetcode.com/problems/day-of-the-year/description/

class Solution {
private:
    vector<int> DaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; //initial days in each month
public:
    bool isLeapYear(int year){ //check is leap year or not
        if(year%4 == 0){                      //if divisable by 4
            if(year%100 == 0){                //    if divisable by 100
                if(year%400 == 0)return true; //        if divisable by 400 -> is leap year
            }                                 //        if undivisable by 400 -> not leap year
            else return true;                 //    if undivisable by 100 
                                              //        -> is leap year
        }                                     //    -> is leap year
        return false;                         //if undivisable by 4 
    }                                         //    -> not leap year

    int countDaysOfThisYear(int year, int month, int day){ //count days from 1971-01-01
        int days=0;
 
        if(isLeapYear(year))DaysInMonth[1] = 29; //set Feb. as 29 days
        else DaysInMonth[1] = 28;                //set Feb. as 28 days
        
        for(int i=1;i<month;i++){             //add up days from Jan. to current_month-1
            days+=DaysInMonth[i-1];
        }

        return  days+=day;                    //add up days in current_month
    }

    int dayOfYear(string date) {
        //convert date to (year, month, day)
        int year = (date[0]-'0')*1000 + (date[1]-'0')*100 + (date[2]-'0')*10 + (date[3]-'0');
        int month = (date[5]-'0')*10 + (date[6]-'0');
        int day = (date[8]-'0')*10 + (date[9]-'0');

        return countDaysOfThisYear(year, month, day);
    }
};
