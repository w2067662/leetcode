//https://leetcode.com/problems/maximum-sum-of-an-hourglass/description/

class Solution {
public:
    int hourglassSum(vector<vector<int>>& grid, pair<int, int> start){ // Calculate the hourglass sum
        int sum = 0;

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(!(i==1 && j==0) && !(i==1 && j==2)){         // IF (x, y) is not (1, 0) or (1, 2)
                    sum += grid[start.first+i][start.second+j]; //    -> sum up the value
                }
            }
        }

        return sum;
    }

    int maxSum(vector<vector<int>>& grid) {
        if(grid.size() < 3 || grid[0].size() < 3) return 0; // IF grid is not big enough to get hourglass sum -> return 0

        int maxSum = 0;

        for(int i=0; i+2<grid.size(); i++){
            for(int j=0; j+2<grid[i].size(); j++){
                int sum = hourglassSum(grid, {i, j});       // Calculate the hourglass sum
                maxSum = max(maxSum, sum);                  // Store the largest hourglass sum
            }
        }

        return maxSum;
    }
};
