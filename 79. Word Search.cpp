//https://leetcode.com/problems/word-search/description/

class Solution {
private:
    const vector<int> steps = {1, 0, -1, 0, 1}; // Define next steps

    bool isValid(vector<vector<char>>& board, pair<int, int> &pos){ // Check position within the range of board or not
        return 0 <= pos.first && pos.first < board.size() && 0 <= pos.second && pos.second < board[0].size();
    }

    bool search(vector<vector<char>> &board, int i, int j, int index, const string &word){ // DFS (Deep-First-Search) to search the word in board
        if(board[i][j] != word[index]) return false;                                       // IF   not matching current char -> FALSE
        if(index == word.length()-1) return true;                                          // IF   matching current char AND is the end of the word -> TRUE
                                                                                           // IF   matching current char AND not the end of the word
        char temp = board[i][j];                                                           //      -> set current char into temp
        board[i][j] = '$';                                                                 //      -> mark current position as visited ('$')

        for(int s=0; s<steps.size()-1; s++){                                               //      For all next possible steps
            pair<int, int> next = {i+steps[s], j+steps[s+1]};

            if(isValid(board, next) && board[next.first][next.second] != '$'){             //      IF   next step is valid and not visited ('$')
                if(search(board, next.first, next.second, index+1, word)){
                    return true;                                                           //           -> search next char
                }
            }
        }

        board[i][j] = temp;                                                                //      Unmark the position as not visited
        
        return false;
    }

public:
    bool exist(vector<vector<char>>& board, string word) {
        for(int i=0; i<board.size(); i++){
            for(int j=0; j<board[i].size(); j++){
                if(board[i][j] == word[0]){                        // IF  current char matches first char of word 
                    if(search(board, i, j, 0, word)) return true;  //     -> search the word
                }
            }
        }

        return false;
    }
};
