//https://leetcode.com/problems/largest-positive-integer-that-exists-with-its-negative/description/

class Solution {
public:
    int findMaxK(vector<int>& nums) {
        set<int> set;
        int ans = -1;

        for(const auto& number: nums){   
            set.insert(number);                          // Store numbers into set
        }

        for(auto it=set.rbegin(); it!=set.rend(); it++){ // Traverse set from the end to begin
            if(*it <= 0) break;                          //     IF number is negetive -> end the loop

            if(set.find(0-*it) != set.end()){            //     IF find the correspond negetive number of current positive number
                ans = *it;                               //        -> Assign to answer
                break;                                   //        -> End the loop
            }
        }

        return ans;
    }
};
