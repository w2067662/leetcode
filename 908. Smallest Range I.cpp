//https://leetcode.com/problems/smallest-range-i/description/

class Solution {
public:
    int smallestRangeI(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end());//sort vector in ascending order

        int max = nums[nums.size()-1]; //get max number from vector
        int min = nums[0];             //get min number from vector

        return max-min > k*2 ? max-min-k*2 : 0; //if difference of max and min is bigger than 2 x k
                                                //  -> answer = difference of max and min - 2 x k
                                                //if not -> answer = 0
    }
};
