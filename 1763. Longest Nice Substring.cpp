//https://leetcode.com/problems/longest-nice-substring/description/　　　　　　　

class Solution {
public:
    string longestNiceSubstring(string s) {
        if(s.length() <= 1) return "";                                      //too short to form a nice string

        set<char> set;

        for(const auto ch: s) set.insert(ch);                               //insert current char into set

        for(int i=0; i<s.length(); i++){                                    //traverse all the chars in string
            if(set.find(tolower(s[i])) == set.end() ||                      //IF   no lower case of current letter OR
               set.find(toupper(s[i])) == set.end()) {                      //     no upper case of current letter
               string s1 = longestNiceSubstring(s.substr(0, i));            //     -> find nice string from left  substring
               string s2 = longestNiceSubstring(s.substr(i+1, s.length())); //     -> find nice string from right  substring

               return s1.length() >= s2.length() ? s1 : s2;                 //return the longest nice substring
            }
        }

        return s;                                                           //current substring is a nice string
    }
};
