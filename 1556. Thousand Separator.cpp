//https://leetcode.com/problems/thousand-separator/description/

class Solution {
public:
    string reverse(string s){             //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }

    string thousandSeparator(int n) {
        string s = to_string(n);          //convert integar to string
        string ans = "";

        for(int i=s.length()-1, count=1; i>=0; i--, count++){ //start appending digits from the end of string to the front
            ans += s[i];
            if(count%3 == 0 && i!=0)ans+='.'; //insert '.' into string for each 3 digits
        }

        return reverse(ans);              //reverse the answer
    }
};
