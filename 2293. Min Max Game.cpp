//https://leetcode.com/problems/min-max-game/description/

class Solution {
public:
    int minMaxGame(vector<int>& nums) {
        int winner;

        while(nums.size()>1){ //game ends when only 1 winner lefted
            vector<int> temp;
            for(int i=0;i<nums.size();i+=2){
                winner = (i/2)%2 == 0 ? min(nums[i], nums[i+1]) : max(nums[i], nums[i+1]); //IF  current pair (i, i+1) that i/2 is even -> get the min
                temp.push_back(winner);                                                    //IF  current pair (i, i+1) that i/2 is  odd -> get the max
            }

            nums = temp; //replace the vector with new round
        }

        return nums[0];
    }
};
