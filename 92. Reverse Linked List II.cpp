//https://leetcode.com/problems/reverse-linked-list-ii/description/

class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int left, int right) {
        if(left == right)return head; //if left = right, no reverse -> return original list

        vector<ListNode*> nodes;
        ListNode* temp = head;
        int length = 0;

        while(temp){
            nodes.push_back(temp);    //store all the nodes into vector
            temp = temp-> next;       //if current node has next, goto next
            length++;                 //count for list length
        }
        
        //ex:
        //list = [1->2(left)->3->4(right)->5], left = 2 ; right = 4
        //list = [1->2(reverse end)<-3<-4(reverse head)  5]  =>  1->4(reverse head); 2(reverse end)->5
        //list = [1->4(reverse head)->3->2(reverse end)->5]

        ListNode* reverseHead = nodes[right-1]; //set nodes[right-1] as reverse list head
        ListNode* reverseEnd = nodes[left-1];   //set nodes[left-1]  as reverse list end

        for(int i=right-1 ; i>left-1; i--){
            nodes[i]->next = nodes[i-1];        //reverse the list from nodes[right-1] to nodes[left-1]
        }

        if(right-1 == length-1) reverseEnd->next = NULL; //if no node after nodes[right-1], set reverse list end to NULL
        else reverseEnd->next = nodes[right];            //if there is node after nodes[right-1], set reverse list end to nodes[right]

        if(left-1 == 0) head = nodes[right-1];           //if no node before nodes[left-1], set reverse list head as new head
        else nodes[left-2]->next = nodes[right-1];       //if there is node before nodes[left-1], set reverse list head to nodes[left-2]

        return head;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
