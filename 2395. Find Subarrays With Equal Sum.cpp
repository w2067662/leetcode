//https://leetcode.com/problems/find-subarrays-with-equal-sum/description/

class Solution {
public:
    bool findSubarrays(vector<int>& nums) {
        set<int> set;

        for(int i=1, sum;i<nums.size();i++){
            if(set.find(nums[i-1]+nums[i]) != set.end())return true; //check if current array sum exist in set or not
            else set.insert(nums[i-1]+nums[i]);                      //if not -> insert current array sum into set
        }

        return false;
    }
};
