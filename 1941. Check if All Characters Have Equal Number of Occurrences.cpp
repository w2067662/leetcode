//https://leetcode.com/problems/check-if-all-characters-have-equal-number-of-occurrences/description/

class Solution {
public:
    bool areOccurrencesEqual(string s) {
        map<char, int> map; //map = {[char]: [appear times]}
        int occurs = -1;

        for(int i=0; i<s.length(); i++){        
            map[s[i]]++;    //store char appear times into map
        }

        for(const auto it:map){
            if(occurs == -1) occurs = it.second;    //set first element's appear times into occurs
            if(it.second != occurs) return false;   //if there exist element containing unequal appear times -> FALSE
        }

        return true;
    }
};
