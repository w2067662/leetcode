//https://leetcode.com/problems/special-positions-in-a-binary-matrix/description/

class Solution {
public:
    bool isSpecial(int i, int j, vector<vector<int>>& mat){
        if(mat[i][j]!=1)return false;              //current number is not 1 -> FALSE

        for(int x=0;x<mat.size();x++){
            if(mat[x][j]!=0 && x!=i)return false;  //not all number in column j is 1 -> FALSE
        }

        for(int y=0;y<mat[i].size();y++){
            if(mat[i][y]!=0 && y!=j)return false;  //not all number in row i is 1 -> FALSE
        }

        return true;
    }

    int numSpecial(vector<vector<int>>& mat) {
        int ans=0;

        for(int i=0;i<mat.size();i++){
            for(int j=0;j<mat[i].size();j++){
                if(isSpecial(i, j, mat)) ans++; //found special number 
            }
        }

        return ans;
    }
};
