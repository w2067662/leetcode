//https://leetcode.com/problems/day-of-the-week/description/

class Solution {
private:
    vector<int> DaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; //initial days in each month
    vector<string> DayOfTheWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
public:
    bool isLeapYear(int year){ //check is leap year or not
        if(year%4 == 0){                      //if divisable by 4
            if(year%100 == 0){                //    if divisable by 100
                if(year%400 == 0)return true; //        if divisable by 400 -> is leap year
            }                                 //        if undivisable by 400 -> not leap year
            else return true;                 //    if undivisable by 100 
                                              //        -> is leap year
        }                                     //    -> is leap year
        return false;                         //if undivisable by 4 
    }                                         //    -> not leap year

    int countDays(int year, int month, int day){ //count days from 1971-01-01
        int days=0;

        for(int i=1971;i<year;i++){           //start from 1971 to current_year-1
            if(isLeapYear(i)) days+=366;      //if is leap year, add 366
            else days+=365;                   //if is not leap year, add 365
        }
 
        if(isLeapYear(year))DaysInMonth[1] = 29; //set Feb. as 29 days
        else DaysInMonth[1] = 28;                //set Feb. as 28 days
        
        for(int i=1;i<month;i++){             //add up days from Jan. to current_month-1
            days+=DaysInMonth[i-1];
        }

        return  days+=day;                    //add up days in current_month
    }

    string dayOfTheWeek(int day, int month, int year) {
        //count total days from 1971-01-01 to current day
        //shift +4 to match {"Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday"}
        //mod 7 for Day of the week
        return DayOfTheWeek[(countDays(year, month, day)+4)%7];
    }
};
