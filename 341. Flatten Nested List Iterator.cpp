//https://leetcode.com/problems/flatten-nested-list-iterator/description/

class NestedIterator {
private:
    int curr;
    vector<int> List;
public:
    NestedIterator(vector<NestedInteger> &nestedList) {
        this->curr = 0;         //set current as 0
        flatten(nestedList);    //flatten the nested list
    }

    void flatten(vector<NestedInteger> &nestedList){  //flatten by recursion
        for(auto element: nestedList){
            if(element.isInteger()) this->List.push_back(element.getInteger());
            else flatten(element.getList());
        }
    }
    
    int next() {
        return hasNext() ? this->List[curr++] : -1;   //IF   list has next ->return next
    }
    
    bool hasNext() {
        return 0 <= curr && curr < this->List.size();
    }
};

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */
