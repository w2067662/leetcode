//https://leetcode.com/problems/partition-labels/description/

class Solution {
public:
    //ex:  s = "ababcbacadefegdehijhklij"
    //   map = ['a':[0,2,6,8],
    //          'b':[1,3,5],
    //          'c':[4,7],
    //          'd':[9,14],
    //          'e':[10,12,15],
    //          'f':[11],
    //          'g':[13],
    //          'h':[16,19],
    //          'i':[17,22],
    //          'j':[18,23],
    //          'k':[20],
    //          'l':[21]]
    //
    // "ababcbacadefegdehijhklij"
    //  ^                         i =  0 ; curr = ['a']                 ; temp = "a"          -> invalid  ;  part = []
    //   ^                        i =  1 ; curr = ['a','b']             ; temp = "ab"         -> invalid  ;  part = []
    //    ^                       i =  2 ; curr = ['a','b']             ; temp = "aba"        -> invalid  ;  part = []
    //    ...
    //         ^                  i =  7 ; curr = ['a','b','c']         ; temp = "ababcbac"   -> invalid  ;  part = []
    //          ^                 i =  8 ; curr = ['a','b','c']         ; temp = "ababcbaca"  -> valid    ;  part = ["ababcbaca"]
    //           ^                i =  9 ; curr = ['d']                 ; temp = "d"          -> invalid  ;  part = ["ababcbaca"]
    //            ^               i = 10 ; curr = ['d','e']             ; temp = "de"         -> invalid  ;  part = ["ababcbaca"]
    //    ...
    //                         ^  i = 23 ; curr = ['h','i','j','k','l'] ; temp = "hijhklij"   -> valid    ;  part = ["ababcbaca","defegde","hijhklij"]
    //
    // part = ["ababcbaca","defegde","hijhklij"] -> answer = [9,7,8]
    //
    vector<int> partitionLabels(string s) {
        map<char, set<int>> map;
        vector<string> part;
        vector<int> ans;
        string temp = "";
        set<char> curr;
        bool valid;

        for(int i=0;i<s.size();i++) map[s[i]].insert(i);   //store each letter's index into map

        for(int i=0;i<s.size();i++){
            temp += s[i];                                  //append char into temp
            curr.insert(s[i]);                             //insert char into current set

            valid = true;
            for(const auto ch: curr){                      //check the current part (temp) is valid or not
                for(const auto index: map[ch]){
                    if(index > i){                         //IF  current char in current set has index > i
                        valid = false;                     //    -> invalid
                        break;
                    };
                }
                if(!valid) break;
            }

            if(valid){                                     //IF  current part (temp) is valid
                part.push_back(temp);                      //    -> append into part
                temp = "";                                 //    -> reset temp to empty string
                curr.clear();                              //    -> clear set
            }
        }

        for(const auto s: part) ans.push_back(s.length()); //push the parts length into answer

        return ans;
    }
};
