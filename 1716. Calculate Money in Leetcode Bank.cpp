//https://leetcode.com/problems/calculate-money-in-leetcode-bank/description/

class Solution {
public:
    int totalMoney(int n) {
        int total = 0;
        int weeks = n/7, days = n%7; //calculate weeks and days

        for(int i=0; i<weeks; i++){   
            total += (1+7)*7/2 + i*7;//count for weeks
        }

        for(int i=0; i<days; i++){
            total += i+weeks+1;      //count for days
        }

        return total;
        
    }
};
