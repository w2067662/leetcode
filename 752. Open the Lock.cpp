//https://leetcode.com/problems/open-the-lock/description/

class Solution {
public:
    //tips:
    //      ^
    //      0 | 0 | 0 | 0   ->  9 | 0 | 0 | 0  (unseen)  OR   1 | 0 | 0 | 0 (unseen) ; queue = {"9000","1000"}
    //      v
    
    //          ^
    //      0 | 0 | 0 | 0   ->  0 | 9 | 0 | 0  (unseen)  OR   0 | 1 | 0 | 0 (unseen) ; queue = {"9000","1000","0900","0100"}
    //          v
    
    //              ^
    //      0 | 0 | 0 | 0   ->  0 | 0 | 9 | 0  (unseen)  OR   0 | 0 | 1 | 0 (unseen) ; queue = {"9000","1000","0900","0100","0090","0010"}
    //              v
    
    //                  ^
    //      0 | 0 | 0 | 0   ->  0 | 0 | 0 | 9  (unseen)  OR   0 | 0 | 0 | 1 (unseen) ; queue = {"9000","1000","0900","0100","0090","0010","0009","0001"}
    //                  v

    //      ^
    //      9 | 0 | 0 | 0.  ->  8 | 0 | 0 | 0  (unseen)  OR   0 | 0 | 0 | 0 (seen)   ; queue = {"1000","0900","0100","0090","0010","0009","0001","8000"}
    //      v

    //      count steps ... until target found  OR  queue empty

    int openLock(vector<string>& deadends, string target) {
        unordered_set<string> seen{deadends.begin(), deadends.end()};  // Create a set to store deadends

        if(seen.count("0000")) return -1;                              // IF  initial state is deadend    -> return -1
        if(target == "0000")   return  0;                              // IF  target is the initial state -> return  0

        int ans = 0;
        queue<string> queue{{"0000"}};                                 // Queue to perform BFS starting from the initial state

                                                                       // (BFS)
        while(!queue.empty()){                                         // While  queue is not empty
            ans++;

            for(int i=queue.size(); i>0; i--){                         //      Traverse all the states in queue
                string word = queue.front();                           //           -> Get queue front
                queue.pop();                                           //           -> Pop queue front

                for(int i=0; i<4; i++){                                //           Traverse all digits of current states
                    const char temp = word[i];
                    for(int j=-1; j<=1; j+=2) {                        //           For  j = -1  -> counterclockwise
                                                                       //                j =  1  -> clockwise

                        word[i] = ((temp - '0' + j + 10) % 10) + '0';  //                -> Rotate the digit

                        if(word == target) return ans;                 //                IF  current state = target -> return answer

                        if(!seen.count(word)) {                        //                IF  not seen current word
                            queue.push(word);                          //                    -> Push current word into queue
                            seen.insert(word);                         //                    -> Mark current word as seen
                        }
                    }

                    word[i] = temp;                                    //           Restore the original digit
                }
            }
        }

        return -1;
    }
};