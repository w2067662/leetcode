//https://leetcode.com/problems/number-of-days-between-two-dates/description/

class Solution {
private:
    vector<int> DaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; //initial days in each month
public:
    bool isLeapYear(int year){ //check is leap year or not
        if(year%4 == 0){                      //if divisable by 4
            if(year%100 == 0){                //    if divisable by 100
                if(year%400 == 0)return true; //        if divisable by 400 -> is leap year
            }                                 //        if undivisable by 400 -> not leap year
            else return true;                 //    if undivisable by 100 
                                              //        -> is leap year
        }                                     //    -> is leap year
        return false;                         //if undivisable by 4 
    }                                         //    -> not leap year

    int countDays(int year, int month, int day){ //count days from 1971-01-01
        int days=0;

        for(int i=1971;i<year;i++){           //start from 1971 to current_year-1
            if(isLeapYear(i)) days+=366;      //if is leap year, add 366
            else days+=365;                   //if is not leap year, add 365
        }
 
        if(isLeapYear(year))DaysInMonth[1] = 29; //set Feb. as 29 days
        else DaysInMonth[1] = 28;                //set Feb. as 28 days
        
        for(int i=1;i<month;i++){             //add up days from Jan. to current_month-1
            days+=DaysInMonth[i-1];
        }

        return  days+=day;                    //add up days in current_month
    }

    int daysBetweenDates(string date1, string date2) {
        //convert date1 to (year1, month1, day1)
        int year1 = (date1[0]-'0')*1000 + (date1[1]-'0')*100 + (date1[2]-'0')*10 + (date1[3]-'0');
        int month1 = (date1[5]-'0')*10 + (date1[6]-'0');
        int day1 = (date1[8]-'0')*10 + (date1[9]-'0');
        
        //convert date2 to (year2, month2, day2)
        int year2 = (date2[0]-'0')*1000 + (date2[1]-'0')*100 + (date2[2]-'0')*10 + (date2[3]-'0');
        int month2 = (date2[5]-'0')*10 + (date2[6]-'0');
        int day2 = (date2[8]-'0')*10 + (date2[9]-'0');

        //count the absolute date differences of two date
        return abs(countDays(year1, month1, day1)-countDays(year2, month2, day2));
    }
};
