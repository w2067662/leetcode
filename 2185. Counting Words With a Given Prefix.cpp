//https://leetcode.com/problems/counting-words-with-a-given-prefix/description/

class Solution {
public:
    int prefixCount(vector<string>& words, string pref) {
        int count =0;

        for(const auto word: words){
            if(word.length() >= pref.length()){
                if(pref == word.substr(0, pref.length())) count++;  //count for words with prefix [pref]
            }
        }

        return count;
    }
};
