//https://leetcode.com/problems/sort-the-people/description/

class Solution {
public:
    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
        for(int i=0; i<heights.size()-1;i++){        //bubble sort
            for(int j=0; j<heights.size()-i-1;j++){
                if(heights[j]<heights[j+1]){         //sort in descending order
                    swap( names[j], names[j+1]);
                    swap( heights[j], heights[j+1]);
                }
            }
        }

        return names;
    }
};
