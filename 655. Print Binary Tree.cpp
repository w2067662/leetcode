//https://leetcode.com/problems/print-binary-tree/description/

class Solution {
public:
    int height(TreeNode* root){ //get current node's height
        return !root ? 0 : max(height(root->left), height(root->right))+1;
    }

    void traverse(TreeNode* root, vector<vector<string>> &formatedTree, int left, int right, int level){
        if(!root) return;

        int mid = left + (right-left)/2;                            //get the middle position
        formatedTree[level][mid] = to_string(root->val);            //print the current node value to correspond position in formated tree

        traverse(root->left , formatedTree,  left, mid-1, level+1); //traverse  left subtree
        traverse(root->right, formatedTree, mid+1, right, level+1); //traverse right subtree

        return;
    }

    vector<vector<string>> printTree(TreeNode* root) {
        int depth = height(root);                                                         //get the depth of the tree

        vector<vector<string>> formatedTree (depth, vector<string> (pow(2,depth)-1, "")); //create the formated tree
                                                                                          //column: depth ; row: 2^depth-1
        traverse(root, formatedTree, 0, formatedTree[0].size()-1, 0);                     //traverse the tree to print the node value into formated tree

        return formatedTree;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
