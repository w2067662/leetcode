//https://leetcode.com/problems/count-operations-to-obtain-zero/description/

class Solution {
public:
    int countOperations(int num1, int num2) {
        int ans = 0, temp;

        while(num1 > 0 && num2 > 0){                   //deduct from another until one is 0
            temp = min(num1, num2);
            num1 > num2 ? num1 -= temp : num2 -= temp;

            ans++;                                     //count for operation times
        }

        return ans;
    }
};
