//https://leetcode.com/problems/number-of-lines-to-write-string/description/

class Solution {
public:
    vector<int> numberOfLines(vector<int>& widths, string s) {
        int line=1;                         //start from first line
        int pixel=0;                        //start from 0 pixel
        for(int i=0;i<s.length();i++){      //traverse the string
            if(pixel+widths[s[i]-'a']>100){ //if add up next letter's width will over 100
                line++;                     //then write letter's pixel to next line
                pixel=0;                    //next line start from 0 pixel
            }
            pixel+=widths[s[i]-'a'];        //add letter's width
        }

        return {line, pixel};
    }
};
