//https://leetcode.com/problems/k-th-symbol-in-grammar/description/

class Solution {
public:
    //ex:  n = 4 , k = 5
    // n = 1   0
    // n = 2   0|1
    // n = 3   01|10
    // n = 4   0110|1001
    // n = 5   01101001|10010110
    //  ...
    //------------------------------
    //   n = 3         n = 4
    //  01 | 10     0110 | 1001
    //  12   34      2 4    6 8       -> complement of N-1
    //              1 3   (5)7        -> same with N-1
    //
    //   n = 2         n = 3
    //   0 | 1        01 | 10 
    //   1   2         2    4         -> complement of N-1
    //                1   (3)         -> same with N-1
    //
    //   n = 1         n = 2
    //     0           0 | 1
    //     1              (2)         -> complement of N-1
    //
    // answer = 1
    //
    int kthGrammar(int n, int k) {
        if(n == 1) return 0;

        if(k%2 == 0) return kthGrammar(n-1, k/2) == 0 ? 1 : 0;
        else return kthGrammar(n-1, (k+1)/2) == 0 ? 0 : 1;
    }
};
