//https://leetcode.com/problems/find-maximum-number-of-string-pairs/description/

class Solution {
public:
    string reverse_string(string str){              //reverse string
        for(int i=0;i<str.length()/2;i++){
            swap(str[i], str[str.length()-1-i]);
        }
        return str;
    }

    bool compare_string(string a, string b){        //compare string
        if (a.length() != b.length())return false;
        for(int i=0;i<a.length();i++){
            if(a[i]!=b[i])return false;
        }
        return true;
    }

    int maximumNumberOfStringPairs(vector<string>& words) {
        int ans=0;
        string temp="";

        for(int i=0;i<words.size()-1;i++){
            temp = reverse_string(words[i]);              //reverse current word,
            for(int j=i+1;j<words.size();j++){            
                if(compare_string(temp, words[j]))ans++;  //and compare with following words, to find pairs
            }
        }

        return ans;
    }
};
