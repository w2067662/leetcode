//https://leetcode.com/problems/license-key-formatting/description/

class Solution {
public:
    string reverse(string s){
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    string licenseKeyFormatting(string s, int k) {
        string temp="";
        string ans="";
        for(int i=0;i<s.length();i++){                 
            if(isdigit(s[i])||isupper(s[i]))temp+=s[i];//ex: s = "5Wd4-SF-d8-4a-21" ; k = 4 
            else if(islower(s[i]))temp+=toupper(s[i]); // temp = "5WD4SFD84A21" (delete dash and make all letters upper case)
        }

        temp =reverse(temp);// temp = "5WD4SFD84A21" -> temp = "12A48DFS4DW5"

        for(int i=0, count=0;i<temp.length();i++){
            ans+=temp[i];
            count++;
            if(count==k && i!=temp.length()-1){ //for each k letters, add a dash
                ans+='-';                       //temp = "12A48DFS4DW5" -> ans = "12A4-8DFS-4DW5"(no dash after last letter)
                count =0;
            }
        }
        return reverse(ans);    //ans = "12A4-8DFS-4DW5" -> ans = "5WD4-SFD8-4A21" (reverse string)
    }
};
