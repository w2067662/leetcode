//https://leetcode.com/problems/kth-largest-element-in-an-array/description/

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end()); //sort the vector in ascending order
        return nums[nums.size()-k];     //return the Kth largest number in vector
    }
};
