//https://leetcode.com/problems/buddy-strings/description/

class Solution {
public:
    bool buddyStrings(string s, string goal) {
        if(s.length() != goal.length()) return false;        //IF  length not the same -> FALSE

        if(s == goal){                                       //IF  string are the same
            set<int> set;
            for(const auto ch: s){
                if(set.find(ch) != set.end())return true;    //    IF  contain duplicate letters -> TRUE
                set.insert(ch);
            }
            return false;                                    //    IF  contain no duplicate letters -> FALSE
        }

        vector<char> vec1, vec2;

        for(int i=0;i<s.length();i++){                       //IF  string not the same
            if(s[i] != goal[i]){
                vec1.push_back(s[i]);                        //    -> store the different letters into vec1 and vec2
                vec2.push_back(goal[i]);
            }
        }

        if(vec1.size() != 2 || vec2.size() !=2)return false; //    IF  different letters are not equal to 2 -> FALSE

        sort(vec1.begin(), vec1.end());
        sort(vec2.begin(), vec2.end());

        for(int i=0;i<vec1.size();i++){
            if(vec1[i] != vec2[i])return false;              //    IF  2 different letters of string and goal are not the same -> FALSE
        }

        return true;
    }
};
