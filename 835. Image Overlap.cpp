//https://leetcode.com/problems/image-overlap/description/

class Solution {
public:
    int largestOverlap(vector<vector<int>>& img1, vector<vector<int>>& img2) {
        vector<pair<int, int>> imgBits1, imgBits2;
        map<pair<int, int>, int> map;
        int maxOverlap = 0;

        for(int i=0; i<img1.size();i++){
            for(int j=0; j<img1[i].size(); j++){
                if(img1[i][j] == 1) imgBits1.push_back({i, j});         // Store all the bit 1's position from img1 to imgBits1
                if(img2[i][j] == 1) imgBits2.push_back({i, j});         // Store all the bit 1's position from img2 to imgBits2
            }
        }

        for(const auto bit1: imgBits1){                                 // For all the bit 1's position of img1
            for(const auto bit2: imgBits2){                             //      For all the bit 1's position of img2
                int shiftX = bit1.first-bit2.first;                     //          -> Calculate the X shifting
                int shiftY = bit1.second-bit2.second;                   //          -> Calculate the Y shifting

                map[{shiftX, shiftY}]++;                                //          -> Add up the same XY shifting's frequency
                maxOverlap = max(maxOverlap, map[{shiftX, shiftY}]);    //          -> Store the max overlap from max XY shifting's frequency
            }
        }

        return maxOverlap;
    }
};
