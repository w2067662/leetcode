//https://leetcode.com/problems/reverse-odd-levels-of-binary-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, int level, vector<vector<TreeNode*>> &levelList){ //traverse the tree and store nodes into level list by level
        if(!root) return;

        if(level >= levelList.size()) levelList.push_back({root});  //IF   no list with current level -> create a list and push current node in it
        else levelList[level].push_back(root);                      //ELSE                            -> push current node into list with current level

        traverse(root->left , level+1, levelList);                  //traverse left  subtree
        traverse(root->right, level+1, levelList);                  //traverse right subtree

        return;
    }

    void reverse(vector<TreeNode*> &list){     //reverse the value of all the nodes in list
        for(int i=0; i<list.size()/2; i++){
            swap(list[i]->val, list[list.size()-i-1]->val);
        }
    }

    TreeNode* reverseOddLevels(TreeNode* root) {
        vector<vector<TreeNode*>> levelList;

        traverse(root, 0, levelList);          //traverse the tree and store the nodes into level list
        
        for(int i=0; i<levelList.size(); i++){ //traverse the list in level list
            if(i%2 != 0){                      //IF   current list's index is odd
                reverse(levelList[i]);         //     -> reverse current list's node value
            }
        }

        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
