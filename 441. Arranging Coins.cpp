//https://leetcode.com/problems/arranging-coins/description/

class Solution {
public:
    int arrangeCoins(int n) {
        long long ans=1;    //here should use long long for handling overflow
        for(long long i=1, sum=0;sum<=n;i++){
            sum+=i; //sum will add like triange, when sum>n the empty space(sum) if more than coins
                    //1 -> 1+2 =3 -> 3+3 =6 -> 6+4 =10...
            ans=i;  //store the levels
        }
        return ans-1;   //the empty space will be 1 level more than coins
    }
};
