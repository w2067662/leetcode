//https://leetcode.com/problems/reverse-only-letters/description/

class Solution {
public:
    bool isletter(char ch){ //check is letter or not
        return isupper(ch) || islower(ch);
    }

    string reverseOnlyLetters(string s) {
        for(int left=0, right=s.length()-1; left<right; ){ //if left >= right, end the loop
            if( isletter(s[left]) && isletter(s[right]) ){ //if left and right both are letter 
                    swap(s[left], s[right]);               //   swap left and right
                    left++;                                //   goto next left
                    right--;                               //   goto next right
            } else if( !isletter(s[left]) && isletter(s[right]) ){ //if only left is not letter
                left++;                                            //   goto next left
            } else if( isletter(s[left]) && !isletter(s[right]) ){ //if only right is not letter
                right--;                                           //   goto next right
            } else {                                               //if both are not letter
                left++;                                            //   goto next left
                right--;                                           //   goto next right
            }
        }
        return s;
    }
};
