//https://leetcode.com/problems/apply-discount-every-n-orders/description/

class Cashier {
private:
    int N, curr;
    int Discount;
    map<int, int> Prices;

public:
    Cashier(int n, int discount, vector<int>& products, vector<int>& prices) { //initialize cashier
        this->curr = 0;
        this->N = n;
        this->Discount = discount;

        for(int i=0;i<products.size();i++){
            Prices[products[i]] = prices[i];
        }
    }
    
    double getBill(vector<int> product, vector<int> amount) {
        double totalPrice = 0;

        for(int i=0; i<product.size(); i++){
            totalPrice += (double)this->Prices[product[i]] * amount[i]; //sum up prices
        }

        this->curr++;
        if(this->curr == N){                                            //IF  current customer is N customer
            this->curr = 0;                                             //    -> reset current as 0
            totalPrice = totalPrice * (100 - this->Discount) / 100;     //    -> calculate discount
        }

        return totalPrice;
    }
};

/**
 * Your Cashier object will be instantiated and called as such:
 * Cashier* obj = new Cashier(n, discount, products, prices);
 * double param_1 = obj->getBill(product,amount);
 */
