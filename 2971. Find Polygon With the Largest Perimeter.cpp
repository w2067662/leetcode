//https://leetcode.com/problems/find-polygon-with-the-largest-perimeter/description/

class Solution {
public:
    //ex:  nums = [1,12,1,2,5,50,3]
    //
    //    nums' = [1,1,2,3, 5,12,50] (sorted)
    //  edgeSum = [1,2,4,7,12,24,74]
    //                         *  ^           edge sum = 24 ; current edge = 50 ; 24 < 50  (not valid)
    //                      *  ^              edge sum = 12 ; current edge = 12 ; 12 = 12  (not valid)
    //                   *  ^                 edge sum =  7 ; current edge =  5 ;  7 >  5  (valid)
    //
    //                                        answer = 7+5 = 12
    //
    long long largestPerimeter(vector<int>& nums) {
        vector<long long> edgeSum (nums.size(), 0);

        sort(nums.begin(), nums.end());                               // Sort the edges in ascending order

        for(int i=0; i<nums.size(); i++){
            edgeSum[i] = (i == 0) ? nums[i] : edgeSum[i-1] + nums[i]; // Calculate the edgeSum from left to right
        }

        for(int i=nums.size()-1; i>=2; i--){                          // For comparing edgeSum and current edge from right to left
            if(edgeSum[i-1] > nums[i]) return edgeSum[i-1] + nums[i]; //    IF edgeSum > current edge
                                                                      //       -> return the sum of all edges of the polygon
        }

        return -1;
    }
};
