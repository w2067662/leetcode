//https://leetcode.com/problems/k-th-smallest-prime-fraction/description/

class Solution {
private:
    class Fraction{
    public:
        int n, d;    // Numerator, Denominator
        double val;
        Fraction(int n, int d):n(n), d(d), val((double)n/(double)d){}
        vector<int> toVector(){ return {n, d}; }
    };

public:
    vector<int> kthSmallestPrimeFraction(vector<int>& arr, int k) {
        vector<Fraction> vec;

        for(int i=0; i<arr.size()-1; i++){                        // Traverse all the possible combinations
            for(int j=i+1; j<arr.size(); j++){
                vec.push_back(Fraction(arr[i], arr[j]));          //    Push all the fractions into vector
            }
        }

        sort(vec.begin(), vec.end(),
            [&](const Fraction& a, const Fraction& b){            // Sort the fractions by value in ascending order
                return a.val < b.val;
            }
        );

        return vec[k-1].toVector();                               // Return the Kth smallest prime fraction
    }
};
