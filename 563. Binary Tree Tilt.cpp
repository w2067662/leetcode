//https://leetcode.com/problems/binary-tree-tilt/

class Solution {
public:
    int treeSum(TreeNode* root) {
        if(!root)return 0;
        return treeSum(root->left)+treeSum(root->right)+root->val;  //if root is not NULL, return left sum + right sum + root->val
    }
    int findTilt(TreeNode* root) {
        if(!root)return 0;
        //if root is not NULL, return left tilt + right tilt + current root's tilt
        return findTilt(root->left) + findTilt(root->right) + abs(treeSum(root->left)-treeSum(root->right));
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
