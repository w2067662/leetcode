//https://leetcode.com/problems/divide-array-in-sets-of-k-consecutive-numbers/description/

class Solution {
public:
    // ex1:  nums = [1,2,3,6,2,3,4,7,8] ; k = 3 

    //              1 2 3 4 5 6 7 8
    //        map = 1 2 2 1 0 1 1 1   (remove [1,2,3])
    //     -> map = 0 1 1 1 0 1 1 1   (remove [2,3,4])
    //     -> map = 0 0 0 0 0 1 1 1   (remove [6,7,8])
    //     -> map = 0 0 0 0 0 0 0 0
    //                                => TRUE

    // ex2:  nums = [1,2,3,4,5] ; k = 4

    //              1 2 3 4 5
    //        map = 1 1 1 1 1         (remove [1,2,3])
    //     -> map = 0 0 0 1 1
    //                                => FALSE
    bool isPossibleDivide(vector<int>& nums, int k) {
        if(nums.size()%k != 0){              // IF  amount of nums numbers is unable to split into K groups
            return false;                    //     -> FALSE
        }

        map<int, int> map;

        for(const auto& num: nums){
            map[num]++;                      // Store the frequency of each number
        }

        for(auto& it: map){                  // Traverse the numbers within map in ascending order
            if(it.second < 0){               //      IF current number's frequency is negative
                return false;                //         -> FALSE
            }

            int deduct = map[it.first];      //      Calculate the frequency of first number in current group
            map[it.first] -= deduct;         //      Deduct the frequency of current number

            for(int i=1; i<k; i++){          //      For  i from 1 to k-1
                map[it.first+i] -= deduct;   //           -> deduct the frequency of current+i number

                if(map[it.first+i] == 0){    //           IF current+1 number has frequency = 0
                    map.erase(it.first+i);   //              -> erase it from map
                }
            }
        }

        return true;                         // Possible to rearrange the nums -> TRUE
    }
};
