//https://leetcode.com/problems/first-missing-positive/description/

class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        set<int> set;
        int curr = 1;

        for(const auto& num: nums){        // For all numbers
            set.insert(num);               //    Insert into set
        }

        for(const auto& num: set){         // For all numbers within set
            if(num <= 0) continue;         //    IF   non-positive -> continue
            else if (num == curr) curr++;  //    IF   num = curr   -> curr + 1
            else return curr;              //    ELSE              -> found the smallest missing positive
        }

        return curr;                       // Current number is the smallest missing positive
    }
};