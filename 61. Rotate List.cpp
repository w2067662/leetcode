//https://leetcode.com/problems/rotate-list/description/

class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        //check if head is NULL
        if(!head)return NULL;
        //if k is 0, or head only have one element
        if(k==0||!head->next)return head; 
        
        ListNode *new_head=head;    //new head for the rotate list
        ListNode *fast=head->next;  //find last element
        ListNode *slow=head;        //find second last element
        int len=1;                  //if head is not NULL, then there already exist one element
        int rotate_times=0;         //for the case { head=[0,1]; k=10^100000+1 }
        
        //set fast and slow at right position
        while(fast->next){
            len++;                  //count for total length for first time
            fast=fast->next;
            slow=slow->next;
        }
        len+=1;                     //1+slow_gothrough_times+1=total length
                                    //[1,2,3,4,5] -> 1 + [slow=slow->next]*3 + 1=len
        
        //rotate times will smaller than [len]
        //for the case { head=[0,1]; k=10^100000+1 }
        rotate_times = k%len;       //(10^100000+1)%2=1
        
        if(rotate_times==0)return new_head;//no need to rotate if 0
        
        while(rotate_times>0){
            rotate_times--;
            
            slow->next=NULL;        //[1,2,3,4(slow),NULL];[5(fast)]
            fast->next=new_head;    //[5(fast),1(new_head),2,3,4(slow),NULL]
            new_head=fast;          //[5(new_head)(fast),1,2,3,4(slow),NULL]
            
            //set fast as new_head->next
            //set slow as new_head
            //[5(new_head)(slow),1(fast),2,3,4,NULL]
            fast=new_head->next;
            slow=new_head;
            
            //reset fast and slow positions
            while(fast->next){
                fast=fast->next;
                slow=slow->next;
            }
            //[5(new_head),1,2,3(slow),4(fast)]
            //next rotate
        }
        
        return new_head;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
