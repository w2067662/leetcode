//https://leetcode.com/problems/unique-number-of-occurrences/description/

class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) {
        map<int, int> count;
        unordered_map<int, vector<int>> occurrences;

        for(int i=0;i<arr.size();i++){    //count the appear times of numbers in array
            count[arr[i]]++;
        }

        for(const auto it: count){        //store into occurrences (key: appear times; value: vector of numbers with same appear times )
            occurrences[it.second].push_back(it.first);
        }

        for(const auto it: occurrences){  //check all occurrences is unique
            if(it.second.size()>1)return false; //more than 1 numbers have same appear times -> occurrence is not unique -> FALSE
        }

        return true; //all occurrences is unique -> TRUE
    }
};
