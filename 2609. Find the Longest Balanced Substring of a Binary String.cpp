//https://leetcode.com/problems/find-the-longest-balanced-substring-of-a-binary-string/description/

class Solution {
public:
    //ex s = "010011000111"
    //vec = ["0","1","00","11","000","111"]
    //        ^   ^                           -> balancedSize = 1 ; answer = 1
    //            ^   ^                       -> (X)
    //                ^    ^                  -> balancedSize = 2 ; answer = 2
    //                     ^    ^             -> (X)
    //                          ^     ^       -> balancedSize = 3 ; answer = 3
    //
    int findTheLongestBalancedSubstring(string s) {
        vector<string> vec;
        string temp = "";
        int ans = 0;

        temp += s[0];

        for(int i=1;i<s.length();i++){                                  //split string by '0' and '1' and store into vector
            if(s[i] != s[i-1]){
                vec.push_back(temp);
                temp = s[i];
            }else temp += s[i];
        }
        if(temp != "") vec.push_back(temp);

        for(int i=1, balancedSize=0;i< vec.size();i++){
            if(vec[i][0] == '1' && vec[i-1][0] == '0'){                 //if left substring consist of '1' AND right substring consist of '0'
                balancedSize = min(vec[i].length(), vec[i-1].length()); //calculate balancedSize
            }
            ans = max(balancedSize*2, ans);                             //store the max balancedSize
        }

        return ans;
    }
};
