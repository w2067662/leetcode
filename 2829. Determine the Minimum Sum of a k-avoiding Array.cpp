//https://leetcode.com/problems/determine-the-minimum-sum-of-a-k-avoiding-array/description/

class Solution {
public:
    int minimumSum(int n, int k) {
        set<int> set;
        int num = 1, sum = 0;

        for(int i=0; i<n; i++, num++){
            if(set.find(k-num) != set.end()) i--;   // IF   exist pair (x, num) => x + num = k , (x: any number in set, num: current number )
                                                    //      -> skip current number
            else {                                  // ELSE 
                sum += num;                         //      -> add up current number to sum
                set.insert(num);                    //      -> insert current number into set
            }
        }

        return sum;
    }
};
