//https://leetcode.com/problems/smallest-number-in-infinite-set/description/

class SmallestInfiniteSet {
private:
    priority_queue<int, vector<int>, greater<int>> pq; // Create priority queue sorted in ascending order
    set<int> set;

public:
    SmallestInfiniteSet() {
        for(int i=1; i<=1000; i++){ // For i from 1 to 1000
            pq.push(i);             //      Push i into priority queue
            set.insert(i);          //      Mark i as 'exist'
        }
    }
    
    int popSmallest() {
        int top = pq.top();   // Get the top element (smallest number) from priority queue

        pq.pop();             // Remove the smallest number from priority queue
        set.erase(top);       // Mark the smallest number as 'not exist'

        return top;           // Return the smallest number
    }
    
    void addBack(int num) {
        if(set.find(num) == set.end()){ // IF current number not exist in set
            pq.push(num);               //    -> Push into priority queue
            set.insert(num);            //    -> Mark current number as 'exist'
        }
    }
};

/**
 * Your SmallestInfiniteSet object will be instantiated and called as such:
 * SmallestInfiniteSet* obj = new SmallestInfiniteSet();
 * int param_1 = obj->popSmallest();
 * obj->addBack(num);
 */
