//https://leetcode.com/problems/group-anagrams/description/

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        map<string, vector<string>> map;
        vector<vector<string>> ans;

        for(const auto str: strs){
            string temp = str;              // Copy string as temp

            sort(temp.begin(), temp.end()); // Sort the temp's chars in alphabetical order

            map[temp].push_back(str);       // Grouping anagrams
        }

        for(const auto it: map){
            ans.push_back(it.second);       // Push the anagram groups into answer
        }

        return ans;
    }
};
