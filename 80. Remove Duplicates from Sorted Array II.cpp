//https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/description/

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        map<int, int> map;  // map = {[number]: [appear times]}
        int index =0;

        for(int i=0; i<nums.size();i++){
            map[nums[i]]++; //store appear times of current number into map
        }

        for(const auto it:map){
            if(it.second > 2){                                          //if current number appear more than twice,
                for(int i=0; i<2; i++) nums[index++] = it.first;        //store into input vector for at most twice
            } else {                                                    //if current number appear not more than twice,
                for(int i=0; i<it.second; i++) nums[index++] = it.first;//store into input vector for 0 OR 1 OR 2 times
            }
        }

        return index;
    }
};
