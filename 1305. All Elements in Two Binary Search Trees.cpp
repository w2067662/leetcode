//https://leetcode.com/problems/all-elements-in-two-binary-search-trees/description/

class Solution {
public:
    void traverse(TreeNode* root, vector<int> &vec){
        if(!root) return;

        vec.push_back(root->val);     //push current node value into vector

        traverse(root->left , vec);   //traverse  left subtree
        traverse(root->right, vec);   //traverse right subtree
        return;
    }

    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> vec;

        traverse(root1, vec);         //get all elements from tree1
        traverse(root2, vec);         //get all elements from tree2

        sort(vec.begin(), vec.end()); //sort the vector in ascending order

        return vec;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
