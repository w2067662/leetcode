//https://leetcode.com/problems/split-strings-by-separator/description/

class Solution {
private:
    vector<string> ans;
public:
    void splitWords(string& word, char ch){ //split word by char
        string temp = "";
        for(int i=0;i<word.length();i++){
            if(word[i]==ch && temp != ""){
                ans.push_back(temp);
                temp = "";
            } else if(word[i]!=ch) temp += word[i];
        }
        if(temp != "")ans.push_back(temp);
        return;
    }

    vector<string> splitWordsBySeparator(vector<string>& words, char separator) {
        for(int i=0;i<words.size();i++){
            splitWords(words[i], separator); //split all the words in vector
        }
        return ans;
    }
};
