//https://leetcode.com/problems/check-if-array-is-sorted-and-rotated/description/

class Solution {
public:
    bool check(vector<int>& nums) {
        for(int i=1; i<nums.size(); i++){
            if(nums[i-1] > nums[i]){                                         //if not in ascending order -> check for rotate
                if(nums[i] > nums[0])return false;                           //        if not rotate -> FALSE

                for(int j=i+1; j<nums.size(); j++){
                    if(nums[j-1] > nums[j] || nums[j] > nums[0])return false;//        if not rotate -> FALSE
                }
            }
        }

        return true;
    }
};
