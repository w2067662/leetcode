//https://leetcode.com/problems/construct-quad-tree/description/

class Solution {
public:
    bool isLeafNode(vector<vector<int>> &grid){ //check current grid can be a leaf node
        set<int> set;
        for(int i=0;i<grid.size();i++){
            for(int j=0;j<grid.size();j++){
                set.insert(grid[i][j]);
            }
        }
        return set.size() == 1;
    }

    int leafNodeValue(const vector<vector<int>> &grid){ //get the leaf node's value
        return grid[0][0];
    }

    int size(vector<vector<int>> &grid){    //check the grid's size
        return grid.size()*grid[0].size();
    }

    vector<vector<int>> topLeftGrid(vector<vector<int>> grid){ //split the topLeftGrid subGrid
        vector<vector<int>> split;
        for(int i=0; i<grid.size()/2; i++){
            vector<int> temp;
            for(int j=0; j<grid.size()/2; j++){
                temp.push_back(grid[i][j]);
            }
            split.push_back(temp);
        }
        return split;
    }

    vector<vector<int>> topRightGrid(vector<vector<int>> grid){ //split the topRight subGrid
        vector<vector<int>> split;
        for(int i=0; i<grid.size()/2; i++){
            vector<int> temp;
            for(int j=grid.size()/2; j<grid.size(); j++){
                temp.push_back(grid[i][j]);
            }
            split.push_back(temp);
        }
        return split;
    }

    vector<vector<int>> bottomLeftGrid(vector<vector<int>> grid){ //split the bottomLeft subGrid
        vector<vector<int>> split;
        for(int i=grid.size()/2; i<grid.size(); i++){
            vector<int> temp;
            for(int j=0;j<grid.size()/2;j++){
                temp.push_back(grid[i][j]);
            }
            split.push_back(temp);
        }
        return split;
    }

    vector<vector<int>> bottomRightGrid(vector<vector<int>> grid){ //split the bottomRight subGrid
        vector<vector<int>> split;
        for(int i=grid.size()/2;i<grid.size();i++){
            vector<int> temp;
            for(int j=grid.size()/2; j<grid.size(); j++){
                temp.push_back(grid[i][j]);
            }
            split.push_back(temp);
        }
        return split;
    }

    Node* construct(vector<vector<int>> grid) {
        Node* node = NULL;

        if(isLeafNode(grid)){                    //IF   current grid is leaf node
            node = new Node(                     //     -> create leaf node with
                leafNodeValue(grid),             //        -> leaf node's value
                true                             //        -> is leaf node 
            );
        } else {                                 //ELSE
            node = new Node(                     //     -> create node with
                1,                               //        -> node value = 1
                false,                           //        -> is not leaf node
                construct(topLeftGrid(grid)),    //        -> topLeft     subtree
                construct(topRightGrid(grid)),   //        -> topRight    subtree
                construct(bottomLeftGrid(grid)), //        -> buttomLeft  subtree
                construct(bottomRightGrid(grid)) //        -> buttomRight subtree
            );
        }

        return node;
    }
};

/*
// Definition for a QuadTree node.
class Node {
public:
    bool val;
    bool isLeaf;
    Node* topLeft;
    Node* topRight;
    Node* bottomLeft;
    Node* bottomRight;
    
    Node() {
        val = false;
        isLeaf = false;
        topLeft = NULL;
        topRight = NULL;
        bottomLeft = NULL;
        bottomRight = NULL;
    }
    
    Node(bool _val, bool _isLeaf) {
        val = _val;
        isLeaf = _isLeaf;
        topLeft = NULL;
        topRight = NULL;
        bottomLeft = NULL;
        bottomRight = NULL;
    }
    
    Node(bool _val, bool _isLeaf, Node* _topLeft, Node* _topRight, Node* _bottomLeft, Node* _bottomRight) {
        val = _val;
        isLeaf = _isLeaf;
        topLeft = _topLeft;
        topRight = _topRight;
        bottomLeft = _bottomLeft;
        bottomRight = _bottomRight;
    }
};
*/
