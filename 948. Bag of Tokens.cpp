//https://leetcode.com/problems/bag-of-tokens/description/

class Solution {
public:
    int bagOfTokensScore(vector<int>& tokens, int power) {
        int score = 0, maxScore = 0;

        sort(tokens.begin(), tokens.end());                          // Sort the tokens in ascending order

        for(int left = 0, right = tokens.size()-1; left <= right; ){ // For left = 0, right = token.size-1, left <= right
            if(power >= tokens[left]){                               //     IF power is at least tokens[left]
                power -= tokens[left];                               //        -> deduct power by tokens[left]
                score++;                                             //        -> score+1
                left++;                                              //        -> goto left+1 
                
                maxScore = max(maxScore, score);                     //        -> store the max score of the whole proccess
            } 
            else if (score >= 1){                                    //     IF score is at least 1
                power += tokens[right];                              //        -> add power by tokens[right]
                score--;                                             //        -> score-1
                right--;                                             //        -> goto right-1
            } 
            else break;                                              //     IF no next possible steps to exchange between power and score
                                                                     //        -> end the loop
        }

        return maxScore;
    }
};
