//https://leetcode.com/problems/all-possible-full-binary-trees/description/

class Solution {
public:
    vector<TreeNode*> allPossibleFBT(int n) {
        if(n < 0 || n%2 == 0) return {};                     // IF n < 0 OR n is even -> no possible answer (empty list)

        if(n == 1) return {new TreeNode()};                  // IF n = 1 -> return list with a single TreeNode

        vector<TreeNode*> ans;

        for(int i=1; i<n; i+=2){                             // Left subtree's node from 1 to n-1, decrease by 2
            vector<TreeNode*> left  = allPossibleFBT(i);     // Recursion to get left  subtree's all possible FBT
            vector<TreeNode*> right = allPossibleFBT(n-i-1); // Recursion to get right subtree's all possible FBT

            for(auto l: left){                               // For all possible left subtree
                for(auto r: right){                          //     For all possible right subtree
                    ans.push_back(new TreeNode(0, l, r));    //          -> construct a new tree with left and right subtree
                }
            }
        }

        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
