#https://leetcode.com/problems/duplicate-emails/

# Write your MySQL query statement below
SELECT email AS Email 
FROM Person 
GROUP BY email  # display in order by email column
HAVING count(email)>1 # having duplicate emails
