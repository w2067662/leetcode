//https://leetcode.com/problems/max-increase-to-keep-city-skyline/description/

class Solution {
public:
    int maxIncreaseKeepingSkyline(vector<vector<int>>& grid) {
        vector<int> rowMax, colMax;
        int increase = 0;

        for(int i=0; i<grid.size(); i++){                         // For all the rows
            int maxNum = 0;
            for(int j=0; j<grid[i].size(); j++){                  
                maxNum = max(maxNum, grid[i][j]);                 //    Store the largest number into rowMax
            }
            rowMax.push_back(maxNum);
        }

        for(int j=0; j<grid[0].size(); j++){                      // For all the columns
            int maxNum = 0;
            for(int i=0; i<grid.size(); i++){
                maxNum = max(maxNum, grid[i][j]);                 //    Store the largest number into colMax
            }
            colMax.push_back(maxNum);
        }

        for(int i=0; i<grid.size(); i++){                         // Traverse the grid
            for(int j=0; j<grid[i].size(); j++){
                increase += min(rowMax[i], colMax[j])-grid[i][j]; //    It is possible to increase from grid[i][j] to min(rowMax[i], colMax[j])
            }
        }

        return increase;                                          // Return the max increase
    }
};
