//https://leetcode.com/problems/add-to-array-form-of-integer/description/

class Solution {
public:
    void reverse(vector<int>& vec){             //reverse vector
        for(int i=0;i<vec.size()/2;i++){
            swap(vec[i], vec[vec.size()-i-1]);
        }
    }

    vector<int> addToArrayForm(vector<int>& num, int k) {
        vector<int> vec;
        
        while(k>0){                             //convert integar k to vector<int>
            vec.push_back(k%10);
            k/=10;
        }

        reverse(num);                           //reverse vector num

        for(int i=0;i<vec.size();i++){          //add up vector num and vector k
            if(i<num.size()) num[i] += vec[i];
            else num.push_back(vec[i]);
        }

        int add = 0;
        for(int i=0;i<num.size();i++){          //check for >=10 integar in vector
            num[i] = num[i] + add;              //add the carried 1 to higher digit
            add = num[i]/10;
            num[i]%=10;
        }
        if(add) num.push_back(1);

        reverse(num);                           //reverse the vector num

        return num;
    }
};
