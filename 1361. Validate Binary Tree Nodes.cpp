//https://leetcode.com/problems/validate-binary-tree-nodes/description/

class Solution {
private:
    struct TreeNode {   //define tree node
       int val;
       TreeNode *left;
       TreeNode *right;
       TreeNode() : val(0), left(nullptr), right(nullptr) {}
       TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
       TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
   };
public:
    bool isValid(TreeNode* root, set<int> &visited){   //check the tree is valid
        if(!root) return true;

        if(visited.find(root->val) != visited.end()) return false;            //IF   found visited node -> there is cycle in the tree -> FALSE
        else visited.insert(root->val);                                       //ELSE -> mark current node as visited

        return isValid(root->left, visited) && isValid(root->right, visited); //check left subtree and right subtree is valid
    }

    bool validateBinaryTreeNodes(int n, vector<int>& leftChild, vector<int>& rightChild) {
        vector<TreeNode*> nodes;
        vector<bool> notRoots(n, false);
        set<int> visited;

        for(int i=0; i<n; i++){
            nodes.push_back(new TreeNode(i));           //create new nodes with value 0 to N-1
        }

        for(int i=0; i<n; i++){
            if(leftChild[i] != -1){                     //IF   current node's left child is not -1 (NULL)
                nodes[i]->left  = nodes[leftChild[i]];  //     -> point to left child
                notRoots[leftChild[i]] = true;          //     -> set left child as not root
            } 
            if(rightChild[i] != -1){                    //IF   current node's right child is not -1 (NULL)
                nodes[i]->right = nodes[rightChild[i]]; //     -> point to right child
                notRoots[rightChild[i]] = true;         //     -> set right child as not root
            }
        }

        for(int i=0; i<notRoots.size();i++){            //traverse all the nodes in notRoots
            if(!notRoots[i]){                           //IF   current node is possible to be the tree's root
                visited.clear();
                if(isValid(nodes[i], visited) && visited.size() == n) return true;// -> check the tree start from current node as root is valid AND 
            }                                                                     //    visited nodes are equals to N
        }

        return false;
    }
};
