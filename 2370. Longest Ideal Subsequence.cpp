//https://leetcode.com/problems/longest-ideal-subsequence/description/

class Solution {
public:
    //ex:   s = "acfgbd"  ;  k = 2
    //           ^                  "a"    ; map = {'a':1}
    //            ^                 "ac"   ; map = {'a':1,'c':2}
    //             ^                "f"    ; map = {'a':1,'c':2,'f':1}
    //              ^               "fg"   ; map = {'a':1,'c':2,'f':1,'g':2}
    //               ^              "acb"  ; map = {'a':1,'b':3,'c':2,'f':1,'g':2}
    //                ^             "acbd" ; map = {'a':1,'b':3,'c':2,'d':4,'f':1,'g':2}
    //                                                                    ^
    //                                                                 max element
    int longestIdealString(string s, int k) {
        vector<int> map(26, 0);

        for(const auto& ch: s){                             // Traverse the chars within string
            int maxLen = 0, x = ch-'a';

            for(int i=max(x-k, 0); i<=min(x+k, 25); i++){   //      For  current char within the range [-K, +K]
                maxLen = max(maxLen, map[i]);               //           -> Store the max length of reachable char from map
            }

            map[x] = maxLen+1;                              //      Store longest ideal string to map
        }

        return *max_element(map.begin(), map.end());        // Return the max element within map as answer
    }
};