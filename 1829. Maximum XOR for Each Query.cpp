//https://leetcode.com/problems/maximum-xor-for-each-query/description/

class Solution {
public:
    //ex:  Input: nums = [2,3,4,7], maximumBit = 3  
    //                                              => target = pow(2,3)-1 = 7
    //                              
    //                    ^         target = 7 ; temp = 2       ; result = 7^2 = 5 ; answer = [5]
    //                      ^       target = 7 ; temp = 2^3 = 1 ; result = 7^1 = 6 ; answer = [5,6]
    //                        ^     target = 7 ; temp = 1^4 = 5 ; result = 7^5 = 2 ; answer = [5,6,2]
    //                          ^   target = 7 ; temp = 5^7 = 2 ; result = 7^2 = 5 ; answer = [5,6,2,5]
    vector<int> getMaximumXor(vector<int>& nums, int maximumBit) {
        vector<int> ans;
        int target = pow(2, maximumBit)-1, result, temp;  // Calculate target with max bit

        for(int i=0; i<nums.size(); i++){                 // For numbers in vector
            temp = (i == 0) ? nums[0] : temp ^ nums[i];   //    Calculate temp value
            result = target ^ temp;                       //    Target XOR the temp value to get result

            ans.push_back(result);                        //    Push into answer
        }

        reverse(ans.begin(), ans.end());                  // Reverse the answer list

        return ans;
    }
};
