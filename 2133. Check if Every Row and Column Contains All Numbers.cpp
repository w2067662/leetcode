//https://leetcode.com/problems/check-if-every-row-and-column-contains-all-numbers/description/

class Solution {
public:
    void reset(vector<bool>& v){    //reset vector
        for(auto element: v) element = false;
    }

    bool valid(vector<bool>& v){    //check vector is valid
        for(auto element: v){
            if(!element)return false;
        }
        return true;
    }

    bool checkValid(vector<vector<int>>& matrix) {
        vector<bool> check (matrix.size(), false);

        for(int i=0;i<matrix.size();i++){       //check for rows
            reset(check);
            for(int j=0;j<matrix[i].size();j++) check[matrix[i][j]-1] = true;
            if(!valid(check))return false;
        }

        for(int j=0;j<matrix[0].size();j++){    //check for columns
            reset(check);
            for(int i=0;i<matrix.size();i++) check[matrix[i][j]-1] = true;
            if(!valid(check))return false;
        }

        return true;
    }
};
