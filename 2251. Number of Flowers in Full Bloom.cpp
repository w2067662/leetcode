//https://leetcode.com/problems/number-of-flowers-in-full-bloom/description/

class Solution {
public:
    //ex: flowers = [[1,6],[3,7],[9,12],[4,13]] ; people = [2,3,7,11]
    //
    //                left = [1,3,4,9] ; right = [6,7,12,13]                                started - ended
    //time = 2  ->              ^                 ^             started = 1 ; ended = 0  ->        1
    //time = 3  ->                ^               ^             started = 2 ; ended = 0  ->        2
    //time = 7  ->                  ^               ^           started = 3 ; ended = 1  ->        2
    //time = 11 ->                    ^                ^        started = 4 ; ended = 2  ->        2
    //                         (no upper bound)
    //
    //                                                  left index   |    right index
    // ṽ  v  v  v  v  ṽ                                     0                  0
    //       ṽ  v  v  v  ṽ                                  1                  1
    //                         ṽ   v   v   ṽ                3                  2
    //          ṽ  v  v  v  v  v   v   v   v   ṽ            2                  3
    // 1  2  3  4  5  6  7  8  9  10  11  12  13
    //    o                                                 1                  0
    //       o                                              2                  0
    //                   o                                  3                  1
    //                                 o                    4                  2
    //                                               (no upper bound)
    vector<int> fullBloomFlowers(vector<vector<int>>& flowers, vector<int>& people) {
        vector<int> left, right;
        vector<int> ans;

        for (const auto pair : flowers){
            left.push_back(pair[0]);      //push range left  into vector left
            right.push_back(pair[1]);     //push range right into vector right
        }
            
        sort(left.begin() , left.end() ); //sort the vector left  in ascending order 
        sort(right.begin(), right.end()); //sort the vector right in ascending order 
        
        for (const auto time : people) {
            int started = upper_bound(left.begin() , left.end() , time) - left.begin() ;
            int ended   = lower_bound(right.begin(), right.end(), time) - right.begin();
            ans.push_back(started - ended); //count the flowers with current time within bloom range
        }

        return ans;
    }
};
