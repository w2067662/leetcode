//https://leetcode.com/problems/prime-in-diagonal/description/

class Solution {
public:
    bool isPrime(int n){    //check for prime
        if(n<=1)return false;
        for (int i = 2; i <= n/2; ++i) {
            if (n % i == 0)return false;
        }
        return true;
    }

    int diagonalPrime(vector<vector<int>>& nums) {
        vector<int> vec;

        for(int i=0;i<nums.size();i++){
            for(int j=0;j<nums[i].size();j++){
                if(i == j || i+j == nums.size()-1) vec.push_back(nums[i][j]); //store the diagonal number into vector
            }
        }

        sort(vec.begin(), vec.end());          //sort the vector in ascending order

        for(int i=vec.size()-1;i>=0;i--){      //traverse number from the largest to the smallest in vector
            if(isPrime(vec[i]))return vec[i];  //return the max prime number
        }

        return 0;
    }
};
