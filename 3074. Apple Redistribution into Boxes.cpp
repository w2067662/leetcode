//https://leetcode.com/problems/apple-redistribution-into-boxes/description/

class Solution {
public:
    int minimumBoxes(vector<int>& apple, vector<int>& capacities) {
        int sum = 0, count = 0;

        sort(capacities.begin(), capacities.end(), [&](const int& a, const int& b){ // Sort capacities in descending order
            return a > b;
        });

        for(const auto& count: apple){                                              // Sum up amount of apples
            sum += count;
        }

        for(const auto& capacity: capacities){                                      // For all capacities 
            if(sum > 0){                                                            //      IF   exist apples that are not distribute into box
                sum -= capacity;                                                    //           -> Deducted by current box's capacity
                count++;                                                            //           -> Count + 1
            } 
            else break;                                                             //      ELSE 
                                                                                    //           -> Break
        }

        return count;
    }
};