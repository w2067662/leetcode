//https://leetcode.com/problems/rings-and-rods/description/

class Solution {
public:
    typedef struct{bool R; bool G; bool B;} RGB; //define RGB as type

    int countPoints(string rings) {
        map<int, RGB> map;  //map = {[Nth rod]: RGB{[Contain Red], [Contain Green], [Contain Blue]}}
        int ans=0;

        for(int i=0;i<rings.length();i+=2){
            switch(rings[i]){                                    //store RGB into map
                case 'R': map[rings[i+1]-'0'].R = true;
                    break;
                case 'G': map[rings[i+1]-'0'].G = true;
                    break;
                case 'B': map[rings[i+1]-'0'].B = true;
                    break;
            }
        }

        for(const auto it:map){
            if(it.second.R && it.second.G && it.second.B) ans++; //check contain Red AND Green AND Blue rings
        }

        return ans;
    }
};
