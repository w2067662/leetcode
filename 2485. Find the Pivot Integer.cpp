//https://leetcode.com/problems/find-the-pivot-integer/description/

class Solution {
public:
    int pivotInteger(int n) {
        int leftSum = -1, rightSum = -1;

        for(int i=1; i<=n; i++){               // For i start from 1 to N
            leftSum = (1+i)*i/2;               //   Sum up 1 to i
            rightSum = (i+n)*(n-i+1)/2;        //   Sum up i to N

            if(leftSum == rightSum) return i;  //   IF left sum = right sum -> return i as pivot
        }

        return -1;                             // Pivot not found
    }
};