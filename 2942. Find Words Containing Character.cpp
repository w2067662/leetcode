//https://leetcode.com/problems/find-words-containing-character/description/

class Solution {
public:
    bool contains(string &s, char x){   //check current string contains char x
        for(const auto ch: s){
            if(ch == x) return true;
        }
        return false;
    }

    vector<int> findWordsContaining(vector<string>& words, char x) {
        vector<int> ans;

        for(int i=0;i<words.size();i++){
            if(contains(words[i], x)) ans.push_back(i); //IF  current word contains char x -> push current index into answer
        }

        return ans;
    }
};
