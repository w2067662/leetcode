//https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/description/

class Solution {
public:
    void insert(TreeNode* root, const int num){ // Insert number into tree
        if(num < root->val){
            if(!root->left) root->left = new TreeNode(num);
            else insert(root->left, num);
        } else{
            if(!root->right) root->right = new TreeNode(num);
            else insert(root->right, num);
        }
        return;
    }

    TreeNode* bstFromPreorder(vector<int>& preorder) {
        TreeNode* root = NULL;

        for(const auto num: preorder){
            if(!root) root = new TreeNode(num); // IF    no root -> create new root
            else insert(root, num);             // ELSE          -> insert number into tree
        }

        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
