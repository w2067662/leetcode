//https://leetcode.com/problems/furthest-point-from-origin/description/

class Solution {
public:
    int furthestDistanceFromOrigin(string moves) {
        int underline=0;
        int L=0, R=0;
        for(int i=0;i<moves.length();i++){
            switch(moves[i]){
                case '_': underline++; break;
                case 'L': L++; break;
                case 'R': R++; break;
            }
        }
        return abs(L-R) + underline; //if L>R, set all underline as L, vice versa
    }
};
