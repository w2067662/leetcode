//https://leetcode.com/problems/divisible-and-non-divisible-sums-difference/description/

class Solution {
public:
    int differenceOfSums(int n, int m) {
        int sumDivisible = 0, sumNonDivisible = 0;

        for(int i=0; i<=n; i++){
            i%m == 0 ? sumDivisible += i : sumNonDivisible += i; //add up sum of Divisible and NonDivisible
        }

        return sumNonDivisible - sumDivisible;  //return the difference
    }
};
