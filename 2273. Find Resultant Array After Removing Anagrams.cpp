//https://leetcode.com/problems/find-resultant-array-after-removing-anagrams/description/

class Solution {
public:
    bool isConsecutiveAnagram(string& a, string& b){ //check 2 words are consecutive anagram or not
        map<char, int> A, B;
        for(const auto ch : a) A[ch]++;
        for(const auto ch : b) B[ch]++;

        for(const auto it: A){
            if(it.second != B[it.first]) return false;
        }

        return A.size() == B.size();
    }

    vector<string> removeAnagrams(vector<string>& words) {
        for(int i=0;i<words.size()-1;){
            if(isConsecutiveAnagram(words[i], words[i+1])) words.erase(words.begin()+i+1); //IF  2 current consecutive words are anagram -> erase the second word from vector
            else i++;
        }

        return words;
    }
};
