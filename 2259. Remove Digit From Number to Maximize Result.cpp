//https://leetcode.com/problems/remove-digit-from-number-to-maximize-result/description/

bool compare(string& a, string& b){             //compare function for the number string
    if(a.length()>b.length())return true;
    else if (a.length()<b.length())return false;
    else {
        for(int i=0;i<a.length();i++){
            if(a[i] > b[i])return true;
            else if (a[i] < b[i])return false;
        }
    }
    return false;
}

class Solution {
public:
    string removeDigit(string number, char digit) {
        vector<string> vec;

        for(int i=0;i<number.length();i++){
            if(number[i] == digit){            //store all the possible answers of removing digit from number into vector 
                string temp = number;
                temp.erase(i,1);
                vec.push_back(temp);
            }
        }

        sort(vec.begin(), vec.end(), compare);  //sort hte vector in descending order

        return vec[0];                          //return the possible answer with largest value
    }
};
