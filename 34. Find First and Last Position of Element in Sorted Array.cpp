//https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/

class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        vector<int> ans = {-1,-1};

        for(int i=0;i<nums.size();i++){
            if(nums[i] == target){
                if(ans[0] == -1) ans[0] = ans [1] = i; //IF    answer pair is {-1, -1} -> set answer = { i, i}
                else ans[1] = i;                       //ELSE                          -> set answer = {i1,i2}
            }
        }

        return ans;
    }
};
