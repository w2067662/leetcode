//https://leetcode.com/problems/append-characters-to-string-to-make-subsequence/description/

class Solution {
public:
    int appendCharacters(string s, string t) {
        int index = 0;                  // Start finding t[index] char from s

        for(const auto ch: s){          // FOR char within string
            if(ch == t[index]) index++; //      t[index] is found -> index + 1
        }
        
        return t.length() - index;      // Return the lefted length (chars to be appended)
    }
};
