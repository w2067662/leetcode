//https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation/description/

class Solution {
public:
    bool is_prime(int n) {   //check prime number
        if (n == 0 || n == 1)  return false;
        for (int i = 2; i <= n / 2; ++i) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    string reverse(string s){   //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }

    string to_binary(int num){  //convert integar to binary
        string s="";
        while(num>0){
            s+=num%2+'0';
            num/=2;
        }
        return reverse(s);
    }

    int countPrimeSetBits(int left, int right) {
        string s="";
        int ans=0;

        for(int i=left;i<=right;i++){       //start frome left to right
            int count=0;
            s = to_binary(i);               //convert integar to binary
            for(int j=0;j<s.length();j++){
                if(s[j]=='1')count++;       //count 1's in binary string
            }
            if(is_prime(count))ans++;       //check count is prime or not 
        }
        return ans;                         //return the amount of binary strings that have prime amount of 1's 
    }
};
