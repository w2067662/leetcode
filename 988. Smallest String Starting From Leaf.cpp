//https://leetcode.com/problems/smallest-string-starting-from-leaf/description/

class Solution {
public:
    bool isLeaf(TreeNode* root){    // Check current root is leaf node or not
        return !root->left && !root->right;
    }

    string reverse(string s){       // Reverse the string
        for(int i=0; i<s.length()/2; i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }

    void traverse(TreeNode* root, vector<string> &words, string &temp){
        if(!root) return;

        temp += root->val + 'a';                         // Append current char into temp string

        if(isLeaf(root)) words.push_back(reverse(temp)); // IF  is leaf node -> store the word into vector

        traverse(root->left , words, temp);              // Traverse  left subtree
        traverse(root->right, words, temp);              // Traverse right subtree

        temp.erase(temp.length()-1);                     // Remove last char from temp string

        return;
    }

    string smallestFromLeaf(TreeNode* root) {
        vector<string> words;
        string temp = "";

        traverse(root, words, temp);                                             // Traverse the tree and get all the words from tree

        sort(words.begin(), words.end(), [&](const string &a, const string &b){  // Sort the string vector lexicographically
            int minLen = min(a.length(), b.length());

            for(int i=0;i<minLen;i++){
                if(a[i] < b[i]) return true;
                else if (a[i] > b[i]) return false;
            }

            return a.length() < b.length() ? true : false;
        });

        return words[0];                                                         // Return the smallest word
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */