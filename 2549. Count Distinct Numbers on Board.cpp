//https://leetcode.com/problems/count-distinct-numbers-on-board/description/

class Solution {
public:
    int distinctIntegers(int n) {
        return n<=1 ? 1 : n-1;
    }
};
