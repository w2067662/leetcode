//https://leetcode.com/problems/find-the-number-of-good-pairs-i/description/

class Solution {
public:
    int numberOfPairs(vector<int>& nums1, vector<int>& nums2, int k) {
        int count = 0;

        for(const auto& a: nums1){
            for(const auto& b: nums2){
                if(a%(b*k) == 0){        // IF  A is divisible by B*K
                    count++;             //     -> count + 1
                }
            }
        }

        return count;
    }
};