//https://leetcode.com/problems/knight-dialer/description/

class Solution {
private:
    const int MOD = pow(10, 9) + 7;     //define MOD
    const vector<vector<int>> steps = { //define next steps
        {4, 6},
        {6, 8},
        {7, 9},
        {4, 8},
        {0, 3, 9},
        {},
        {0, 1, 7},
        {2, 6},
        {1, 3},
        {2, 4},
    };
    
public:
    int knightDialer(int n) {
        vector<long long> curr (10, 1);               //vector for current moves of each digit
        long long total = 0;

        for(int jump=1; jump<n; jump++){
            vector<long long> next (10, 0);           //vector for next moves of each digit

            for(int i=0; i<next.size(); i++){
                for(int j=0; j<steps[i].size(); j++){
                    next[i] += curr[steps[i][j]];     //calculate next moves of each digit
                }
                next[i] %= MOD;
            }

            curr = next;                              //replace current moves by next moves
        }

        for(const auto moves: curr){
            total = (total + moves) % MOD;            //sum up total moves
        }

        return total;
    }
};
