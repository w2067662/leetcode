//https://leetcode.com/problems/create-binary-tree-from-descriptions/description/

class Solution {
public:
    TreeNode* createBinaryTree(vector<vector<int>>& descriptions) {
        map<int, TreeNode*> map;
        unordered_set<int> nodes;
        set<int> notRoot;
        TreeNode* root = NULL;

        for(const auto description: descriptions){
            nodes.insert(description[0]);                             //insert all the possible value into nodes
            nodes.insert(description[1]);                             //insert all the possible value into nodes
            notRoot.insert(description[1]);                           //insert node values that are not root
        }

        for(const auto val: nodes){
            map[val] = new TreeNode(val);                             //create all the nodes by value
        }

        for(const auto description: descriptions){
            switch(description[2]){
                case 1:                                               //IF   1
                    map[description[0]]->left =map[description[1]];   //     -> set as left  child
                    break; 
                case 0:                                               //IF   0
                    map[description[0]]->right =map[description[1]];  //     -> set as right child
                    break;
            }
        }

        for(const auto node: nodes){
            if(notRoot.find(node) == notRoot.end()) root = map[node]; //find the only possible node that is root
        }

        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
