//https://leetcode.com/problems/consecutive-characters/description/

class Solution {
public:
    int maxPower(string s) {
        int count = 1;
        int max = 1;

        for(int i=1; i<s.length(); i++){
            if(s[i] == s[i-1]) count++;     //if    found consecutive letters, count+1
            else count = 1;                 //else  reset count as 1

            max = count > max ? count : max;//store max amount of consecutive letters
        }
        return max;
    }
};
