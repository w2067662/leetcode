//https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/description/

class Solution {
public:
    vector<vector<int>> groupThePeople(vector<int>& groupSizes) {
        vector<vector<int>> ans;
        map<int, vector<int>> map;              //map = {[groupSizes]: vector{[number index1], [number index2], ...}}

        for(int i=0; i<groupSizes.size(); i++){
            map[groupSizes[i]].push_back(i);    //store same groupSize elements into map
        }

        for(const auto it:map){
            if(it.second.size()%it.first == 0){                  //if amount of same groupSize elements = groupSize * N -> there is valid answer
                for(int i=0; i<it.second.size()/it.first; i++){  //i start from 0 to N-1 (there will be N vectors for current groupSize)
                    vector<int> temp;
                    for(int j=0; j<it.first; j++){
                        temp.push_back(it.second[i*it.first+j]); //push the elements' index into vector
                    }
                    ans.push_back(temp);                         //push current vector into answer
                }
            }
        }

        return ans;
    }
};
