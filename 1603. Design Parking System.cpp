//https://leetcode.com/problems/design-parking-system/description/

class ParkingSystem {
private:
    vector<int> Slot;
public:
    ParkingSystem(int big, int medium, int small) {
        Slot.push_back(big);
        Slot.push_back(medium);
        Slot.push_back(small);
    }
    
    bool addCar(int carType) {
        if(Slot[carType-1]<=0)return false;
        else Slot[carType-1]--;
        return true;
    }
};

/**
 * Your ParkingSystem object will be instantiated and called as such:
 * ParkingSystem* obj = new ParkingSystem(big, medium, small);
 * bool param_1 = obj->addCar(carType);
 */
