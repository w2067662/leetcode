//https://leetcode.com/problems/k-inverse-pairs-array/description/

class Solution {
private:
    int MOD = 1e9 + 7;
    
public:
    int kInversePairs(int n, int k) {
        vector<vector<int>> dp (1001, vector<int> (1001, 0));        // Create matrix for dynamic programming
        dp[0][0] = 1;                                                // Initialize dp[0][0] as 1

        for (int i=1; i<=n; i++) {                                   // For i from 1 to n (string length)
            for (int j=0; j<=k; j++) {                               //     For j from 0 to k (inverse pairs)
                for (int x=0; x<=min(i-1, j); x++) {                 //         For x from 0 to min(i-1, j) (iteration from 0 to max inverse pairs)

                    if (j-x >= 0) {                                  // IF  j-x >= 0 (exist lefted inverse pairs)
                        dp[i][j] = (dp[i][j] + dp[i-1][j-x]) % MOD;  //     -> add up the dp[i-1][j-x] to dp[i][j] (current)
                                                                     //                      ^ i-1: string length -1
                                                                     //                           ^ j-x: lefted inverse pairs
                    }

                }
            }
        }

        return dp[n][k];
    }
};
