//https://leetcode.com/problems/escape-the-ghosts/description/

class Solution {
public:
    int distance(vector<int> &p1, vector<int> &p2){ //calculate the distance of 2 points
        return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]);
    }

    bool escapeGhosts(vector<vector<int>>& ghosts, vector<int>& target) {
        vector<int> start = {0, 0};
        int escapeDis = distance(start, target);                    //calculate the escape distance from start point to target

        for(auto& ghost: ghosts){
            if(distance(ghost, target) <= escapeDis) return false;  //IF  exist any ghost can reach target faster or at the same time -> FALSE
        }

        return true;
    }
};
