//https://leetcode.com/problems/split-a-string-in-balanced-strings/description/

class Solution {
public:
    int balancedStringSplit(string s) {
        int ans=0;
        int countR=0;
        int countL=0;

        for(int i=0;i<s.length();i++){
            switch (s[i]){
                case 'R': countR++; break;
                case 'L': countL++; break;
            }
            if(countR == countL){     //if balance, 
                ans++;                //answer + 1
                countR=countL=0;      //reset countR and countL
            }
        }

        return ans;
    }
};
