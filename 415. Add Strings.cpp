//https://leetcode.com/problems/add-strings/description/

class Solution {
public:
    string reverse(string s){   //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    //Step1: initialize(reverse and add zero)
    //num1 = "999990" -> reverse(nums1) = "099999" -> len = 6
    //num2 = "9990"   -> reverse(nums2) = "0999"   -> len = 4 -> add 0 = "099900"
    //
    //Step2: Adding (from left to right)
    //num1 = "099999"
    //num2 = "099900"
    //ans  = "0"; temp = 0; add=0
    //ans  = "08"; temp = 18; add=1
    //ans  = "089"; temp = 18; add=1
    //ans  = "0899"; temp = 18; add=1
    //ans  = "08990"; temp = 10; add=1
    //ans  = "089900"; temp = 10; add=1
    //
    //Step3: Check for add and Reverse ans
    //add = 1 -> ans  = "0899001"
    //reverse(ans) = "1009980"

    string addStrings(string num1, string num2) {
        //step 1
        num1 = reverse(num1);
        num2 = reverse(num2);
        int len = max(num1.length(), num2.length());
        while(num1.length()<len) num1+='0';
        while(num2.length()<len) num2+='0';

        //step 2
        string ans="";
        int add=0;
        for(int i=0, temp=0; i<len ; i++){
            temp = (num1[i]-'0')+(num2[i]-'0')+add;
            ans += temp%10 +'0';
            add=(temp>=10)?1:0;
        }

        //step3
        if(add&1)ans+="1";
        return reverse(ans);
    }
};
