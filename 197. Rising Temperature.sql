#https://leetcode.com/problems/rising-temperature/description/

# Write your MySQL query statement below
SELECT w1.id
FROM Weather AS w1 , Weather AS w2
WHERE w1.Temperature > w2.Temperature AND DATEDIFF(w1.recordDate , w2.recordDate) = 1 #use DATEDIFF function to check dates
