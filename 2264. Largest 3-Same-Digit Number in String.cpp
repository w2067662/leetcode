//https://leetcode.com/problems/largest-3-same-digit-number-in-string/description/

class Solution {
public:
    string largestGoodInteger(string num) {
        string ans = "";

        for(int i=0; i+2 < num.length(); i++){
            if(num[i] == num[i+1] && num[i+1] == num[i+2]) {
                if(ans == "") ans = num.substr(i, 3);                 //IF   not exist good integer -> store current good integer
                else ans = num[i] > ans[0] ? num.substr(i, 3) : ans;  //ELSE compare with current good integer and store the largest one
            }
        }

        return ans;
    }
};
