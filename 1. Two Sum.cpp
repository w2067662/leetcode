//https://leetcode.com/problems/two-sum/description/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        map<int, int> map;
       
        for(int i = 0; i < nums.size(); i++){
            if(map.find(target - nums[i]) == map.end())     // IF    not find target - nums[i]
                map[nums[i]] = i;                           //       -> store {nums[i], index} in map
            else return {map[target - nums[i]], i};         // ELSE  return until target - nums[i] is found in map
        }
 
        return {-1, -1};
    }
};
