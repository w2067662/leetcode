//https://leetcode.com/problems/detect-capital/description/

class Solution {
public:
    bool detectCapitalUse(string word) {
        int upper=0;
        int lower=0;
        for(int i=0;i<word.length();i++){ //count for upper letters and lower letters
            if(isupper(word[i])) upper++;
            if(islower(word[i])) lower++;
        }
        //return if all letters are upper OR all letters are lower OR only first letter is upper 
        return upper==word.length() || lower==word.length() || (upper==1 && isupper(word[0]));
    }
};
