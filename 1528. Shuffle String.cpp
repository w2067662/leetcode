//https://leetcode.com/problems/shuffle-string/description/

class Solution {
public:
    string restoreString(string s, vector<int>& indices) {
        map<int, char>map;
        string ans="";
        for(int i=0;i<indices.size();i++){
            map[indices[i]]=s[i];   //store {[index]:[char]} into map
        }
        for(const auto it:map){
            ans+=it.second;         //append letters by index
        }
        return ans;
    }
};
