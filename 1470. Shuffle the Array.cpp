//https://leetcode.com/problems/shuffle-the-array/description/

class Solution {
public:
    vector<int> shuffle(vector<int>& nums, int n) {
        vector<int> ans;
        for(int i=0, j=n;i<n;i++, j++){
            ans.push_back(nums[i]);     //store Xn to vector
            ans.push_back(nums[j]);     //store Yn to vector  -> vector = [X0, Y0, X1, Y1, ...]
        }
        return ans;
    }
};
