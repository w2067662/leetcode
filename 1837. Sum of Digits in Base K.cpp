//https://leetcode.com/problems/sum-of-digits-in-base-k/description/

class Solution {
public:
    int sumBase(int n, int k) {
        int ans = 0;

        while(n>0){     //convert to base K
            ans += n%k; //add up digit
            n /= k;         
        }

        return ans;
    }
};
