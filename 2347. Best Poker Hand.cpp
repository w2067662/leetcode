//https://leetcode.com/problems/best-poker-hand/description/

class Solution {
public:
    bool hasFlush(vector<int>& ranks, vector<char>& suits){         //check Flush
        for(int i=1;i<suits.size();i++){
            if(suits[i] != suits[0])return false;
        }
        return true;
    }

    bool hasThreeOfAKind(vector<int>& ranks, vector<char>& suits){  //check Three of a Kind
        map<int, int> map;
        for(const auto rank: ranks)map[rank]++;
        for(const auto it:map){
            if(it.second >= 3) return true;
        }
        return false;
    }

    bool hasPair(vector<int>& ranks, vector<char>& suits){          //check Pair
        map<int, int> map;
        for(const auto rank: ranks)map[rank]++;
        for(const auto it:map){
            if(it.second >= 2) return true;
        }
        return false;
    }

    string bestHand(vector<int>& ranks, vector<char>& suits) {
        if(hasFlush(ranks, suits)) return "Flush";
        else if (hasThreeOfAKind(ranks, suits)) return "Three of a Kind";
        else if (hasPair(ranks, suits)) return "Pair";
        else return "High Card";
    }
};
