//https://leetcode.com/problems/remove-zero-sum-consecutive-nodes-from-linked-list/description/

class Solution {
public:
    //ex: head = [1,2,-3,3,-3]
    
    //    list: (-1) -> 1 -> 2 -> -3 -> 3 -> -3 -> NULL
    //         BH,TMP
    //                  ^    ^     ^                        1+2+(-3) = 0 (remove)

    //    list: (-1) -> 3 -> -3 -> NULL
    //         BH,TMP
    //                  ^     ^                             3+(-3) = 0   (remove)

    //    list: (-1) -> NULL
    //         BH,TMP
    //                                                      answer = []
    
    bool remove(ListNode* beforeHead){          // Remove the 0-sum sublist with beforeHead node as start node
        ListNode* temp = beforeHead->next;
        int sum = 0;

        while(temp){                            // WHILE temp is not NULL
            sum += temp->val;                   //      Sum + temp node's value

            if(sum == 0){                       //      IF found 0-sum sublist
                beforeHead->next = temp->next;  //         -> Remove sublist
                return true;                    //         -> Return TRUE
            }

            temp = temp->next;                  // Temp goto next
        }

        return false;                           // Return FALSE (0-sum sublist not found)
    }

    ListNode* removeZeroSumSublists(ListNode* head) {
        ListNode* beforeHead = new ListNode(-1, head); // Create a node before head
        ListNode* temp = beforeHead;                   // Create temp node

        while(true){
            if(!temp) break;                           // IF temp is NULL -> end the loop

            bool removed = remove(temp);               // Check and remove 0-sum sublist with temp as start node
            temp = removed ? beforeHead : temp->next;  // IF    any 0-sum sublist is removed -> temp goto beforeHead and recheck from beforeHead
                                                       // ELSE                               -> temp goto next
        }

        return beforeHead->next;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */