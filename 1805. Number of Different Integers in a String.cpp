//https://leetcode.com/problems/number-of-different-integers-in-a-string/description/

class Solution {
public:
    int numDifferentIntegers(string word) {
        string temp = "";
        set<string> set;

        for(const auto ch: word){
            if(islower(ch) && temp != ""){ //IF   encounter lower case letter
                set.insert(temp);          //     -> store the number string into set
                temp = "";
            }
            else if(isdigit(ch)){          //IF   encounter digit
                if(temp == "0")temp = "";  //     IF   temp string has leading 0 -> set temp as empty string
                temp += ch;                //     -> append digit into temp
            }
        }
        if(temp != "")set.insert(temp);    //insert last number string

        return set.size();
    }
};
