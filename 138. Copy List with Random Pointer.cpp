//https://leetcode.com/problems/copy-list-with-random-pointer/description/

class Solution {
public:
    Node* copyRandomList(Node* head) {
        //check if head is NULL
        if(!head)return NULL;
        
        Node *curr=head;//for rolling the original list
        Node *copy=NULL;//for copy list head
        
        //step1:
        //1 -> 2->3->NULL
        //     |
        //1'----
        
        //1  x 2->3->NULL
        //|    |
        //1'----
        
        //1->1'->2->3->NULL
        //          |
        //       2'--

        //1->1'->2 x 3->NULL
        //       |   |
        //       2'---

        //1->1'->2->2'->3->NULL
        //                  |
        //              3'---

        //1->1'->2->2'->3 x NULL
        //              |   |
        //              3'---
        
        //finally:
        //1->1'->2->2'->3->3'->NULL
        
        while(curr){
            Node *temp=new Node(curr->val);
            if(!copy) copy=temp;//1(head)->1'(copy_head)->2->3->NULL
            
            temp->next=curr->next;
            curr->next=temp;
            curr=curr->next->next;
        }
        
        //step2:
        //1->1'->2->2'->3->3'->NULL
        //if  1:random->3  , then 1':random->3'
        //if  2:random->NULL, then do nothing
        
        //      random
        //----------------
        //|              |
        //1 ->1'->2->2'->3->3'->NULL
        //    |             |
        //    ---------------
        //        random'
        
        curr=head;
        while(curr){
            if(curr->random)curr->next->random=curr->random->next;
            curr=curr->next->next;
        }
        
        
        //step3:
        //---------
        //|       |
        //1 x 1'->2->2'->3->3'->NULL
        
        //    --------
        //    |      |
        //    1'x 2->2'->3->3'->NULL
        //        |
        //1--------
        
        //    1'------
        //           |
        //        2->2'->3->3'->NULL
        //        |
        //1--------

        //    1'------
        //           |
        //           2'->3->3'->NULL
        //               |
        //1-------2-------

        //    1'-----2'------
        //                  |
        //               3->3'->NULL
        //               |
        //1-------2-------

        //    1'-----2'------
        //                  |
        //                  3'->NULL
        //                      |
        //1-------2------3-------

        //    1'-----2'-----3'---
        //                      |
        //                      NULL
        //                      |
        //1-------2------3-------
        
        //copied:   1'->2'->3'->NULL
        //original: 1 ->2 ->3 ->NULL
        
        curr=head;
        Node *copy_temp=copy;//for rolling the original list
        while(curr){
            curr->next=curr->next->next;
            if(copy_temp->next)copy_temp->next=copy_temp->next->next;
            curr=curr->next;
            copy_temp=copy_temp->next;
        }
        
        return copy;
    }
};

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;
    
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
*/
