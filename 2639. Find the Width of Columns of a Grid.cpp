//https://leetcode.com/problems/find-the-width-of-columns-of-a-grid/description/

class Solution {
public:
    vector<int> findColumnWidth(vector<vector<int>>& grid) {
        vector<int> ans;

        for(int j=0, len, maxLength; j<grid[0].size(); j++){
            maxLength = 0;
            for(int i=0; i<grid.size(); i++){
                len = to_string(grid[i][j]).length();           //calculate length of current number string
                maxLength = len > maxLength ? len : maxLength;  //get the max length of current column
            }
            ans.push_back(maxLength);                           //push max length of current column into answer
        }

        return ans;
    }
};
