//https://leetcode.com/problems/most-common-word/description/

class Solution {
public:
    string mostCommonWord(string paragraph, vector<string>& banned) {
        map<string, int> words;
        map<int, vector<string>> map;
        string temp="";

        for(int i=0;i<paragraph.length();i++){
            if((paragraph[i]==' '|| paragraph[i]==',' || paragraph[i]=='.')&& temp!=""){ //if encounter ' ' ; ',' ; '.', and temp is not empty
                words[temp]++;                                                           //store the word
                temp="";                                                                 //reset temp
            }
            else if(islower(paragraph[i]) || isupper(paragraph[i])){                     //if encounter lower or upper letters
                temp+=tolower(paragraph[i]);                                             //append to temp
            }
        }
        if(temp!="")words[temp]++;                            //if temp is not empty,  store the last word

        for(const auto& it:words){
            map[it.second].push_back(it.first);               //store the words into map, sort with appear times(ascending order) 
        }

        for(auto it = map.rbegin(); it != map.rend(); it++){   
            for(int i=0;i<it->second.size();i++){             //traverse all the words from map by appear times (descending)
                bool isBanned=false;
                for(int j=0;j<banned.size();j++){               
                    if(it->second[i].compare(banned[j])==0){  //if the current word in banned, goto next word
                        isBanned = true;                        
                        break;
                    }
                } 
                if(!isBanned) return it->second[i];           //if the current word is not banned, return current word as answer
            }
        }

        return "";
    }
};
