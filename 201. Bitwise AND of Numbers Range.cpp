//https://leetcode.com/problems/bitwise-and-of-numbers-range/description/

class Solution {
public:
    // ex1:
    //          0000 0000 0000 0000 0000 0000 0000 0101 (5)
    //          0000 0000 0000 0000 0000 0000 0000 0111 (7)
    //          ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^
    // prefix = 0000 0000 0000 0000 0000 0000 0000 0100 (4)

    // ex2:
    //          0000 0000 0000 0000 0000 0000 0000 0110 (6)
    //          0000 0000 0000 0000 0000 0000 0000 0111 (7)
    //          ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^^^ ^^^
    // prefix = 0000 0000 0000 0000 0000 0000 0000 0110 (6)

    // ex3:
    //          0000 0000 0000 0000 0000 0000 0000 0001 (1)
    //          0111 1111 1111 1111 1111 1111 1111 1111 (2147483647)
    //          ^
    // prefix = 0000 0000 0000 0000 0000 0000 0000 0000 (0)

    int rangeBitwiseAnd(int left, int right) {
        bitset<32> bitsLeft(left), bitsRight(right);    // Convert number into bitset (32 bits)
        string AND = "";
        long ans = 0;                                   // Use long to prevent overflow

        string bitStringLeft  = bitsLeft.to_string();   // Convert left  bits to bits string
        string bitStringRight = bitsRight.to_string();  // Convert right bits to bits string

        for(int i=0; i<bitStringLeft.length(); i++){
            if(bitStringLeft[i] == bitStringRight[i]){
                AND += bitStringLeft[i];                // Get the prefix bits string of left and right
            } 
            else break;
        }

        for(int i=AND.length(); i<32; i++){
            AND += "0";                                 // Fill up the bits string to 32 bits
        }

        for(int i=0; i<32; i++){
            ans = ans*2 + AND[i]-'0';                   // Convert bits string to integar
        }

        return (int)ans;
    }
};
