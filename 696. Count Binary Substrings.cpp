//https://leetcode.com/problems/count-binary-substrings/description/

class Solution {
public:
    //ex1: s = "00110011" 
    //  consecutiveCount = [2,2,2,2] ("00", "11", "00", "11") ; answer = 0
    //                      ^ ^                                 answer = 0 + min(2,2) = 2
    //                        ^ ^                               answer = 2 + min(2,2) = 4
    //                          ^ ^                             answer = 6 + min(2,2) = 6
    //
    //ex2: s = "10101"
    //  consecutiveCount = [1,1,1,1,1] ("1", "0", "1", "0", "1") ; answer = 0
    //                      ^ ^                                    answer = 0 + min(1,1) = 1
    //                        ^ ^                                  answer = 1 + min(1,1) = 2
    //                          ^ ^                                answer = 2 + min(1,1) = 3
    //                            ^ ^                              answer = 3 + min(1,1) = 4
    int countBinarySubstrings(string s) {
        vector<int> consecutiveCount;
        int ans = 0;
        int count = 1;

        for(int i=1;i<s.length();i++){
            if(s[i]!=s[i-1]){                       //if encounter not consecutive number
                consecutiveCount.push_back(count);  //      push count into vector
                count = 1;                          //      reset count as 1
            }
            else count++;                           //if encounter consecutive number
        }                                           //      count+1
        consecutiveCount.push_back(count);          //push last count into vector

        if(consecutiveCount.size()<=1)return 0;     //if vector size = 0 -> string contain only 0 OR 1 -> answer = 0

        for(int i=1;i<consecutiveCount.size();i++){                 // i start from 1 to vector's end
            ans += min(consecutiveCount[i-1], consecutiveCount[i]); //add up the min of pair(i-1, i)
        }

        return ans;
    }
};
