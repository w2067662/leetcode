//https://leetcode.com/problems/merge-strings-alternately/description/

class Solution {
public:
    string mergeAlternately(string word1, string word2) {
        string longer  = word1.length() >= word2.length() ? word1 : word2;  //get longer word
        string shorter = word1.length() <  word2.length() ? word1 : word2;  //get shorter word

        string ans = "";

        for(int i=0; i<shorter.length(); i++){                              //append letters alternately from word1 and word2
            ans += word1[i];
            ans += word2[i];
        }

        return ans + longer.substr(ans.length()/2, longer.length()-ans.length()/2+1); //append rest of the letters from longer word to answer
    }
};
