//https://leetcode.com/problems/number-of-arithmetic-triplets/description/

class Solution {
public:
    int arithmeticTriplets(vector<int>& nums, int diff) {
        int ans = 0;
        for(int i=0 ; i<nums.size()-2; i++){                //traverse triplet
            for(int j=i+1; j<nums.size()-1;j++){
                for(int k=j+1; k<nums.size();k++){
                    if(nums[k]-nums[j] == diff && nums[j]-nums[i] == diff) ans++; //if triplet(i,j,k) => k-j = j-i = diff, answer+1
                }
            }
        }

        return ans;
    }
};
