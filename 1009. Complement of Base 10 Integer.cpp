//https://leetcode.com/problems/complement-of-base-10-integer/description/

class Solution {
public:
    int bitwiseComplement(int n) {
        if(n == 0)return 1;

        string s = "";
        int ans = 0;

        while(n>0){                          // Convert integar to binary string
            s += n%2+'0';
            n/=2;
        }

        for(int i=0;i<s.length();i++){       // Do the complement
            s[i] = s[i] == '1' ? '0' : '1';
        }

        for(int i=s.length()-1; i>=0; i--){  // Convert binary string to integar
            ans = ans*2 + s[i]-'0';
        }

        return ans;
    }
};
