//https://leetcode.com/problems/regions-cut-by-slashes/description/

//ex: grid = ["/\\",   ->  "/\"
//            "\\/"]   ->  "\/"
//
//  matrix = [[1 1 0 0 1 1],      1 1         1 1
//            [1 0 1 1 0 1],      1     1 1     1
//            [0 1 1 1 1 0],  ->      1 1 1 1      -> regions = 5
//            [0 1 1 1 1 0],          1 1 1 1
//            [1 0 1 1 0 1],      1     1 1     1
//            [1 1 0 0 1 1],      1 1         1 1
//
class Solution {
private:
    const vector<int> steps = {1,0,-1,0,1};
    
public:
    bool isValid(vector<vector<bool>> &matrix, int i, int j){ //check current position is valid or not
        return 0 <= i && i < matrix.size() && 0 <= j && j < matrix[i].size();
    }

    void drawLeftSlash(vector<vector<bool>> &matrix, int i, int j){ //draw left slash on matrix
        for(int x=0; x<3; x++){
            for(int y=0; y<3; y++){
                matrix[i*3 + x][j*3 + y] = (x+y != 2) ? true : false;
            }
        }
    }

    void drawRightSlash(vector<vector<bool>> &matrix, int i, int j){ //draw right slash on matrix
        for(int x=0; x<3; x++){
            for(int y=0; y<3; y++){
                matrix[i*3 + x][j*3 + y] = (x != y) ? true : false;
            }
        }
    }

    void remark(vector<vector<bool>> &matrix, int i, int j){ //remark the area of current region as 0 by DFS
        if(!matrix[i][j]) return;

        matrix[i][j] = false;

        for(int s=0;s<steps.size()-1;s++){
            if(isValid(matrix, i+steps[s], j+steps[s+1])) {
                remark(matrix, i+steps[s], j+steps[s+1]);
            }
        }
    }

    int regionsBySlashes(vector<string>& grid) {
        vector<vector<bool>> matrix (grid.size()*3, vector<bool> (grid[0].size()*3, 1)); //create a matrix with 3 size larger scale
        int regions = 0;

        for(int i=0; i<grid.size(); i++){
            for(int j=0; j<grid[i].length(); j++){
                switch(grid[i][j]){
                    case '/':                         //IF current char is '/'
                        drawLeftSlash(matrix, i, j);  //   -> draw left slash on matrix
                        break;
                    case '\\':                        //IF current char is '\'
                        drawRightSlash(matrix, i, j); //   -> draw right slash on matrix
                        break;
                }
            }
        }

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                if(matrix[i][j]) {
                    regions++;                        //count for regions
                    remark(matrix, i, j);             //remark the area of current region as 0
                }
            }
        }

        return regions;
    }
};
