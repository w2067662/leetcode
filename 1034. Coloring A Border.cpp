//https://leetcode.com/problems/coloring-a-border/description/

class Solution {
private:
    vector<int> steps = {1,0,-1,0,1}; // Define next steps

public:
    //ex:   grid = [[1,2,2,2,1],  ; color = 3
    //              [2,2,2,2,1],
    //              [2,2,2,2,1],
    //              [1,2,2,2,1],
    //              [1,1,1,1,1]]
    //
    //    answer = [[1,3,3,3,1],   marked = [[x,3,3,3,x],
    //              [3,2,2,3,1],             [3,x,x,3,x],
    //              [3,2,2,3,1],             [3,x,x,3,x],
    //              [1,3,3,3,1],             [x,3,3,3,x],
    //              [1,1,1,1,1]]             [x,x,x,x,x]]
    //
    bool isValid(vector<vector<int>>& grid, const pair<int, int>& pos){ // Check current position is within the range of grid or not
        return 0 <= pos.first && pos.first < grid.size() && 0 <= pos.second && pos.second < grid[0].size();
    }

    bool isBorder(vector<vector<int>>& grid, const pair<int, int>& pos){ // Chech current position is border or not
        for(int s=0; s<steps.size()-1; s++){                                                           // For left, right down, up
            pair<int, int> surround = {pos.first + steps[s], pos.second + steps[s+1]};

            if(isValid(grid, surround)){                                                               //     IF surrounded position is valid
                if(grid[surround.first][surround.second] != grid[pos.first][pos.second]) return true;  //           IF exist non-equal value in surrounded -> is border
            } else return true;                                                                        //     ELSE -> is border
        }

        return false; // Surrouded are valid and has equal value -> not border
    }

    void DFS(vector<vector<int>>& grid, vector<vector<int>>& marked, set<pair<int, int>>& visited, pair<int, int> curr, const int& color){ // DFS to search continuous components' border
        visited.insert(curr);                                                           // Mark current position as visited

        if(isBorder(grid, curr)){                                                       // IF current position is border
            marked[curr.first][curr.second] = color;                                    //    -> mark its position on marked matrix
        }

        for(int s=0; s<steps.size()-1; s++){                                            // For all next steps (continuous surroundings)
            pair<int, int> next = {curr.first + steps[s], curr.second + steps[s+1]};

            if(isValid(grid, next) && visited.find(next) == visited.end()){             //    IF next step is valid AND not visited
                if(grid[next.first][next.second] == grid[curr.first][curr.second]){     //          IF next step is continuous component
                    DFS(grid, marked, visited, next, color);                            //             -> DFS to search next position
                }
            }
        }

        return;
    }

    vector<vector<int>> colorBorder(vector<vector<int>>& grid, int row, int col, int color) {
        vector<vector<int>> marked(grid.size(), vector<int>(grid[0].size(), -1));   // Create a matrix for marking border
        set<pair<int, int>> visited;

        DFS(grid, marked, visited, {row, col}, color);                              // DFS to search the continuous components' border

        for(int i=0; i<grid.size(); i++){                        
            for(int j=0; j<grid[i].size(); j++){
                if(marked[i][j] != -1) grid[i][j] = marked[i][j];                   // Re-colored the border in grid from marked matrix
            }
        }

        return grid;
    }
};
