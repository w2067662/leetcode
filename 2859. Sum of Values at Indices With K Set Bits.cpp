//https://leetcode.com/problems/sum-of-values-at-indices-with-k-set-bits/description/

class Solution {
public:
    int sumIndicesWithKSetBits(vector<int>& nums, int k) {
        int ans = 0;
        for(int i=0, curr, count;i<nums.size();i++){
            curr = i, count = 0;
            while(curr>0){                      //convert to binary
                count += curr%2 == 0 ? 0 : 1;   //count for 1's
                curr/=2;
            }

            if(count == k)ans += nums[i];       //IF   count = k -> answer add up nums[i]
        }

        return ans;
    }
};
