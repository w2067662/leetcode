//https://leetcode.com/problems/find-the-highest-altitude/description/

class Solution {
public:
    int largestAltitude(vector<int>& gain) {
        int altitudes = 0;
        int max = INT_MIN;

        for(int i=0;i<gain.size();i++){
            altitudes += gain[i];                    //calculate current altitude
            max = altitudes > max ? altitudes : max; //store the largest altitude
        }

        return max >= 0 ? max : 0; //if max altitude is negative -> answer = 0
    }
};
