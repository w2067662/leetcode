//https://leetcode.com/problems/count-vowel-substrings-of-a-string/description/
//word = "cuaieuouac"
//       "cuaie     "
//       " uaieu    "
//       " uaieuo   "   v
//       " uaieuou  "   v
//       " uaieuoua "   v
//       " uaieuouac"
//       "  aieuo   "   v
//       "  aieuou  "   v
//       "  aieuoua "   v
//       "  aieuouac"
//       "   ieuou  "
//       "   ieuoua "   v
//       "   ieuouac"
//       "    euoua "
//       "    euouac"
//       "     uouac"
//ans =                 7

class Solution {
public:
    bool isVowel(char c){
        return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
    }

    bool checkVowel(string s){
        if (s.length()<5)return 0;
        vector<bool> hasVowel(6, false);            //{hasA, hasE, hasI, hasO, hasU, hasNonVowel}
        for(int i=0;i<s.length();i++){
            switch(s[i]){
                case 'a': hasVowel[0]=true; break;
                case 'e': hasVowel[1]=true; break;
                case 'i': hasVowel[2]=true; break;
                case 'o': hasVowel[3]=true; break;
                case 'u': hasVowel[4]=true; break;
                default:  hasVowel[5]=true; break;
            }
        }
        return (hasVowel[0] && hasVowel[1] &&  hasVowel[2] &&                   //contain only vowels
                hasVowel[3] && hasVowel[4] && !hasVowel[5]) ? true : false;
    }

    int countVowelSubstrings(string word) {
        if(word.length()<5) return 0;
        int ans=0;
        for(int i=0, j=i+4;i<word.length()-4;){
            if(checkVowel(word.substr(i, j-i+1))){
                ans++;
                if(j<word.length()-1)j++;
                else{
                    i++;
                    j=i+4;
                }
            }
            else{
                if(isVowel(word[i]) && j<word.length()-1)j++;
                else{
                    i++;
                    j=i+4;
                }
            }
        }
        return ans;
    }
};
