//https://leetcode.com/problems/difference-between-ones-and-zeros-in-row-and-column/description/

class Solution {
public:
    //ex:   grid = [[0,1,1],   rowDiff[0] = 2-1 = 1
    //              [1,0,1],   rowDiff[1] = 2-1 = 1
    //              [0,0,1]]   rowDiff[2] = 1-2 = -1
    //               ^ ^ ^
    //               | | colDiff[2] = 3-0 = 3
    //               | colDiff[1] = 1-2 = -1
    //               colDiff[0] = 1-2 = -1
    //
    //      diff = [[ 0, 0, 4],
    //              [ 0, 0, 4],
    //              [-2,-2, 2]]
    //
    vector<vector<int>> onesMinusZeros(vector<vector<int>>& grid) {
        vector<vector<int>> diff (grid.size(), vector<int> (grid[0].size()));
        vector<int> colDiffs, rowDiffs;

        for(int i=0; i<grid.size(); i++){
            int rowDiff = 0;
            for(int j=0; j<grid[i].size(); j++){
                rowDiff += grid[i][j] == 1 ? 1 : -1;    //calculate rowDiff
            }
            rowDiffs.push_back(rowDiff);                //store all the rowDiffs
        }

        for(int j=0; j<grid[0].size(); j++){
            int colDiff = 0;
            for(int i=0; i<grid.size(); i++){
                colDiff += grid[i][j] == 1 ? 1 : -1;    //calculate colDiff
            }
            colDiffs.push_back(colDiff);                //store all the colDiffs
        }

        for(int i=0; i<grid.size(); i++){
            for(int j=0; j<grid[i].size(); j++){
                diff[i][j] = rowDiffs[i] + colDiffs[j]; //calculate current position's diff
            }
        }

        return diff;
    }
};
