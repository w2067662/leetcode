//https://leetcode.com/problems/subtree-of-another-tree/description/

class Solution {
private:
    bool ans;
public:
    bool isSameTree(TreeNode* a, TreeNode* b){
        if((!a && b) || (a && !b))return false;     //one is NULL   -> not the same tree
        else if(!a && !b)return true;               //both are NULL -> same tree

        if(a->val != b->val)return false;           //different value -> not the same tree

        if(!isSameTree(a->left , b->left )) return false;   //if left  subtree is false -> not the same tree
        if(!isSameTree(a->right, b->right)) return false;   //if right subtree is false -> not the same tree

        return true;
    }
    void recurrence(TreeNode* root, TreeNode* subRoot){
        if(!root)return;
        if(root->val == subRoot->val && isSameTree(root, subRoot)){ //if two node have same value, check for same tree or not
                ans = true;
                return;
        }
        recurrence(root->left , subRoot);
        recurrence(root->right, subRoot);
        return;
    }
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        ans=false;
        recurrence(root, subRoot);
        return ans;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
