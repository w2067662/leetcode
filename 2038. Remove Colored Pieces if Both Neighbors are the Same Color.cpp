//https://leetcode.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/description/

class Solution {
public:
    bool winnerOfGame(string colors) {
        int A = 0, B = 0;
        for(int i=1;i<colors.length()-1;i++){
            if(colors[i-1] == 'A' && colors[i] == 'A' && colors[i+1] == 'A')A++;//IF  current 'A' surrounded by 'A' -> A++
            if(colors[i-1] == 'B' && colors[i] == 'B' && colors[i+1] == 'B')B++;//IF  current 'B' surrounded by 'B' -> B++
        }

        return A > 0 && A > B; //IF  A is not 0 AND countA > countB -> Alice wins -> TRUE
    }
};
