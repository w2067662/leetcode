//https://leetcode.com/problems/game-of-life/description/

class Solution {
private:
    const vector<int> steps = {1,0,-1,0,1,1,-1,-1,1};   //next steps

public:
    bool isValid(vector<vector<int>>& board, int i, int j){ //check current position is valid or not
        return 0 <= i && i < board.size() && 0 <= j && j < board[i].size();
    }

    void gameOfLife(vector<vector<int>>& board) {
        vector<vector<int>> update (board.size(), vector<int>(board[0].size(), 0)); //create a new updated board

        for(int i=0;i<board.size();i++){
            for(int j=0;j<board[i].size();j++){
                int live = 0;

                for(int s=0;s<steps.size()-1;s++){
                    if(isValid(board, i+steps[s], j+steps[s+1])){
                        if(board[i+steps[s]][j+steps[s+1]] == 1) live++;            //count live neighbor cells
                    }
                }

                if(board[i][j] == 1) update[i][j] = (live < 2 || live > 3) ? 0 : 1; //IF  current cell is live
                                                                                    //    IF live neighbor cells  < 2 or  > 3 -> dead
                                                                                    //    IF live neighbor cells >= 2 or <= 3 -> live
                else if(board[i][j] == 0 && live == 3) update[i][j] = 1;            //IF  current cell is dead
            }                                                                       //    IF live neighbor cells  = 3         -> live
        }

        board = update;
    }
};
