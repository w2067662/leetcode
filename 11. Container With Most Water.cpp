//https://leetcode.com/problems/container-with-most-water/description/

class Solution {
public:
    int maxArea(vector<int>& height) {
        int max=0,temp=0;
        for(int i=0,j=height.size()-1; i!=j;){ //set i as left, j as right, end the loop when i = j
            if(height[i]<height[j]){
                temp= height[i]*(j-i);         //count the area
                i++;
            }
            else{
                temp= height[j]*(j-i);         //count the area
                j--;
            }
            max = temp>max ? temp : max;       //check if temp is bigger than max
        }
        return max;
    }
};
