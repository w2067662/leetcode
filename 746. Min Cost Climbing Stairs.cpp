//https://leetcode.com/problems/min-cost-climbing-stairs/description/

//ex: cost = [1,100,1,1,1,100,1,1,100,1]
//                  ^ cost[2] = (cost[2] + min(cost[0], cost[1])) = 1 + min(1,100) = 2
//->  cost = [1,100,2,1,1,100,1,1,100,1]
//                    ^ cost[3] = (cost[3] + min(cost[1], cost[2])) = 1 + min(100,2) = 3
//  ... ...
//->  cost = [1,100,2,3,3,103,4,5,104,6] 
//answer = min(104, 6) = 6

class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        for(int i=2;i<cost.size();i++){
            cost[i]+=min(cost[i-1],cost[i-2]);
        }
        return min(cost[cost.size()-1],cost[cost.size()-2]);
    }
};
