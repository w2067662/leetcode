//https://leetcode.com/problems/largest-number-after-digit-swaps-by-parity/description/

class Solution {
public:
    int largestInteger(int num) {
        string s = to_string(num);

        for(int i=0;i<s.length()-1;i++){
            for(int j=i+1;j<s.length();j++){
                if((s[i]-'0')%2 == (s[j]-'0')%2 && s[i] < s[j]) swap(s[i], s[j]); //IF   (i, j) which s[i] AND s[j] are both even or odd AND s[i] < s[j] -> swap these 2 digits
            }
        }

        return stoi(s);
    }
};
