//https://leetcode.com/problems/make-a-square-with-the-same-color/description/

class Solution {
public:
    int diff(const vector<vector<char>>& grid, const int& x, const int& y){ // Count the differences of black and white in 2x2 grid
        int black = 0, white = 0;

        for(int i=0; i<2; i++){
            for(int j=0; j<2; j++){
                (grid[x+i][y+j] == 'B') ? black++ : white++;
            }
        }

        return abs(black-white);
    }

    bool canMakeSquare(vector<vector<char>>& grid) {
        for(int i=0; i<2; i++){                      // Traverse all the possible 2x2 grid
            for(int j=0; j<2; j++){
                int d = diff(grid, i, j);            //    Calculate the differences of black and white

                if(d == 2 || d == 4) return true;    //    IF  differences = 2 OR 4
                                                     //        -> TRUE
            }
        }

        return false;                                // No possible 2x2 grid fulfill the condition -> FALSE
    }
};