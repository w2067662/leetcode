//https://leetcode.com/problems/check-if-the-sentence-is-pangram/description/

class Solution {
public:
    bool checkIfPangram(string sentence) {
        vector<int> vec(26); //for storing letters

        for(int i=0;i<sentence.length();i++){
            vec[sentence[i]-'a']++;      //count for appear times of each letter in the sentence
        }

        for(int i=0;i< vec.size();i++){
            if(vec[i] == 0)return false; //if the sentence do not contain all the letters -> no Pangram
        }

        return true;
    }
};
