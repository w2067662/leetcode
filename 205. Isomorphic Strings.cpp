//https://leetcode.com/problems/isomorphic-strings/description/

class Solution {
public:
    bool isIsomorphic(string s, string t) {
        map<char, char> map1, map2;

        for(int i=0; i<s.length(); i++){                                   // For i from 0 to s.length
            if( (map1.find(s[i]) != map1.end() && map1[s[i]] != t[i]) ||   //       IF    s[i] is mapped AND not mapped to t[i] OR
                (map2.find(t[i]) != map2.end() && map2[t[i]] != s[i])  ){  //             t[i] is mapped AND not mapped to s[i]
                return false;                                              //             -> FALSE
            }
            else {                                                         //       ELSE
                map1[s[i]] = t[i];                                         //             -> map s[i] to t[i]
                map2[t[i]] = s[i]                                          //             -> map t[i] to s[i]
            }
        }

        return true;
    }
};