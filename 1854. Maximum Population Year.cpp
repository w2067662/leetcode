//https://leetcode.com/problems/maximum-population-year/description/

class Solution {
public:
    int maximumPopulation(vector<vector<int>>& logs) {
        unordered_map<int, int> umap;   //umap = {[year]: [populations]}
        map<int, vector<int>> map;      //umap = {[populations]: vector{[year1], [year2], ...}} (yearN < yearN+1)
        int maxPopulation = 0;

        for(const auto log: logs){                           //count the population of each year
            for(int i=log[0]; i<log[1]; i++){
                umap[i]++;
                maxPopulation = max(maxPopulation, umap[i]); //store the max population amount
            }
        }

        for(const auto it:umap){
            map[it.second].push_back(it.first);              //store the datas from umap to map
        }

        sort(map[maxPopulation].begin(), map[maxPopulation].end()); //sort the vector of max population years in ascending order

        return map[maxPopulation][0];                        //return the ealiest year with max population
    }
};
