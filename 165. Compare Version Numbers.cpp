//https://leetcode.com/problems/compare-version-numbers/description/

class Solution {
public:
    int compareVersion(string version1, string version2) {
        for(int i=0, j=0; i<version1.length() || j<version2.length(); i++, j++){                  // FOR  i OR j < version string
            int v1 = 0, v2 = 0;

            while (i < version1.length() && version1[i] != '.') v1 = v1*10 + (version1[i++]-'0'); //    Get current patch number of version1
            while (j < version2.length() && version2[j] != '.') v2 = v2*10 + (version2[j++]-'0'); //    Get current patch number of version2

            if(v1 < v2) return -1;  // IF version1 < version2 -> return -1
            if(v1 > v2) return  1;  // IF version1 > version2 -> return  1
        }

        return 0;                   // IF  equal -> return 0
    }
};