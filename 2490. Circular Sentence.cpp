//https://leetcode.com/problems/circular-sentence/description/

class Solution {
public:
    bool isCircularSentence(string sentence) {
        bool isCircular = true;

        if(sentence[0]!=sentence[sentence.length()-1])isCircular = false;           //sentence's first and last letter aren't equal

        for(int i=0;i<sentence.length();i++){
            if(sentence[i]==' ' && sentence[i-1]!=sentence[i+1]) isCircular = false;//last  letter of left  word AND
                                                                                    //first letter of right word aren't equal
        }

        return isCircular;
    }
};
