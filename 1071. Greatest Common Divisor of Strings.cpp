//https://leetcode.com/problems/greatest-common-divisor-of-strings/description/

class Solution {
public:
    string gcdOfStrings(string str1, string str2) {
        return (str1 + str2 == str2 + str1)? str1.substr(0, gcd(size(str1),size(str2))): ""; // IF   str1 + str2 = str2 + str1, 
                                                                                             //      str1 is divisible by str2 OR str2 is divisible by str1
                                                                                             //      -> return shorter substring (str1 OR str2)
                                                                                             // ELSE no common divisor of strings
                                                                                             //      -> return empty string
    }
};
