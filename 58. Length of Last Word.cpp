//https://leetcode.com/problems/length-of-last-word/description/

class Solution {
public:
    int lengthOfLastWord(string s) {
        int len = 0;
        bool found = false;

        for(int i=s.length()-1; i>=0; i--){             // For last char to first char
            if(s[i]==' ' && found) break;               //      IF encounter ' ' AND last word has found
                                                        //         -> end the loop
            else if(islower(s[i]) || isupper(s[i])){    //      IF encounter letters    
                len++;                                  //         -> length + 1
                found = true;                           //         -> found last word
            }
        }

        return len;
    }
};