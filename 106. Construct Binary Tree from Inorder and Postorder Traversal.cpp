//https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/

class Solution {
private:
    map<int, int> map;
    TreeNode* root;

public:
    void insert(TreeNode* root, const int &num){
        if(map[root->val] < map[num]){                        // IF  current number's index is bigger than root->val's index
            if(!root->right) root->right = new TreeNode(num); //     IF    current root has no right child -> set current number as new right child
            else insert(root->right, num);                    //     ELSE                                  -> insert to root->right
        } else {                                              // IF  current number's index is smaller than root->val's index
            if(!root->left) root->left = new TreeNode(num);   //     IF    current root has no left  child -> set current number as new left  child
            else insert(root->left, num);                     //     ELSE                                  -> insert to root->left
        }

        return;
    }

    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        for(int i=0;i<inorder.size(); i++){
            map[inorder[i]] = i;                                                 // Store the index of numbers from inorder into map
        }

        for(int i=postorder.size()-1; i>=0; i--){                                // Start from the end of postorder to build tree
            if(i == postorder.size()-1) this->root = new TreeNode(postorder[i]); //     IF   current number is the last nubmer in post order -> set as new root
            else insert(this->root, postorder[i]);                               //     ELSE                                                 -> insert into tree
        }

        return this->root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
