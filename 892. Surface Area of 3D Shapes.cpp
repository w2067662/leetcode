//https://leetcode.com/problems/surface-area-of-3d-shapes/description/

class Solution {
private:
    int Imax, Jmax;
public:
    //ex: 
    //grid = [[1,3,1],  answer = 0
    //        [1,0,7],
    //        [1,5,1]]
    //
    //----[step1]:count surfaces of top and bottom--------
    //
    //  o,o,o       
    //  o,x,o (8 surfaces) x 2(top AND bottom) = 16 -> answer = 0(+16) = 16
    //  o,o,o
    //
    //----[step2]:count surfaces on the outter of grid----
    //
    //    1 3 1
    //    _ _ _
    // 1 |     | 1
    // 1 |     | 7 (24 surfaces) -> answer = 16(+24) = 40
    // 1 |_ _ _| 1
    //    1 5 1
    //
    //----[step3]:count surfaces on the inner of grid----
    //
    //     2 2                 3
    //  0 _|_|_ 6              _
    //  0 _|_|_ 6 -middle-> 1 |_| 7 (40 surfaces) -> answer = 40(+40) = 80
    //     | |                 5
    //     4 4
    //
    int surfaceArea(vector<vector<int>>& grid) {
        Imax = grid.size()-1;
        Jmax = grid[0].size()-1;
        int surface = 0;

        for(int i=0; i<grid.size(); i++){
            for(int j=0; j<grid[i].size(); j++){
                //[step1]
                if(grid[i][j] != 0) surface += 2;
                //[step2]
                if(i == 0) surface += grid[i][j];
                if(i == Imax ) surface += grid[i][j];
                if(j == 0) surface += grid[i][j];
                if(j == Jmax ) surface += grid[i][j];
            }
        }
        //[step3]
        for(int i=0; i<grid.size(); i++){
            for(int j=0; j<grid[i].size()-1; j++){
                surface += abs(grid[i][j]-grid[i][j+1]);
            }
        }
        for(int j=0; j<grid[0].size(); j++){
            for(int i=0; i<grid.size()-1; i++){
                surface += abs(grid[i][j]-grid[i+1][j]);
            }
        }

        return surface;
    }
};
