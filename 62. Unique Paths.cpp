//https://leetcode.com/problems/unique-paths/description/

class Solution {
public:
    //m = 3, n = 2
    //matrix = {{1, 1},
    //          {1, 2},    
    //          {1, *3}}

    //m = 3, n = 3
    //matrix = {{1, 1, 1},
    //          {1, 2, 3},    
    //          {1, 3, *6}}

    int uniquePaths(int m, int n) {
        vector<vector<int>> matrix(m, vector<int>(n));

        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){

                if(i==0){
                    if(j==0) matrix[i][j]=1;
                    else matrix[i][j]+=matrix[i][j-1];
                }
                else{
                    if(j==0) matrix[i][j]+=matrix[i-1][j];
                    else matrix[i][j]= matrix[i-1][j] + matrix[i][j-1];
                }

            }
        }
        return matrix[m-1][n-1];
    }
};
