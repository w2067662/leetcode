//https://leetcode.com/problems/sort-integers-by-the-number-of-1-bits/description/

class Solution {
public:
    int CountBits(int num){
        int bits=0;
        string binary="";
        
        while(num>0){                           //convert number to binary
           binary+=num%2+'0';   
           num/=2;
        }
        for(int i=0;i<binary.length();i++){     //count bits in binary string
            if(binary[i]=='1')bits++;
        }
        return bits;
    }

    vector<int> sortByBits(vector<int>& arr) {
        map<int, vector<int>>map;                      //map = {[counts]:vector{[number1], [number2]...}} (numberN < numberN+1)
        vector<int> ans;

        for(int i=0;i<arr.size();i++){
            map[CountBits(arr[i])].push_back(arr[i]);  //count the bits of each number, and store into map
        }

        for(auto it:map){
            sort(it.second.begin(), it.second.end());  //sort the vector which contain nubmers that have same amount of bits

            for(int i=0;i<it.second.size();i++){
                ans.push_back(it.second[i]);           //push the numbers into answer in ascending order
            }
        }

        return ans;
    }
};
