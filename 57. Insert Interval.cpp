//https://leetcode.com/problems/insert-interval/description/

class Solution {
public:
    //ex: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]] ; newInterval = [4,8]
    //   intervals' = [[1,2],[3,5],[4,8],[6,7],[8,10],[12,16]] (sort in ascending order by start)
    //                  ^                                        ans = [[1,2]]
    //                        ^                                  ans = [[1,2],[3,5]]
    //                              ^                            ans = [[1,2],[3,8]]
    //                                    ^                      ans = [[1,2],[3,8]]
    //                                          ^                ans = [[1,2],[3,10]]
    //                                                  ^        ans = [[1,2],[3,10],[12,16]]
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        vector<vector<int>> ans;

        intervals.push_back(newInterval);                                               // Append newInterval into intervals

        sort(intervals.begin(), intervals.end(), [&](vector<int>& a, vector<int>& b){
            return a[0] < b[0];                                                         // Sort intervals in ascending order by start
        });

        for(const auto& range: intervals){                                              // Traverse all the ranges in intervals
            if(ans.size() == 0 || range[0] > ans[ans.size()-1][1]) {                    //      IF answer is enpty OR
                                                                                        //         current range's start > last range's end
                ans.push_back(range);                                                   //         -> push range into answer
            }
            else {                                                                      //      ELSE
                ans[ans.size()-1][0] = min(ans[ans.size()-1][0], range[0]);             //         -> merge last range in answer with current range
                ans[ans.size()-1][1] = max(ans[ans.size()-1][1], range[1]);
            }
        }

        return ans;
    }
};