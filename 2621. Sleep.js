//https://leetcode.com/problems/sleep/description/

/**
 * @param {number} millis
 */
async function sleep(millis) {
    await new Promise((resolve) => {//create new promise with resolve as param in a function
        setTimeout(resolve, millis) //setTimeout(function, param)
    });
}

/** 
 * let t = Date.now()
 * sleep(100).then(() => console.log(Date.now() - t)) // 100
 */
