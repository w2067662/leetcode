//https://leetcode.com/problems/split-with-minimum-sum/description/

class Solution {
public:
    int splitNum(int num) {
        vector<int> vec;
        int num1 = 0, num2 = 0;

        while(num>0){
            vec.push_back(num%10);             //split and push digits into vector
            num/=10;
        }

        sort(vec.begin(), vec.end());          //sort the vector in ascending order

        for(int i=0; i+2<=vec.size(); i+=2){   //calculate num1 and num2 by digits in vector alternately
            num1 = num1 * 10 + vec[i];
            num2 = num2 * 10 + vec[i+1];
        }

        num1 = vec.size()%2 == 0 ? num1 : num1 * 10 + vec[vec.size()-1]; //if there is lefted digit, calculate num1 by lefted digit

        return num1 + num2;
    }
};
