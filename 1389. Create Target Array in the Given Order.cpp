//https://leetcode.com/problems/create-target-array-in-the-given-order/description/

//ex:
//nums       index     target
//0            0        [0]
//1            1        [0,1]
//2            2        [0,1,2]
//3            2        [0,1,3,2]
//4            1        [0,4,1,3,2]
class Solution {
public:
    vector<int> createTargetArray(vector<int>& nums, vector<int>& index) {
        vector<int> ans;

        for(int i=0;i<nums.size();i++){ //insert numbers in vector
            ans.insert(ans.begin()+index[i], nums[i]);
        }
        return ans;
    }
};
