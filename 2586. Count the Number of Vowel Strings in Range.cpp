//https://leetcode.com/problems/count-the-number-of-vowel-strings-in-range/description/

class Solution {
public:
    bool isVowel(char ch){  //check vowel
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

    int vowelStrings(vector<string>& words, int left, int right) {
        int ans = 0;

        for(int i=left; i<=right; i++){
            if(isVowel(words[i][0]) && isVowel(words[i][words[i].length()-1])) ans++; //if current word start and end with vowel, answer+1
        }

        return ans;
    }
};
