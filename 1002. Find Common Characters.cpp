//https://leetcode.com/problems/find-common-characters/description/

class Solution {
public:
    // ex:  words = ["bella","label","roller"]                abcdefghijklnopqrstuvwxyz
    //                ^                              common = 1100100000020000000000000
    //                                         intersection = 0000000000000000000000000

    //                        ^                      common = 1100100000020000000000000
    //                                         intersection = 1100100000020000000000000
    //                                        -------------------------------------------
    //                                            -> common = 1100100000020000000000000

    //                                ^              common = 1100100000020000000000000
    //                                         intersection = 0000100000020100200000000
    //                                        -------------------------------------------
    //                                            -> common = 0000100000020000000000000  => answer = ["e", "l", "l"]                                         

    vector<string> commonChars(vector<string>& words) {
        map<char, int> common, intersection;
        vector<string> ans;

        for(int i=0; i<words.size(); i++){                       // Traverse all the words
            if(i == 0){                                          //     IF current word is first word
                for(const auto& ch: words[i]){
                    common[ch]++;                                //        -> store the frequency of the word
                }
                continue;                                        //        -> goto next word
            }

            intersection.clear();                                //     Clear the intersection map
            for(const auto& ch: words[i]){
                intersection[ch]++;                              //     Store the frequency of the word
            }

            for(char ch='a'; ch<='z'; ch++){                     //     FOR  char from 'a' to 'z'
                common[ch] = min(common[ch], intersection[ch]);  //          -> store the min frequency of each char from the intersection
            }
        }

        for(const auto& it: common){         // Append all the common chars to answer
            for(int i=0; i<it.second; i++){
                string s = "";
                s += it.first;
                ans.push_back(s);
            }
        }

        return ans;
    }
};