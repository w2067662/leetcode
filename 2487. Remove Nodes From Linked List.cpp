//https://leetcode.com/problems/remove-nodes-from-linked-list/description/

class Solution {
public:
    ListNode* removeNodes(ListNode* head) {
        vector<ListNode*> nodes, reserved;
        ListNode* newHead = NULL;
        int maxNum = 0;

        while(head){              
            nodes.push_back(head);                // Store the nodes into vector
            head = head -> next;
        }

        for(int i=nodes.size()-1; i>=0; i--){     // FOR i from N-1 to 0
            if(i == nodes.size()-1){              //      IF  current node is the last node 
                reserved.push_back(nodes[i]);     //          -> Push into reserved
            }

            if(nodes[i]->val >= maxNum){          //      IF  no exist larger number within current node's right sublist
                reserved.push_back(nodes[i]);     //          -> Push into reserved
            }

            maxNum = max(maxNum, nodes[i]->val);  //      Store the max number of right sublist
        }

        for(int i=reserved.size()-1; i>=0; i--){                  // FOR i from  N-1 to 0
            if(i == reserved.size()-1){                           //      IF  current node is the last node 
                newHead = reserved[i];                            //          -> Set as new head
            }
                                                                           
            reserved[i]->next = (i == 0) ? NULL : reserved[i-1];  //      IF   current node is the first node -> set its next as NULL
                                                                  //      ELSE                                -> set next node to i-1
        }

        return newHead;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
