//https://leetcode.com/problems/minimum-number-of-steps-to-make-two-strings-anagram/description/

class Solution {
public:
    int minSteps(string s, string t) {
        vector<int> charsOfS (26, 0), charsOfT(26, 0);
        int diff = 0;

        for(const auto ch: s) charsOfS[ch-'a']++;                   //count for appear times of string S's chars
        for(const auto ch: t) charsOfT[ch-'a']++;                   //count for appear times of string T's chars

        for(int i=0;i<26;i++) diff += abs(charsOfS[i]-charsOfT[i]); //add up difference of current char between string S and T

        return diff/2;                                              //the min steps = difference/2
    }
};
