#https://leetcode.com/problems/method-chaining/description/

import pandas as pd

def findHeavyAnimals(animals: pd.DataFrame) -> pd.DataFrame:
    animals = animals.sort_values(by='weight', ascending=False)
    return animals.loc[animals["weight"]>100,['name']]

    #return animals.sort_values(by='weight', ascending=False)[animals["weight"]>100][['name']]

#sort_values(by?, ascending?) : sort the values by certain column in ascending order or not
