//https://leetcode.com/problems/restore-ip-addresses/description/

class Solution {
public:
    bool isValid(int n){    // Check if a number is within the valid range [0, 255]
        return 0 <= n && n <= 255;
    }

    string toIP(const int& a, const int& b, const int& c, const int& d){    // Form the 4 numbers into an IP address string
        return to_string(a) + "." + to_string(b) + "." + to_string(c) + "." + to_string(d);
    }

    vector<string> restoreIpAddresses(string s) {
        vector<string> ans;
        string k;
        
        for (int a=1; a<=3; a++) {                                 // Iterate over all possible combinations of 4 numbers forming the IP address
            for (int b=1; b<=3; b++) {
                for (int c=1; c<=3; c++) {
                    for(int d=1; d<=3; d++){
                        if (a+b+c+d == s.length()) {               // IF  the sum of lengths of all segments equals the length of the string         
                            int s1 = stoi(s.substr(0, a));         //     ->  Extract 4 segments from the string
                            int s2 = stoi(s.substr(a, b));
                            int s3 = stoi(s.substr(a + b, c));
                            int s4 = stoi(s.substr(a + b + c, d));
                        
                            if (isValid(s1) && isValid(s2) && isValid(s3) && isValid(s4)) {   // IF  all segments are valid
                                string ip = toIP(s1, s2, s3, s4);                             //     -> Form the IP address

                                if(ip.length() == s.length()+3){                              //     IF  all digits within string are used to form the IP
                                    ans.push_back(ip);                                        //         -> Push IP into answer
                                }
                            }
                        }
                    }
                }
            }
        }

        return ans;
    }
};
