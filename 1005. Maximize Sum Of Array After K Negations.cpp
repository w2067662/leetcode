//https://leetcode.com/problems/maximize-sum-of-array-after-k-negations/description/

class Solution {
public:
    int largestSumAfterKNegations(vector<int>& nums, int k) {
        int sum = 0;

        for(int i=0; i<k; i++){
            sort(nums.begin(), nums.end());    // Sort the vector in ascending order for each time
            nums[0] = -nums[0];                // Change sign of the smallest number in the vector
        }

        for(const auto num: nums) sum += num;  // Sum up numbers in vector

        return sum;
    }
};
