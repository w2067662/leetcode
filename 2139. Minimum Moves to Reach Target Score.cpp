//https://leetcode.com/problems/minimum-moves-to-reach-target-score/description/

class Solution {
public:
    int minMoves(int target, int maxDoubles) {
        int moves = 0;

        while(target > 1){                          // End the loop until target = 1
            if(maxDoubles != 0 && target%2 != 0){   //      IF   maxDoubles > 0 AND target is odd
                target--;                           //           -> target-1 
            }
            else if(maxDoubles == 0){               //      IF   maxDoubles = 0
                moves += target-1;                  //           -> moves + (target-1)
                break;                              //           -> end the loop
            } 
            else {                                  //      ELSE
                target /= 2;                        //           -> target/2
                maxDoubles--;                       //           -> maxDoubles-1 (use 1 double)
            }

            moves++;                                //      moves+1
        }

        return moves;
    }
};
