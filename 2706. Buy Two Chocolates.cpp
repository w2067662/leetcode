//https://leetcode.com/problems/buy-two-chocolates/description/

class Solution {
public:
    int buyChoco(vector<int>& prices, int money) {
        sort(prices.begin(), prices.end()); //sort the prices in ascending order
        return prices[0]+prices[1] <= money ? money - (prices[0]+prices[1]) : money; //IF    the cheapiest 2 chocolates are affordable
    }                                                                                //      return the balance
                                                                                     //ELSE  return the money
};
