//https://leetcode.com/problems/maximum-nesting-depth-of-the-parentheses/description/

class Solution {
public:
    int maxDepth(string s) {
        int maxDepth = 0, braces = 0;

        for(const auto& ch: s){
            switch(ch){
                case '(':  braces++; break;   // IF encounter '(' -> braces + 1
                case ')':  braces--; break;   // IF encounter ')' -> braces - 1
            }

            maxDepth = max(maxDepth, braces); // Store max depth of braces
        }

        return maxDepth;
    }
};