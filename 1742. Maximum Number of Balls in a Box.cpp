//https://leetcode.com/problems/maximum-number-of-balls-in-a-box/description/

class Solution {       
public:
    int digitSum(int num){  //count for digit sum
        int res = 0;
        while(num>0){
            res += num%10;
            num/=10;
        }
        return res;
    }
    
    int countBalls(int lowLimit, int highLimit) {
        map<int, int> map;                           //map = {[box number]: [same digitSum balls appear times]}
        int max = 0;

        for(int i=lowLimit; i<=highLimit; i++){
            map[digitSum(i)]++;                      //put same digitSum balls into correspond boxes
        }

        for(const auto it:map){
            max = it.second > max ? it.second : max; //find max appear times of balls with same digitSum
        }

        return max;
    }
};
