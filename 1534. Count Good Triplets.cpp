https://leetcode.com/problems/count-good-triplets/description/

class Solution {
public:
    bool isGoodTriplet(int i, int j, int k, int a, int b, int c){
        if(abs(i-j)>a)return false;
        if(abs(j-k)>b)return false;
        if(abs(i-k)>c)return false;
        return true;
    }

    int countGoodTriplets(vector<int>& arr, int a, int b, int c) {
        int ans = 0;
        for(int i=0;i<arr.size()-2;i++){                                      //traverse all possible triplets
            for(int j=i+1;j<arr.size()-1;j++){
                for(int k=j+1;k<arr.size();k++){
                    if(isGoodTriplet(arr[i], arr[j], arr[k], a, b, c)) ans++; //add 1, if (arr[i], arr[j], arr[k]) is good triplet
                }
            }
        }
        return ans;
    }
};
