//https://leetcode.com/problems/maximum-number-of-coins-you-can-get/description/

class Solution {
public:
    //ex:    piles = [9,8,7,6,5,1,2,3,4]
    //      sorted = [1,2,3,4,5,6,7,8,9]
    //                      ^   ^   ^     (taken)
    //       coins = 8+6+4 = 18
    //
    int maxCoins(vector<int>& piles) {
        sort(piles.begin(), piles.end());                   //sort vector in ascending order

        int takes = piles.size()/3, coins = 0;              //count for taken times

        for(int t=0, i=piles.size()-2; t<takes; t++, i-=2){
            coins += piles[i];                              //add up the taken coins
        }

        return coins;
    }
};
