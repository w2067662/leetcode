//https://leetcode.com/problems/find-common-elements-between-two-arrays/description/

class Solution {
public:
    vector<int> findIntersectionValues(vector<int>& nums1, vector<int>& nums2) {
        map<int, int> map1, map2;
        int value1 = 0, value2 = 0;

        for(const auto num: nums1) map1[num]++;                        //store the nubmer and appear times from nums1 into map1
        for(const auto num: nums2) map2[num]++;                        //store the nubmer and appear times from nums2 into map2

        for(const auto it: map1){
            if(map2.find(it.first) != map2.end()) value1 += it.second; //IF find current number in nums1 from map2
        }                                                              //   -> value1 app up current number's appear times

        for(const auto it: map2){
            if(map1.find(it.first) != map1.end()) value2 += it.second; //IF find current number in nums2 from map1
        }                                                              //   -> value2 app up current number's appear times

        return {value1, value2};
    }
};
