//https://leetcode.com/problems/maximum-units-on-a-truck/description/

class Solution {
public:
    int maximumUnits(vector<vector<int>>& boxTypes, int truckSize) {
        int ans = 0;

        for(int i=0; i<boxTypes.size()-1; i++){                                      //bubble sort (take the box with larger unit first)
            for(int j=0; j<boxTypes.size()-i-1; j++){
                if(boxTypes[j][1]<boxTypes[j+1][1]) swap(boxTypes[j], boxTypes[j+1]);//sort boxTypes by element's units in descending order
            }
        }

        for(int i=0, taken=0; i<boxTypes.size(); i++){
            if(truckSize == 0)break;                                                 //IF    truck is full, not able to take any boxes
            taken = truckSize >= boxTypes[i][0] ? boxTypes[i][0] : truckSize;        //IF    truck is able to take all the boxes of current element,
                                                                                     //      then take all of it
                                                                                     //ELSE  take the boxes until truck is full
            truckSize -= taken;                                                      //calculate lefted truck space
            ans += taken * boxTypes[i][1];                                           //add up the total units of current element
        }

        return ans;
    }
};
