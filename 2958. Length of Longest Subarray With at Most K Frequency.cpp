//https://leetcode.com/problems/length-of-longest-subarray-with-at-most-k-frequency/description/

class Solution {
public:
    //ex: nums = [ 1, 2, 3, 1, 2, 3, 1, 2], k = 2
    //                                              frequency = {}              ;  maxLen = 0
    //            LR                                frequency = {1:1}           ;  maxLen = 1
    //            L   R                             frequency = {1:1,2:1}       ;  maxLen = 2
    //            L      R                          frequency = {1:1,2:1,3:1}   ;  maxLen = 3
    //            L         R                       frequency = {1:1,2:1,3:2}   ;  maxLen = 4
    //            L            R                    frequency = {1:1,2:2,3:2}   ;  maxLen = 5
    //            L               R                 frequency = {1:2,2:2,3:2}   ;  maxLen = 6
    //            L                  R              frequency = {1:3,2:2,3:2}   ;  maxLen = 6
    //                                                             ^                            (3 > 2) -> LEFT+1
    //                L              R              frequency = {1:2,2:2,3:2}   ;  maxLen = 6
    //                L                 R           frequency = {1:2,2:3,3:2}   ;  maxLen = 6
    //                                                                 ^                        (3 > 2) -> LEFT+1
    //                   L              R           frequency = {1:2,2:2,3:2}   ;  maxLen = 6
    //                                                                          -> maxLen = 6
    int maxSubarrayLength(vector<int>& nums, int k) {
        map<int, int> frequency;
        int maxLen = 0;

        for(int left=0, right=0; right<nums.size(); ){          // For RIGHT < nums.size
            frequency[nums[right]]++;                           //      nums[RIGHT]'s frequency + 1

            while(frequency[nums[right]] > k && left < right){  //      WHILE nums[RIGHT]'s frequency > K AND
                                                                //            LEFT < RIGHT
                frequency[nums[left]]--;                        //            -> nums[LEFT]'s frequency - 1
                left++;                                         //            -> Goto next LEFT
            }

            maxLen = max(maxLen, right-left+1);                 //      Store the max length
            
            right++;                                            //      Goto next RIGHT
        }

        return maxLen;
    }
};