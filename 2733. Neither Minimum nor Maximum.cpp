//https://leetcode.com/problems/neither-minimum-nor-maximum/description/

class Solution {
public:
    int findNonMinOrMax(vector<int>& nums) {
        int max = INT_MIN;
        int min = INT_MAX;

        for(int i=0;i<nums.size();i++){
            max = nums[i] > max ? nums[i] : max;
            min = nums[i] < min ? nums[i] : min;
        }

        for(int i=0;i<nums.size();i++){
            if(nums[i] != max && nums[i] != min) return nums[i]; //found the number that is not max mor min
        }

        return -1;
    }
};
