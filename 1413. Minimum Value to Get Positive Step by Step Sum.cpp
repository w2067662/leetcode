//https://leetcode.com/problems/minimum-value-to-get-positive-step-by-step-sum/description/

class Solution {
public:
    int minStartValue(vector<int>& nums) {
        int ans=1;  //start searching answer from 1
        int temp;
        bool isAns;

        while(true){
            temp = ans;
            isAns = true;
            for(int i=0;i<nums.size();i++){
                temp+=nums[i];          //calculate step by step sum
                if(temp<1){             //if step by step sum is non-positive
                    isAns = false;      //then this number is not the answer
                    break;
                }
            }
            if(isAns) return ans;       //this number is the answer
            ans++;                      //next number
        }
        return -1;
    }
};
