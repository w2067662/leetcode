//https://leetcode.com/problems/employee-importance/description/

class Solution {
public:
    void getTotal(map<int, Employee*> &map, Employee* curr, int &total){  //get the total from current root
        total += curr->importance;

        for(const auto next: curr->subordinates){
            getTotal(map, map[next], total);
        }
    }

    int getImportance(vector<Employee*> employees, int id) {
        map<int, Employee*> map;
        int total = 0;

        for(const auto employee: employees) map[employee->id] = employee; //store the employees into map

        getTotal(map, map[id], total);                                    //get the total importance of employee's ID

        return total;
    }
};

/*
// Definition for Employee.
class Employee {
public:
    int id;
    int importance;
    vector<int> subordinates;
};
*/
