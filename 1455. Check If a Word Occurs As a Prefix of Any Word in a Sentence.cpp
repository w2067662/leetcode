//https://leetcode.com/problems/check-if-a-word-occurs-as-a-prefix-of-any-word-in-a-sentence/description/

class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        vector<string> words;
        string temp = "";

        for(int i=0;i<sentence.length();i++){            //store the words into vector
            if(sentence[i] == ' '){
                words.push_back(temp);
                temp = "";
            }else{
                temp += sentence[i];
            }
        }
        if(temp != "")words.push_back(temp);

        for(int i=0;i<words.size();i++){
            if(searchWord.length() <= words[i].length()){ 
                if(searchWord == words[i].substr(0, searchWord.length())) return i+1; //check current word has prefix equals to searchWord or not
            }
        }

        return -1;
    }
};
