//https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if(!head || !head->next) return head;   //if list length<2 return list
        
        ListNode* first = head; 
        ListNode* second = head;

        second = second->next;  //list length atleast 2, set first at head node, set second at head->next node

        while(second->next){    //if second don't have next, end the loop
            if(first->val == second->val){  //if first->val is equal to second->val, 
                first->next=second->next;   //first->next will be second->next, 
                second=second->next;        //second will be second->next
            }
            else{   //if first->val not equal to second->val, first and second go to next
                first=first->next;
                second=second->next;
            }
        }

        //check for the end
        //ex: list = head->...->1->1->NULL
        //    list = head->...->1->NULL
        if(first->val == second->val){  
            first->next=NULL;
        }

        return head;
    }
};
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
