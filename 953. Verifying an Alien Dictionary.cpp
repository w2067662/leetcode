//https://leetcode.com/problems/verifying-an-alien-dictionary/description/

class Solution {
private:
    map<char, int> map; //storing priority of letters

    bool compare(string& a, string& b){                 //compare string for sorting
        int minLen = min(a.length(), b.length());

        for(int i=0;i<minLen;i++){                      //compare letters by alien order
            if(map[a[i]] < map[b[i]])return true;
            if(map[a[i]] > map[b[i]])return false;
        }

        if(a.length() < b.length())return true;         //'∅' is smaller than any letters 
        else if (a.length() > b.length())return false;
        else return true;
    }
public:
    bool isAlienSorted(vector<string>& words, string order) {
        vector<string> sorted(words);               //copy vector from origin

        int i=0;
        for(const auto ch: order) map[ch] = i++;    //store the priority of letters from order into map

        sort(sorted.begin(), sorted.end(), [this](string& a, string& b){return compare(a, b);}); //sort the words in lexicographical rules

        for(int i=0;i<words.size();i++){
            if(words[i] != sorted[i])return false;  //check the original vector is alien sorted or not
        }
        
        return true;
    }
};
