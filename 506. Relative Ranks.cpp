//https://leetcode.com/problems/relative-ranks/description/

class Solution {
public:
    vector<string> findRelativeRanks(vector<int>& score) {
        vector<string> ans (score.size(), "");   // Create a vector<string> as same size as score for storing answers
        map<int, int> map;
        
        for(int i=0; i<score.size(); i++){
            map.insert({score[i], i});           // map = [{score, index}]
        }

        int rank = score.size();
        for(const auto& element: map){                              // For element from the lowest score to the highest score
            switch (rank){
                case 1:
                    ans[element.second] += "Gold Medal";   break;   //      IF 1st -> "Gold Medal"
                case 2:
                    ans[element.second] += "Silver Medal"; break;   //      IF 2nd -> "Silver Medal"
                case 3:
                    ans[element.second] += "Bronze Medal"; break;   //      IF 3rd -> "Bronze Medal"
                default:
                    ans[element.second] += to_string(rank);         //      Others -> "n"
            }
            rank--;                                                 //      Goto next higher score
        }

        return ans;
    }
};
