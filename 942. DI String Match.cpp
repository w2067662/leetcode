//https://leetcode.com/problems/di-string-match/description/

class Solution {
public:
    //ex1: s = "IDID"
    //[step 1] find 'I' and store count into ans[index] (from left to right)
    //          ^      index = 0 ; count = 0  -> ans = [0, -, -, -, -]
    //            ^    index = 2 ; count = 1  -> ans = [0, -, 1, -, -]
    //[step 2] store count into last element in ans
    //                 index = 5 ; count = 2  -> ans = [0, -, 1, -, 2]
    //[step 3] find 'D' and store count into ans[index] (from right to left)
    //             ^   index = 4 ; count = 3  -> ans = [0, -, 1, 3, 2]
    //           ^     index = 1 ; count = 4  -> ans = [0, 4, 1, 3, 2]
    //ans = [0, 4, 1, 3, 2]
    //
    vector<int> diStringMatch(string s) {
        vector<int>ans (s.length()+1);
        int count=0;
        //[step 1]
        for(int i=0;i<s.length();i++){
            if(s[i]=='I'){
                ans[i]=count;
                count++;
            }
        }
        //[step 2]
        ans[ans.size()-1]=count;
        count++;
        //[step 3]
        for(int i=s.length()-1;i>=0;i--){
            if(s[i]=='D'){
                ans[i]=count;
                count++;
            }
        }

        return ans;
    }
};
