//https://leetcode.com/problems/find-first-palindromic-string-in-the-array/description/

class Solution {
public:
    bool isPalindrome(string s){                    //check is palindrome or not
        for(int i=0;i<s.length()/2;i++){
            if(s[i] != s[s.length()-i-1])return false;
        }
        return true;
    }

    string firstPalindrome(vector<string>& words) {
        for(const auto word: words){
            if(isPalindrome(word)) return word;     //return the first palindrome
        }

        return "";
    }
};
