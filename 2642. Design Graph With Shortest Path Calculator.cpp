//https://leetcode.com/problems/design-graph-with-shortest-path-calculator/description/

class Graph {
private:
    vector<vector<pair<int, int>>> adjacencyList;

public:
    Graph(int n, vector<std::vector<int>>& edges) {
        adjacencyList.resize(n);
        for(const auto edge: edges) adjacencyList[edge[0]].push_back({edge[1], edge[2]});
    }

    void addEdge(vector<int> edge) {
        adjacencyList[edge[0]].push_back({edge[1], edge[2]});
    }

    int shortestPath(int from, int to) {
        return dijkstra(from, to);
    }

    //-----------------------
    //  Supporting function
    //-----------------------

    int dijkstra(int from, int to) {
        vector<int> distances(this->adjacencyList.size(), INT_MAX);           //initialize distance of all nodes as internal (INT_MAX)
        distances[from] = 0;                                                  //set distance of FROM node as 0

        int curr, next, cost;                                                 //set up params

        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<>> pq; //use priority queue as container (sort by edge cost in ascending order)
        pq.push({0, from});                                                   //push FROM node into priority queue 

        while (!pq.empty()) {                                                 //WHILE  priority queue not empty
            curr = pq.top().second;                                           //       get current node from top of priority queue
            cost = pq.top().first;                                            //       get current cost from top of priority queue
            pq.pop();                                                         //       pop top element from  priority queue

            if(cost > distances[curr]) continue;                              //       IF  current cost is not smaller than distance of current node
                                                                              //           -> get next node

            if(curr == to) return cost;                                       //       IF  found TO node
                                                                              //           -> return shortest cost

            for (auto edge: adjacencyList[curr]) {                            //       traverse all the edges of current node to next node
                next = edge.first;
                cost = edge.second;

                int newRouteCost = cost + distances[curr];                    //       calculate new route's cost

                if (newRouteCost < distances[next]) {                         //       IF  newRouteCost is smaller than distance of next node
                    distances[next] = newRouteCost;                           //           -> assign newRouteCost to distance of next node
                    pq.push({newRouteCost, next});                            //           -> push next node into priority queue
                }
            }
        }

        return distances[to] == INT_MAX ? -1 : distances[to];                 //IF  path no exist -> return -1
    }
};
