//https://leetcode.com/problems/sorting-the-sentence/description/

class Solution {
public:
    string sortSentence(string s) {
        map<int, string> map;   //map = {[index]: [word]}
        string ans ="";
        string temp ="";

        for(int i=0;i<s.length(); i++){
            if(s[i] == ' '){                                                  //if encounter ' ',
                map[temp[temp.length()-1]] = temp.substr(0, temp.length()-1); //store the word into map by index
                temp ="";
            }else{
                temp += s[i];
            }
        }
        if(temp != "") map[temp[temp.length()-1]]= temp.substr(0, temp.length()-1); //store the last word

        for(const auto it:map){
            ans += it.second + " "; //appead the word with after space from map to answer
        }

        return ans.substr(0, ans.length()-1); //return the answer string without last space
    }
};
