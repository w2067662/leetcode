//https://leetcode.com/problems/check-if-numbers-are-ascending-in-a-sentence/description/

class Solution {
public:
    bool checkAscending(vector<int> vec){   //check ascending order
        int max=0;
        for(int i=0;i<vec.size();i++){
            if(vec[i]<=max) return false;
            else max=vec[i];
        }
        return true;
    }
    bool areNumbersAscending(string s) {
        vector<int> vec;
        int number=0;
        for(int i=0;i<s.length();i++){
            if(s[i]==' '){      //if encounter ' ', then store the number into vector
                if(number!=0){
                    vec.push_back(number);
                    number=0;
                }
            }
            else if(isdigit(s[i])){     //if char is valid digit, add up numbers
                number =number*10+s[i]-'0';
            }
        }
        if(number!=0){      //last char might be digit, ex:"word 1"
            vec.push_back(number);
            number=0;
        }
        return checkAscending(vec); //check ascending order
    }
};
