//https://leetcode.com/problems/binary-tree-right-side-view/description/

class Solution {
private:
    vector<vector<int>> ListByLevel;
public:
    void toLevelList(TreeNode* root, int level){  //re-construct tree to 2D list by level
        if(!root) return;

        if(level >= ListByLevel.size()) ListByLevel.push_back({root->val});
        else ListByLevel[level].push_back(root->val);

        toLevelList(root->left , level+1);
        toLevelList(root->right, level+1);

        return;
    }

    vector<int> rightSideView(TreeNode* root) {
        vector<int> vec;

        toLevelList(root, 0);
        for(const auto list: ListByLevel) vec.push_back(list[list.size()-1]); //push the last element from each sublist into vector

        return vec;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
