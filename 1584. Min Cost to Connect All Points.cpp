//https://leetcode.com/problems/min-cost-to-connect-all-points/description/

class DisjointSet { //for connecting nodes
private:
    vector<int> set;//sets
public:
    DisjointSet(int n){                                   //constructer
	    for (int x=0; x<n; x++)set.push_back(x);          //store N nodes into DisjointSet
    }

    int find(int x){                                      //find node X and return the belonged set
	    return x == set[x] ? x : (set[x] = find(set[x])); //shorten the path when finding nodes
    }

    bool equivalence(int x, int y){                       //check two nodes are connected
	    return find(x) == find(y);                        //if two nodes are in the same set, -> connected
    }

    void merge(int x, int y){                             //merge two set togather
	    set[find(x)] = find(y);                           //set X's belonged set as Y's belonged set
    }
};

class Solution {
public:
    int minCostConnectPoints(vector<vector<int>>& points) { 
        map<int, vector<pair<int, int>>> weightedGraph;   //weightedGraph = { [cost]: vector{ {node1, node2}, {node2, node3} } }
        DisjointSet disjointSet(points.size());           //create DisjointSet and initialize
        int cost;
        int ans = 0;

        for(int i=0;i<points.size()-1;i++){
            for(int j=i+1;j<points.size();j++){
                if(i!=j){
                    cost = abs(points[i][0]-points[j][0]) + abs(points[i][1]-points[j][1]);//count for two nodes' manhattan distance (cost)
                    weightedGraph[cost].push_back({i, j});                                 //store edge = {cost :{node1, node2}} into weightedGraph
                }
            }
        }

        for(const auto it:weightedGraph){                                                  //traverse the edge from the lowest cost edge to the highest cost edge 
            for(int i=0;i<it.second.size();i++){
                if(!disjointSet.equivalence(it.second[i].first, it.second[i].second)){     //if two nodes are not connected (in the same set)
                    ans += it.first;                                                       //answer app up current node's cost
                    disjointSet.merge(it.second[i].first, it.second[i].second);            //connect two nodes and make them in a same set
                }
            }
        }

        return ans;
    }
};
