//https://leetcode.com/problems/number-of-good-pairs/description/

class Solution {
public:
    int numIdenticalPairs(vector<int>& nums) {
        map<int, int>map;
        int ans=0;

        for(int i=0;i<nums.size();i++){
            map[nums[i]]++;
        }

        for(const auto it:map){
            ans+= it.second * (it.second-1) / 2; //C(n, 2)
        }

        return ans;
    }
};
