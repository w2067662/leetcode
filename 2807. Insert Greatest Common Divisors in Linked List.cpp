//https://leetcode.com/problems/insert-greatest-common-divisors-in-linked-list/description/

class Solution {
public:
    int GCD(int a, int b){ //get the greatest common divisor from 2 numbers
        if(a < b) swap(a, b);

        while(a%b != 0){
            a %= b;
            swap(a, b);
        }

        return b;
    }

    ListNode* insertGreatestCommonDivisors(ListNode* head) {
        ListNode* curr = head;

        while(curr && curr->next){                                                  //IF exist current node and exist next node
            curr->next = new ListNode(GCD(curr->val, curr->next->val), curr->next); //   -> create a node with GCD value of current and next node and insert between
            curr = curr->next->next;                                                //   -> goto next next node
        }

        return head;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
