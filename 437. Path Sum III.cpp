//https://leetcode.com/problems/path-sum-iii/description/

class Solution {
public:
    int hasPathSum(vector<int> &path, const int &targetSum){ //count for path sum that equals to targetSum
        long long sum, count = 0;
        for(int i=0; i<path.size(); i++){
            sum = 0;
            for(int j=i; j<path.size();j++){
                sum += path[j];
            }
            if(sum == (long long)targetSum) count++;
        }
        return count;
    }

    void traverse(TreeNode* root, const int &targetSum, vector<int> &path, int &count){
        if(!root) return;

        path.push_back(root->val);                      //push current node value into path
        traverse(root->left , targetSum, path, count);  //traverse left  subtree

        count += hasPathSum(path, targetSum);           //count the possible path sets that has sum = targetSum

        traverse(root->right, targetSum, path, count);  //traverse right subtree
        path.pop_back();                                //pop current node value from path

        return;
    }

    int pathSum(TreeNode* root, int targetSum) {
        int count = 0;
        vector<int> path;

        traverse(root, targetSum, path, count); //traverse the tree

        return count;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
