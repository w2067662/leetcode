//https://leetcode.com/problems/maximum-ascending-subarray-sum/description/

class Solution {
public:
    int maxAscendingSum(vector<int>& nums) {
        int last = 0;
        int currSum = 0;
        int ans = 0;

        for(const auto num: nums){
            if(last < num)currSum += num; //IF    is ascending -> currSum add up number
            else currSum = num;           //ELSE               -> restart adding

            last = num;

            ans = max(ans, currSum);      //store max current sum to find max Ascending Sum
        }

        return ans;
    }
};
