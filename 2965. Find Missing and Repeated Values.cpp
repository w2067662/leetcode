//https://leetcode.com/problems/find-missing-and-repeated-values/description/

class Solution {
public:
    vector<int> findMissingAndRepeatedValues(vector<vector<int>>& grid) {
        map<int, int> map;
        int missing = 1, repeated = -1;

        for(const auto col: grid){
            for(const auto num: col){
                map[num]++;                         //store the numbers into map
            }
        }

        for(const auto it: map){
            if(it.second >= 2) repeated = it.first; //get the repeated number
            if(it.first == missing) missing++;      //get the missing  number
        }

        return {repeated, missing};
    }
};
