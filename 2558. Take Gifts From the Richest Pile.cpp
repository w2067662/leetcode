//https://leetcode.com/problems/take-gifts-from-the-richest-pile/description/

class Solution {
public:
    long long pickGifts(vector<int>& gifts, int k) {
        long long ans=0;

        for(int i=0;i<k;i++){                                   //do following action for K times
            sort(gifts.begin(), gifts.end());                   //srot the vector in ascending order
            gifts[gifts.size()-1] = sqrt(gifts[gifts.size()-1]);//calculate and assign the largest element with its value's SQRT
        }

        for(int i=0;i<gifts.size();i++){
            ans += gifts[i];                                    //sum up the processed gifts
        }

        return ans;
    }
};
