//https://leetcode.com/problems/most-frequent-subtree-sum/description/

class Solution {
public:
    int sum(TreeNode* root){    //sum up the subtree's value
        return !root ? 0 : sum(root->left) + sum(root->right) + root->val;
    }

    void traverse(TreeNode* root, unordered_map<int, int> &umap){ //traverse the tree
        if(!root)return;

        umap[sum(root)]++; //insert the sum of current subtree into umap

        traverse(root->left , umap);
        traverse(root->right, umap);
        return;
    }

    vector<int> findFrequentTreeSum(TreeNode* root) {
        unordered_map<int, int> umap;
        map<int, vector<int>, greater<int>> map;
        vector<int> ans;

        traverse(root, umap);                   //traverse the tree

        for(const auto it: umap){
            map[it.second].push_back(it.first); //store the sum and frequency from umap into map
        }

        return map.begin()->second;             //return the sum that appears the most frequenctly
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
