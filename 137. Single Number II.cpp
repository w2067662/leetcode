//https://leetcode.com/problems/single-number-ii/description/

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int single = nums[nums.size()-1];            //set single number as last number in vector

        for(int i=0; i+2<nums.size()-1; i+=3){       //traverse the vector with 3 numbers in a set
            if(nums[i] != nums[i+1]) return nums[i]; //IF  current number != current+1 number -> current number must be single
        }
        
        return single;
    }
};
