//https://leetcode.com/problems/flip-equivalent-binary-trees/description/

class Solution {
public:
    bool flipEquiv(TreeNode* root1, TreeNode* root2) {
        if (!root1 && !root2) return true;          //IF   both roots are NULL -> TRUE
        if (!root1 || !root2) return false;         //IF   only 1 root is NULL -> FALSE
        if (root1->val != root2->val) return false; //IF   2 roots have different value -> FALSE
                                                    //IF   2 roots are not NULL AND have same value
        return (flipEquiv(root1->left, root2->left ) && flipEquiv(root1->right, root2->right)) || // -> check subtree
               (flipEquiv(root1->left, root2->right) && flipEquiv(root1->right, root2->left));    // -> flip and check subtree
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
