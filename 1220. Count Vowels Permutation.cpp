//https://leetcode.com/problems/count-vowels-permutation/description/

class Solution {
private:
    typedef unordered_map<char, long long> VowelContainer;
    const int MOD = 1e9 + 7;
public:
    //a -> e
    //e -> a, i
    //i -> a, e, o, u
    //o -> i, u
    //u -> a
    //
    //(a,e,i,o,u) -> (3a,2e,2i,1o,2u) -> (6a,5e,3i,2o,3u) -> ...
    //
    int countVowelPermutation(int n) {
        VowelContainer vc1, vc2;

        //calculate vowal map for first time
        vc1['a'] = 1;
        vc1['e'] = 1;
        vc1['i'] = 1;
        vc1['o'] = 1;
        vc1['u'] = 1;

        for(int i=1; i<n; i++){ //calculate next vowal map for N-1 times
            vc2['a'] =  (vc1['e']);
            vc2['e'] =  (vc1['a'] + vc1['i']) % MOD;
            vc2['i'] =  (vc1['a'] + vc1['e'] + vc1['o'] + vc1['u']) % MOD;
            vc2['o'] =  (vc1['i'] + vc1['u']) % MOD;
            vc2['u'] =  (vc1['a']);

            swap(vc1, vc2);     //swap VowelContainers
        }

        return (vc1['a'] + vc1['e'] + vc1['i'] + vc1['o'] + vc1['u']) % MOD;
    }
};
