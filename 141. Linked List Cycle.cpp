//https://leetcode.com/problems/linked-list-cycle/description/

class Solution {
public:
    bool hasCycle(ListNode *head) {
        if(head==nullptr)return false;
        
        ListNode *slow=head;
        ListNode *fast=head;
        
        while(fast->next&&fast->next->next){
            slow=slow->next;
            fast=fast->next->next;
            
            if(fast==slow)return true; //if fast = slow, there is cycle
        }
        return false;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
