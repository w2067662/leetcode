//https://leetcode.com/problems/implement-trie-prefix-tree/description/

class Trie {
private:
    class TrieNode{ //define TrieNode
        public:
            char ch;
            map<char, TrieNode*> children; //with multiple children node
            TrieNode(){};
            TrieNode(char c):ch(c){};
    };
    
    TrieNode* root;
public:
    Trie() {
        this->root = new TrieNode; //initialize with creating new root
    }
    
    void insert(string word) {
        insertWord(this->root, word, 0);
    }
    
    bool search(string word) {
        return searchWord(this->root, word, 0);
    }
    
    bool startsWith(string prefix) {
        return searchPrefix(this->root, prefix, 0);
    }
    //--------------------------
    //   Supporting functions
    //--------------------------
    void insertWord(TrieNode* root, const string& word, int index){
        if(index >= word.length()){
            root->children['$'] = new TrieNode('$');                  //set the string end as '$'
            return;
        } 

        if(root->children.find(word[index]) == root->children.end()){ //IF   no child node's value match current char
            root->children[word[index]] = new TrieNode(word[index]);  //     -> create a new child node with current char value
        }
        insertWord(root->children[word[index]], word, index+1);       //insert next char
    }

    bool searchWord(TrieNode* root, const string& word, int index){
        if(index >= word.length()){
            return root->children.find('$') != root->children.end();  //IF   no child node's value = '$'
        }                                                             //     -> not contain this word in Trie

        if(root->children.find(word[index]) == root->children.end()) return false;//IF   not contain current char -> FALSE
        return searchWord(root->children[word[index]], word, index+1);
    }

    bool searchPrefix(TrieNode* root, const string& word, int index){
        if(index >= word.length()) return true;                       //IF   search the word until the end -> there exist word with this prefix in the Trie -> TRUE

        if(root->children.find(word[index]) == root->children.end()) return false;
        return searchPrefix(root->children[word[index]], word, index+1);
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
