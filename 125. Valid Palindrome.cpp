//https://leetcode.com/problems/valid-palindrome/description/

class Solution {
public:
    bool validPalindrome(string s) {    //check for Palindrome
        for(int i=0;i<s.length()/2;i++){
            if(s[i]!=s[s.length()-i-1]) return false;
        }
        return true;
    }
    bool isPalindrome(string s) {
        string phrase ="";

        //reprocessing phrase
        //ex:   s      = "phrase 1: This is a Huge and Scary Dog."
        //      phrase = "phrase1thisisahugeandscarydog"
        for(int i=0;i<s.length();i++){ 
            if(islower(s[i]) || isupper(s[i])){
                if(isupper(s[i])) s[i] = tolower(s[i]);
                phrase+=s[i];
            }
            else if(isdigit(s[i])){
                phrase+=s[i];
            }
        }
        return validPalindrome(phrase);     //check for Palindrome
    }
};
