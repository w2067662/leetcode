//https://leetcode.com/problems/find-unique-binary-string/description/

class Solution {
public:
    string findDifferentBinaryString(vector<string>& nums) {
        set<string> set;
        string temp = "";

        for(const auto num: nums) set.insert(num);           //insert all the binary numbers into set

        for(int i=0; i<nums[0].length(); i++) temp += "0";   //start from binary number with all bits are 0

        while(true){
            for(int i=temp.length()-1; i>=1; i--){
                if(temp[i] >= '2'){                          //check carry from right most bit to left
                    temp[i] = '0';
                    temp[i-1]++;
                } else break;
            }

            if(set.find(temp) == set.end())return temp;      //IF current temp string is different (not in set) -> return current string

            temp[temp.length()-1]++;                         //right most bit + 1
        }

        return "";
    }
};
