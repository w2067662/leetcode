//https://leetcode.com/problems/check-if-binary-string-has-at-most-one-segment-of-ones/description/

class Solution {
public:
    bool checkOnesSegment(string s) {
        int switched = false;   

        for(int i=0;i<s.length();i++){
            if(s[i] == '0' && !switched) switched = true;   //IF   '1' -> '0', set switched = TRUE
            else if(s[i] == '1' && switched) return false;  //IF   encounter '1' AND is already switched
        }                                                   //     -> FALSE

        return true;
    }
};
