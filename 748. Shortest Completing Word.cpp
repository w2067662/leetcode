//https://leetcode.com/problems/shortest-completing-word/description/

class Solution {
public:
    string shortestCompletingWord(string licensePlate, vector<string>& words) {
        map<char, int>map;
        unordered_map<char, int>word_map;

        for(int i=0;i<words.size();i++){             //sort the vector in word-length ascending order by bubble sort
            for(int j=0;j<words.size()-1-i;j++){
                if (words[j].length() > words[j+1].length()) swap(words[j], words[j+1]);
            }
        }

        for(int i=0;i<licensePlate.length();i++){    //store the appear times of letters from license plate into map
            if(islower(licensePlate[i]) || isupper(licensePlate[i])){
                map[tolower(licensePlate[i])]++;
            }
        }

        for(int i=0;i<words.size();i++){             //traverse words from the vector
            for(int j=0;j<words[i].length();j++){    //store the letters of current word into word_map
                word_map[words[i][j]]++;
            }

            bool isComplete=true;
            for(const auto&letter: map){                                   //compare map and word_map
                if(word_map[letter.first]<letter.second) isComplete=false; //if current word not contain all the letters appear in license plate, 
            }                                                              //then current word is incomplete
            if(isComplete) return words[i];          //if current word is complete, return as answer
            word_map.clear();                        //if not, then clear the map and goto next word
        }
        return "";
    }
};
