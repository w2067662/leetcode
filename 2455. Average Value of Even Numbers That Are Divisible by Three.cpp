//https://leetcode.com/problems/average-value-of-even-numbers-that-are-divisible-by-three/description/

class Solution {
public:
    int averageValue(vector<int>& nums) {
        int ans=0;
        int count=0;

        for(int i=0;i<nums.size();i++){     //find even numbers divisible by 3
            if(nums[i]%6 == 0){
                ans+=nums[i];
                count++;                    //count for possible numbers
            } 
        }
        return count!=0 ? ans/count : 0;    //if there is no even numbers divisible by 3 -> answer = 0
    }
};
