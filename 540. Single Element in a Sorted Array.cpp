//https://leetcode.com/problems/single-element-in-a-sorted-array/description/

class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int single = nums[nums.size()-1];             //set the single as last number

        for(int i=0; i<nums.size()-1; i+=2){          //travere i (always even) from 0 to N-2
            if(nums[i] != nums[i+1]) return nums[i];  //IF nums[i] != nums[i+1] -> nums[i] must be the single number
        }                                             //                        -> return nums[i]

        return single;
    }
};
