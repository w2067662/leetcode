//https://leetcode.com/problems/check-if-a-string-can-break-another-string/description/

class Solution {
public:
    bool canBreak(string &s1, string &s2){
        if(s1.length() != s2.length()) return false; //IF  s1 and s2 has different length -> FALSE

        for(int i=0; i<s1.length(); i++){
            if(s1[i] < s2[i]) return false;          //IF  s1's current char < s2's current char -> FALSE
        }

        return true;
    }

    bool checkIfCanBreak(string s1, string s2) {
        sort(s1.begin(), s1.end());       //sort the char in ascending order
        sort(s2.begin(), s2.end());       //sort the char in ascending order

        if(canBreak(s1, s2)) return true; //check s1 can break s2
        if(canBreak(s2, s1)) return true; //check s2 can break s1

        return false;
    }
};
