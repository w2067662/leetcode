//https://leetcode.com/problems/determine-if-two-events-have-conflict/description/

class Solution {
public:
    int timeStringToInteger(string s){                      //change time string to integer
        return (s[0]*10 + s[1]) * 60 + (s[3]*10 + s[4]);
    }
    bool haveConflict(vector<string>& event1, vector<string>& event2) {
        //have Conflict if
        //      Event 2's start time NOT bigger  than Event 1's end   time  AND
        //      Event 2's end   time NOT smaller than Event 1's start time
        return !(timeStringToInteger(event2[0]) > timeStringToInteger(event1[1])) && 
               !(timeStringToInteger(event2[1]) < timeStringToInteger(event1[0]));   
    }
};
