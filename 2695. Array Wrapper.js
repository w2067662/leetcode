//https://leetcode.com/problems/array-wrapper/description/

/**
 * @param {number[]} nums
 */
var ArrayWrapper = function(nums) {
    this.nums = nums;   //[1,2] -> [Object]
};

ArrayWrapper.prototype.valueOf = function() {
    return this.nums.reduce((acc,num) => acc + num, 0);  //sum up all the numbers in the array
}                                                 //^ initial value
//arr.reduce(callback[accumulator, currentValue, currentIndex(optional), array(optional)], initialValue(optional))

ArrayWrapper.prototype.toString = function() {
    return `[${this.nums.join(',')}]`;  //  [1,2]  ->   "1,2"    ->  "[1,2]"
}                                       //(number) -> (join ',') -> (string with brace)



/**
 * const obj1 = new ArrayWrapper([1,2]);
 * const obj2 = new ArrayWrapper([3,4]);
 * obj1 + obj2; // 10
 * String(obj1); // "[1,2]"
 * String(obj2); // "[3,4]"
 */
