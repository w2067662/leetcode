//https://leetcode.com/problems/minimum-operations-to-collect-elements/description/

class Solution {
public:
    int minOperations(vector<int>& nums, int k) {
        set<int> set;
        int count = k, ans = 0;

        for(int i=nums.size()-1;i>=0;i--){
            ans++;                //count for operation times
            if( 1 <= nums[i] && nums[i] <= k && set.find(nums[i]) == set.end()) count--; //IF  current number is within 1-K -> found 1 number, count-1
            if(count == 0) break; //IF  all numbers within 1-K is found -> end the loop
            set.insert(nums[i]);  //insert current number into set
        }

        return ans;
    }
};
