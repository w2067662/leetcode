//https://leetcode.com/problems/special-array-with-x-elements-greater-than-or-equal-x/description/

class Solution {
public:
    int specialArray(vector<int>& nums) {
        for(int x=1, count; x<=nums.size(); x++){   // For x start from 1 to nums.size
            count = 0;

            for(int i=0; i<nums.size(); i++){       //      Traverse the elements in vector
                if(nums[i] >= x) count++;           //          IF element's value >= x -> count + 1
            }

            if(x == count) return x;                //      IF x = count -> exist x that x elements in vector is >= x
        }

        return -1;                                  // No possible x exist
    }
};
