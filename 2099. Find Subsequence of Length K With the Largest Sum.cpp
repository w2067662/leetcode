//https://leetcode.com/problems/find-subsequence-of-length-k-with-the-largest-sum/description/

class Solution {
public:
    vector<int> maxSubsequence(vector<int>& nums, int k) {
        map<int, int> map;
        vector<int> ans, copied(nums);                   //copy the vector

        sort(copied.begin(), copied.end());              //sort the copied vector in ascending order to get subsequence with max sum with K length

        for(int i=copied.size()-k; i<copied.size(); i++){
            map[copied[i]]++;                            //store the numbers from subsequence into map
        }

        for(const auto num: nums){
            if(map[num] > 0){                            //IF   encounter the number that is store in the map
                map[num]--;
                ans.push_back(num);                      //     -> push into answer (follow the original index)
            }
        }

        return ans;
    }
};
