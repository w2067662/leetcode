//https://leetcode.com/problems/perfect-number/description/

class Solution {
public:
    bool checkPerfectNumber(int num) {
        set<int> set;
        int sum=0;
        for(int i=1;i<num;i++){
            if(num%i==0)set.insert(i);  //insert divisibale number into set
        }
        for(const auto& number:set){    //add up numbers from set
            sum+=number;
        }
        return sum == num;  //check for perfect number
    }
};
