//https://leetcode.com/problems/number-complement/description/

class Solution {
public:
    string reverse(string s){
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    int findComplement(int num) {
        string bitString="";
        int bit = 0;
        int complementNum = 0;

        while(num>0){               //integer to up-side-down bit string 
            bitString+=num%2+'0';   //ex: num = 6 -> bitString = "011"
            num/=2;
        }

        bitString = reverse(bitString); //reverse string
                                        //bitString = "011" -> bitString = "110"
        for(int i=0;i<bitString.length();i++){
            if(bitString[i] == '1')bit=0;        //complment of bitString
            else if (bitString[i] == '0')bit=1;  //bitString = "110" -> bit = 001(base2)

            complementNum = complementNum*2 + bit;  //bit to complment integer
                                                    //bit = 001(base2) -> complementNum = 1
        }

        return complementNum;
    }
};
