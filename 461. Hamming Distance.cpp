//https://leetcode.com/problems/hamming-distance/description/

class Solution {
public:
    int hammingDistance(int x, int y) {
        bitset<32> a=x;
        bitset<32> b=y; //set integer to bitset 

        string str1 = a.to_string();
        string str2 = b.to_string();    //change bitset to string 

        int ans=0;

        for(int i=0;i<str1.size();i++){
            if(str1[i]!=str2[i])ans++;  //compare 0 and 1, if different then ans+1
        }

        return ans;
    }
};
