#https://leetcode.com/problems/sales-person/description/

# Write your MySQL query statement below
SELECT name 
FROM SalesPerson  
WHERE name NOT IN                                                            #the name not include in following conditions
    (SELECT S.name FROM SalesPerson S, Company C, Orders O 
     WHERE S.sales_id = O.sales_id AND C.com_id = O.com_id AND C.name='RED') #have order with company "RED"
