//https://leetcode.com/problems/range-addition-ii/description/

class Solution {
public:
    int maxCount(int m, int n, vector<vector<int>>& ops) {
        for(int i=0;i<ops.size();i++){
            if(ops[i][0]<m) m = ops[i][0];      //minify the range of m if there is smaller m
            if(ops[i][1]<n) n = ops[i][1];      //minify the range of n if there is smaller n
        }
        return m*n; //return the amount of elements in minifized range
    }
};
