//https://leetcode.com/problems/make-the-string-great/description/

class Solution {
private:
    bool greater(string& s){    // Make string Great
        bool makeGreater = false;
        string ans = "";

        for(int i=1; i<s.length(); i++){            // For i start from 1 to string's end (compare i-1 and i)
            if(abs(s[i]-s[i-1]) == abs('A'-'a')){   //      IF found two adjacent chars the same but one in upper case and one in lower case 
                makeGreater = true;                 //         Have made current string good -> check again in next recursion
                s[i]=' ';                           //         Set s[i] empty
                s[i-1]=' ';                         //         Set s[i-1] empty
            }
        }

        for(auto& ch: s){            // For all the chars
            if(ch != ' ') ans += ch; //      IF current char is not ' ', append the char to answer
        }

        s = ans;                     // Replace current string with greater string 

        return makeGreater;          // IF current string have been made greater -> return TRUE  (check again in next recursion)
                                     // IF current is the greatest               -> return FALSE (end the while loop)
    }

public:
    string makeGood(string s) {
        if(s.length() == 1) return s;
        
        while(greater(s)){} // Do recursion using while loop to make the string the greatest

        return s;
    }
};