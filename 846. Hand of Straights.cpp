//https://leetcode.com/problems/hand-of-straights/description/

class Solution {
public:
    // ex1:  hand = [1,2,3,6,2,3,4,7,8] ; groupSize = 3 

    //              1 2 3 4 5 6 7 8
    //        map = 1 2 2 1 0 1 1 1   (remove [1,2,3])
    //     -> map = 0 1 1 1 0 1 1 1   (remove [2,3,4])
    //     -> map = 0 0 0 0 0 1 1 1   (remove [6,7,8])
    //     -> map = 0 0 0 0 0 0 0 0
    //                                => TRUE

    // ex2:  hand = [1,2,3,4,5] ; groupSize = 4

    //              1 2 3 4 5
    //        map = 1 1 1 1 1         (remove [1,2,3])
    //     -> map = 0 0 0 1 1
    //                                => FALSE

    bool isNStraightHand(vector<int>& hand, int groupSize) {
        if(hand.size()%groupSize != 0){      // IF  amount of hand numbers is unable to split into N groups
            return false;                    //     -> FALSE
        }

        map<int, int> map;

        for(const auto& num: hand){
            map[num]++;                      // Store the frequency of each number
        }

        for(auto& it: map){                  // Traverse the numbers within map in ascending order
            if(it.second < 0){               //      IF current number's frequency is negative
                return false;                //         -> FALSE
            }

            int deduct = map[it.first];      //      Calculate the frequency of first number in current group
            map[it.first] -= deduct;         //      Deduct the frequency of current number

            for(int i=1; i<groupSize; i++){  //      For  i from 1 to groupSize-1
                map[it.first+i] -= deduct;   //           -> deduct the frequency of current+i number

                if(map[it.first+i] == 0){    //           IF current+1 number has frequency = 0
                    map.erase(it.first+i);   //              -> erase it from map
                }
            }
        }

        return true;                         // Possible to rearrange the hand -> TRUE
    }
};