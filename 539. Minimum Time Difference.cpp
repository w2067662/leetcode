//https://leetcode.com/problems/minimum-time-difference/description/

class Solution {
public:
    int timeToInt(string &t){   //convert time string to integar
        return ((t[0]-'0') * 10 + (t[1]-'0')) * 60 + ((t[3]-'0') * 10 + (t[4]-'0'));
    }

    int difference(int &a, int &b){ //calculate difference of 2 time integar
        int diff = abs(a-b);
        return diff >= 720 ? 720 - diff % 720 : diff;
    }

    int findMinDifference(vector<string>& timePoints) {
        vector<int> timeInts;
        int minDiff = INT_MAX;

        for(auto& time: timePoints){
            timeInts.push_back(timeToInt(time));                              //convert time string to integar and push into vector
        }

        for(int i=0; i<timeInts.size()-1; i++){
            for(int j=i+1; j<timeInts.size(); j++){
                minDiff = min(minDiff, difference(timeInts[i], timeInts[j])); //store the min difference of time
            }
        }

        return minDiff;
    }
};
