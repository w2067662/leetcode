//https://leetcode.com/problems/assign-cookies/description/

class Solution {
public:
    //ex: g =[5,4,1,1] -> children = [{1,2}, {4,1}, {5,1}]
    //    s =[3,5,1]   -> cookies  = [{1,1}, {3,1}, {5,1}]  ;  ans  = 0
    //
    // c = {1,1} ; ch = {1,2(-1)} -> c = {1,0} ; ch = {1,1} -> ans += 1
    // c = {3,1} ; ch = {1,1(-1)} -> c = {3,0} ; ch = {1,0} -> ans += 1
    // c = {5,1} ; ch = {1,0} -> next ch
    // c = {5,1} ; ch = {4,1(-1)} -> c = {5,0} ; ch = {4,0} -> ans += 1
    //      
    int findContentChildren(vector<int>& g, vector<int>& s) {
        map<int, int>children, cookies;
        int ans = 0;

        for(int i=0; i<g.size(); i++) children[g[i]]++;    //store appear times of same greedy into children
        for(int i=0; i<s.size(); i++) cookies[s[i]]++;     //store appear times of same satisfaction in cookies

        for(auto &c: cookies){
            for(auto &ch: children){
                if(c.first >= ch.first && ch.second != 0){ //IF satisfaction > greedy -> offset each other to one is 0
                    if(c.second >= ch.second){
                        c.second -= ch.second;
                        ans += ch.second;
                        ch.second = 0;
                    }
                    else{
                        ch.second -= c.second;
                        ans += c.second;
                        c.second = 0;
                    }
                }
                if(c.second == 0)break;
            } 
        }
        return ans;
    }
};
