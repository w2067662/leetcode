//https://leetcode.com/problems/find-the-distinct-difference-array/submissions/

class Solution {
public:
    vector<int> distinctDifferenceArray(vector<int>& nums) {
        vector<int> vec;
        map<int, int> prefix; //prefix = {[number]: [appear times]}
        map<int, int> suffix; //suffix = {[number]: [appear times]}

        for(int i=0;i<nums.size();i++){ 
            suffix[nums[i]]++;
        }

        for(int i=0;i<nums.size();i++){
            prefix[nums[i]]++;
            suffix[nums[i]]--;
            if (suffix[nums[i]] == 0) suffix.erase(nums[i]);
            vec.push_back(prefix.size()-suffix.size()); //diff[i] = amount of distinct numbers in prefix - amount of distinct numbers in suffix
        }

        return vec;
    }
};
