//https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/

class Solution {
private:
    vector<vector<int>> ListByLevel;

public:
    void toLevelList(TreeNode* root, int level){  // Re-construct tree to 2D list by level
        if(!root) return;

        if(level >= ListByLevel.size()) ListByLevel.push_back({root->val});
        else ListByLevel[level].push_back(root->val);

        toLevelList(root->left , level+1);
        toLevelList(root->right, level+1);

        return;
    }

    void toZiaZag(){    // Zig-zag the list
        for(int i=0; i<ListByLevel.size(); i++){
            if(i%2 != 0){
                for(int j=0; j<ListByLevel[i].size()/2; j++){
                    swap(ListByLevel[i][j], ListByLevel[i][ListByLevel[i].size()-j-1]);
                }
            }
        }
    }

    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        toLevelList(root, 0);   // Construct into level list
        toZiaZag();             // Zig-zag the list
        return ListByLevel;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
