//https://leetcode.com/problems/number-of-pairs-of-interchangeable-rectangles/description/

class Solution {
public:
    long long interchangeableRectangles(vector<vector<int>>& rectangles) {
        map<double, int> map;
        long long count = 0;

        for(const auto rec: rectangles){                    // For all rectangles
            map[((double)rec[0]/(double)rec[1])]++;         //      Store the width/height ratio frequency into map {[width/height ratio]: frequency}
        }

        for(const auto it: map){
            count += (long long)it.second*(it.second-1)/2;  // Calculate the equivalent pairs
        }

        return count;
    }
};
