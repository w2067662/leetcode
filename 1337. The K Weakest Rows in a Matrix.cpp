//https://leetcode.com/problems/the-k-weakest-rows-in-a-matrix/description/

class Solution {
public:
    vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
        vector<pair<int, int>> vec;             //vec = { pair{[soldiers]:[row]} }
        vector<int> ans;

        for(int i=0, count;i<mat.size();i++){   //count for the sim of soldiers of each row
            count = 0;
            for(int j=0;j<mat[i].size();j++){
                if(mat[i][j] == 1)count++;
            }
            vec.push_back({count, i});          //store the values into vector
        }
        
        sort(vec.begin(), vec.end());           //sort the vector in ascending order     

        for(int i=0; i<k;i++){
            ans.push_back(vec[i].second);       //push the first K weakest rows into vector
        }

        return ans;
    }
};
