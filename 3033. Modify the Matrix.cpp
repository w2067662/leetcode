//https://leetcode.com/problems/modify-the-matrix/description/

class Solution {
public:
    vector<vector<int>> modifiedMatrix(vector<vector<int>>& matrix) {
        vector<int> maxNums(matrix[0].size(), -1);

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                maxNums[j] = max(maxNums[j], matrix[i][j]);  // Store the max number of each column
            }
        }

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                if(matrix[i][j] == -1){                      // IF current is -1
                    matrix[i][j] = maxNums[j];               //    -> replace by max number of current column
                }
            }
        }

        return matrix;
    }
};
