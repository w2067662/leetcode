//https://leetcode.com/problems/letter-case-permutation/description/

class Solution {
public:
    //ex: s = "a1b2"
    //         ^          answer = ["a", "A"]
    //          ^         answer = ["a1", "A1"]
    //           ^        answer = ["a1b", "a1B", "A1b", "A1B"]
    //            ^       answer = ["a1b2", "a1B2", "A1b2", "A1B2"]
    //
    vector<string> letterCasePermutation(string s) {
        vector<string> vec1, vec2;

        vec1.push_back("");

        for(int i=0; i<s.length(); i++){
            if(islower(s[i]) || isupper(s[i])){    //IF  current char is lower case or upper case
                vec2.clear();

                for(const auto str: vec1){         //    -> append the lower case and upper case of current char to each string in the vec1
                    string temp = str;
                    temp += tolower(s[i]); 
                    vec2.push_back(temp);
                    temp = str;
                    temp += toupper(s[i]);
                    vec2.push_back(temp);
                }

                vec1 = vec2;
            } else {
                for(auto& str: vec1) str += s[i];  //ELSE append digit to each string in the vec1
            }
        }

        return vec1;
    }
};
