//https://leetcode.com/problems/largest-substring-between-two-equal-characters/description/

class Solution {
public:
    int maxLengthBetweenEqualCharacters(string s) {
        map<char, vector<int>> map;             //map = {[char]: vector{[index1], [index2], ...}}
        int ans = -1;
        int temp;

        for(int i=0;i<s.length();i++){          //store elements into map
            map[s[i]].push_back(i);
        }

        for(auto it:map){
            if(it.second.size() >= 2){                               //if current char appears more than 2 times in string
                sort(it.second.begin(), it.second.end());            //sort the index in ascending order
                temp = it.second[it.second.size()-1]-it.second[0]-1; //calculate the within characters
                ans = temp > ans ? temp : ans;                       //store the laregest length between equal characters
            }
        }

        return ans;
    }
};
