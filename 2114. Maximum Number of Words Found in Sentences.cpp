//https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/description/

class Solution {
public:
    int countWord(string s){    //count for words
        int count = 0;
        for(int i=0; i<s.length(); i++){
            if(s[i] == ' ') count++;
        }

        return count+1;
    }
    int mostWordsFound(vector<string>& sentences) {
        int max=0;

        for(int i=0, words;i<sentences.size();i++){
            words = countWord(sentences[i]);    //count words in sentences
            max = words > max ? words : max;    //store max with max amount of words in a sentence
        }

        return max;
    }
};
