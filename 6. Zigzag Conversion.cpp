//https://leetcode.com/problems/zigzag-conversion/

class Solution {
public:
    string convert(string s, int numRows) {
        if(numRows<=1) return s; //return string if numRows=1

        //store N empty string in vec
        vector<string> vec;
        for(int i=0;i<numRows;i++){
            vec.push_back("");
        }

        //mark char in zigzag pattern, 
        //ex: "THISISTEST", numRows=3
        //     1232123212
        int line =1;
        bool up = true;
        for(int i=0;i<s.length();i++){
            vec[line-1]+=s[i];
            if(up){
                line+=1;
                if(line==numRows) up = false;
            }
            else{
                line-=1;
                if(line==1) up = true;
            }
        }

        //append strings
        string ans="";
        for(int i=0;i<vec.size();i++){
            ans+=vec[i];
        }
        return ans;
    }
};
