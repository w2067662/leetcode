//https://leetcode.com/problems/summary-ranges/description/

class Solution {
private:
    int front, end, temp;
public:
    vector<string> queueToVectorString(queue<pair<int, int>>& queue){
        vector<string> res;
        string temp ="";
        while(!queue.empty()){
            if(queue.front().first == queue.front().second){
                temp = to_string(queue.front().first);
            }
            else{
                temp = to_string(queue.front().first) + "->" + to_string(queue.front().second);
            }
            res.push_back(temp);
            queue.pop();
        }
        return res;
    }
    vector<string> summaryRanges(vector<int>& nums) {
        if(nums.size()==0)return {};

        queue<pair<int, int>>queue;
        //ex:nums  = [0,1,2,4,5,7]
        //   queue = [{0,2}, {4,5}, {7,7}]
        for(int i=0;i<nums.size();i++){
            if(i==0){       //front and temp have to be initialized as first number of nums 
                front = temp = nums[i];
                continue;
            }
            end = nums[i];
            if(end == temp+1) temp++;   //if end equals to temp+1, temp+1(ascending)
            else{
                queue.push({front, temp});
                front = temp = end;     //front and temp set as end
            }
        }
        queue.push({front, temp});

        //[0,2] --> "0->2"
        //[4,5] --> "4->5"
        //[7,7] --> "7"
        return queueToVectorString(queue);  //change queue structure to vector<string>
    }
};
