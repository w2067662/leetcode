//https://leetcode.com/problems/minimum-string-length-after-removing-substrings/description/

class Solution {
public:
    bool reformed(string& s){   //reform the string by removing "AB" OR "CD"
        bool isReformed = false;
        string temp = "";

        for(int i=1; i<s.length();i++){
            if((s[i-1] == 'A' && s[i] == 'B') || (s[i-1] == 'C' && s[i] == 'D')){
                isReformed = true;
                s[i-1] = s[i] = ' ';
            }
        }

        for(int i=0; i<s.length();i++){
            if(isupper(s[i])) temp += s[i];
        }

        s = temp;

        return isReformed;
    }

    int minLength(string s) {
        while(reformed(s)){}    //IF  string is reformed -> do and check for reform again
        return s.length();
    }
};
