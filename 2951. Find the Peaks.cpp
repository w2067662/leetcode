//https://leetcode.com/problems/find-the-peaks/description/

class Solution {
public:
    vector<int> findPeaks(vector<int>& mountain) {
        vector<int> peaks;

        for(int i=1; i<mountain.size()-1; i++){
            if(mountain[i-1] < mountain[i] && mountain[i] > mountain[i+1]) peaks.push_back(i); //push the peak into vector
        }

        return peaks;
    }
};
