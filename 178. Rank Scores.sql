#https://leetcode.com/problems/rank-scores/description/

# Write your MySQL query statement below
SELECT score, DENSE_RANK() OVER(ORDER BY score DESC) AS "rank"
FROM Scores

#Syntax:
#DENSE_RANK ( ) OVER ( [ <partition_by_clause> ] < order_by_clause > )
#
#Explain:
#   Return the splited and ordered rank from certain column, same value
# will be same rank.
#
