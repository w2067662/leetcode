//https://leetcode.com/problems/majority-element/description/

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int max=1;
        int ans=nums[0];
        map<int, int>map;

        //ex: nums = [6,6,6,7,7]
        //    map  = [*{6,3}, {7,2}]

        for(int i=0;i<nums.size();i++){ //store appear times of numbers in map
            if(map.find(nums[i])==map.end()) map.insert({nums[i], 1}); 
            else map[nums[i]]++;
        }
        for(const auto element: map){ //find the biggest appear times of numbers in map
            if (element.second > max){
                ans = element.first;
                max = element.second;
            } 
        }
        return ans;
    }
};
