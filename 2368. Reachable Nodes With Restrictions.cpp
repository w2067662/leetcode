//https://leetcode.com/problems/reachable-nodes-with-restrictions/description/

class DisjointSet { //class of Disjoint set 
private:
    vector<int> node;
public:
    //initialize Disjoint Set
    DisjointSet(int n){ 
        for(int i=0; i<n ;i++) node.push_back(i);
    }
    //find the belonged set of the node
    int find(int x){
	    return x == node[x] ? x : (node[x] = find(node[x]));
    }
    //check 2 nodes are in the same set or not
    bool equivalence(int x, int y){
	    return find(x) == find(y);
    }
    //merge 2 sets
    void merge(int x, int y){
	    node[find(x)] = find(y);
    }
};

class Solution {
public:
    int reachableNodes(int n, vector<vector<int>>& edges, vector<int>& restricted) {
        DisjointSet disjointSet(n);
        set<int> banned;
        int count = 0;

        for(const auto node: restricted) banned.insert(node); //insert all the restricted nodes into set

        for(const auto edge: edges){                          //IF   current edge with nodes = (edge[0], edge[1])
            if(banned.find(edge[0]) == banned.end() &&        //     edge[0] is not restricted AND
               banned.find(edge[1]) == banned.end() ){        //     edge[1] is not restricted
                disjointSet.merge(edge[0], edge[1]);          //     -> connect these 2 nodes
            }
        }

        for(int i=0; i<n; i++){                               //traverse all the nodes from 0 to N-1
            if(disjointSet.equivalence(0, i)) count++;        //IF   current node is reachable from node 0 -> count+1
        }

        return count;
    }
};
