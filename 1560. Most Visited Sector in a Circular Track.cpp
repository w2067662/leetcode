//https://leetcode.com/problems/most-visited-sector-in-a-circular-track/description/

class Solution {
public:
    //ex: n = 3,  rounds = [3,2,1,2,1,3,2,1,2,1,3,2,3,1]
    //  1,  2,  3
    //          v           ^
    //  v   v                 ^
    //  v       v               ^
    //      v                     ^
    //  v       v                   ^
    //      v   v                     ^
    //  v   v                           ^
    //  v       v                         ^
    //      v                               ^
    //  v       v                             ^
    //      v   v                               ^
    //  v   v                                     ^
    //          v                                   ^
    //  v                                             ^
    // (8) (7) (8)  -> answer = [1,3]
    vector<int> mostVisited(int n, vector<int>& rounds) {
        unordered_map<int, int> umap; //umap = { [sector]:[visited times] }
        map<int, vector<int>> map;    //map = { [visited times]:vector{[sector1], [sector2], ...} }
        int maxVisited = 0;

        for(int i=1; i<=n; i++){      //initial map with N slots
            umap[i] = 0;
        }

        umap[rounds[0]]++;

        for(int i=1; i<rounds.size(); i++){                    //process the circular tracking
            if(rounds[i] < rounds[i-1]){
                for(int j=rounds[i-1]+1; j<=n; j++) umap[j]++;
                for(int j=1; j<=rounds[i]; j++) umap[j]++;
            }else{
                for(int j=rounds[i-1]+1; j<=rounds[i]; j++) umap[j]++;
            }
        }

        for(const auto it:umap){                                //store the element from umap to map
            map[it.second].push_back(it.first);
            maxVisited = max(it.second, maxVisited);
        }

        sort(map[maxVisited].begin(), map[maxVisited].end());   //sort the vector with max visited times in ascending order

        return map[maxVisited];
    }
};
