//https://leetcode.com/problems/queue-reconstruction-by-height/description/

class Solution {
public:
    //ex:   people = [[7,0],[4,4],[7,1],[5,0],[6,1],[5,2]]
    //
    //     people' = [[5,0],[7,0],[6,1],[7,1],[5,2],[4,4]] (sorted)
    //                   ^                                          count = 0 ; K = 0
    //                         ^                                    count = 0 ; K = 0
    //                               ^                              count = 1 ; K = 1
    //                                     ^                        count = 1 ; K = 1
    //                                           ^                  count = 4 ; K = 2  (count > K)
    //     people' = [[5,0],[7,0],[6,1],[5,2],[7,1],[4,4]] (swap)
    //                                     ^                        count = 3 ; K = 2  (count > K)
    //     people' = [[5,0],[7,0],[5,2],[6,1],[7,1],[4,4]] (swap)
    //                               ^                              count = 2 ; K = 2
    //                                                 ^            count = 5 ; K = 4  (count > K)
    //     people' = [[5,0],[7,0],[5,2],[6,1],[4,4],[7,1]] (swap)
    //                                           ^                  count = 4 ; K = 4
    //
    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) {         //let people[i] = (Hi, Ki)
        sort(people.begin(), people.end(), [&](vector<int> &a, vector<int> &b){ //first sort the vector by K in ascending order
                                                                                //second sort the vector by H in ascending order
            if(a[1] < b[1]) return true;
            else if(a[1] > b[1]) return false;
            else{
                return a[0] <= b[0];
            }
        });

        for(int i=1; i<people.size(); i++){
            int count = 0;

            for(int j=0; j<i; j++){
                if(people[j][0] >= people[i][0]) count++;                       //count the amount of taller people infront
            }

            if(count > people[i][1]){                                           //IF count > Ki
                int curr = i;
                while(count > people[curr][1] && curr > 0) {                    //   -> swap(i, i-1) until the count = Ki
                    if(people[curr-1][0] >= people[curr][0]) count--;
                    swap(people[curr], people[curr-1]);
                    curr--;
                }
            }
        }

        return people;
    }
};
