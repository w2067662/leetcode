//https://leetcode.com/problems/merge-similar-items/description/

class Solution {
public:
    vector<vector<int>> mergeSimilarItems(vector<vector<int>>& items1, vector<vector<int>>& items2) {
        map<int, int> map;                     //map = {[value]: [weight]}
        vector<vector<int>> ans;

        for(int i=0;i<items1.size();i++){      //store items from items1 into map
            map[items1[i][0]] = items1[i][1];
        }

        for(int i=0;i<items2.size();i++){      //app up items wieght in map with items weight from items2
            map[items2[i][0]] += items2[i][1]; 
        }

        for(const auto it:map){
            ans.push_back({it.first, it.second}); //store merged items into answer
        }

        return ans;
    }
};
