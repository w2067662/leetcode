//https://leetcode.com/problems/latest-time-by-replacing-hidden-digits/description/

class Solution {
public:
    string maximumTime(string time) {
        if(time[0] == '?' && time[1] == '?'){       //IF   "??:--"
            time[0] = '2';                          //      23
            time[1] = '3';
        }else if(time[0] == '?' && time[1] != '?'){ //IF   "?-:--"
            time[0] = time[1] > '3' ? '1' : '2';    //      04-09, 14-19 , 20-23
        }else if(time[0] != '?' && time[1] == '?'){ //IF   "-?:--"
            time[1] = time[0] < '2' ? '9' : '3';    //      09, 19, 23
        }

        if(time[3] == '?' && time[4] == '?'){       //IF   "--:??"
            time[3] = '5';                          //         59
            time[4] = '9';
        }else if(time[3] == '?' && time[4] != '?'){ //IF   "--:?-"
            time[3] = '5';                          //         50-59
        }else if(time[3] != '?' && time[4] == '?'){ //IF   "--:-?"
            time[4] = '9';                          //         09, 19, 29, 39, 49, 59
        }
        
        return time;
    }
};
