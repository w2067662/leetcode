//https://leetcode.com/problems/find-the-integer-added-to-array-i/description/

class Solution {
public:
    int addedInteger(vector<int>& nums1, vector<int>& nums2) {
        int sum1 = accumulate(nums1.begin(), nums1.end(), 0);        // Calculate sum of nums1
        int sum2 = accumulate(nums2.begin(), nums2.end(), 0);        // Calculate sum of nums2
        
        return ((sum2-sum1) >= 0) ?                                  
            (sum2-sum1)/nums1.size() : -((sum1-sum2)/nums1.size());  // Calculate the added X to each element
    }
};