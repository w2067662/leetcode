//https://leetcode.com/problems/convert-integer-to-the-sum-of-two-no-zero-integers/description/

class Solution {
public:
    bool notContainZero(int num){          //check contain zero or not
        string s = to_string(num);
        for(int i=0;i<s.length();i++){
            if(s[i]=='0')return false;
        }
        return true;
    }

    vector<int> getNoZeroIntegers(int n) {
        for(int i=1;i<n;i++){                                            //i start from 1 to n-1
            if(notContainZero(i) && notContainZero(n-i))return {i, n-i}; //if i and n-i, then answer = {i, n-i}
        }
        return {-1,-1};
    }
};
