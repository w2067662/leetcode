//https://leetcode.com/problems/number-of-good-leaf-nodes-pairs/description/

typedef set<TreeNode*> Set; // Define set container

class Solution {
public:
    void traverse(TreeNode* root, map<TreeNode*, vector<TreeNode*>> &ajacentList){ // Traverse the tree to get ajacent list
        if(!root) return;

        if(root->left){
            ajacentList[root].push_back(root->left);
            ajacentList[root->left].push_back(root);
            traverse(root->left, ajacentList);
        }

        if(root->right){
            ajacentList[root].push_back(root->right);
            ajacentList[root->right].push_back(root);
            traverse(root->right, ajacentList);
        }

        return;
    }

    void getLeafNodes(TreeNode* root, set<TreeNode*> &set){ // Traverse the tree to get the leaf nodes
        if(!root) return;

        if(!root->left && !root->right) set.insert(root);

        getLeafNodes(root->left , set);
        getLeafNodes(root->right, set);

        return;
    }

    void DFS(map<TreeNode*, vector<TreeNode*>> &ajacentList, TreeNode* curr, int currDis, Set &set, Set &visited, const int distance, int &count){
            if(currDis > distance) return;                                            // IF out of distance -> return

            visited.insert(curr);                                                     // Mark current node as visited

            for(auto &next: ajacentList[curr]){                                       // Traverse all the next node
                if(visited.find(next) == visited.end()){                              // IF next node not visited
                    DFS(ajacentList, next, currDis+1, set, visited, distance, count); //    -> DFS next node
                }
            }

            if(set.find(curr) != set.end() && currDis != 0) count++;                  // IF current node is leaf node AND is not start node
                                                                                      //    -> count+1 (found 1 pair)
            return;
        }

    int countPairs(TreeNode* root, int distance) {
        map<TreeNode*, vector<TreeNode*>> ajacentList;
        Set set;
        int count = 0;

        traverse(root, ajacentList);                                      // Traverse the tree to get ajacent list

        getLeafNodes(root, set);                                          // Get all the leaf nodes

        for(auto &leafNode: set){                                         // Traverse all the leaf nodes as start point
            Set visited;
            DFS(ajacentList, leafNode, 0, set, visited, distance, count); // Use DFS to find possible pairs
        }

        return count/2;                                                   // Different sequences are considered same pair
                                                                          //   -> return count/2
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
