//https://leetcode.com/problems/island-perimeter/description/

class Solution {
public:
    bool isValid(const vector<vector<int>>& grid, const int &x, const int &y){  // Check current position is valid
        return 0 <= x && x < grid.size() && 0 <= y && y < grid[x].size();
    }

    int islandPerimeter(vector<vector<int>>& grid) {
        int ans = 0;

        for(int i=0; i<grid.size(); i++){                                       // Traverse the matrix
            for(int j=0; j<grid[i].size(); j++){
                if(grid[i][j]){                                                 //      IF current position is land
                    ans += (isValid(grid, i-1, j  ) && grid[i-1][j]) ? 0 : 1;   //         IF   neighbor is water -> answer + 1
                    ans += (isValid(grid, i+1, j  ) && grid[i+1][j]) ? 0 : 1;   //         ELSE                   -> answer + 0
                    ans += (isValid(grid, i  , j-1) && grid[i][j-1]) ? 0 : 1;
                    ans += (isValid(grid, i  , j+1) && grid[i][j+1]) ? 0 : 1;
                }
            }
        }

        return ans;
    }
};
