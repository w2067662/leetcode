//https://leetcode.com/problems/replace-all-digits-with-characters/description/

class Solution {
public:
    void shift(char& a, char& b){       //shift s[i+1] according to s[i]
        b = a + b-'0';
    }

    string replaceDigits(string s) {
        for(int i=0;i<s.length();i+=2){
            shift(s[i], s[i+1]);        //shift all the odd index digit and replace it with shifted letter
        }
        return s;
    }
};
