//https://leetcode.com/problems/maximum-difference-between-increasing-elements/description/

class Solution {
public:
    int maximumDifference(vector<int>& nums) {
        int ans = -1;

        for(int i=0;i<nums.size()-1;i++){
            for(int j=i+1; j<nums.size();j++){  
                if(nums[j] > nums[i]){               //IF   exist (i, j) that nums[i] < nums[j]
                    ans = max(ans, nums[j]-nums[i]); //     -> store the max value from answer and difference of current pair (i, j)
                }
            }
        }

        return ans;
    }
};
