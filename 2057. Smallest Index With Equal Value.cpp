//https://leetcode.com/problems/smallest-index-with-equal-value/description/

class Solution {
public:
    int smallestEqual(vector<int>& nums) {
        int ans=INT_MAX;
        bool noEqual = true;
        for(int i=0;i<nums.size();i++){
            if(i%10 == nums[i]){
                ans = i<ans ? i : ans;
                noEqual = false;
            }
        }
        return noEqual ? -1 : ans;       //if no equal ruturn -1
    }
};
