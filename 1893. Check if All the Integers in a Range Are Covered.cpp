//https://leetcode.com/problems/check-if-all-the-integers-in-a-range-are-covered/description/

class Solution {
public:
    bool isCovered(vector<vector<int>>& ranges, int left, int right) {
        set<int> set;

        for(const auto pair: ranges){
            for(int i=pair[0]; i<=pair[1]; i++) set.insert(i);
        }

        for(int i=left; i<=right; i++){
            if(set.find(i) == set.end())return false;
        }

        return true;
    }
};
