//https://leetcode.com/problems/number-of-valid-words-in-a-sentence/description/

class Solution {
public:
    bool isValidWord(string s){ //check the word is valid or not
        int dashCount = 0;

        for(int i=0;i<s.length();i++){
            if(isdigit(s[i])) return false;                                 //IF   current char is digit -> FALSE
            else if(s[i] == '-'){                                           //IF   current char is dash ('-')
                if(i == 0 || i == s.length()-1) return false;               //     IF   dash appear in the beginning or in the end -> FALSE
                else if(!islower(s[i-1]) || !islower(s[i+1])) return false; //     IF   dash not surrounded by lower case letters  -> FALSE
                else dashCount++;                                           //     -> count+1
            }
            else if(!islower(s[i]) && i != s.length()-1) return false;      //IF   current char is not lower case letter AND
        }                                                                   //     not the last char
                                                                            //     -> FALSE
        return dashCount <= 1; //check contain at most 1 dash or not
    }

    int countValidWords(string sentence) {
        string temp = "";
        int ans = 0;

        for(const auto ch: sentence){              //get words from sentence
            if(ch == ' ' && temp != ""){
                if(isValidWord(temp))ans++;
                temp = "";
            }else if(ch != ' ')temp += ch;
        }
        if(temp != "" && isValidWord(temp)) ans++; //get last word from sentence

        return ans;
    }
};
