//https://leetcode.com/problems/prime-arrangements/description/

class Solution {
public:
    //ex:  n = 5
    //  [1,2,3,4,5]
    //     ^ ^   ^      prime numbers -> 3!    = 3x2x1 = 6
    //   ^     ^    not prime numbers -> 2!    = 2x1   = 2
    //                                -> 3!x2! = 6x2   = 12
    //
    bool isPrime(int n){  //check
        if(n <= 1) return false; //IF  is negative, 0 ,1 -> not Prime

        for(int i=2; i<=n/2; i++){
            if(n%i == 0) return false; //IF  divisible by any number from 2 ~ N/2 -> not Prime
        }

        return true;
    }

    int numPrimeArrangements(int n) {
        int primeCount = 0;
        long long ans = 1;

        int MOD = pow(10, 9) + 7;

        for(int i=1; i<=n; i++){
            if(isPrime(i))primeCount++; //count for primes
        }

        for(int i=1; i<=primeCount; i++) ans = ans*i % MOD;  //calculate permutations:  K!
        for(int i=1; i<=n-primeCount; i++) ans = ans*i % MOD;//calculate permutations:  (N-k)!
                                                             // -> K!(N-k)!
        return ans % MOD;
    }
};
