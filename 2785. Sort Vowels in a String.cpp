//https://leetcode.com/problems/sort-vowels-in-a-string/description/

bool cmp(char &a, char &b){ //upper case > lower case ; 'A' > 'E' > 'I' > 'O' > 'U' > 'a' > 'e' > 'i' > 'o' > 'u'
    return a < b;
}

class Solution {
public:
    bool isVowel(char &ch){ //check is vowel or not
        return tolower(ch) == 'a' || tolower(ch) == 'e' || tolower(ch) == 'i' || tolower(ch) == 'o' || tolower(ch) == 'u';
    }

    string sortVowels(string s) {
        vector<char> vowels;
        int index = 0;

        for(auto ch: s){
            if(isVowel(ch)) vowels.push_back(ch); //IF  current char is vowel -> push into vector
        }

        sort(vowels.begin(), vowels.end(), cmp);  //sort the vowels in ascending order

        for(auto& ch: s){
            if(isVowel(ch)) ch = vowels[index++]; //IF  current char is vowel -> replaced by sorted vowel
        }

        return s;
    }
};
