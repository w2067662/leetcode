#https://leetcode.com/problems/calculate-special-bonus/description/

# Write your MySQL query statement below
SELECT 
    employee_id,
    IF( employee_id%2 != 0 AND name NOT LIKE 'M%', salary, 0) AS bonus
FROM Employees
ORDER BY employee_id

#IF( condition, [value_if_true], [value_if_false] )
