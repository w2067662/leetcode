//https://leetcode.com/problems/finding-3-digit-even-numbers/description/

class Solution {
public:
    vector<int> findEvenNumbers(vector<int>& digits) {
        set<int> set;
        vector<int> ans;
        int temp;

        for(int i=0;i<digits.size();i++){
            for(int j=0;j<digits.size();j++){
                for(int k=0;k<digits.size();k++){
                    if(i != j && j != k && k != i){                     //IF   triplet (i,j,k)
                        temp = digits[i]*100 + digits[j]*10 + digits[k];//     that 100xi  + 10xj +k
                        if(temp >= 100 && temp%2 == 0) set.insert(temp);//     is even AND bigger than 100 -> insert into set
                    }
                }
            }
        }

        for(const auto num:set)ans.push_back(num);                      //store the numbers from set into vector

        sort(ans.begin(), ans.end());                                   //sort the vector in ascending order

        return ans;
    }
};
