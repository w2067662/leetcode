//https://leetcode.com/problems/form-smallest-number-from-two-digit-arrays/description/

class Solution {
public:
    int minNumber(vector<int>& nums1, vector<int>& nums2) {
        set<int> set;
        vector<int> vec;

        sort(nums1.begin(), nums1.end());                     //sort nums1 in ascending order
        sort(nums2.begin(), nums2.end());                     //sort nums2 in ascending order

        for(auto num:nums1){
            set.insert(num);                                  //insert numbers from nums1 into set
        }

        for(auto num:nums2){
            if(set.find(num) != set.end()) vec.push_back(num);//if nums2 contains numbers that are also exist in nums1 -> store into vector
        }

        if(vec.size() != 0)return vec[0];                     //if common number exist -> return the smallest common number
        return nums1[0] < nums2[0] ? nums1[0]*10 + nums2[0] : nums2[0]*10 + nums1[0]; //return the number that consist by the smallest number of nums1 and nums2
    }
};
