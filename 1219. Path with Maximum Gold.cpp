//https://leetcode.com/problems/path-with-maximum-gold/description/

class Solution {
private:
    vector<int> steps = {1, 0, -1, 0, 1};   // Steps for (UP, DOWN, LEFT, RIGHT) orientations

public:
    bool isValid(vector<vector<int>>& grid, const int& x, const int& y){  // Check current position is valid
        return 0 <= x && x < grid.size() && 0 <= y && y < grid[0].size();
    }

    int DFS(vector<vector<int>>& grid, int x, int y){
        if(!isValid(grid, x, y) || grid[x][y] == 0){                            // IF  current position is invalid OR value is 0
            return 0;                                                           //     -> return 0
        }

        int curr, maxGold;
        maxGold = curr = grid[x][y];                                            // Store current value as current AND max gold

        grid[x][y] = 0;                                                         // Mark current position as visited

        for(int s=0; s<steps.size()-1; s++){                                    // FOR  next steps
            maxGold = max(maxGold, curr + DFS(grid, x+steps[s], y+steps[s+1])); //      -> DFS to find the max gold
        }

        grid[x][y] = curr;                                                      // Unmark current position

        return maxGold;
    }

    int getMaximumGold(vector<vector<int>>& grid) {
        int maxGold = 0;

        for(int i=0; i<grid.size(); i++){                    // Traverse the grid
            for(int j=0; j<grid[i].size(); j++){
                if(grid[i][j] != 0){                         //     IF  current position with non-zero value
                    maxGold = max(maxGold, DFS(grid, i, j)); //         -> calculate max gold by DFS
                }
            }
        }

        return maxGold;
    }
};