//https://leetcode.com/problems/score-after-flipping-matrix/description/

class Solution {
public:
    void flipRow(vector<vector<int>>& grid, int i){     // Flip the row
        for(int y=0; y<grid[i].size(); y++){
            grid[i][y] = grid[i][y] == 0 ? 1 : 0;
        }
    }

    void flipColumn(vector<vector<int>>& grid, int j){  // Flip the column
        for(int x=0; x<grid.size(); x++){
            grid[x][j] = grid[x][j] == 0 ? 1 : 0;
        }
    }

    int columnSum(vector<vector<int>>& grid, int j){    // Count the column sum
        int sum = 0;
        for(int x=0; x<grid.size(); x++){
            sum += grid[x][j];
        }
        return sum;
    }

    int countScore(vector<vector<int>>& grid){          // Count total score of grid
        int score = 0;
        for(int i=0, temp; i<grid.size();i++){
            temp = 0;
            for(int j=0; j<grid[i].size(); j++){
                temp = temp*2 +grid[i][j];
            }
            score += temp;
        }
        return score;
    }

    int matrixScore(vector<vector<int>>& grid) {
        for(int i=0; i<grid.size(); i++){
            if(grid[i][0] == 0) flipRow(grid, i);                        // IF   first digit in current row is 0 
        }                                                                //      -> flip the row

        for(int j=0; j<grid[0].size(); j++){
            if(columnSum(grid, j) <= grid.size()/2) flipColumn(grid, j); // IF   sum of current column not more than half of total column
        }                                                                //      -> flip the column

        return countScore(grid);                                         // Count the total score of grid
    }
};
