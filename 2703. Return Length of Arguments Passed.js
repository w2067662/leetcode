//https://leetcode.com/problems/return-length-of-arguments-passed/description/

/**
 * @return {number}
 */
var argumentsLength = function(...args) {
    return args.length
};

/**
 * argumentsLength(1, 2, 3); // 3
 */
