//https://leetcode.com/problems/binary-tree-inorder-traversal/description/

class Solution {
public:
    void traverse(TreeNode* root, vector<int> &vec){ //traverse the tree in order
        if(!root) return;

        traverse(root->left, vec);
        vec.push_back(root->val);
        traverse(root->right, vec);

        return;
    }

    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> vec;
        traverse(root, vec); //traverse the tree
        return vec;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
