//https://leetcode.com/problems/average-salary-excluding-the-minimum-and-maximum-salary/description/

class Solution {
public:
    double average(vector<int>& salary) {
        sort(salary.begin(), salary.end());    //sort the vector in ascending order

        double sum = 0;
        for(int i=1; i<salary.size()-1; i++){
            sum += salary[i];                  //sum up salary without max and min salary
        }

        return sum/(salary.size()-2);          //count the average salary without max and min salary
    }
};
