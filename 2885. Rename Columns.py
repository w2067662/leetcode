#https://leetcode.com/problems/rename-columns/description/

import pandas as pd

def renameColumns(students: pd.DataFrame) -> pd.DataFrame:
    return students.rename(columns = {
        'id': 'student_id',
        'first' : 'first_name',
        'last' : 'last_name',
        'age' : 'age_in_years'
    })

#rename() : rename the column name
    
