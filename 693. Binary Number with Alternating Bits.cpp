//https://leetcode.com/problems/binary-number-with-alternating-bits/description/

class Solution {
public:
    bool hasAlternatingBits(int n) {
        string s="";

        while(n>0){     //int to binary string
            s+=n%2+'0';
            n/=2;
        }

        for(int i=1;i<s.length();i++){      //check for alternating bits
            if(s[i]==s[i-1])return false;
        }
        return true;
    }
};
