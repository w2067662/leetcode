//https://leetcode.com/problems/convert-1d-array-into-2d-array/description/

class Solution {
public:
    vector<vector<int>> construct2DArray(vector<int>& original, int m, int n) {
        if(original.size() != m*n) return {};

        vector<vector<int>> matrix(m, vector<int>(n)); //create matrix
        for(int i=0, index=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){
                matrix[i][j] =  original[index++];     //convert 1D array to 2D matrix
            }
        }

        return matrix;
    }
};
