//https://leetcode.com/problems/subarray-product-less-than-k/description/

class Solution {
public:
    //ex: nums = [10,5,2,6], k = 100
    //                                  Product =   1          ; count = 0
    //            LR                    Product =  10 ( <100)  ; count = 1(+1)
    //            L  R                  Product =  50 ( <100)  ; count = 3(+2)
    //            L    R                Product = 100 (>=100)  ; count = 3
    //               L R                Product =  10 ( <100)  ; count = 5(+2)
    //               L   R              Product =  60 ( <100)  ; count = 8(+3)
    //                                                        -> count = 8
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        int product = 1, count = 0;

        for(int left = 0, right = 0; right < nums.size(); ) { // For  RIGHT < nums.size
            product *= nums[right];                           //        Product multiplied by nums[RIGHT]

            while(product >= k && left <= right) {            //        While product >= K AND LEFT <= RIGHT
                product /= nums[left];                        //                Product divided by nums[LEFT]
                left++;                                       //                Goto next LEFT
            }

            count += right - left + 1;                        //        Add up subarrays' count of current window(LEFT, RIGHT)

            right++;                                          //        Goto next RIGHT
        }

        return count;
    }
};