//https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/

class Solution {
private:
    vector<vector<int>> ListByLevel;
public:
    void toLevelList(TreeNode* root, int level){  //re-construct tree to 2D list by level
        if(!root) return;

        if(level >= ListByLevel.size()) ListByLevel.push_back({root->val});
        else ListByLevel[level].push_back(root->val);

        toLevelList(root->left , level+1);
        toLevelList(root->right, level+1);

        return;
    }

    void reverse(){           //reverse the list
        for(int i=0; i<ListByLevel.size()/2; i++){
            swap(ListByLevel[i], ListByLevel[ListByLevel.size()-i-1]);
        }
    }

    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        toLevelList(root, 0); //construct into level list
        reverse();            //reverse the level list
        return ListByLevel;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
