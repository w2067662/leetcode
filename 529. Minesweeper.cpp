//https://leetcode.com/problems/minesweeper/description/

class Solution {
public:
    bool valid(const vector<vector<char>>& board, const vector<int>& curr){ //check current position is valid
        return 0 <= curr[0] && curr[0] < board.size() && 0 <= curr[1] && curr[1] < board[0].size();
    }

    int countBomb(const vector<vector<char>>& board, const vector<int>& curr){ //find the bombs from the surrounded position
        int count = 0;
        for(int i=curr[0]-1; i<=curr[0]+1; i++){
            for(int j=curr[1]-1; j<=curr[1]+1; j++){
                if(valid(board, {i,j})){
                    if(board[i][j] == 'M') count++;
                }
            }
        }
        return count;
    }

    void reveal(vector<vector<char>>& board, const vector<int>& click){ //reveal the position by using BSF algorithm
        const vector<int> steps = {1,0,-1,0,1,1,-1,-1,1}; //for next steps
        vector<int> curr, next;                           //for current step
        int count;                                        //count for bomb
        queue<vector<int>> queue;
        set<pair<int, int>> visited;                      //for visited position

        queue.push(click);
        visited.insert({click[0], click[1]});

        while(!queue.empty()){
            curr = queue.front();                                     //get current position
            queue.pop();

            count = countBomb(board, curr);                           //count for bombs
            board[curr[0]][curr[1]] = count == 0 ? 'B' : count + '0'; //update the sign according to the amount of surrounded bombs

            if(!count){                                               //IF   no surrounded bombs in current position
                for(int i=0; i<8; i++){
                    next = {curr[0]+steps[i], curr[1]+steps[i+1]};    //        get next position AND

                    if(valid(board, next)){                           //        IF   next position is not visited AND unrevealed
                        if(board[next[0]][next[1]] == 'E' && visited.find({next[0], next[1]}) == visited.end()){
                            queue.push(next);                         //        -> push into queue
                            visited.insert({next[0], next[1]});       //        -> mark as visted
                        }
                    }
                }
            }
        }
    }

    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        switch(board[click[0]][click[1]]){
            case 'M':                            //IF  click the bomb
                board[click[0]][click[1]] = 'X'; //       -> bomb exploded
                break;
            case 'E':                            //IF  click the unrevealed
                reveal(board, click);            //       -> reveal the unrevealed position
                break;
            default:
                break;
        }
        return board;
    }
};
