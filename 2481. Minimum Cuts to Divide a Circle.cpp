//https://leetcode.com/problems/minimum-cuts-to-divide-a-circle/description/

class Solution {
public:
    int numberOfCuts(int n) {
        return n==1 ? 0 : n&1 ? n: n/2; //if n=1, return 0
                                        //   n is odd , return n
                                        //   n is even, return n/2
    }
};
