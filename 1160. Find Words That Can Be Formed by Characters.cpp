//https://leetcode.com/problems/find-words-that-can-be-formed-by-characters/description/

class Solution {
public:
    typedef map<char, int> LetterCounter;
    int countCharacters(vector<string>& words, string chars) {
        LetterCounter  map;
        LetterCounter umap;
        int ans=0;
        bool canBeFormed=true;

        for(int i=0;i<chars.size();i++){
            map[chars[i]]++;                //store {[char]:[appear times]} into map
        }

        for(int i=0;i<words.size();i++){
            for(int j=0;j<words[i].length();j++){
                umap[words[i][j]]++;        //store {[words[i].char]:[appear times]} into umap
            }

            canBeFormed = true;             //initial canBeFormed as true
            for(auto it:umap){
                if(map.find(it.first)==map.end() || map[it.first] < it.second) canBeFormed = false; //if no enough letters in map to form words[i]
            }                                                                                       //then words[i] can not be formed, set canBeFormed to false
            if(canBeFormed) ans += words[i].length(); //if words[i] can be formed answers + len(words)
            umap.clear();                             //clear umap for next word's letters
        }

        return ans;
    }
};
