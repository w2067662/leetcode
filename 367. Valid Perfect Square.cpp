//https://leetcode.com/problems/valid-perfect-square/description/

class Solution {
public:
    bool isPerfectSquare(int num) {
        return (int)sqrt(num)*(int)sqrt(num)==num;
    }
};
