//https://leetcode.com/problems/teemo-attacking/description/

class Solution {
public:
    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
        int temp=timeSeries[0];
        int ans=0;
        for(int i = 0;i<timeSeries.size();i++){     //traverse all the numbers in timeSeries
            ans+=duration;                          //ans = poision duration * timeSeries Size
            if(timeSeries[i]< temp+duration && i!=0){   //find numbers within the duration of poison
                ans-=temp+duration-timeSeries[i];       // -> duplicate poison time have to be deducted
            } 
            temp= timeSeries[i];    //store the current timeSeries number
        }
        
        return ans;
    }
};
