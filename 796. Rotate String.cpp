//https://leetcode.com/problems/rotate-string/description/

class Solution {
public:
    //s = "abcde", goal = "cdeab" 
    //goal = "cdeabcdeab" = goal +goal 
    //s    =  abcde     ->  not the same
    //s    =   abcde    ->  not the same
    //s    =    abcde   ->  not the same
    //s    =     abcde  ->  the same
    //answer = true
    bool rotateString(string s, string goal) {
        goal+=goal;
        for(int i=0;i<goal.length()-s.length();i++){
            if (s.compare(goal.substr(i, s.length())) == 0) return true;
        }
        return false;
    }
};
