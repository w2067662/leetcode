//https://leetcode.com/problems/minimum-penalty-for-a-shop/description/

class Solution {
public:
    int bestClosingTime(string customers) {
        int currPenalty = 0, minPenalty = 0, bestTime = 0;

        for(int i=0; i<customers.length(); i++){
            if(customers[i] == 'Y') currPenalty++;  //calculate the penalty from closing time = 0
        }

        minPenalty = currPenalty;

        for(int i=0; i<customers.length(); i++){    //calculate the penalty from closing time = 1 ~ N+1
            if(customers[i] == 'Y') currPenalty--;
            if(customers[i] == 'N') currPenalty++;

            if(currPenalty < minPenalty){           //IF current penalty < min penalty
                minPenalty = currPenalty;           //   -> store current penalty as min penalty
                bestTime = i+1;                     //   -> store current closing time as best time
            }
        }

        return bestTime;
    }
};
