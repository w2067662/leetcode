//https://leetcode.com/problems/set-matrix-zeroes/description/

class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        set<pair<int, int>> set;

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                if(matrix[i][j] == 0){                                       //IF  current number is 0
                    for(int x=0; x<matrix.size()   ; x++) set.insert({x, j});//    -> mark its column
                    for(int y=0; y<matrix[0].size(); y++) set.insert({i, y});//    -> mark its row
                }
            }
        }

        for(const auto position: set) matrix[position.first][position.second] = 0; //replace all the marked position as 0

        return;
    }
};
