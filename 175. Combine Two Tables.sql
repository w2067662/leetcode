#https://leetcode.com/problems/combine-two-tables/description/

# Write your MySQL query statement below
SELECT p.firstName, p.lastName, a.city, a.state
From Person p LEFT JOIN Address a   #left join two tables
ON p.personId = a.personId  #use ON for the condition
