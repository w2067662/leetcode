//https://leetcode.com/problems/maximum-number-of-operations-with-the-same-score-i/description/

class Solution {
public:
    int maxOperations(vector<int>& nums) {
        int count = 0, prevSum = -1;

        for(int i=0; i+1<nums.size(); i+=2){                     // For i from 0 to nums.size (i+=2)
            if(prevSum != -1 && prevSum != nums[i] + nums[i+1]){ //     IF previous sum is not empty AND
                                                                 //        previous sum not equal to 
                                                                 //        -> end the loop
                break;
            }

            prevSum = nums[i] + nums[i+1];                       //     Store nums[i] + nums[i+1] as previous num
            count++;                                             //     Count+1
        }

        return count;
    }
};
