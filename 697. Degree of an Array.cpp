//https://leetcode.com/problems/degree-of-an-array/description/

class Solution {
public:
    typedef map<int, pair<int, int>>range; // define range type pair(mostLeft, mostRight)
    int findShortestSubArray(vector<int>& nums) {
        unordered_map<int, int>umap; //{[number]:[appear times]}
        range range;                 //{[number]:{[mostLeft], [mostRight]}}
        map<int, vector<int>>map;    //{[appear times]:{[number1], [number2]...}} (ascending order)

        int ans=INT_MAX;             //min subarray

        for(int i=0;i<nums.size();i++){             //traverse the vector
            umap[nums[i]]++;                        //insert into umap
            if(range.find(nums[i])!=range.end()){   //if number already in range,
                range[nums[i]].second=i;            //update [mostRight]
            }
            else range[nums[i]]={i, i};             //else first time insert into range
        }

        for(const auto& it:umap){
            map[it.second].push_back(it.first);     //store data from umap struct to map struct
        }

        auto it=map.rbegin(); //start from map's end (descending order traverse)
        for(int i=0;i<it->second.size();i++){
            int temp =range[it->second[i]].second-range[it->second[i]].first+1; //get range
            ans=temp <ans?temp:ans; //if range is smaller, become smallest
        }
        return ans;
    }
};
