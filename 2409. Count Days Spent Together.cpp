//https://leetcode.com/problems/count-days-spent-together/description/

class Solution {
private:
    const vector<int> daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; //days in month
public:
    int dateToInt(string date){   //convert date to integar
        int month = (date[0]-'0')*10 + (date[1]-'0');
        int day   = (date[3]-'0')*10 + (date[4]-'0');
        int res = 0;

        for(int i=0;i<month-1;i++){
            res += daysInMonth[i];
        }

        return res + day;
    }

    int countDaysTogether(string arriveAlice, string leaveAlice, string arriveBob, string leaveBob) {
        set<int> set;

        for(int i=dateToInt(arriveAlice); i<=dateToInt(leaveAlice);i++){ //count for days of Alice and Bob's union
            set.insert(i);
        }

        for(int i=dateToInt(arriveBob); i<=dateToInt(leaveBob);i++){
            set.insert(i);
        }

        return (dateToInt(leaveAlice)-dateToInt(arriveAlice)+1)          //Alice + Bob - union = intersection
              +(dateToInt(leaveBob)  -dateToInt(arriveBob)  +1)
              -set.size();
    }
};
