//https://leetcode.com/problems/find-the-xor-of-numbers-which-appear-twice/description/

class Solution {
public:
    int duplicateNumbersXOR(vector<int>& nums) {
        map<int, int> map;
        int XOR = 0;

        for(const auto& num: nums){
            map[num]++;              // Count the frequency of each number
        }

        for(const auto& it: map){
            if(it.second >= 2){      // IF  current number is duplicate number
                XOR ^= it.first;     //     -> xor its value
            }
        }

        return XOR;
    }
};