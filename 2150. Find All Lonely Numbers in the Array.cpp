//https://leetcode.com/problems/find-all-lonely-numbers-in-the-array/description/

class Solution {
public:
    vector<int> findLonely(vector<int>& nums) {
        map<int, int> map;
        vector<int> ans;

        for(const auto num: nums){  // For all the numbers
            map[num]++;             //      Insert the {[Number]: [Frequency]} into map
        }

        for(const auto it: map){
            if( it.second < 2 &&                                                            // IF current number appear not more than 2 times AND
                (map.find(it.first-1) == map.end() && map.find(it.first+1) == map.end()) ){ //    not exist number-1 and number+1 in map
                    ans.push_back(it.first);                                                //    -> push into answer (lonely number)
                }
        }

        return ans;
    }
};
