//https://leetcode.com/problems/sign-of-the-product-of-an-array/description/

class Solution {
public:
    int arraySign(vector<int>& nums) {
        bool isNegetive = false;
        for(const auto num: nums){
            if(num == 0)return 0;     //IF  current number is 0, product = 0
            if(num < 0) isNegetive = isNegetive ? false : true;
        }

        return isNegetive ? -1 : 1;   //check product is negetive or not (positive)
    }
};
