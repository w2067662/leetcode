//https://leetcode.com/problems/find-duplicate-file-in-system/description/

class File{ //define File
public:
    string Path, FileName, Content;
    File(string path, string filename, string content):Path(path), FileName(filename), Content(content){}
};

class Solution {
public:
    vector<File*> toFiles(string input){ //convert input string to files
        vector<File*> files;
        string path, name, content, temp = "";

        for(const auto ch: input){
            switch(ch){
                case ' ':
                    if(temp == "") break;
                    path = temp;
                    temp = "";
                    break;
                case '(':
                    name = temp;
                    temp = "";
                    break;
                case ')':
                    content = temp;
                    temp = "";
                    files.push_back(new File(path, name, content));
                    break;
                default:
                    temp += ch;
            }
        }

        return files;
    }

    vector<vector<string>> findDuplicate(vector<string>& paths) {
        map<string, vector<File*>> sameContent;
        vector<vector<string>> answer;

        for(const auto path: paths){
            auto files = toFiles(path);                                //convert the input string to files

            for(const auto file: files){
                sameContent[file->Content].push_back(file);            //store the same content files into map
            }
        }

        for(const auto it: sameContent){
            if(it.second.size()>1){                                    //IF exist duplicate files with same content
                vector<string> temp;
                for(const auto file: it.second) {
                    temp.push_back(file->Path + "/" + file->FileName); //   -> push all the duplicate files' path into answer
                }
                answer.push_back(temp);
            }
        }

        return answer;
    }
};
