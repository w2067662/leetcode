//https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/description/

class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        vector<vector<int>> merged;

        sort(points.begin(), points.end(), [&](vector<int>& a, vector<int>& b){         // Sort the points in ascending order by starts
            return a[0] < b[0];
        });

        for(const auto& range: points){                                                 // Traverse all the points
            if(merged.empty() || range[0] > merged[merged.size()-1][1]){                //      IF    merged vector is empty OR
                                                                                        //            current range's start > last range's end
                merged.push_back(range);                                                //            -> push range into merged
            }
            else {                                                                      //      ELSE 
                merged[merged.size()-1][0] = min(merged[merged.size()-1][0], range[0]); //            -> merge last range of merged with current range
                merged[merged.size()-1][1] = min(merged[merged.size()-1][1], range[1]);
            }
        }

        return merged.size();                                                           // Return the size of merged (min arrows to shoot all the balloons)
    }
};