//https://leetcode.com/problems/find-the-k-beauty-of-a-number/description/

class Solution {
public:
    int toInt(string s){ //convert number string to integar
        int res = 0;
        for(const auto ch: s){
            res = res*10 + ch -'0';
        }
        return res;
    }

    int divisorSubstrings(int num, int k) {
        int count = 0, divisor;

        string s = to_string(num);

        for(int i=0; i+k-1<s.length(); i++){
            divisor = toInt(s.substr(i,k));          //get the length K substring as divisor
            if( divisor != 0){                       //IF   divisor is not 0
                count += num % divisor == 0 ? 1 : 0; //     IF    number is divisible by current divisor -> count+1
            }
        }

        return count;
    }
};
