//https://leetcode.com/problems/find-bottom-left-tree-value/description/

class Solution {
public:
    void traverse(TreeNode* root, int level, vector<vector<int>> &levelList){
        if(!root) return;

        if(level >= levelList.size()) levelList.push_back({root->val}); // IF   no current level -> push current vector with current node value into current level
        else levelList[level].push_back(root->val);                     // ELSE -> push current node value into current level's vector

        traverse(root->left , level+1, levelList); // Traverse left  subtree
        traverse(root->right, level+1, levelList); // Traverse right subtree
        return;
    }

    int findBottomLeftValue(TreeNode* root) {
        vector<vector<int>> levelList;

        traverse(root, 0, levelList);            // Traverse the tree

        return levelList[levelList.size()-1][0]; // Return the BottomLeft value of the last row
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
