//https://leetcode.com/problems/truncate-sentence/description/

class Solution {
public:
    string truncateSentence(string s, int k) {
        vector<string> words;
        string ans="";
        string temp="";

        for(int i=0;i<s.length();i++){  //store words from sentence
            if(s[i]==' '){
                words.push_back(temp);
                temp="";
            }
            else temp+=s[i];
        }
        words.push_back(temp);  //store last word

        for(int i=0;i<k;i++){   //truncateSentence to at most k words
            ans+=words[i];
            if(i!=k-1)ans+=' ';
        }

        return ans;
    }
};
