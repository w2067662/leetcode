//https://leetcode.com/problems/merge-in-between-linked-lists/description/

class Solution {
public:
    ListNode* mergeInBetween(ListNode* list1, int a, int b, ListNode* list2) {
        ListNode *node1 = list1, *node2 = list1;
        
        for(int i=0; i<a-1; i++) {     // Move node1 to the (A-1)th node
            node1 = node1->next;
        }
        
        for(int i=0; i<b; i++) {       // Move node2 to the Bth node
            node2 = node2->next;
        }

        node1->next = list2;           // (A-1)th node of list1 points to list2's head
        
        while (list2->next) {
            list2 = list2->next;       // Find the end of list2
        }
        
        list2->next = node2->next;     // The end of list2 points to next node of Bth node of list1

        return list1;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */