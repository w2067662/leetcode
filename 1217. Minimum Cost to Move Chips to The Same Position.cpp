//https://leetcode.com/problems/minimum-cost-to-move-chips-to-the-same-position/description/

class Solution {
public:
    int minCostToMoveChips(vector<int>& position) {
        int odd=0;
        int even=0;

        for(int i=0;i<position.size();i++){ 
            if(position[i]%2==0) even++;    //count for even numbers
            else odd++;                     //count for  odd numbers
        }
                                            //CAUSE odd -> odd OR even -> even cost 0
                                            //      odd -> even OR even -> odd cost 1
        return min(odd, even);              //SO    the min amount of odd numbers and even numbers = min cost                               
    }
};
