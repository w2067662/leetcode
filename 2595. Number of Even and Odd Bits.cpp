//https://leetcode.com/problems/number-of-even-and-odd-bits/description/

class Solution {
public:
    vector<int> evenOddBit(int n) {
        int even = 0, odd = 0;
        string temp = "";

        while(n>0){              //convert to binary string
            temp += n%2+'0';
            n/=2;
        }

        for(int i=0;i<temp.size();i++){
            if(temp[i] == '1') i%2 == 0 ? even++ : odd++;   //IF    found '1' AND index is even, even+1
        }                                                   //IF    found '1' AND index is  odd, odd +1

        return {even, odd};
    }
};
