//https://leetcode.com/problems/find-all-k-distant-indices-in-an-array/description/

class Solution {
public:
    vector<int> findKDistantIndices(vector<int>& nums, int key, int k) {
        vector<int> ans, indexs;
        set<int> set;

        for(int i=0;i<nums.size();i++){
            if(nums[i] == key) indexs.push_back(i);          //IF   number = key, store the index of current number into vector
        }

        for(const auto index: indexs){                       //for each index that number[index] = key
            for(int i=index-k; i<=index+k; i++){             //within the distance K (from index-K to index+K)
                if(0 <= i && i < nums.size()) set.insert(i); //insert the index into set
            }
        }

        for(const auto index: set) ans.push_back(index);     //push the possible answers from set into vector in ascending order

        return ans;
    }
};
