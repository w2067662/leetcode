//https://leetcode.com/problems/valid-palindrome-ii/description/

class Solution {
public:
    bool checkPalindrome(string s){ //check the paindrome
        for(int i=0;i<s.length()/2;i++){
            if(s[i]!=s[s.length()-i-1]) return false;
        }
        return true;
    }

    bool validPalindrome(string s) {
        for(int i=0;i<s.length()/2;i++){
            //skip the different letter for only once, and check palindrome from i+1 to j OR from i to j-1
            if(s[i]!=s[s.length()-i-1]) return checkPalindrome(s.substr(i+1, s.length()-i*2-1)) || checkPalindrome(s.substr(i, s.length()-i*2-1));
        }
        return true;
    }
};
