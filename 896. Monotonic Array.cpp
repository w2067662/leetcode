//https://leetcode.com/problems/monotonic-array/description/

class Solution {
public:
    bool isMonotonic(vector<int>& nums) {
        vector<int> vec;
        bool isAscending;

        for(int i=0; i<nums.size()-1; i++){ 
            vec.push_back(nums[i+1]-nums[i]);    //count for nums[i+1]-nums[i], and push into vector
        }

        for(int i=0; i<vec.size(); i++){
            if(vec[i] != 0){
                isAscending = vec[i] > 0 ? true : false;   //check the number array should be ascending or descending
                break;
            }
        }

        switch(isAscending){                     
            case true:                            //IF   array should be ascending
                for(int i=0; i<vec.size(); i++){  //     check the array is monotonic ascending array
                    if(vec[i] < 0) return false;  //     IF  contain found descending pair numbers -> return FALSE
                }
                break;
            case false:                           //IF   array should be descending
                for(int i=0; i<vec.size(); i++){  //     check the array is monotonic decending array
                    if(vec[i] > 0) return false;  //     IF  contain found ascending pair numbers  -> return TRUE
                }
                break;
        }

        return true;
    }
};
