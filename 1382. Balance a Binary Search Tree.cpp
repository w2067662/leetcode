//https://leetcode.com/problems/balance-a-binary-search-tree/description/

class Solution {
public:
    void inOrder(TreeNode* root, vector<int> &inorder){ // Push the node value inorder into vector
        if(!root) return;

        inOrder(root->left , inorder);
        inorder.push_back(root->val);
        inOrder(root->right, inorder);

        return;
    }

    TreeNode* buildTree(vector<int> &inorder, int left, int right){ // Build the tree
        if(left > right) return NULL;

        int mid = left + (right-left)/2;      // Calculate the middle index

        return new TreeNode(                  // Create a new node and return to parent
            inorder[mid],
            buildTree(inorder,  left, mid-1),
            buildTree(inorder, mid+1, right)
        );
    }

    TreeNode* balanceBST(TreeNode* root) {
        vector<int> inorder;

        inOrder(root, inorder);

        return buildTree(inorder, 0, inorder.size()-1); // Use inorder list to build the tree
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
 