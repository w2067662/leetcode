//https://leetcode.com/problems/maximum-product-of-two-elements-in-an-array/description/

class Solution {
public:
    int maxProduct(vector<int>& nums) {
        sort(nums.begin(), nums.end());                         //sort the vector in ascending order
        return (nums[nums.size()-1]-1)*(nums[nums.size()-2]-1); //calculate max product of two elements in an array
    }
};
