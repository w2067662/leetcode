//https://leetcode.com/problems/binary-tree-paths/description/

class Solution {
private:
    vector<int> path;    //for tree path
    vector<string> ans;
public:
    //ex: path = [1,2,5]
    //      s = "1->2->5"
    //    ans = ["1->2->5"]
    void makePath(){
        string s="";
        for(int i=0;i<path.size();i++){
            if(i == path.size()-1) s+=to_string(path[i]);
            else s+= to_string(path[i])+ "->";
        }
        ans.push_back(s);
    }
    void recurrence(TreeNode* root){
        if(!root)return;

        path.push_back(root->val);   //push current node's value as the path end
        if(!root->left && !root->right)makePath();  //if find leafnode, make path

        recurrence(root->left );
        recurrence(root->right);

        path.pop_back(); //pop the path end
        return;
    }
    vector<string> binaryTreePaths(TreeNode* root) {
        recurrence(root);
        return ans;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
