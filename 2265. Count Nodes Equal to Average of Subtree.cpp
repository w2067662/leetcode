//https://leetcode.com/problems/count-nodes-equal-to-average-of-subtree/description/

class Solution {
public:
    int sum(TreeNode* root){    //calculate the sum of the tree
        return !root ? 0 : sum(root->left) + sum(root->right) + root->val;
    }

    int size(TreeNode* root){   //get the size of the tree
        return !root ? 0 : size(root->left) + size(root->right) + 1;
    }

    void traverse(TreeNode* root, int &count){         //traverse the tree to find possible nodes  
        if(!root) return;

        if(sum(root)/size(root) == root->val) count++; //IF   current node's value = tree's sum / tree's value -> count++

        traverse(root->left , count);                  //traverse  left subtree
        traverse(root->right, count);                  //traverse right subtree

        return;
    }

    int averageOfSubtree(TreeNode* root) {
        int count = 0;
        traverse(root, count);
        return count;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
