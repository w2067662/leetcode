//https://leetcode.com/problems/find-winner-on-a-tic-tac-toe-game/description/

class Solution {
public:
    bool checkWin(vector<vector<int>>& matrix, int player){ //check this player wins
        int count = 0;  //for counting marks -> when count = 3 -> player wins

        for(int i=0;i<matrix.size();i++){                   //check each row 
            count = 0;
            for(int j=0;j<matrix[i].size();j++){
                if(matrix[i][j] == player) count++;
            }
            if(count == 3)return true;
        }

        for(int j=0;j<matrix[0].size();j++){                //check each column
            count = 0;
            for(int i=0;i<matrix.size();i++){
                if(matrix[i][j] == player) count++;
            }
            if(count == 3)return true;
        }

        count = 0;
        for(int i=0; i<matrix.size();i++){                  //check diagonal (left top to right bottom)
            if(matrix[i][i] == player) count++;
        }
        if(count == 3)return true;

        count = 0;
        for(int i=0; i<matrix.size();i++){                  //check diagonal (right top to left bottom)
            if(matrix[i][matrix.size()-1-i] == player) count++;
        }
        if(count == 3)return true;

        return false;
    }

    bool checkPending(vector<vector<int>>& matrix){ //check pending (no one wins yet, still movements to play)
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){
                if(matrix[i][j] == -1) return true; //found unmarked position on matrix -> Pending
            }
        }
        return false;
    }

    string tictactoe(vector<vector<int>>& moves) {
        vector<vector<int>> matrix (3, vector<int>(3, -1));
        const int PlayerA = 0;
        const int PlayerB = 1;
        int currentPlayer = PlayerA; //start from PlayerA

        for(int i=0;i<moves.size();i++){
            if(currentPlayer == PlayerA){                   //if current player is PlayerA
                matrix[moves[i][0]][moves[i][1]] = PlayerA; //mark 0 on matrix
                currentPlayer = PlayerB;                    //switch to PlayerB
            } else if (currentPlayer == PlayerB){           //if current player is PlayerB
                matrix[moves[i][0]][moves[i][1]] = PlayerB; //mark 1 on matrix
                currentPlayer = PlayerA;                    //switch to PlayerA
            }
        }

        if(checkWin(matrix, PlayerA)) return "A";   //check PlayerA wins
        if(checkWin(matrix, PlayerB)) return "B";   //check PlayerB wins
        if(checkPending(matrix)) return "Pending";  //check Pending
        return "Draw";                              //Is Draw
    }
};
