//https://leetcode.com/problems/sum-of-digits-of-string-after-convert/description/

class Solution {
public:
    string  transform(string s){            //transform the letters (count for digit sum)
        int res = 0;            

        for(int i=0;i<s.length();i++){
            res += s[i] - '0';
        }

        return to_string(res);
    }

    int getLucky(string s, int k) {
        string temp = "";
        int ans = 0;

        for(int i=0;i<s.length();i++){
            temp += to_string(s[i]-'a'+1);  //convert letter string to number string
        }

        for(int i=0;i<k;i++){
            temp = transform(temp);         //trasform for k times
        }

        for(int i=0;i<temp.length();i++){
            ans = ans*10 + temp[i] - '0';   //convert result string to result integar
        }

        return ans;
    }
};
