//https://leetcode.com/problems/count-number-of-pairs-with-absolute-difference-k/description/

class Solution {
public:
    int countKDifference(vector<int>& nums, int k) {
        int ans = 0;

        for(int i=0; i<nums.size()-1; i++){
            for(int j=i+1; j<nums.size(); j++){
                if(abs(nums[j]-nums[i]) == k) ans++; //if difference of number pair (i, j) = k, answer+1
            }
        }

        return ans;
    }
};
