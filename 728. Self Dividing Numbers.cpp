//https://leetcode.com/problems/self-dividing-numbers/description/

class Solution {
public:
    bool self_divisible (int num){          //check self divisible or not
        string temp = to_string(num);
        for (int i=0;i<temp.length();i++){
            if((temp[i]-'0')==0 || num%(temp[i]-'0') !=0) return false; //no contain '0' or non-self divisible digit
        }
        return true;
    }
    vector<int> selfDividingNumbers(int left, int right) {
        vector<int> ans;
        for(int i=left;i<=right;i++){                   //traverse the range
            if (self_divisible(i)) ans.push_back(i);    //check for self divisible
        }
        return ans;
    }
};
