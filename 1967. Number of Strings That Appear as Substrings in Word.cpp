//https://leetcode.com/problems/number-of-strings-that-appear-as-substrings-in-word/description/

class Solution {
public:
    int numOfStrings(vector<string>& patterns, string word) {
        int ans = 0;
        for(int i=0;i<patterns.size();i++){                                  //traverse strings in patterns
            int Jmax = word.length()-patterns[i].length()+1;
            for(int j=0; j<Jmax; j++){                                       //j start from 0 to word.length-patterns[i].length+1
                if(patterns[i].length() <= word.length()){                   //patterns[i].length should be smaller than or equal to word.length
                    if(patterns[i] == word.substr(j, patterns[i].length())){ //if patterns[i] is word's substring, answer+1
                        ans++;
                        break;
                    } 
                }
            }
        }
        return ans;
    }
};
