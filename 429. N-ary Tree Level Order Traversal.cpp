//https://leetcode.com/problems/n-ary-tree-level-order-traversal/description/

class Solution {
public:
    void traverse(Node* root, int level, vector<vector<int>> &levelList){
        if(!root)return;

        if(level >= levelList.size()) levelList.push_back({root->val}); //IF   no current level -> push current level vector with current node value into level list
        else levelList[level].push_back(root->val);                     //ELSE -> push current node value into current level

        for(const auto child: root->children){
            traverse(child, level+1, levelList); //traverse all the children
        }

        return;
    }

    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> levelList;

        traverse(root, 0, levelList);   //traverse the N-ary Tree Level

        return levelList;
    }
};

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
