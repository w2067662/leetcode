//https://leetcode.com/problems/minimum-operations-to-reduce-x-to-zero/description/

class Solution {
public:
    //ex: nums = [1,1,4,2,3], x = 5 
    //
    //  sum(1,1,4,2,3) = sum(1,1,4) + sum(2,3)
    //  sum(1,1,4,2,3) = sum(1,1,4) + x
    //  sum(1,1,4,2,3) - x = sum(1,1,4) 
    //  6 = sum(1,1,4)
    //      ^ find the max length of exclusive subarray
    //        -> arr.size - maxLen of exclusive subarray = min operations
    //
    int minOperations(vector<int>& nums, int x) {
        int maxLength = -1, currSum = 0, totalSum = 0;

        for(const auto num: nums) totalSum += num;               //calculate the sum
        
        for (int left=0, right=0; right<nums.size(); right++) {  //traverse the array using window -> range = {left, right}
            currSum += nums[r];

            while (left <= right && currSum > totalSum - x) {    //WHILE  currSum > totalSum - x (target)
                currSum -= nums[left++];                         //       -> deduct nums[left] and left+1
            }                                                    //          until currSum <= totalSum - x (target)
                
            if(currSum == totalSum - x) {                        //IF  found totalSum - x (target)
                maxLength = max(maxLength, right-left+1);        //    -> store the max length of possible subarrays
            }  
        }

        return maxLength == -1 ? -1 : nums.size()-maxLength;     //IF  target found -> return arr.size - maxLen of exclusive subarray = min operations
                                                                 //ELSE　-> return -1
    }
};
