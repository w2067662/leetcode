//https://leetcode.com/problems/last-visited-integers/description/

class Solution {
private:
    vector<int> vec;
    int prev;
public:
    int toIntegar(string s){    //convert string to integar
        int num = 0;
        for(const auto ch: s){
            num = num*10 + ch -'0';
        }
        return num;
    }

    vector<int> lastVisitedIntegers(vector<string>& words) {
        vector<int> visited;
        prev = -1;

        for(const auto word: words){
            if(word == "prev"){                     //IF   current word is "prev"
                if(prev < 0) visited.push_back(-1); //     IF   no previous -> push -1 into visited
                else {                              //     ELSE
                    visited.push_back(vec[prev]);   //                      -> push previous into visited
                    prev--;                         //                      -> previous-1
                }
            } else {                                //IF   current word is number string
                vec.push_back(toIntegar(word));     //     -> push into vector
                prev = vec.size()-1;                //     -> set previous at last number in vector
            }
        }

        return visited;
    }
};
