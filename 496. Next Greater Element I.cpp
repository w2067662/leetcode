//https://leetcode.com/problems/next-greater-element-i/description/

class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {
        map<int ,int>map;   //store the number's index of nums2
        vector<int> ans;
        
        for(int i=0;i<nums2.size();i++){    //map = [{num, index}...]
            map.insert({nums2[i], i});
        }

        bool GreaterElementFound = false;   //tag for finding Greater Element

        for(int i=0;i<nums1.size();i++){
            GreaterElementFound = false;    //reset tag as false each loop
            for(int j=map[nums1[i]];j<nums2.size();j++){ //start from num's current index to nums2's end
                if(nums2[j]>nums1[i]){
                    ans.push_back(nums2[j]);    //push first found Greater Element to answers
                    GreaterElementFound = true;
                    break;
                }
            }
            if(!GreaterElementFound){   //if no Greater Element, push -1 to answers
                ans.push_back(-1);
            }
        }
        return ans;
    }
};
