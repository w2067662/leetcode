//https://leetcode.com/problems/encode-and-decode-tinyurl/description/

class Solution {
private:
    map<string, string> map;    //map = {[shortUrl] : [longUrl]}
public:

    // Encodes a URL to a shortened URL.
    string encode(string longUrl) {
        string shortUrl = generateShortUrl();   //generate a unique shortUrl

        while(map.find(shortUrl) != map.end()){
            shortUrl = generateShortUrl();
        }

        map[shortUrl] = longUrl;                //store into map
        
        return shortUrl;
    }

    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl) {
        return this->map[shortUrl];             //use shortUrl as key to get longUrl
    }

    // Generate a unique short url
    string generateShortUrl() { 
        const string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //contain 0-9, a-z, A-Z
        const int length = 6;   //contain exactly 6 char

        string randomString;
        randomString.reserve(length);

        // Seed the random number generator
        srand(static_cast<unsigned int>(time(nullptr)));

        for (int i = 0; i < length; ++i) {
            int index = rand() % characters.length(); //random select a char
            randomString += characters[index];        //append to randomString
        }
        return randomString;
    }
};

// Your Solution object will be instantiated and called as such:
// Solution solution;
// solution.decode(solution.encode(url));
