//https://leetcode.com/problems/greatest-english-letter-in-upper-and-lower-case/description/

class Solution {
public:
    string greatestLetter(string s) {
        vector<pair<int, int>> vec(26, {0, 0});
        string ans ="";

        for(const auto ch: s){
            if(isupper(ch)) vec[ch-'A'].second++;       //count for appear times of upper case letter
            else if(islower(ch)) vec[ch-'a'].first++;   //count for appear times of lower case letter
        }

        for(int i=vec.size()-1;i>=0;i--){
            if(vec[i].first != 0 && vec[i].second != 0){//IF   current letter has upper and lower
                ans += i+'A';                           //     -> return current letter's upper case
                return ans;
            } 
        }

        return "";
    }
};
