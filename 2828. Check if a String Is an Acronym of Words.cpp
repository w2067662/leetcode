//https://leetcode.com/problems/check-if-a-string-is-an-acronym-of-words/description/

class Solution {
public:
    bool isAcronym(vector<string>& words, string s) {
        if(words.size()!=s.length())return false;

        for(int i=0;i<s.length();i++){
            if(words[i][0]!=s[i])return false; //check current word's first letter is the same to string's current letter or not
        }

        return true;
    }
};
