//https://leetcode.com/problems/kth-missing-positive-number/description/

class Solution {
public:
    int findKthPositive(vector<int>& arr, int k) {
        set<int> set;
        int current = 0;

        for(auto num:arr) set.insert(num);        //insert all the numbers from array into set

        while(k>0){
            current++;                            //start finding missing numbers from 1
            if(set.find(current) == set.end())k--;//if found 1 missing number, K-1
        }

        return current;                           //return the Kth missing number
    }
};
