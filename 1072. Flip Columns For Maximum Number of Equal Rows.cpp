//https://leetcode.com/problems/flip-columns-for-maximum-number-of-equal-rows/description/

class Solution {
public:
    //ex:  matrix = [[0,0,0],
    //               [0,0,1],
    //               [1,1,0]]
    //
    //
    //    pattern = ["TTT",   ->  map = {{"TTT": 1}, {"TTF": 2}}
    //               "TTF",
    //               "TTF"]
    //
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) {
        map<string, int> map;
        int maxEqualRows = 0;

        for(int i=0; i<matrix.size(); i++){                   // For all the rows
            int prev = matrix[i][0];
            string pattern = "T";                             //    Start with 'T'

            for(int j=0; j<matrix[i].size(); j++){
                pattern += prev == matrix[i][j] ? 'T' : 'F';  //    Convert the bits to pattern string
                prev = matrix[i][j];
            }

            map[pattern]++;                                   //    Store the frequency of same pattern
        }

        for(const auto it: map){
            maxEqualRows = max(maxEqualRows, it.second);      // Get the max frequency (max equal rows after flips)
        }

        return maxEqualRows;
    }
};
