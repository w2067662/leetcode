//https://leetcode.com/problems/combination-sum/description/

class Solution {
public:
    vector<vector<int>>list;    //answer list

    void take_element(bool take, int index, int target, vector<int>& candidates, vector<int> vec){
        if(take){                                   //if take the current element,
            target-=candidates[index];              //target = target-candidates[i]
            vec.push_back(candidates[index]);       //vector = [..., candidates[i]] (push the element to vector's back)
        }
        else{                                       //if don't take the current element,
            index++;                                //goto next element,
            if(index >= candidates.size())return;   //if next element is out of candidates' size, then return
        }

        //check for target's value
        if(target==0){                                            //if target = 0,
            list.push_back(vec);                                  //find 1 possible conbination
        }
        else if(target<0){                                        //if target < 0,
            return;                                               //conbination sum is bigger than target
        }
        else{                                                     //if target > 0, (goto next recursion)
            take_element(true, index, target, candidates, vec);   //take candidates[index]
            take_element(false, index, target, candidates, vec);  //don't take candidates[index]
        }

    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        vector<int> vec;
        //*** hint: binary tree, left branch(take the element); right branch(don't take the element, goto next) ***
        take_element(true , 0, target, candidates, vec);    //take the first element
        take_element(false, 0, target, candidates, vec);    //not take

        return list;
    }
};
