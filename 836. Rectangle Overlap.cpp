//https://leetcode.com/problems/rectangle-overlap/description/

class Solution {
public:
    //            ____________ (x3,y3)
    //           |            |
    //    _______|____ (x1,y1)|
    //   |       |    |       |
    //   |       |____|_______|
    //   |   (x2,y2)  |
    //   |____________|
    //(x0,y0)
    //
    bool isRectangleOverlap(vector<int>& rec1, vector<int>& rec2) {
        int x0 = rec1[0], y0 = rec1[1], x1 = rec1[2], y1 = rec1[3];
        int x2 = rec2[0], y2 = rec2[1], x3 = rec2[2], y3 = rec2[3];

        return x0 < x3 && y0 < y3 && x2 < x1 && y2 < y1;
    }
};
