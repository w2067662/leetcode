//https://leetcode.com/problems/minimum-number-of-operations-to-make-array-xor-equal-to-k/description/

class Solution {
public:
    //ex:
    
    //     nums = [ 2 (010),   ^
    //              1 (001),   |
    //              3 (011),   |
    //              4 (100) ]  |
    //        k =   1 (001)    |
    //                 ^       0 xor 1 xor 0 xor 0 xor 0 = 1 (x) -> answer + 1
    //                  ^      0 xor 0 xor 1 xor 0 xor 1 = 0 (o)
    //                   ^     1 xor 0 xor 1 xor 1 xor 0 = 1 (x) -> answer + 1
    //                                                              answer = 2

    //     nums = [ 6 (110),   ^
    //              1 (001),   |
    //              2 (010),   |
    //              4 (100) ]  |
    //        k =   1 (001)    |
    //                 ^       0 xor 1 xor 0 xor 0 xor 1 = 0 (o)
    //                  ^      0 xor 0 xor 1 xor 0 xor 1 = 0 (o)
    //                   ^     1 xor 0 xor 0 xor 1 xor 0 = 0 (o)

    int minOperations(vector<int>& nums, int k){
        int XOR = 0, count = 0;

        for(const auto& num: nums){
            XOR ^= num;                   // Calculate total XOR
        }

        while(k || XOR){                  // WHILE   K or XOR  is not 0 
            count += ((k%2) != (XOR%2));  //         IF  last bit is different -> count + 1

            k   >>= 1;                     //         -> K   Shift right 1 bit
            XOR >>= 1;                     //         -> XOR Shift right 1 bit
        }

        return count;
    }
};