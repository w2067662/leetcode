//https://leetcode.com/problems/sum-of-all-subset-xor-totals/description/

class Solution {
public:
    int subsetXORSum(vector<int>& nums) {
        int sum = 0;

        for (const auto& num: nums) {
            sum |= num;     // Sum the XOR contributions of each element
        }

        return sum * (1 << (nums.size() - 1));  // Multiply by 2^(n-1)
    }
};
