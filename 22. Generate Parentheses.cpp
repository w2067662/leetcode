//https://leetcode.com/problems/generate-parentheses/description/

class Solution {
public:
    //ex: n=2, [x]:won't do the recursion
    //"("
    //"(("                             ;"()"
    //"(((" [x]          ;"(()"        ;"()("           ;"())" [x]
    //"(((("[x];"((()"[x];"(()(";"(())";"()(("[x];"()()";"())("[x];"()))"[x]
    //valid answers = "(())";"()()"

    vector<string> recursion(int left, int right, string str, vector<string>& vec){
        if(left >0) recursion(left-1, right  , str+"(", vec);   //if left brace can be added, add left brace, and do the recursion
        if(right>0 && right>left) recursion(left  , right-1, str+")", vec); //if right brace can be added,
                                                                            //and there are unpaired left braces lefted, 
                                                                            //then add right brace, and do the recursion

        if(left ==0 && right ==0)vec.push_back(str);    //all the left and right braces are used,
                                                        //then the string should be the possible answer.
        return vec;
    }
    vector<string> generateParenthesis(int n) {
        vector<string> ans; //empty vector for answer strings

        return recursion(n, n,"", ans); //start tree recursion from here,(n's left braces and n's right braces)
    }
};
