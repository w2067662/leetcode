//https://leetcode.com/problems/daily-temperatures/description/

class Solution {
public:
    //ex: temperatures = [73,74,75,71,69,72,76,73]                                        
    //                                                         ; stack = []                     ; ans = [0,0,0,0,0,0,0,0]  
    //                     ^                         index = 0 ; stack = [{0,73}]               ; ans = [0,0,0,0,0,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                     *  ^                       73 < 74  ; stack = [] (pop)               ; ans = [1,0,0,0,0,0,0,0]
    //                                               index = 1 ; stack = [{1,74}]               ; ans = [1,0,0,0,0,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                        *  ^                    74 < 75  ; stack = [] (pop)               ; ans = [1,1,0,0,0,0,0,0]
    //                                               index = 2 ; stack = [{2,75}]               ; ans = [1,1,0,0,0,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                           *  ^                 75 > 71  ; stack = [{2,75},{3,71}]        ; ans = [1,1,0,0,0,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                              *  ^              71 > 69  ; stack = [{2,75},{3,71},{4,69}] ; ans = [1,1,0,0,0,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                                 *  ^           69 < 72  ; stack = [{2,75},{3,71}] (pop)  ; ans = [1,1,0,0,1,0,0,0]
    //                              *     ^           71 < 72  ; stack = [{2,75}] (pop)         ; ans = [1,1,0,2,1,0,0,0]
    //                                               index = 5 ; stack = [{2,75},{5,72}]        ; ans = [1,1,0,2,1,0,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                                    *  ^        72 < 76  ; stack = [{2,75}] (pop)         ; ans = [1,1,0,2,1,1,0,0]
    //                           *           ^        75 < 76  ; stack = [] (pop)               ; ans = [1,1,4,2,1,1,0,0]
    //                                               index = 6 ; stack = [{6,76}]               ; ans = [1,1,4,2,1,1,0,0]
    //                   [73,74,75,71,69,72,76,73]
    //                                       *  ^    index = 7 ; stack = [{6,76},{7,73}]        ; ans = [1,1,4,2,1,1,0,0]
    //
    //                                                                                       ->   ans = [1,1,4,2,1,1,0,0]
    //
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        stack<pair<int ,int>> stack;
        vector<int> ans(temperatures.size(), 0);
        
        for(int i=0; i<temperatures.size(); i++){                   // For i from 0 to temperatures.size
            while(!s.empty() && s.top().second < temperatures[i]){  //      WHILE  stack is not empty AND
                                                                    //             stack.top < current temperature
                ans[s.top().first] = i-s.top().first;               //             -> set stack.top's next warmer day's distance to answer
                s.pop();                                            //             -> pop stack top element
            }
            s.push({i, temperatures[i]});                           //      Push current {index, current temperature} into stack
        }
        
        return ans;
    }
};
