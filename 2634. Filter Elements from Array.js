//https://leetcode.com/problems/filter-elements-from-array/description/

/**
 * @param {number[]} arr
 * @param {Function} fn
 * @return {number[]}
 */
var filter = function(arr, fn) {
    var ans=[];
    for(let i=0, j=0;i<arr.length;i++){
        if (fn(arr[i], i))ans[j++] = arr[i];
    }
    return ans;
};
