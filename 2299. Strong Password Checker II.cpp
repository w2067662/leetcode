//https://leetcode.com/problems/strong-password-checker-ii/description/

class Solution {
public:
    bool isSpecial(char ch){
        switch(ch){
            case '!':case '@':case '#':case '$':case '%':case '^':
            case '&':case '*':case '(':case ')':case '-':case '+': return true;
        }
        return false;
    }

    bool strongPasswordCheckerII(string password) {
        if(password.length() < 8)return false;

        bool hasLower = false, hasUpper = false, hasDigit = false, hasSpecial = false;

        for(const auto ch: password){                  
            if(islower(ch)) hasLower = true;           //check for lower case
            else if (isupper(ch)) hasUpper = true;     //check for upper case
            else if (isdigit(ch)) hasDigit = true;     //check for digit
            else if (isSpecial(ch)) hasSpecial = true; //check for special char
        }

        for(int i=1;i<password.length();i++){
            if(password[i-1]==password[i])return false;//check for consecutive letters
        }

        return hasLower && hasUpper && hasDigit && hasSpecial;
    }
};
