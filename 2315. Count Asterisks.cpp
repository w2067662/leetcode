//https://leetcode.com/problems/count-asterisks/description/

class Solution {
public:
    int countAsterisks(string s) {
        vector<string> vec;
        string temp = "";
        int count = 0;

        for(const auto ch:s){               //split the words from string and store into vector
            if(ch == '|'){
                vec.push_back(temp);
                temp = "";
            }
            else temp += ch;
        }
        if(temp != "") vec.push_back(temp);

        for(int i=0;i<vec.size();i++){
            if(i%2 == 0){                   //count the Asterisks of the words with even index
                for(const auto ch: vec[i]){
                    if(ch == '*') count++;
                }
            }
        }

        return count;
    }
};
