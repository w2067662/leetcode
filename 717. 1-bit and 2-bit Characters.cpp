//https://leetcode.com/problems/1-bit-and-2-bit-characters/description/

class Solution {
public:
    bool isOneBitCharacter(vector<int>& bits) {
        for(int i=0;i<bits.size();){
            if(i == bits.size()-1) return true; //check the last 0 is unpaired or not
            i += (bits[i]==1) ? 2 : 1; //if encounter 1, then 1 should be paired to another 1 or 0 -> i+=2
                                       //if encounter 0, then this 0 is unpaired -> i+=1
        }
        return false;
    }
};
