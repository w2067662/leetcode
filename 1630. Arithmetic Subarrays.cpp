//https://leetcode.com/problems/arithmetic-subarrays/description/

class Solution {
public:
    bool isArithmetic(vector<int>& arr){    //check array is arithmetic or not
        if(arr.size() <= 2) return true;

        int diff = arr[1]-arr[0];

        for(int s=2; s<arr.size(); s++){
            if(arr[s]-arr[s-1] != diff) return false;
        }

        return true;
    }

    vector<bool> checkArithmeticSubarrays(vector<int>& nums, vector<int>& l, vector<int>& r) {
        vector<bool> ans;

        for(int i=0;i<l.size();i++){
            vector<int> temp;

            for(int j=l[i]; j<=r[i]; j++) temp.push_back(nums[j]);  //push the number within range = (l,r) into temp

            sort(temp.begin(), temp.end());                         //sort temp in ascending order

            ans.push_back(isArithmetic(temp));                      //check the temp array is arithmetic or not, and push into answer
        }

        return ans;
    }
};
