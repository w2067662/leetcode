#https://leetcode.com/problems/classes-more-than-5-students/description/

# Write your MySQL query statement below
SELECT class
FROM Courses
GROUP BY class
HAVING count(class)>=5  #using count() function should be using GROUP BY and HAVING statement
