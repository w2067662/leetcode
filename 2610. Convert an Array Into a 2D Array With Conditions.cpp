//https://leetcode.com/problems/convert-an-array-into-a-2d-array-with-conditions/description/

class Solution {
public:
    vector<vector<int>> findMatrix(vector<int>& nums) {
        vector<vector<int>> ans;
        map<int, int> map;

        for(const auto num: nums) map[num]++;                                           //push the appear times of numbers into map

        for(const auto it: map){                                                        //traverse the map
            for(int i=0; i<it.second; i++){                                             //i start from 0 to current number's appear times
                i < ans.size() ? ans[i].push_back(it.first) : ans.push_back({it.first});//IF i < answer's size -> push current number into existed vector
                                                                                        //ELSE -> push new vector with current number into answer
            }
        }

        return ans;
    }
};
