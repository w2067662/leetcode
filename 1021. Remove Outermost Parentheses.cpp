//https://leetcode.com/problems/remove-outermost-parentheses/description/

class Solution {
public:
    string delete_space(string s){          //delete replaced " " from the string
        string res="";
        for(int i=0;i<s.length();i++){
            if(s[i]!=' ')res+=s[i];
        }
        return res;
    }

    string removeOuterParentheses(string s) {
        bool haveLeftBrace=false;
        int brace=0;

        for(int i=0;i<s.length();i++){
            if(haveLeftBrace){              //if already have left brace,
                if(s[i]=='(') brace++;      //and encounter left brace, brace++
                else if(s[i]==')'){
                    if(brace==0){           //find matched right brace
                        s[i]=' ';           //change ")" to " "
                        haveLeftBrace=false;//now don't have Left Brace
                    }
                    else brace--;           //if already have left brace,
                }                           //and encounter right brace,
                                            //and is not matched right brace, brace--
            }else{
                if(s[i]=='('){              //if encounter left brace first time
                    s[i]=' ';               //change "(" to " "
                    haveLeftBrace=true;     //now have Left Brace
                }
            }
        }
        return delete_space(s);             //delete replaced " " from the string
    }
};
