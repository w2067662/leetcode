//https://leetcode.com/problems/perfect-squares/description/

class Solution {
public:
    //ex: n = 5
    //                                  DP[*,*,*,*,*,*] (*: INT_MAX)
    // DP[0] = 0
    //                               -> DP[0,*,*,*,*,*]
    // i = 1 ; j = 1
    // DP[1-1^2] + 1 = DP[0] + 1 = 1   
    //                               -> DP[0,1,*,*,*,*]
    // i = 2 ; j = 1
    // DP[2-1^2] + 1 = DP[1] + 1 = 2
    //                               -> DP[0,1,2,*,*,*]
    // i = 2 ; j = 1
    // DP[2-1^2] + 1 = DP[1] + 1 = 2
    //                               -> DP[0,1,2,*,*,*]
    // i = 3 ; j = 1
    // DP[3-1^2] + 1 = DP[2] + 1 = 3
    //                               -> DP[0,1,2,3,*,*]
    // i = 4 ; j = 1
    // DP[4-1^2] + 1 = DP[3] + 1 = 4
    // i = 4 ; j = 2
    // DP[4-2^2] + 1 = DP[0] + 1 = 1  
    // min(4,1) = 1
    //                               -> DP[0,1,2,3,1,*]
    // i = 5 ; j = 1
    // DP[5-1^2] + 1 = DP[4] + 1 = 2
    // i = 5 ; j = 2
    // DP[5-2^2] + 1 = DP[1] + 1 = 2
    // min(1,1) = 1
    //                               -> DP[0,1,2,3,1,2]

    // answer = 2
    //
    int numSquares(int n) {
        vector<int> DP(n+1, INT_MAX);

        DP[0] = 0;                                        // Set DP[0] as 0

        for(int i=1; i<=n; i++) {                         // For i from 1 to N
            for(int j=1; pow(j, 2)<=i; j++){              //    For j from 1 to M^2 <= i 
                DP[i] = min(DP[i], DP[i-pow(j, 2)] + 1);  //        Get the min elements that can form current number i
                //                 ^                 ^
                //                 |                 the number pow(j, 2) as a element
                //                 the min element to form i-pow(j, 2)
            }
        }

        return DP[n];
    }
};
