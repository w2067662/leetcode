//https://leetcode.com/problems/reverse-words-in-a-string-iii/description/

class Solution {
public:
    string reverse(string& s){  //reverse the words
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }

    string reverseWords(string s) {
        vector<string> words;
        string temp ="";
        string ans = "";

        for(const auto ch: s){               //store the words into vector
            if(ch == ' '){
                words.push_back(temp);
                temp = "";
            }else temp += ch;
        }
        if(temp != "")words.push_back(temp);

        for(int i=0;i<words.size();i++){      
            ans += reverse(words[i]);        //append the revered words to answer
            if(i != words.size()-1) ans+=" ";
        } 

        return ans;
    }
};
