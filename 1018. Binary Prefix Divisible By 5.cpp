//https://leetcode.com/problems/binary-prefix-divisible-by-5/description/

class Solution {
public:
    vector<bool> prefixesDivBy5(vector<int>& nums) {
        vector<bool> ans;
        int lastDecimalDigit = 0;

        for(const auto num: nums){
            lastDecimalDigit = (lastDecimalDigit*2 + num)%10; //calculate the last digit in decimal
            ans.push_back(lastDecimalDigit%5 == 0);           //IF    last digit of decimal number is divisible by 5 -> TRUE
                                                              //ELSE  -> FALSE
        }

        return ans;

    }
};
