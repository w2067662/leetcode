//https://leetcode.com/problems/palindrome-linked-list/description/

class Solution {
public:
    bool isPalindrome(ListNode* head) {
        vector<int> vec;

        while(head){                            // Traverse the linked-list
            vec.push_back(head->val);           //      -> Push values into vector
            head= head->next;                   //      -> Goto next node
        }

        for(int i=0; i<vec.size()/2; i++){      // Check for Palindrome
            if(vec[i] != vec[vec.size()-i-1]){
                return false;
            }
        }

        return true;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */