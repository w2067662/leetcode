//https://leetcode.com/problems/fair-candy-swap/description/

class Solution {
public:
    vector<int> fairCandySwap(vector<int>& aliceSizes, vector<int>& bobSizes) {
        int aliceHas =0;
        int bobHas   =0;
        int differ;

        for(int i=0;i<aliceSizes.size();i++) aliceHas += aliceSizes[i]; //count for alice's total candies
        for(int i=0;i<bobSizes.size()  ;i++) bobHas   += bobSizes[i];   //count for bob's   total candies

        differ = (bobHas - aliceHas)/2; //CAUSE after swapping one box from alice and one box from bob,
                                        //      the total candies of alice and bob should be equal
                                        //SO    the difference amount candies of one box from alice and one box from bob 
                                        //      sould be (bobHas - aliceHas)/2

        for(int i=0;i<aliceSizes.size();i++){
            for(int j=0;j<bobSizes.size();j++){
                if(bobSizes[j]-aliceSizes[i]==differ) return {aliceSizes[i],bobSizes[j]}; //find a pair of boxes that match the condition
            }
        }
        return {-1,-1}; //no possible answer
    }
};
