//https://leetcode.com/problems/account-balance-after-rounded-purchase/description/

class Solution {
public:
    //ex1: purchaseAmount = 25
    //     purchaseAmount % 10 = 5 >= 5 -> nearest rounded number of 25 = 30 -> answer = 100 - 30 = 70
    //ex2: purchaseAmount = 24
    //     purchaseAmount % 10 = 4 <  5 -> nearest rounded number of 24 = 20 -> answer = 100 - 20 = 80
    int accountBalanceAfterPurchase(int purchaseAmount) {
        return purchaseAmount%10 < 5 ? 100-purchaseAmount/10*10 : 100-(purchaseAmount/10+1)*10;
    }
};
