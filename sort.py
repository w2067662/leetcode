import os
import re
from datetime import datetime


# Get the directory of the current script
def get_script_directory():
    return os.path.dirname(os.path.abspath(__file__))


# Get the list of file names in the script's directory
def get_file_names(script_directory):
    file_names = os.listdir(script_directory)

    # Exclude certain files
    exclude_files = {".git", "README.md", "list.md", "sort.py"}
    return [name for name in file_names if name not in exclude_files]


# Extract numeric prefix and sort the file names
def get_numeric_prefix(name):
    match = re.match(r'^\d+', name)
    return int(match.group()) if match else float('inf')


# Create README.md file
def create_readme_file(script_directory, sorted_file_names):
    readme_file_path = os.path.join(script_directory, "README.md")

    current_time = datetime.now().strftime("%Y-%m-%d")

    with open(readme_file_path, "w", encoding="utf-8") as file:
        file.write("# 🤔 Leetcode\n\n")
        file.write("My solutions for Leetcode problems.\n\n")
        file.write("[Finished Problems List](list.md)\n\n")
        file.write(f"Total updated solutions: {len(sorted_file_names)}\n\n")
        file.write(f"Last Updated: {current_time}\n")

    print(f"README.md has been created at {readme_file_path}")


# Create list.md file
def create_list_file(script_directory, sorted_file_names):
    list_file_path = os.path.join(script_directory, "list.md")

    with open(list_file_path, "w", encoding="utf-8") as file:
        file.write("```\n\n")
        for name in sorted_file_names:
            clean_name = re.sub(r'\.\w+', '', name)  # Remove file extension
            file.write(f"{clean_name}\n")
        file.write("```\n\n")

    print(f"list.md has been created at {list_file_path}")


# ------------------------------
#             Main
# ------------------------------
if __name__ == "__main__":
    script_directory = get_script_directory()
    file_names = get_file_names(script_directory)
    sorted_file_names = sorted(file_names, key=get_numeric_prefix)

    # Output files
    create_readme_file(script_directory, sorted_file_names)
    create_list_file(script_directory, sorted_file_names)
