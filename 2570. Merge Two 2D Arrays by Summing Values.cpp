//https://leetcode.com/problems/merge-two-2d-arrays-by-summing-values/description/

class Solution {
public:
    vector<vector<int>> mergeArrays(vector<vector<int>>& nums1, vector<vector<int>>& nums2) {
        map<int, int> map;
        vector<vector<int>> ans;

        for(int i=0;i<nums1.size();i++){
            map[nums1[i][0]] = nums1[i][1];      //store key and value from nums1 into map
        }

        for(int i=0;i<nums2.size();i++){
            map[nums2[i][0]] += nums2[i][1];     //merge key and value from nums2 into map
        }

        for(const auto it:map){
            ans.push_back({it.first, it.second});//store merged key and value from map into answer
        }
        
        return ans;
    }
};
