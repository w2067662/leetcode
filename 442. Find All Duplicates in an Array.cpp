//https://leetcode.com/problems/find-all-duplicates-in-an-array/description/

class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {
        map<int, int> map;
        vector<int> duplicates;

        for(const auto num: nums){
            map[num]++;                         // Store appear times of numbers
        }

        for(const auto it: map){
            if(it.second >= 2){
                duplicates.push_back(it.first); // Push duplicates number into answer
            }
        }

        return duplicates;
    }
};
