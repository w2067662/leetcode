//https://leetcode.com/problems/remove-trailing-zeros-from-a-string/description/

class Solution {
public:
    string removeTrailingZeros(string num) {
        int count =0;

        for(int i=num.length()-1; i>=0; i--){   //count trailing zeros from string end
            if(num[i] != '0')break;
            count++;
        }

        return num.substr(0, num.size()-count); //return the substring without trailing zeros
    }
};
