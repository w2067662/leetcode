//https://leetcode.com/problems/count-subarrays-where-max-element-appears-at-least-k-times/description/

class Solution {
public:
    //ex: nums = [ 1, 3, 2, 3, 3],  k = 2   (maxNum = 3)
    //                                       maxNumFreq = 0       ;  count = 0
    //            LR                         maxNumFreq = 0 (< 2) ;  count = 0
    //            L   R                      maxNumFreq = 1 (< 2) ;  count = 0
    //            L      R                   maxNumFreq = 1 (< 2) ;  count = 0
    //            L         R                maxNumFreq = 2 (>=2) ;  count = 2(+2)
    //                L     R                maxNumFreq = 2 (>=2) ;  count = 4(+2)
    //                   L  R                maxNumFreq = 1 (< 2) ;  count = 4
    //                   L     R             maxNumFreq = 2 (>=2) ;  count = 5(+1)
    //                      L  R             maxNumFreq = 2 (>=2) ;  count = 6(+1)
    //                        LR             maxNumFreq = 1 (< 2) ;  count = 6
    //                                                            -> count = 6
    long long countSubarrays(vector<int>& nums, int k) {
        long long count = 0, maxNumFreq = 0;

        int maxNum = *max_element(nums.begin(), nums.end());  // Get max number from vector

        for(int left = 0, right = 0; right < nums.size(); ){  // For RIGHT < nums.size
            maxNumFreq += (nums[right] == maxNum);            //    IF nums[RIGHT] = maxNum
                                                              //       -> maxNum's frequency + 1

            while(maxNumFreq >= k){                           //    WHILE maxNum's frequency >= K
                maxNumFreq -= (nums[left] == maxNum);         //          IF nums[LEFT] = maxNum
                                                              //             -> maxNum's frequency - 1
                left++;                                       //          Goto next LEFT

                count += nums.size()-right;
            }

            right++;                                          //    Goto next RIGHT
        }

        return count;
    }
};