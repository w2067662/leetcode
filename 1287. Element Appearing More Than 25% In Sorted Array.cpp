//https://leetcode.com/problems/element-appearing-more-than-25-in-sorted-array/description/

class Solution {
public:
    int findSpecialInteger(vector<int>& arr) {
        unordered_map<int, int>umap;
        map<int, int>map;

        for(int i=0;i<arr.size();i++){
            umap[arr[i]]++;             //store {[number]:[appear times]} into umap
        }

        for(auto it:umap){
            map[it.second]=it.first;    //store {[appear times]:[number]} from umap to map
        }

        auto it=map.rbegin();           //get the number that have the largest appear times
        return it->second;
    }
};
