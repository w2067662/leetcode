//https://leetcode.com/problems/divide-array-into-equal-pairs/description/

class Solution {
public:
    bool divideArray(vector<int>& nums) {
        map<int, int> map;

        for(const auto num: nums)map[num]++; //store numbers appear times into map

        for(const auto it: map){
            if(it.second%2 != 0)return false;//IF  the appear times of current number is not 2 * N -> not able to form a equal pair
        }                                    //    -> FALSE

        return true;
    }
};
