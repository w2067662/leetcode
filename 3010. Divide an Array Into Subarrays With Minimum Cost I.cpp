//https://leetcode.com/problems/divide-an-array-into-subarrays-with-minimum-cost-i/description/

class Solution {
public:
    int minimumCost(vector<int>& nums) {
        sort(nums.begin()+1, nums.end());   // Sort the numbers from 1 to nums.end

        return nums[0] + nums[1] + nums[2]; // Get the min cost = nums[0] (fixed) + 
                                            //                    smallest from 1 to nums.end +
                                            //                    second smallest from 1 to nums.end
    }
};
