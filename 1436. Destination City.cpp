//https://leetcode.com/problems/destination-city/description/

class Solution {
public:
    string destCity(vector<vector<string>>& paths) {
        map<string, string> map;
        string destination = paths[0][0];   //set destination at first city

        for(int i=0;i<paths.size();i++){
            map[paths[i][0]]= paths[i][1];  //mapping city1 -> city2 from paths[i] = (city1, city2)
        }

        while(map.find(destination) != map.end()){
            destination = map[destination]; //goto next city until no next city
        }

        return destination;
    }
};
