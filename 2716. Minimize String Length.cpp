//https://leetcode.com/problems/minimize-string-length/description/

class Solution {
public:
    int minimizedStringLength(string s) {
        set<char> set;
        
        for(int i=0; i<s.length(); i++){
            set .insert(s[i]);
        }

        return set.size();
    }
};
