//https://leetcode.com/problems/find-the-winner-of-an-array-game/description/

class Solution {
public:
    //ex1:  arr = [3,2,1], k = 10
    //
    //    k >= arr.size   =>   10 >= 3
    //
    //    arr = [1,2,3] (sorted) -> answer = arr[2] = 3
    //

    //ex2:  arr = [2,1,3,5,4,6,7], k = 2
    //
    //    k <  arr.size   =>   2  <  7
    //
    //    arr = [2,1,3,5,4,6,7]
    //           ^ ^                  2 > 1 (2 wins) ; winCount = 1  -> push loser (1) to back
    //    arr = [2,1,3,5,4,6,7,1]
    //           ^   ^                2 < 3 (3 wins) ; winCount = 1  -> push loser (2) to back and set new winner (3) at front
    //    arr = [3,1,3,5,4,6,7,1,2]
    //           ^     ^              3 < 5 (5 wins) ; winCount = 1  -> push loser (3) to back and set new winner (5) at front
    //    arr = [5,1,3,5,4,6,7,1,2,3]
    //           ^       ^            5 > 4 (5 wins) ; winCount = 2 (= K) ->  answer = arr[0] = 5
    // 
    int getWinner(vector<int>& arr, int k) {
        if(k >= arr.size()){               //IF  K > arr's size
            sort(arr.begin(), arr.end());  //    -> sort the vector in ascending order
            return arr[arr.size()-1];      //    -> return the max number as answer
        }

        int winCount = 0, index = 1;

        while(winCount < k){               //While   winCount < K
            if(arr[0] > arr[index]){       //        IF   front element > current element
                winCount++;                //             -> winCount + 1
                arr.push_back(arr[index]); //             -> push current element (loser) to the end
            } else {                       //        IF   front element < current element
                winCount = 1;              //             -> winCount = 1
                arr[0] = arr[index];       //             -> set current element (winner) at front 
                arr.push_back(arr[0]);     //             -> push front element (loser) to the end
            }
            index++;                       //        compare next element
        }

        return arr[0];                     //return final winner
    }
};
