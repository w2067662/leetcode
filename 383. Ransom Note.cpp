//https://leetcode.com/problems/ransom-note/description/

class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        map<char, pair<int, int>>map;
        for(int i=0;i<ransomNote.length();i++){  //count for appear times of each char in strings ransomNote
            if(map.find(ransomNote[i])==map.end()) map.insert({ransomNote[i],{1, 0}} );
            else map[ransomNote[i]].first++;
        }
        for(int i=0;i<magazine.length();i++){  //count for appear times of each char in strings magazine
            if(map.find(magazine[i])==map.end()) map.insert({magazine[i],{0, 1}} );
            else map[magazine[i]].second++;
        }
        for(const auto& element: map){  //if the char in string ransomNote has more appear times than string magazine,r
            if(element.second.first > element.second.second) return false;  //then return false;
        }
        return true;    //else return true;
    }
};
