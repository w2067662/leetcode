//https://leetcode.com/problems/recover-binary-search-tree/description/

class Solution {
public:
    bool checkLeft(TreeNode* root, TreeNode* curr){  //check left subtree to find mistake
         if(!root) return false;

         if(root->val > curr->val){                  //IF   found node contain number that is larger than current node
             swap(root->val, curr->val);             //     -> swap it
             return true;
         }

         return checkLeft(root->left, curr) || checkLeft(root->right, curr);
    }

    bool checkRight(TreeNode* root, TreeNode* curr){ //check right subtree to find mistake
        if(!root) return false;

         if(root->val < curr->val){                  //IF   found node contain number that is smaller than current node
             swap(root->val, curr->val);             //     -> swap it
             return true;
         }

         return checkRight(root->left, curr) || checkRight(root->right, curr);
    }

    void recoverTree(TreeNode* root) {
        if(!root) return;

        if(checkLeft(root->left, root) || checkRight(root->right, root)) recoverTree(root); //IF  wrong node exist
                                                                                            //    -> check current node again
        recoverTree(root->left );  //traverse  left subtree
        recoverTree(root->right);  //traverse right subtree

        return;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
