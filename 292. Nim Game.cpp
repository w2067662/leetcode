//https://leetcode.com/problems/nim-game/description/

class Solution {
public:
    bool canWinNim(int n) {
        return n%(3+1) != 0;    //if n = (3+1)*N, it is not able to win
    }
};
