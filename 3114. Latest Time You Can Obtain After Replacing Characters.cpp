//https://leetcode.com/problems/latest-time-you-can-obtain-after-replacing-characters/description/

class Solution {
public:
    string format(int hour, int minute){ // Format (hour, minute) to "XX:XX" format
        string time = "";

        time += (hour >= 10) ? to_string(hour) : "0" + to_string(hour);
        time += ":";
        time += (minute>= 10) ? to_string(minute) : "0" + to_string(minute);

        return time;
    }

    string findLatestTime(string s) {
        int hour = 0, minute = 0;
                                                                                // HOUR:
        if(s[0] == '?' && s[1] == '?') hour = 11;                               // IF "??"  -> "11"
        else if (s[0] == '?') hour = (s[1]-'0' > 1) ? s[1]-'0' : 10+s[1]-'0';   // IF "?X"  -> "0X" OR "11" (X = '2'~'9')
        else if (s[1] == '?') hour = (s[0]-'0' > 0) ? 11 : 9;                   // IF "X?"  -> "11" OR "09"
        else hour = (s[0]-'0')*10 + (s[1]-'0');
                                                                                // MINUTE:
        if(s[3] == '?' && s[4] == '?') minute = 59;                             // IF "??"  -> "59"
        else if (s[3] == '?') minute = 50+s[4]-'0';                             // IF "?X"  -> "5X" (X = '0'~'9')
        else if (s[4] == '?') minute = (s[3]-'0')*10+9;                         // IF "X?"  -> "X9" (X = '0'~'5')
        else minute = (s[3]-'0')*10 + (s[4]-'0');

        return format(hour, minute);
    }
};