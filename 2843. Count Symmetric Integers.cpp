//https://leetcode.com/problems/count-symmetric-integers/description/

class Solution {
public:
    bool isSymmetric(string s){             //check number is symmetric
        if(s.size()%2 != 0) return false;   //is number string' size is not 2*N
        int firstSum = 0;
        int lastSum = 0;
        for(int i=0;i<s.length()/2;i++){    //sum up from left and right to middle
            firstSum += s[i]-'0';
            lastSum += s[s.length()-i-1]-'0';
        }

        return firstSum == lastSum;         //check firstSum quals to lastSum
    }

    int countSymmetricIntegers(int low, int high) {
        int ans=0;
        for(int i=low;i<=high;i++){
            if(isSymmetric(to_string(i))) ans++;
        }
        return ans;
    }
};
