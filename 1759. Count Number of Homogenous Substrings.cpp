//https://leetcode.com/problems/count-number-of-homogenous-substrings/description/

class Solution {
public:
    //ex:  s = "abbcccaa"
    //           ^         'a' != 'b' ;  prevCount =  1 ; currCount =  1 ; totalCount =  2
    //            ^        'b' == 'b' ;  prevCount =  1 ; currCount =  2 ; totalCount =  4
    //             ^       'b' != 'c' ;  prevCount =  2 ; currCount =  1 ; totalCount =  5
    //              ^      'c' == 'c' ;  prevCount =  1 ; currCount =  2 ; totalCount =  7
    //               ^     'c' == 'c' ;  prevCount =  2 ; currCount =  3 ; totalCount =  10
    //                ^    'c' != 'a' ;  prevCount =  1 ; currCount =  1 ; totalCount =  11
    //                 ^   'a' == 'a' ;  prevCount =  1 ; currCount =  2 ; totalCount =  13
    //
    int countHomogenous(string s) {
        int MOD = pow(10, 9) + 7;
        int totalCount = 1, prevCount = 1, currCount;

        for(int i=1; i<s.size(); i++){
            currCount = 1;                                       //set current count as 1
            if(s[i-1] == s[i]) currCount += prevCount;           //IF  current char is continuous char 
                                                                 //    -> add up previous count to current count

            totalCount = (totalCount % MOD) + (currCount % MOD); //sum up current count
            prevCount = currCount;                               //set current count as previous count
        }

        return totalCount % MOD;
    }
};
