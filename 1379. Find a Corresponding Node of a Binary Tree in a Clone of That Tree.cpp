//https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/description/

class Solution {
    TreeNode* ref;
public:
    void recurrence(TreeNode* original, TreeNode* cloned, TreeNode* target){    //find the reference of target in cloned
        if(!original)return;
        if(original == target){
            ref=cloned;
            return;
        }
        recurrence(original->left ,cloned->left ,target);
        recurrence(original->right,cloned->right,target);
        return;
    }
    TreeNode* getTargetCopy(TreeNode* original, TreeNode* cloned, TreeNode* target) {
        recurrence(original, cloned, target);
        return ref;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
