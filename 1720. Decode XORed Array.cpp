//https://leetcode.com/problems/decode-xored-array/description/

class Solution {
public:
    //ex: encoded = [6,2,7,3], first = 4
    //                                                                   answer = [4]
    //               ^       , last number in answer = 4 -> 6 ^ 4 = 2 -> answer = [4, 2]
    //                 ^     , last number in answer = 2 -> 2 ^ 2 = 0 -> answer = [4, 2, 0]
    //                   ^   , last number in answer = 0 -> 7 ^ 0 = 7 -> answer = [4, 2, 0, 7]
    //                     ^ , last number in answer = 7 -> 3 ^ 7 = 4 -> answer = [4, 2, 0, 7, 4]
    
    vector<int> decode(vector<int>& encoded, int first) {
        vector<int> ans;

        ans.push_back(first);                            //push first number into answer

        for(int i=0;i<encoded.size();i++){               //i start from 0 to encoded vector's end
            ans.push_back(encoded[i]^ans[ans.size()-1]); //push back currend number XOR last number in answer
        }

        return ans;
    }
};
