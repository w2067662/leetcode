//https://leetcode.com/problems/count-the-number-of-consistent-strings/description/

class Solution {
public:
    int countConsistentStrings(string allowed, vector<string>& words) {
        set<char> set;
        int ans = 0;
        bool consistent;

        for(int i=0;i<allowed.length();i++) set.insert(allowed[i]); //insert allowed letter into set

        for(int i=0; i<words.size(); i++){                          //traverse all the words in vector
            consistent = true;
            for(int j=0;j<words[i].length();j++){                   //traverse all the letters in word
                if(set.find(words[i][j]) == set.end()){             //if current letter is not allowed
                    consistent =false;                              //      this word is not consistent -> consistent = FALSE
                    break;
                }
            }
            if (consistent) ans++;                                  //if current word is consistent, answer+1
        }

        return ans;
    }
};
