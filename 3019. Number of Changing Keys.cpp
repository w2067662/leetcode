//https://leetcode.com/problems/number-of-changing-keys/description/

class Solution {
public:
    int countKeyChanges(string s) {
        int count = 0;

        for(int i=1; i<s.length(); i++){
            if(tolower(s[i-1]) != tolower(s[i])) count++; // IF change key (CAPs and SHIFT not counted)
                                                          //    -> count+1
        }

        return count;
    }
};
