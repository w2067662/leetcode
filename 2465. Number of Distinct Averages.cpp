//https://leetcode.com/problems/number-of-distinct-averages/description/

class Solution {
public:
    int distinctAverages(vector<int>& nums) {
        set<int> set;
        sort(nums.begin(), nums.end()); //sorting the nums list

        for(int i=0;i<nums.size()/2;i++){
            set.insert(nums[i]+nums[nums.size()-1-i]); //store sum = nums[i] + nums[n-i]
        }
        
        return set.size();  //return the amount of different sum in set
    }
};
