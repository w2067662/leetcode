//https://leetcode.com/problems/counting-bits/description/

class Solution {
public:
    vector<int> countBits(int n) {
        vector<int> vec;
        int count =0;
        for(int i=0, num;i<=n;i++){ //num for 0~n
            num=i;
            while(num>0){   //change to binary
                if(num&1)count++;   //count for 1's
                num/=2;
            }
            vec.push_back(count);
            count=0;    //reset count
        }
        return vec;
    }
};
