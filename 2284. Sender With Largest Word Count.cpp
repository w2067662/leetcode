//https://leetcode.com/problems/sender-with-largest-word-count/description/

class Solution {
public:
    pair<string, int> lexicographicallyLargest(vector<pair<string, int>> pairs){ // Get the lexicographically largest pair from pairs
        sort(pairs.begin(), pairs.end(), [&](pair<string, int>& a, pair<string, int>& b){ // Sort the pair by first string element lexicographically
            for(int i=0; i<min(a.first.length(), b.first.length()); i++){
                if(a.first[i] > b.first[i]) return true;
                else if(a.first[i] < b.first[i]) return false;
            }
            return a.first.length() > b.first.length();
        });

        return pairs[0];
    }

    int wordsCount(const string& s){ // Count for words
        int spaces = 0;

        for(const auto ch: s){
            if(ch == ' ') spaces++;
        }

        return spaces+1;
    }

    string largestWordCount(vector<string>& messages, vector<string>& senders) {
        map<string, int> map;
        pair<string, int> ans;

        for(int i=0; i<messages.size(); i++){
            map[senders[i]] += wordsCount(messages[i]);                        // Insert {[Sender]: [Words count]} into map
        }

        ans = {map.begin()->first, map.begin()->second};                       // Set the map's begin as answer

        for(const auto it: map){                                                                            // For all the elements in maps
            if(it.second > ans.second) ans = {it.first, it.second};                                         //      IF current element's words count > answer's words count
                                                                                                            //         -> set current element as answer 
            else if (it.second == ans.second) ans = lexicographicallyLargest({ans, {it.first, it.second}}); //      IF current element's words count = answer's words count
                                                                                                            //         -> set the lexicographically largest sender as answer
        }

        return ans.first;
    }
};

