//https://leetcode.com/problems/palindromic-substrings/description/

class Solution {
public:
    //ex:  s = "abccbabc"
    //          ^             odd = ["a"]                ;   even = ["ab"]                  ; count =  1
    //           ^            odd = ["b","abc"]          ;   even = ["bc","abbc"]           ; count =  2(+1)
    //            ^           odd = ["c","bcc","abccb"]  ;   even = ["cc","bccb","abccba"]  ; count =  6(+4)
    //             ^          odd = ["c","ccb","bccba"]  ;   even = ["cb","ccba","bccbab"]  ; count =  7(+1)
    //              ^         odd = ["b","cba","ccbab"]  ;   even = ["ba","cbab","ccbabc"]  ; count =  8(+1)
    //               ^        odd = ["a","bab","cbabc"]  ;   even = ["ab","babc"]           ; count = 11(+3)
    //                ^       odd = ["b","abc"]          ;   even = ["bc"]                  ; count = 12(+1)
    //                 ^      odd = ["c"]                ;   even = []                      ; count = 13(+1)
    //
    int palindromes(const string &s, int l, int r){
        int count = 0;

        while(0 <= l && r < s.length() && s[l] == s[r]){   //WHILE  left and right is in the range
            count++, l--, r++;                             //       -> count s[left] = s[right]
        }

        return count;
    }
    
    int countSubstrings(string s) {
        int count = 0;

        for(int i=0; i<s.length(); i++){
            count += palindromes(s, i, i) + palindromes(s, i, i+1); //count = count + odd length palindromes + even length palindromes
        }

        return count;
    }
};
