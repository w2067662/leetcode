//https://leetcode.com/problems/minimum-right-shifts-to-sort-the-array/description/

class Solution {
public:
    int minimumRightShifts(vector<int>& nums) {
        int ans = -1;
        int tag = 0;

        for(int i=1; i<nums.size();i++){
            if(nums[i-1]>nums[i]){
                if(nums[nums.size()-1]<nums[0]) ans = nums.size()-i; //IF   number array can be sorted by right shifting -> calculate the minimum right shift times to sort the array
                tag++;                  //IF   last number and current number is descending, tag++
            }
        }

        if(tag == 0)return 0;           //already sorted
        else if (tag == 1) return ans;  //can be sorted by right shifting
        return -1;                      //can not be sorted by right shifting
    }
};
