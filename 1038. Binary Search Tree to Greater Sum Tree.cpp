//https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, int &curr){ // Traverse the tree inorder
        if(!root) return;

        traverse(root->right, curr); // Traverse right subtree

        curr += root->val;           // Add up node value to current
        root->val = curr;            // Replace node value with current

        traverse(root->left , curr); // Traverse left  subtree

        return;
    }

    TreeNode* bstToGst(TreeNode* root) {
        int curr = 0;

        traverse(root, curr); // Traverse the tree

        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
