//https://leetcode.com/problems/reformat-phone-number/description/

class Solution {
public:
    string reformatNumber(string number) {
        vector<string> vec;
        string nums = "";
        string ans = "";

        for(int i=0; i<number.length();i++){
            if(isdigit(number[i]))nums += number[i]; //get the digits from the number string
        }

        while(nums.length()>0){                      //split the digits into groups and store into vector
            if(nums.length()>4){
                vec.push_back(nums.substr(0,3));
                nums.erase(0,3);
            } else if (nums.length()<4){
                vec.push_back(nums.substr(0,3));
                nums = "";
            } else {
                vec.push_back(nums.substr(0,2));
                vec.push_back(nums.substr(2,2));
                nums = "";
            }
        }

        for(int i=0; i<vec.size(); i++){             //reform the phone number with '-'
            ans += vec[i];
            if(i != vec.size()-1) ans += "-";
        }

        return ans;
    }
};
