//https://leetcode.com/problems/sum-of-subarray-minimums/description/

class Solution {
private:
    static const int MOD = 1e9 + 7;

public:
    //Explain:    6,1,2,7,8,4,5,3,9
    //                      ^
    //           6,1,2|7,8,(4),5|3,9         find the nearest smaller number of left and right
    //                  
    //           7,8,(4)   7,8,(4),5         get the possible subarrays contain (4)
    //             8,(4)   8,(4),5
    //               (4)   (4),5
    //
    //     left = [7,8,4] ; right = [4,5]    min sum of [7,8,4,5] = left.len x (4) x right.len = 3 x 4 x 2 = 24
    //
    //
    //ex: arr = [3,1,2,4]
    //           ^           1 x 3 x 1 = 3   ; min sum = 3
    //             ^         2 x 1 x 3 = 6   ; min sum = 9  (+6)
    //               ^       1 x 2 x 2 = 4   ; min sum = 13 (+4)
    //                 ^     1 x 4 x 1 = 4   ; min sum = 17 (+4)
    //
    //    answer = 17
    //
    int sumSubarrayMins(std::vector<int>& arr) {
        vector<long long> left(arr.size()), right(arr.size());
        stack<long long> s1, s2;                                // For storing the number's index that is smaller than current number
        long long result = 0;

                                                                   // Calculate left array
        for (int i=0; i<arr.size(); i++) {                         // For all the numbers in array (traverse from left to right)
            while (!s1.empty() && arr[s1.top()] >= arr[i]) {       //      While number of stack1 top >= current number
                s1.pop();                                          //            -> pop until find the nearest smaller number
            }

            left[i] = s1.empty() ? i + 1 : i - s1.top();           //      IF   no number in stack (no nearest smaller number)
                                                                   //           -> left[i] = i+1 (len from 0 to i)
                                                                   //      ELSE (exist nearest smaller number)
                                                                   //           -> left[i] = i-stack1.top (len from stack1.top to i)

            s1.push(i);                                            //      Push current number into stack
        }

                                                                   // Calculate right array
        for (int i = arr.size() - 1; i >= 0; i--) {                // For all the numbers in array (traverse from right to left)
            while (!s2.empty() && arr[s2.top()] > arr[i]) {        //      While number of stack2 top > current number
                s2.pop();                                          //            -> pop until find the nearest smaller number
            }

            right[i] = s2.empty() ? arr.size() - i : s2.top() - i; //      IF   no number in stack (no nearest smaller number)
                                                                   //           -> right[i] = arr.size-i (len from i to arr.size-1)
                                                                   //      ELSE (exist nearest smaller number)
                                                                   //           -> right[i] = stack2.top-i (len from i to stack2.top)

            s2.push(i);                                            //      Push current number into stack
        }

        for (int i = 0; i < arr.size(); i++) {                     // Traverse the array
            result = (result + arr[i] * left[i] * right[i]) % MOD; //      Calculate the total minSum of each element in array
        }

        return (int)result;
    }
};
