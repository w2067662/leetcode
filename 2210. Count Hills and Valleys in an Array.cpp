//https://leetcode.com/problems/count-hills-and-valleys-in-an-array/description/

class Solution {
public:
    int countHillValley(vector<int>& nums) {
        int count = 0;
        vector<int> vec;

        for(int i=1;i<nums.size();i++){
            if(nums[i] == nums[i-1]) vec.push_back(i); //IF  (i-1, i) has the same value, store the second number's index from this consecutive number pair
        }

        for(int i=vec.size()-1; i>=0;i--){
            nums.erase(nums.begin()+vec[i]-1);         //erase all the consecutive numbers by its index in vector
        }

        for(int i=1;i<nums.size()-1;i++){
            if((nums[i] > nums[i-1] && nums[i] > nums[i+1]) ||        //check current number is hill or valley
               (nums[i] < nums[i-1] && nums[i] < nums[i+1])) count++; //IF  is hill or valley -> count+1
        }

        return count;
    }
};
