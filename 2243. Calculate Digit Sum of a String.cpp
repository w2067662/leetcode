//https://leetcode.com/problems/calculate-digit-sum-of-a-string/description/

class Solution {
public:
    string digitSum(string s, int k) {
        string temp;
        int sum;

        while(s.length() > k){                            //IF   current number string's length is smaller than or equal to K -> end the loop
            temp = "";
            for(int i=0;i<s.length();i+=k){               //split number string into parts
                sum = 0;
                for(int j=i; j<s.length() && j<i+k; j++){ //sum up digit in each part
                    sum += s[j]-'0';
                }
                temp += to_string(sum);                   //appead current digit sum into temp
            }

            s = temp;                                     //replace string s with temp
        }

        return s;
    }
};
