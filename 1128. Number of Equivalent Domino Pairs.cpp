//https://leetcode.com/problems/number-of-equivalent-domino-pairs/description/

class Solution {
public:
    int numEquivDominoPairs(vector<vector<int>>& dominoes) {
        map<pair<int, int>, vector<int>>map; //map = { pair{[num1], [num2]} : vector{[index of current pair]} }
        int ans =0;

        for(int i=0;i<dominoes.size();i++){
            if(dominoes[i][0]>dominoes[i][1]){          //if num1 > num2
                swap(dominoes[i][0], dominoes[i][1]);   //  SWAP num1, num2
            }
            map[{dominoes[i][0], dominoes[i][1]}].push_back(i); //insert by pair = {num1, num2} as key and push current index into vector
        }

        for(const auto it: map){
            ans += it.second.size() * (it.second.size()-1) / 2; //add up possible answer for each key pair, possible answer = Nx(N-1)/2
        }

        return ans;
    }
};
