//https://leetcode.com/problems/find-in-mountain-array/description/

class Solution {
public:
    // Binary search for finding peak in mountain array
    int findPeak(MountainArray &mountainArr){
        int left = 0, right = mountainArr.length()-1, mid, peak;
        int leftValue, rightValue, midValue;

        while(left < right){
            mid = left + (right - left)/2;

            leftValue  = mountainArr.get(mid - 1);
            midValue   = mountainArr.get(mid);
            rightValue = mountainArr.get(mid + 1);

            if (leftValue  < midValue && midValue > rightValue ) {
                peak = mid;
                break;
            } 
            else if (midValue < rightValue) left = mid + 1;
            else right = mid;
        }
        return peak;
    }
    // Binary search for ascending array
    int ascendBinarySearch(MountainArray &mountainArr, int left, int right, int target){
        int mid, midValue;

        while(left < right){
            mid = left + (right - left)/2;

            midValue = mountainArr.get(mid);

            if (midValue == target) return mid;
            else if (midValue < target) left = mid + 1;
            else right = mid;
        }
        return -1;
    }
    // Binary search for descending array
    int descendBinarySearch(MountainArray &mountainArr, int left, int right, int target){
        int mid, midValue;

        while(left < right){
            mid = left + (right - left)/2;

            midValue = mountainArr.get(mid);

            if (midValue == target) return mid;
            else if (midValue > target) left = mid + 1;
            else right = mid;
        }
        return -1;
    }

    int findInMountainArray(int target, MountainArray &mountainArr) {
        int peak = findPeak(mountainArr);                                           //find the peak
        int ans = -1;

        ans = ascendBinarySearch(mountainArr,     0,                 peak, target); //find the target in  ascending subarray
        if(ans != -1) return ans;
        ans = descendBinarySearch(mountainArr, peak, mountainArr.length(), target); //find the target in descending subarray
        return ans;
    }
};

/**
 * // This is the MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * class MountainArray {
 *   public:
 *     int get(int index);
 *     int length();
 * };
 */
