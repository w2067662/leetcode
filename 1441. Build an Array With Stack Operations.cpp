//https://leetcode.com/problems/build-an-array-with-stack-operations/description/

class Solution {
public:
    vector<string> buildArray(vector<int>& target, int n) {
        vector<string> operation;
        int curr = 1;

        for(const auto num: target){
            while(curr < num){               //IF   current number < current target
                operation.push_back("Push"); //     -> push
                operation.push_back("Pop");  //     -> pop
                curr++;                      //     -> current + 1
            } 
            if(curr > n) break;              //IF   current number is over stream size -> end the loop

            operation.push_back("Push");     //IF   current number = current target -> push
            curr++;                          //     -> current + 1
        }

        return operation;
    }
};
