//https://leetcode.com/problems/minimum-subsequence-in-non-increasing-order/description/

class Solution {
public:
    vector<int> minSubsequence(vector<int>& nums) {
        if(nums.size()==1)return nums;  //if vector only contain 1 number, return vector as answer

        sort(nums.begin(), nums.end()); //sort the vector in ascending order

        vector<int> ans;
        int leftSum=0;
        int rightSum=0;
        for(int i=0;i<nums.size();i++){
            leftSum += nums[i];         //sum up from left to right
        }

        for(int i=nums.size()-1;i>=0;i--){ //i start from last one to 0
            ans.push_back(nums[i]);        //push current number into vector
            rightSum += nums[i];           //sum up from right
            leftSum -= nums[i];            //deduct current number
            if(rightSum > leftSum) return ans; //if sum of right subsequence is strictly bigger than right subsequence,
                                               //return the answer
        }
        return {};
    }
};
