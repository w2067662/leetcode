//https://leetcode.com/problems/restore-the-array-from-adjacent-pairs/description/

class Solution {
public:
    //ex:  adjacentPairs = [[4,-2],[1,4],[-3,1]]
    //     adjacentList  = [-2:{4},
    //                      -3:{1},
    //                       1:{-3,4},
    //                       4:{-2,1}]  -> end point can be -2 OR -3
    //
    // start from -2       visited = [-2]        ; list = [-2]
    // -2 ->  4            visited = [-2,4]      ; list = [-2,4]
    //  4 -> -2 (visited)  visited = [-2,4]      ; list = [-2,4]
    //  4 ->  1 (visited)  visited = [-2,4,1]    ; list = [-2,4,1]
    //  1 -> -3            visited = [-2,4,1,-3] ; list = [-2,4,1,-3]
    // -3 ->  1 (visited)  visited = [-2,4,1,-3] ; list = [-2,4,1,-3]
    // end at -3
    //
    //  list = [-2,4,1,-3]
    //
    vector<int> restoreArray(vector<vector<int>>& adjacentPairs) {
        map<int, vector<int>> adjacentList;
        vector<int> list;
        set<int> visited;
        bool notEnd = true;
        int curr, next;

        for(const auto pair: adjacentPairs){           //FOR  all the adjacent pairs
            adjacentList[pair[0]].push_back(pair[1]);  //     -> pair[0] points to pair[1]
            adjacentList[pair[1]].push_back(pair[0]);  //     -> pair[1] points to pair[0]
        }

        for(const auto node: adjacentList){
            if(node.second.size() == 1){               //IF   current node only points to 1 node
                curr = node.first;                     //     -> current node is the endpoint -> set as current
                break;                                 
            }
        }

        visited.insert(curr);                          //start from current node to traverse the list
        list.push_back(curr);                          //push current node into answer list

        while(notEnd){                                      //While   not the end of the list

            notEnd = false; 
            for(int i=0; i<adjacentList[curr].size(); i++){ 
                next = adjacentList[curr][i];
                if(visited.find(next) == visited.end()){    //IF    next node is not visited
                    visited.insert(next);                   //      -> mark as visited
                    list.push_back(next);                   //      -> push into answer list
                    curr = next;                            //      -> set next node as current node
                    notEnd = true;                          //      -> not the end point -> set notEnd as TRUE
                    break;
                }
            }

            if(!notEnd) break;                              //IF    is the end of the list -> end the while loop
        }
        
        return list;
    }
};
