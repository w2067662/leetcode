//https://leetcode.com/problems/find-greatest-common-divisor-of-array/description/

class Solution {
public:
    int GCD(int a, int b){ //count for greatest common divisor
        int res;
        while( b!=0 ){
            res = b;
            b = a%b;
            a = res;
        }
        return res;
    }

    int findGCD(vector<int>& nums) {
        sort(nums.begin(), nums.end());            //sort the vector in ascending order
        return GCD(nums[0], nums[nums.size()-1]);  //count the GCD of max number and min number in the vector
    }
};
