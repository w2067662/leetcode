//https://leetcode.com/problems/words-within-two-edits-of-dictionary/description/

class Solution {
public:
    bool canBeEdit(string &dic, string &word){  //check word can be edit or not
        int count = 0;
        for(int i=0; i<dic.size(); i++){
            if(dic[i] != word[i]) count++;
            if(count > 2) return false;
        }
        return true;
    }

    vector<string> twoEditWords(vector<string>& queries, vector<string>& dictionary) {
        vector<string> words;

        for(auto word: queries){
            for(auto dic: dictionary){
                if(canBeEdit(dic, word)){   //IF word can be edit 
                    words.push_back(word);  //   -> push into answer
                    break;
                } 
            }
        }

        return words;
    }
};
