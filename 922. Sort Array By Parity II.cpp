//https://leetcode.com/problems/sort-array-by-parity-ii/description/

class Solution {
public:
    //ex: nums = [4,2,5,7] -> time complexity = O(n)
    //            ^          even  ->  evenIndex = 0 ; oddIndex = 1 ->  ans = [4, -, -, -]  ->  evenIndex = 0(+2) = 2
    //              ^        even  ->  evenIndex = 2 ; oddIndex = 1 ->  ans = [4, -, 2, -]  ->  evenIndex = 2(+2) = 4
    //                ^      odd   ->  evenIndex = 2 ; oddIndex = 1 ->  ans = [4, 5, 2, -]  ->  oddIndex  = 1(+2) = 3
    //                  ^    odd   ->  evenIndex = 2 ; oddIndex = 3 ->  ans = [4, 5, 2, 7]  ->  oddIndex  = 3(+2) = 5
    //ans = [4, 5, 2, 7]
    //
    vector<int> sortArrayByParityII(vector<int>& nums) {
        vector<int> ans (nums.size());
        int evenIndex = 0;
        int oddIndex  = 1;
        
        for(int i=0;i<nums.size();i++){
            if(nums[i]%2==0){
                ans[evenIndex]=nums[i];
                evenIndex +=2;
            }else{
                ans[oddIndex]=nums[i];
                oddIndex  +=2;
            }
        }
        return ans;
    }
};
