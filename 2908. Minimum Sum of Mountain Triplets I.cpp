//https://leetcode.com/problems/minimum-sum-of-mountain-triplets-i/description/

class Solution {
public:
    int minimumSum(vector<int>& nums) {
        int minSum = INT_MAX;

        for(int i=0; i<nums.size()-2; i++){
            for(int j=i+1; j<nums.size()-1; j++){
                for(int k=j+1; k<nums.size(); k++){
                    if(nums[i] < nums[j] && nums[k] < nums[j]) minSum = min(minSum, nums[i]+nums[j]+nums[k]); //find possible mountain triplets
                }                                                                                             //store the min sum
            }
        }

        return minSum == INT_MAX ? -1 : minSum; //if no possible answer -> return -1
    }
};
