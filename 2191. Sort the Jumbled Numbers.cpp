//https://leetcode.com/problems/sort-the-jumbled-numbers/description/

class Solution {
public:
    int transform(const vector<int>& mapping, const int num){   // Transform the number by mappings
        string s = to_string(num);
        int res = 0;

        for(const auto ch: s){
            res = res*10 + mapping[ch-'0'];
        }

        return res;
    }

    vector<int> sortJumbled(vector<int>& mapping, vector<int>& nums) {
        vector<pair<int, int>> pairs;
        vector<int> ans;

        for(const auto num: nums){
            pairs.push_back({transform(mapping, num), num});                         // Push {[Transformed number]: [Number]} into pairs
        }

        sort(pairs.begin(), pairs.end(), [&](pair<int, int>& a, pair<int, int>& b){  // Sort the pairs by transformed number
            return a.first < b.first;
        });

        for(const auto pair: pairs){
            ans.push_back(pair.second);                                              // Push the number from sorted pairs into answer
        }

        return ans;
    }
};
