//https://leetcode.com/problems/pass-the-pillow/description/

class Solution {
public:
    int passThePillow(int n, int time) {
        bool goRight = true;
        int current = 1;

        for(int i=1; i<=time; i++){
            current += goRight? 1 : -1;
            if(current == n) goRight = false;   // IF  pass to the last  person -> start passing left 
            if(current == 1) goRight = true;    // IF  pass to the first person -> start passing right
        }

        return current;
    }
};
