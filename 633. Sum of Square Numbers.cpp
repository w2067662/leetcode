//https://leetcode.com/problems/sum-of-square-numbers/description/

class Solution {
public:
    bool judgeSquareSum(int c) {
        for(long a=0; pow(a, 2)<=c; a++){    // FOR  A from 0 to ⌊sqrt(C)⌋ (floor of square root of C)
            double b = sqrt(c - pow(a, 2));  //     Calculate B  (square root of (C - A^2))

            if(b == (int)b){                 //     IF  B is a integar
                return true;                 //         -> TRUE (is square sum)
            }
        }

        return false;                        //         -> FALSE (is not square sum)
    }
};