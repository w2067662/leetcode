//https://leetcode.com/problems/maximum-area-of-longest-diagonal-rectangle/description/

class Solution {
public:
    double diagonalLen(int w, int l){   //calculate diagonal length
        return sqrt(pow(w, 2) + pow(l, 2));
    }

    int areaOfMaxDiagonal(vector<vector<int>>& dimensions) {
        map<double, vector<int>> map;

        for(const auto dimension: dimensions){
            map[diagonalLen(dimension[0], dimension[1])].push_back(dimension[0] * dimension[1]); //store {diagonal length: area} into map
        }

        auto it = map.rbegin();                        //get the vector of max diagonal length
        sort(it->second.rbegin(), it->second.rend());  //sort the area in ascending order

        return it->second[0];                          //return the max area of max diagonal length
    }
};
