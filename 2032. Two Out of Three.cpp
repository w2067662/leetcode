//https://leetcode.com/problems/two-out-of-three/description/

class Solution {
public:
    vector<int> twoOutOfThree(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3) {
        vector<int> ans;
        set<int> all;
        set<int> one;
        set<int> two;
        set<int> three;
        int count = 0;

        for(int i=0;i<nums1.size();i++){    //insert numbers from nums1 into set one
            one.insert(nums1[i]);
            all.insert(nums1[i]);
        }

        for(int i=0;i<nums2.size();i++){    //insert numbers from nums2 into set two
            two.insert(nums2[i]);
            all.insert(nums2[i]);
        } 

        for(int i=0;i<nums3.size();i++){    //insert numbers from nums3 into set three
            three.insert(nums3[i]);
            all.insert(nums3[i]);
        } 

        for(const auto it:all){
            count = 0;
            if(one.find(it)!=one.end()) count++;
            if(two.find(it)!=two.end()) count++;
            if(three.find(it)!=three.end()) count++;
            if(count >= 2) ans.push_back(it);   //if current number appear in at least two arrays, push it into answer
        }

        return ans;
    }
};
