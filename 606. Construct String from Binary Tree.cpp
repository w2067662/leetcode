//https://leetcode.com/problems/construct-string-from-binary-tree/description/

class Solution {
public:
    string tree2str(TreeNode* root) {
        if(!root) return "";
                                                                                        //ex: tree = [0,null, 1]
        string res = to_string(root->val);                                              //str = "0"
        if(!(!root->left && !root->right)) res += "(" + tree2str(root->left ) + ")";    //str = "0()"
        if(root->right) res += "(" + tree2str(root->right) + ")";                       //str = "0()(1)"

        return res;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
