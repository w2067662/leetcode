//https://leetcode.com/problems/construct-smallest-number-from-di-string/description/

class Solution {
public:
    //ex1: pattern = "IIIDIDDD"
    //                ^            string = "12"
    //                 ^           string = "123"
    //                  ^          string = "1234"
    //                   ^         string = "12354"
    //                    ^        string = "123546"
    //                     ^       string = "1235476"
    //                      ^      string = "12354876"
    //                       ^     string = "123549876"
    //
    //ex2: pattern = "DID"
    //                ^            string = "21"
    //                 ^           string = "213"
    //                  ^          string = "2143"
    //
    string smallestNumber(string pattern) {
        string ans = "";
        int maxNum = 0;

        for(int i=0; i<pattern.length(); i++){  // Traverse the pattern string
            if(i == 0){                         // IF current char is first char
                switch(pattern[i]){
                    case 'I':                   //      IF char is 'I' (increasing)
                        ans += "12";            //         -> append "12" to answer
                        break;
                    case 'D':                   //      IF char is 'D' (decreasing)
                        ans += "21";            //         -> append "21" to answer
                        break;
                }
                maxNum = 3;                     //      Set the max number as 3 (for next appending)

                continue;
            }

            switch(pattern[i]){
                case 'I':                                           // IF current char is 'I' (increasing)
                    ans += to_string(maxNum);                       //    -> append the maxNum into answer
                    break;
                case 'D':                                           // IF current char is 'D' (decreasing)
                    ans[ans.size()-1]++;                            //    -> [last char]+1
            	    ans += ans[ans.size()-1]-1;                     //    -> append the [last char]-1 into answer

                    for(int j=0; j<ans.size()-2; j++){              // For chars infront second last char
                        if(ans[j] >= ans[ans.size()-2]) ans[j]++;   //    IF char >= second last char -> [char]+1
                    }

                    break;
            }
            maxNum++;                                               // maxNum+1
        }

        return ans;
    }
};

