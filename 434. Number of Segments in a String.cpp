//https://leetcode.com/problems/number-of-segments-in-a-string/description/

class Solution {
public:
    int countSegments(string s) {
        s+=" ";
        int count =0;
        bool wordFound = false;
        for(int i=0;i<s.length();i++){
            if(s[i]==' '){
                if(wordFound){      //if word is found, count as 1 segment
                    count++;
                    wordFound = false;
                }
            }
            else{
                wordFound = true;   //if it is not ' ', then a letter in a word is found
            }
        }
        return count;
    }
};
