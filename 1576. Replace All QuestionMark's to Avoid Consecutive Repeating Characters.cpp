//https://leetcode.com/problems/replace-all-s-to-avoid-consecutive-repeating-characters/description/

class Solution {
public:
    void applyLetterToQuestionMark(string& s, int i){
        int left  = i-1;
        int right = i+1;
        char leftChar, rightChar;

        if(left>=0  && left<s.length())leftChar = s[left];   //if index of left is valid, get left char
        else leftChar = '?';                                 //else set left char as '?'

        if(right>=0 && right<s.length())rightChar = s[right];//if index of right is valid, get right char
        else rightChar = '?';                                //else set right char as '?'

        for(char ch='a'; ch <='z'; ch++){                    //start from 'a' to 'z'
            if(ch != leftChar && ch != rightChar){           //if current char won't make letters consecutive
                s[i]=ch;                                     //then replace Question Mark to current char
                break;
            } 
        }
        return;
    }

    string modifyString(string s) {
        for(int i=0;i<s.length();i++){
            if(s[i] == '?') applyLetterToQuestionMark(s, i); //replace Question Mark to a valid letter 
        }
        return s;
    }
};
