//https://leetcode.com/problems/single-number-iii/description/

class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        vector<int> singles;

        sort(nums.begin(), nums.end());             // Sort the vector in ascending order

        for(int i=0; i+1<nums.size(); i+=2){        // Traverse the vector with 2 numbers for each time
            if(nums[i] != nums[i+1]){               //      IF current number != current + 1 number
                singles.push_back(nums[i]);         //         -> current number must be single number -> push into answer
                i--;                                //         -> shift left by 1
            }
        }

        if(nums[nums.size()-1] != nums[nums.size()-2]){     // IF  last number in the vector is single
            singles.push_back(nums[nums.size()-1]);         //     -> push into vector
        }

        return singles;
    }
};
