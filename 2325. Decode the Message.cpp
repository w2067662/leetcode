//https://leetcode.com/problems/decode-the-message/description/

class Solution {
public:
    string decodeMessage(string key, string message) {
        map<char, char> map;

        char ch='a';
        for(int i=0; i<key.length(); i++){
            if(islower(key[i]) && map.find(key[i]) == map.end()){   //store key into map
                map[key[i]] = ch++;
            }
        }

        for(int i=0; i<message.length(); i++){
            if(islower(message[i])){
                message[i] = map[message[i]];   //change char by key from the map
            }
        }

        return message;
    }
};
