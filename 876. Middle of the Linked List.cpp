//https://leetcode.com/problems/middle-of-the-linked-list/description/

class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        if(!head) return NULL;

        ListNode *fast = head, *slow = head;

        while(fast->next){                   // While fast has next
            fast = fast->next;               //     fast goto next

            if(fast->next){                  //     IF    fast has next
                fast = fast->next;           //           -> fast goto next
                slow = slow->next;           //           -> slow goto next
            }
            else return slow->next;          //     ELSE  (end of list)
                                             //           -> return slow's next as answer
        }

        return slow;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
