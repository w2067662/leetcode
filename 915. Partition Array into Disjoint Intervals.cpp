//https://leetcode.com/problems/partition-array-into-disjoint-intervals/description/

class Solution {
public:
    bool isValid(map<int, int>& left, map<int, int>& right){ // Check all numbers in left is less than or equal to all numbers in right
        return left.rbegin()->first <= right.begin()->first;
    }

    int partitionDisjoint(vector<int>& nums) {
        map<int, int> left, right;

        for(const auto num: nums){
            right[num]++;                                   // Store the all the numbers' frequency into right map
        }

        for(int i=0; i<nums.size()-1; i++){                 // For i from 0 to N-1
            right[nums[i]]--;                               //     -> frequency of current number in right -1
            left [nums[i]]++;                               //     -> frequency of current number in left  +1

            if(right[nums[i]] == 0) right.erase(nums[i]);   //     IF frequency of current number in right = 0
                                                            //        -> erase from right map

            if(isValid(left, right)) return i+1;            //     IF valid to partition disjoint left and right
                                                            //        -> return current index +1 as answer
        }

        return -1;                                          // No valid partition
    }
};
