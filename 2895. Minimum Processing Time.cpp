//https://leetcode.com/problems/minimum-processing-time/description/

class Solution {
public:
    int minProcessingTime(vector<int>& processorTime, vector<int>& tasks) {
        int maxPT = 0;

        sort(processorTime.begin(), processorTime.end());       //sort the processorTime in ascending order

        sort(tasks.begin(), tasks.end(), [&](int &a, int &b){   //sort the tasks in descending order
            return a > b;
        });

        for(int i=0; i<processorTime.size(); i++){
            maxPT = max(maxPT, processorTime[i] + tasks[i*4]);  //calculate and store the max processorTime
        }

        return maxPT;
    }
};
