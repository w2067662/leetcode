//https://leetcode.com/problems/check-if-one-string-swap-can-make-strings-equal/description/

class Solution {
public:
    bool areAlmostEqual(string s1, string s2) {
        int count =0;
        map<char, int>map;

        for(int i=0;i<s1.length();i++){
            if(s1[i] != s2[i])count++;      //count for different char
            map[s1[i]]++;
            map[s2[i]]--;
        }

        for(const auto it:map){             //check if letters are with same amount
            if(it.second != 0)return false;
        }

        return count == 0 || count == 2;    //at most 1 swap to make 2 strings equal
    }
};
