//https://leetcode.com/problems/mean-of-array-after-removing-some-elements/description/

class Solution {
public:
    double trimMean(vector<int>& arr) {
        double removeAmount = arr.size()/20;    //count for remove amount
        double sum = 0;

        sort(arr.begin(), arr.end());           //sort the vector in ascending order

        for(int i=removeAmount; i<arr.size()-removeAmount; i++){
            sum += arr[i];                      //sum up unremoved elements
        }

        return sum/(arr.size()-removeAmount*2); //count for mean
    }
};
