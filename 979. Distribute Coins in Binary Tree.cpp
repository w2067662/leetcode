//https://leetcode.com/problems/distribute-coins-in-binary-tree/description/

// ex: root = [3,0,0]

//    Tree          Excess
//      3             0       3 + (-1) + (-1) - 1 = 0 (root's excess should be 0) ; moves = 2 (|-1|+|-1|=2) 
//     / \           /  \ 
//    0   0        -1   -1    0 + 0 + 0 - 1 = -1                                  ; moves = 0
//                 / \  / \ 
//               (0)(0)(0)(0) (NULL)                                              ; moves = 0

class Solution {
private:
    int DFS(TreeNode* root, int& moves){
        if(!root) return 0;

        int leftExcess  = DFS(root->left , moves);        // Calculate left  subtree's excess coins
        int rightExcess = DFS(root->right, moves);        // Calculate right subtree's excess coins
        moves += abs(leftExcess) + abs(rightExcess);      // Calculate moves

        return root->val + leftExcess + rightExcess - 1;  // Return current node's excess to its parent
    }

public:
    int distributeCoins(TreeNode* root) {
        int moves = 0;
        DFS(root, moves);   // Use DFS to distribute coins
        return moves;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */