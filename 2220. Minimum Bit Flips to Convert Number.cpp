//https://leetcode.com/problems/minimum-bit-flips-to-convert-number/description/

class Solution {
public:
    string toBinary(int n){ //convert to binary
        string res = "";
        while(n>0){
            res += n%2 +'0';
            n/=2;
        }
        return res;
    }

    int minBitFlips(int start, int goal) {
        int count = 0;

        string startBinary = toBinary(start);
        string goalBinary  = toBinary(goal);

        int maxLen = max(startBinary.length(), goalBinary.length());

        for(int i=startBinary.length(); i<= maxLen; i++) startBinary += "0"; //add prefix 0 to binary string
        for(int i=goalBinary.length() ; i<= maxLen; i++) goalBinary  += "0"; //add prefix 0 to binary string

        for(int i=0;i<startBinary.length();i++){
            if(startBinary[i] != goalBinary[i])count++; //count for different bits
        }

        return count;
    }
};
