//https://leetcode.com/problems/maximum-number-of-fish-in-a-grid/description/

class Solution {
private:
    vector<int> steps = {1,0,-1,0,1}; // Define steps

public:
    bool isValid(vector<vector<int>>& grid, const pair<int, int>& pos){   // Check position is within the range of grid or not
        return 0 <= pos.first && pos.first < grid.size() && 0 <= pos.second && pos.second < grid[pos.first].size();
    }

    void collectFish(vector<vector<int>>& grid, pair<int, int> curr, set<pair<int, int>>& visited, int &fish){ // Catch the fish by DFS
        if(grid[curr.first][curr.second] == 0) return;                              // IF current value is 0 -> return (no fish in land cell)

        visited.insert(curr);                                                       // Mark current position as visited

        fish += grid[curr.first][curr.second];                                      // Add up the fishes of current water cell

        for(int s=0; s<steps.size()-1; s++){                                        // For all the next steps
            pair<int, int> next = {curr.first+steps[s], curr.second+steps[s+1]};

            if(isValid(grid, next) && visited.find(next) == visited.end()){         //      IF next step is valid AND not visited
                collectFish(grid, next, visited, fish);                             //         -> DFS to collect the fish
            }
        }

        return;
    }

    int findMaxFish(vector<vector<int>>& grid) {
        set<pair<int, int>> visited;
        int maxFish = 0;

        for(int i=0; i<grid.size(); i++){                                        // Traverse the grid
            for(int j=0; j<grid[i].size(); j++){
                if(grid[i][j] != 0 && visited.find({i, j}) == visited.end()){    //     IF current cell is not 0 AND not visited
                    int fish = 0;
                    collectFish(grid, {i, j}, visited, fish);                    //        -> start collecting the fish from current cell
                    maxFish = max(maxFish, fish);                                //        -> store the largest fishes in continuous water cells
                }
            }
        }

        return maxFish;
    }
};
