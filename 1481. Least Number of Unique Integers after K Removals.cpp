//https://leetcode.com/problems/least-number-of-unique-integers-after-k-removals/description/

class Solution {
public:
    int findLeastNumOfUniqueInts(vector<int>& arr, int k) {
        map<int, int> map;
        vector<pair<int, int>> vec;
        int count = 0;

        for(const auto num: arr){
            map[num]++;                            // Store number's frequency into map
        }

        for(const auto it: map){
            vec.push_back({it.first, it.second});  // Push {number: frequency} pairs from map into vector
        }

        sort(vec.begin(), vec.end(), [&](pair<int, int>& a, pair<int, int>& b){
            return a.second < b.second;            // Sort the vector by frequency in ascending order
        });

        for(auto& pair: vec){                      // For all the pairs
            int minVal = min(k, pair.second);      //     -> Get the min value of K and current number's frequency
            k -= minVal;                           //     -> K deduct by min value
            pair.second -= minVal;                 //     -> frequency deduct by min value

            count += (pair.second > 0) ? 1 : 0;    //     IF frequency > 0 -> count+1
        }

        return count;
    }
};
