//https://leetcode.com/problems/maximum-number-of-balloons/description/

class Solution {
public:
    int maxNumberOfBalloons(string text) {
        map<char, int> map;
        int ans=INT_MAX;
        unordered_map<char, bool> umap= { //container of "balloon" letters
            {'b', false},{'a', false},
            {'n', false},{'o', false},
            {'l', false}
        };

        for(int i=0;i<text.length();i++){ //store all letters form text to map
             map[text[i]]++;
        }

        for(const auto it:map){
            switch(it.first){
                case 'b':case 'a':case 'n':
                    ans = (it.second<ans)? it.second : ans;     //check max "balloon" can be formed
                    if(it.second>=1)umap[it.first]=true;        //'b', 'a', 'n' should appear atleast once
                    break;
                case 'l':case 'o':
                    ans = (it.second/2<ans)? it.second/2 : ans; //check max "balloon" can be formed
                    if(it.second>=2)umap[it.first]=true;        //'l', 'o' should appear atleast twice
                    break;
            }
        }

        for(const auto it:umap){    //if 'b', 'a', 'n' not atleast 1 OR 'l', 'o' not atleast 2
           if(!it.second)return 0;  //not possible to form "balloon" -> return 0
        }

        return ans;
    }
};
