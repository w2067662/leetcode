//https://leetcode.com/problems/number-of-common-factors/description/

class Solution {
public:
    int commonFactors(int a, int b) {
        int count = 0, smallerNum = min(a, b);

        for(int i=1;i<=smallerNum;i++){         //i start from 1 to min(a, b)
            if(a%i == 0 && b%i == 0) count++;   //IF   a and b are both divisible by i -> count+1
        }

        return count;
    }
};
