#https://leetcode.com/problems/employees-earning-more-than-their-managers/description/

# Write your MySQL query statement below
SELECT e1.name AS Employee
From Employee e1 LEFT JOIN Employee e2
ON e1.managerId = e2.id
WHERE e1.salary > e2.salary
