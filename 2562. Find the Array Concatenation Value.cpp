//https://leetcode.com/problems/find-the-array-concatenation-value/description/

class Solution {
public:
    long long concat(int a, int b){ //concat 2 integar numbers into long long type 
        long long res =0;
        int count =0;

        res += b;

        while(b>0){
            count++;
            b/=10;
        }

        return res + a*pow(10, count);
    }

    long long findTheArrayConcVal(vector<int>& nums) {
        long long ans = 0;
        for(int i=0;i<nums.size()/2;i++){
            ans += concat(nums[i], nums[nums.size()-i-1]);        //concat number pairs
        }

        return nums.size()%2==0 ? ans: ans + nums[nums.size()/2]; //IF  there are odd amount of numbers, add up the middle one
    }
};
