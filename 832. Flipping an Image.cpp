//https://leetcode.com/problems/flipping-an-image/description/

class Solution {
public:
    vector<vector<int>> flipAndInvertImage(vector<vector<int>>& image) {
        for(int i=0;i<image.size();i++){   //flip image
            for(int j=0;j<image[i].size()/2;j++){
                swap(image[i][j], image[i][image[i].size()-j-1]);
            }
        }

        for(int i=0;i<image.size();i++){   //invert image
            for(int j=0;j<image[i].size();j++){
                image[i][j]= image[i][j]==1? 0:1;
            }
        }
        return image;
    }
};
