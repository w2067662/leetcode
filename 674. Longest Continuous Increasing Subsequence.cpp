//https://leetcode.com/problems/longest-continuous-increasing-subsequence/description/

class Solution {
public:
    int findLengthOfLCIS(vector<int>& nums) {
        int count=1;
        int max=1;

        for(int i=1;i<nums.size();i++){
            if(nums[i]>nums[i-1])count++;   //if nums[i] is bigger than nums[i-1], subsequence.size()++
            else count=1;                   //else reset subsequence.size()=1
            max = count>max ? count:max;    //store the max subsequence.size()
        } 

        return max;
    }
};
