//https://leetcode.com/problems/longest-harmonious-subsequence/description/

class Solution {
public:
    int findLHS(vector<int>& nums) {
        if(nums.size()==1)return 0; //only one element return 0

        int max=0;
        map<int, int> map;
        for(int i=0;i<nums.size();i++)map[nums[i]]++; //store the appear time of each number in map

        for(auto i=map.begin(), j=map.begin();i!=map.end();i++, j++){//set j = i's next element, from lower number to highest one
            if(j==map.begin())j++;
            if(abs(i->first-j->first)==1 && i->second+j->second>max){//if element i and j's value differ exactly 1, 
                    max=i->second+j->second;                         //check for i and j's appear times sum and store the largest sum
            }
        }
        return max;
    }
};
