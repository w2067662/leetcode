//https://leetcode.com/problems/binary-gap/description/

class Solution {
public:
    //ex:   n = 22  
    //-> string = "01101"
    //-> vec = [1, 2, 4] ('1's index)          maxDistance = 0
    //          ^  ^     distance = 1 > 0  ->  maxDistance = 1
    //             ^  ^  distance = 2 > 1  ->  maxDistance = 2
    //
    int binaryGap(int n) {
        vector<int> vec;
        int maxDistance = 0;;
        string binary = "";

        while(n>0){     
            binary += n%2 + '0';    //convert to binary string
            n/=2;
        }

        for(int i=0; i<binary.length(); i++){
            if(binary[i] == '1')vec.push_back(i);   //push '1's index into vector
        }

        for(int i=0; i<vec.size()-1; i++){
            maxDistance = vec[i+1]-vec[i] > maxDistance ? vec[i+1]-vec[i] : maxDistance; //get the maxDistance of adjacent 1's
        }

        return maxDistance;
    }
};
