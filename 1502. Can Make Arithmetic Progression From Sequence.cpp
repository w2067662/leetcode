//https://leetcode.com/problems/can-make-arithmetic-progression-from-sequence/description/

class Solution {
public:
    bool canMakeArithmeticProgression(vector<int>& arr) {
        if(arr.size()==2)return true;                         //if vector contain only 2 element -> always true

        sort(arr.begin(), arr.end());                         //sort the vector

        for(int i=1, diff=arr[1]-arr[0] ;i<arr.size()-1;i++){ //reset diff = arr[1]-arr[0], index start from 1 to arr.size-1 
            if(arr[i+1]-arr[i]!=diff)return false;            //if not Arithmetic Progression (consecutive numbers with same difference),
        }                                                     //then return false

        return true;
    }
};
