//https://leetcode.com/problems/unique-email-addresses/description/

class Solution {
public:
    string simplifyEmail(string s){ //simplify the email
        string localName  ="";
        string domainName ="";

        //get the lacalName
        for(int i=0; i<s.length();i++){
            if(s[i]=='+' || s[i]=='@')break;           //if encounter '+' or '@', end the loop
            else if (islower(s[i]))localName += s[i];  //if encounter lower case append letter to local name
        }

        //get the domainName
        for(int i = 0;i<s.length();i++){
            if(s[i]=='@'){                             //if encounter '@'
                for(int j = i+1;j<s.length();j++){     //i start from '@'s index+1 to string's end
                    if (islower(s[j]) || s[j]=='.')domainName += s[j]; //if encounter lower case or '.' append to domain name
                }
                break;                                 //end the loop
            }
        }

        return localName + "@" + domainName;    //return [localName]@[domainName]
    }

    int numUniqueEmails(vector<string>& emails) {
        set<string> set;
        for(int i=0; i<emails.size();i++){
            set.insert(simplifyEmail(emails[i]));
        }

        return set.size();  //the amount of actual emails that recieve mail
    }
};
