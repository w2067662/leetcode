//https://leetcode.com/problems/student-attendance-record-i/description/

class Solution {
public:
    bool checkRecord(string s) {
        int absent =0;
        int late =0;
        int consecutiveLate=0;
        for(int i=0;i<s.length();i++){
            if(s[i]=='A'){
                absent++;
                late =0;                //reset late day
            }
            else if(s[i]=='L'){
                late++;
                if(late>consecutiveLate) consecutiveLate =late; //store max consecutive Late day
            }
            else if(s[i]=='P')late =0;  //reset late day
        }
        return absent<2 && consecutiveLate<3; //return strictly Absent fewer than 2 in total, 
                                              //AND consecutive Late day fewer than 3
    }
};
