//https://leetcode.com/problems/count-integers-with-even-digit-sum/description/

class Solution {
public:
    int digitSum(int n){        //calculate digit sum
        int res = 0;
        while(n>0){
            res += n%10;
            n/=10;
        }
        return res;
    }

    int countEven(int num) {
        int count =0;

        for(int i=1;i<=num;i++){
            if(digitSum(i)%2 == 0)count++; //IF  digit sum is even -> count+1
        }

        return count;
    }
};
