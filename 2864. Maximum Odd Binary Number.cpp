//https://leetcode.com/problems/maximum-odd-binary-number/description/

class Solution {
public:
    string maximumOddBinaryNumber(string s) {
        string ans = "";
        int count =0;

        for(int i=0;i<s.length();i++){
            if(s[i] == '1')count++;                //count for '1' in string
            ans += i == s.length()-1 ? '1' : '0';  //set last digit in answer as '1'
        }

        for(int i=0;i<count-1;i++){
            ans[i] = '1';                          //assign rest of '1's to answer from left to right
        }

        return ans;
    }
};
