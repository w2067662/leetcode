//https://leetcode.com/problems/pseudo-palindromic-paths-in-a-binary-tree/description/

class Solution {
public:
    bool isLeafNode(TreeNode* root){ // Check currrent node is leaf node or not
        return !root->left && !root->right;
    }

    bool isPseudoPalindromicPath(map<int, int>& path){ // Check path is Pseudo Palindromic
        int odd = 0;

        for(const auto it: path){
            if(it.second%2 != 0) odd++;
        }

        return odd <= 1; // Odd frequency should <= 1 to form a Pseudo Palindromic path
    }

    void traverse(TreeNode* root, map<int, int>& path, int &count){
        if(!root) return;

        path[root->val]++;                                     // Add frequency of current node value

        if(isLeafNode(root) && isPseudoPalindromicPath(path)){ // IF is leaf node and is Pseudo Palindromic Path -> count+1
            count++;
        }

        traverse(root->left , path, count);                    // Traverse left  subtree
        traverse(root->right, path, count);                    // Traverse right subtree

        path[root->val]--;                                     // Deduct frequency of node value

        return;
    }

    int pseudoPalindromicPaths (TreeNode* root) {
        map<int, int> path;
        int count = 0;

        traverse(root, path, count); // Traverse the tree to find the Pseudo Palindromic Paths

        return count;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
