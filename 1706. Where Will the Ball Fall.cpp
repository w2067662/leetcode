//https://leetcode.com/problems/where-will-the-ball-fall/description/

class Solution {
private:
    // Right: (0 ,1)
    // Down : (1 ,0)
    // Left : (-1,0)
    vector<int> steps = {0,1,0,-1};

public:
    //ex:

    // grid = [[ 1, 1, 1,-1,-1],            1 -> [1 0 0    -1 -> [0 0 1
    //         [ 1, 1, 1,-1,-1],                  0 1 0           0 1 0
    //         [-1,-1,-1, 1, 1],                  0 0 1]          1 0 0]
    //         [ 1, 1, 1, 1,-1],
    //         [-1,-1,-1,-1,-1]]

    // matrix =                                           v       v       v       v       v    (start point)
    // [ 1 0 0 | 1 0 0 | 1 0 0 | 0 0 1 | 0 0 1      ->  1 0 0 | 1 0 0 | 1 0 0 | 0 0 1 | 0 0 1  
    //   0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 
    //   0 0 1 | 0 0 1 | 0 0 1 | 1 0 0 | 1 0 0 
    //   ------ ------- ------- ------- -------
    //   1 0 0 | 1 0 0 | 1 0 0 | 0 0 1 | 0 0 1               |
    //   0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 | 0 1 0             1 v-> 1 0        <- C ->  (use DFS to find the route)
    //   0 0 1 | 0 0 1 | 0 0 1 | 1 0 0 | 1 0 0             0 1 | 0 1           |
    //   ------ ------- ------- ------- -------            0 1 | 0 1           v
    //   0 0 1 | 0 0 1 | 0 0 1 | 1 0 0 | 1 0 0             1 <-v 1 0
    //   0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 | 0 1 0             1 | 0 1 0
    //   1 0 0 | 1 0 0 | 1 0 0 | 0 0 1 | 0 0 1               v
    //   ------ ------- ------- ------- -------
    //   1 0 0 | 1 0 0 | 1 0 0 | 1 0 0 | 0 0 1 
    //   0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 
    //   0 0 1 | 0 0 1 | 0 0 1 | 0 0 1 | 1 0 0 
    //   ------ ------- ------- ------- -------
    //   0 0 1 | 0 0 1 | 0 0 1 | 0 0 1 | 0 0 1 
    //   0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 | 0 1 0 
    //   1 0 0 | 1 0 0 | 1 0 0 | 1 0 0 | 1 0 0 ]    ->  1 0 0 | 1 0 0 | 1 0 0 | 1 0 0 | 1 0 0  
    //                                                    v v     v v     v v     v v     v v
    //                                                    0       1       2       3       4    (out column)
    //
    vector<vector<int>> toMatrix(vector<vector<int>>& grid){
        vector<vector<int>> matrix(grid.size()*3 , vector<int>(grid[0].size()*3, 0)); // Create matrix with size = (grid.width x 3, grid.height x 3)

        for(int i=0; i<grid.size(); i++){
            for(int j=0; j<grid[i].size(); j++){
                switch(grid[i][j]){
                    case -1:                                              // IF current value is -1
                        for(int x=0; x<3 ;x++){                           
                            for(int y=0; y<3 ;y++){
                                if(x+y+1 == 3) matrix[i*3+x][j*3+y] = 1;  //    -> draw the 3 x 3 matrix
                            }
                        }
                        break;
                    case 1:                                               // IF current value is 1
                        for(int x=0; x<3 ;x++){
                            for(int y=0; y<3 ;y++){
                                if(x == y) matrix[i*3+x][j*3+y] = 1;      //    -> draw the 3 x 3 matrix
                            }
                        }
                        break;
                }
            }
        }

        return matrix;
    }

    vector<int> fall(vector<vector<int>>& grid, vector<vector<int>>& matrix){ // Return the answers of fallen columns of balls
        vector<int> ans;

        for(int i=0; i<grid[0].size(); i++){                // For all the start points
            set<pair<int, int>> visited;
            int outColumn = -1;                             //     Set default out column as -1 (no out column)

            DFS(matrix, {0, i*3+1}, visited, outColumn);    //     Use DFS to find the route to out column

            ans.push_back(outColumn);                       //     Push the out column into answer
        }

        return ans;
    }

    bool isValid(vector<vector<int>>& matrix,const pair<int, int>& curr){   // Check current position is within the range of matrix or not
        return 0 <= curr.first && curr.first < matrix.size() && 0 <= curr.second && curr.second < matrix[0].size();
    }

    void DFS(vector<vector<int>>& matrix, pair<int, int> pos, set<pair<int, int>>& visited, int& outColumn){
        visited.insert(pos);                                                   // Mark current position as visited

        if(pos.first == matrix.size()-1){                                      // IF current position reaches the bottom of matrix
            outColumn = pos.second/3;                                          //    -> store the out column as answer
            return;                                                            //    -> end the DFS
        }

        for(int s=0; s<steps.size()-1; s++){                                   // For next steps left, right, down
            pair<int, int> next = {pos.first+steps[s], pos.second+steps[s+1]};

            if (isValid(matrix, next) &&                                       //     IF next step is valid AND
                matrix[next.first][next.second] == 0 &&                        //        next step is route (value = 0)
                visited.find(next) == visited.end()){                          //        next step is not visited
                DFS(matrix, next, visited, outColumn);                         //        -> DFS next step
            }
        }

        return;                                                                // Traverse all the possible routes and return
    }

    vector<int> findBall(vector<vector<int>>& grid) {
        vector<vector<int>> matrix = toMatrix(grid);   // Create the matrix for calculation
        return fall(grid, matrix);                     // Calculate the fallen column of balls
    }
};
