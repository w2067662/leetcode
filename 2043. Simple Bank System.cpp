//https://leetcode.com/problems/simple-bank-system/description/

class Bank {
private:
    vector<long long> Balance;  //vector for accounts balance
    
public:
    Bank(vector<long long>& balance) {  //initialize accounts balance
        //create a empty account
        long long emptyAccount=0;
        Balance.push_back(emptyAccount);
        
        //ID of accountN  is N
        for(int i=0;i<balance.size();i++){
            Balance.push_back(balance[i]);
        }
    }
    
    bool transfer(int account1, int account2, long long money) {    //account1 -> transfer money ->account2
        if(!isValidAccount(account1)||!isValidAccount(account2))return false;
        if(Balance[account1]<money)return false;
        else{
            Balance[account1]-=money;
            Balance[account2]+=money;
            return true;
        }
    }
    
    bool deposit(int account, long long money) {
        if(!isValidAccount(account))return false;
        Balance[account]+=money;
        return true;
    }
    
    bool withdraw(int account, long long money) {
        if(!isValidAccount(account))return false;
        if(Balance[account]<money)return false;
        else{
            Balance[account]-=money;
            return true;
        }
    }

    bool isValidAccount(int accountID){     //check account validity
        return accountID>=1 && accountID<=Balance.size();
    }
};

/**
 * Your Bank object will be instantiated and called as such:
 * Bank* obj = new Bank(balance);
 * bool param_1 = obj->transfer(account1,account2,money);
 * bool param_2 = obj->deposit(account,money);
 * bool param_3 = obj->withdraw(account,money);
 */
