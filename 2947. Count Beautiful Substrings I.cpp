//https://leetcode.com/problems/count-beautiful-substrings-i/description/

class Solution {
public:
    //ex:   s = "baeyh"     
    //           ^^      i = 2 ; j = 0 ; count = 0
    //            ^^     i = 2 ; j = 1 ; count = 0
    //             ^^    i = 2 ; j = 2 ; count = 0
    //              ^^   i = 2 ; j = 3 ; count = 0
    //           ^^^^    i = 4 ; j = 0 ; count = 1(+1)
    //            ^^^^   i = 4 ; j = 1 ; count = 2(+1)
    //
    bool isVowel(char &ch){ //check char is vowel or not
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

    int beautifulSubstrings(string s, int k) {
        int count = 0;

        for(int i=2; i<=s.length(); i+=2){                                    //i start from 2 to 2N (2, 4, 6, 8...)
            int vowel = 0, nonVowel = 0;

            for(int j=0; j+i-1<s.length(); j++){                              //j start from 0 to Len-i
                if(j == 0){                                                   //IF  j = 0
                    for(int k=0; k<=j+i-1;k++)                                //    -> calculate vowels and non-vowels within current substring
                        isVowel(s[k]) ? vowel++ : nonVowel++;
                    }
                } else {                                                      //IF  j != 0
                    isVowel(s[j-1]) ? vowel-- : nonVowel--;                   //    -> shift right the window (re-calculate the the vowels and non-vowels)
                    isVowel(s[j+i-1]) ? vowel++ : nonVowel++;
                }
                
                if(vowel == nonVowel && (vowel * nonVowel % k) == 0) count++; //IF  substring is beautiful -> count+1
            }
        }

        return count;
    }
};
