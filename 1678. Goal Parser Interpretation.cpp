//https://leetcode.com/problems/goal-parser-interpretation/description/

class Solution {
public:
    string interpret(string command) {
        string ans ="";
        int brace = false;
        int contain = false;
        
        for(int i=0; i<command.length();i++){
            switch(command[i]){
                case 'G':                   //if encounter 'G'
                    ans += "G";             //  append 'G' to answer
                    break;                  
                case '(':                   //if encounter '('
                    brace = true;           //  found left brace -> set brace  = TRUE
                    break;
                case ')':                   //if encounter ')'
                    if(contain)ans += "al"; //  if letters contain in brace -> append "al" into answer
                    else ans += "o";        //  if no letters contain in brace -> append 'o' into answer

                    contain = false;        //  reset contain = FALSE
                    brace = false;          //  reset brace = FALSE
                    break;
                default:                    //if found other letters
                    contain = true;         //  there are letters contain in brace -> set contain = TRUE
                    break;
            }
        }
        return ans;
    }
};
