//https://leetcode.com/problems/minimum-hours-of-training-to-win-a-competition/description/

class Solution {
public:
    int minNumberOfHours(int initialEnergy, int initialExperience, vector<int>& energy, vector<int>& experience) {
        int neededEnergy = 0, neededExp = 0;
        for(int i=0;i<energy.size();i++){
            if(initialEnergy <= energy[i]){                     //IF　　current energy is not able to defeat enemy
                neededEnergy += energy[i]+1-initialEnergy;      //      -> add up needed training hours
                initialEnergy = energy[i]+1;
            }

            if(initialExperience <= experience[i]){             //IF　　current experience is not able to defeat enemy
                neededExp += experience[i]+1-initialExperience; //      -> add up needed training hours
                initialExperience = experience[i]+1;
            }

            initialEnergy     -= energy[i];                     //update energy and experience after defeat the enemy
            initialExperience += experience[i];
        }

        return neededEnergy + neededExp;
    }
};
