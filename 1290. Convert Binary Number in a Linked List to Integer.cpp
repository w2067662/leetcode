//https://leetcode.com/problems/convert-binary-number-in-a-linked-list-to-integer/description/

class Solution {
public:
    int getDecimalValue(ListNode* head) {
        int ans=0;
        while(head->next){
            ans = ans*2 + head->val;    //binary to int
            head=head->next;            //go to next node
        }
        ans = ans*2 + head->val;
        return ans;
    }
};
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
