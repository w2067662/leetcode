//https://leetcode.com/problems/maximize-happiness-of-selected-children/description/

class Solution {
public:
    //ex:  happiness = [1,2,3]         
    //    happiness' = [3,2,1] (sorted) ;  k = 2  ;  res = 0
    //                  ^               ;  k = 1  ;  res = 0 + 3-0(index) = 3
    //                    ^             ;  k = 0  ;  res = 3 + 2-1(index) = 4
    //                                               res = 4
    long long maximumHappinessSum(vector<int>& happiness, int k) {
        sort(happiness.begin(), happiness.end(),        // Sort the vector in descending order
            [&](const int& a, const int& b){            
                return a > b;
            }
        );

        int i = 0;
        long long res = 0;

        while(k--){                                     // WHILE  K >= 0
            happiness[i] = max(happiness[i] - i, 0);    //      Recalculate the happiness of current child (each turn happiness decrease by 1)
            res += happiness[i++];                      //      Add up the happiness of current child to answer (Goto next child)
        }

        return res;
    }
};
