//https://leetcode.com/problems/replace-words/description/

class Trie { // Define Trie
private:
    class TrieNode{ // Define TrieNode
        public:
            char ch;
            map<char, TrieNode*> children; // With multiple children node
            TrieNode(){};
            TrieNode(char c):ch(c){};
    };
    
    TrieNode* root;

    void insertWord(TrieNode* root, const string& word, int index){
        if(index >= word.length()){
            root->children['$'] = new TrieNode('$');                  // Set the string end as '$'
            return;
        } 

        if(root->children.find(word[index]) == root->children.end()){ // IF  no child node's value match current char
            root->children[word[index]] = new TrieNode(word[index]);  //     -> create a new child node with current char value
        }

        insertWord(root->children[word[index]], word, index+1);       // Insert next char
    }

    void replaceWord(TrieNode* root, string& word, int index){
        if(root->children.find('$') != root->children.end()){         // IF  found the shortest prefix word in dictionary
            word = word.substr(0,index);                              //     -> raplace it
            return;
        }

        if(root->children.find(word[index]) == root->children.end()){ // IF  no child node's value match current char
            return;                                                   //     -> ends the recursion
        }

        replaceWord(root->children[word[index]], word, index+1);
    }
    
public:
    Trie() {
        this->root = new TrieNode;  // Initialize with creating new root
    }
    
    void insert(const string &word) {
        insertWord(this->root, word, 0);
    }

    void replace(string &word){
        replaceWord(this->root, word, 0);
    }
};

class Solution {
public:
    vector<string> toWords(string &s){  // Convert string to vector of words 
        vector<string> vec;
        string temp = "";

        for(const auto ch: s){
            if(ch == ' '){
                vec.push_back(temp);
                temp = "";
            } else temp += ch;
        }
        if(temp != "") vec.push_back(temp);

        return vec;
    }

    string toString(vector<string> words){  // Convert words vector to string
        string s = "";
        for(int i=0;i<words.size();i++){
            s += words[i];
            if(i != words.size()-1) s += " ";
        }
        return s;
    }

    string replaceWords(vector<string>& dictionary, string sentence) {
        vector<string> words = toWords(sentence);
        Trie trie;

        for(const auto word: dictionary){
            trie.insert(word);            // Store the words from dictionary into Trie
        }

        for(auto& word: words){
            trie.replace(word);           // Replace the word in sentence
        }

        return toString(words);
    }
};
