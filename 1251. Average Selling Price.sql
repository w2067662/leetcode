#https://leetcode.com/problems/average-selling-price/description/

# Write your MySQL query statement below
SELECT p.product_id, 
ROUND(SUM(p.price*u.units)/SUM(u.units), 2) AS average_price
FROM Prices AS p
LEFT JOIN UnitsSold AS u
ON p.product_id=u.product_id AND purchase_date>=start_date AND purchase_date<=end_date
GROUP BY p.product_id
