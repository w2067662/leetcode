//https://leetcode.com/problems/difference-between-element-sum-and-digit-sum-of-an-array/description/

class Solution {
public:
    int getDigitSum(int num){   //sum up digit in the number
        int sum=0;
        string s = to_string(num);
        for(int j=0;j<s.length();j++){
            sum += s[j] - '0';
        }
        return sum;
    }

    int differenceOfSum(vector<int>& nums) {
        int elementSum = 0;
        int digitSum = 0;

        for(int i=0;i<nums.size();i++){
            elementSum += nums[i];            //sum up elements value
            digitSum += getDigitSum(nums[i]); //sum up numbers digit sum
        }

        return abs(elementSum-digitSum);
    }
};
