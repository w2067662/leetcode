//https://leetcode.com/problems/add-two-promises/description/

/**
 * @param {Promise} promise1
 * @param {Promise} promise2
 * @return {Promise}
 */
var addTwoPromises = async function(promise1, promise2) {
    //!!await can only be used in async function
    //waiting for all pomises are resolved
    const [value1, value2] = await Promise.all([promise1, promise2]); //Promise.all(iterable); 
    return value1 + value2;
};

/**
 * addTwoPromises(Promise.resolve(2), Promise.resolve(2))
 *   .then(console.log); // 4
 */
