//https://leetcode.com/problems/equal-row-and-column-pairs/description/

class Solution {
public:
    int equalPairs(vector<vector<int>>& grid) {
        vector<string> colStrings, rowStrings;
        int count = 0;

        for(int i=0; i<grid.size(); i++){           // Store all the number in each row into strings
            string temp = "";

            for(int j=0; j<grid[i].size(); j++){
                temp += to_string(grid[i][j]);
                temp += ',';
            }

            rowStrings.push_back(temp);
        }

        for(int j=0; j<grid[0].size(); j++){        // Store all the number in each column into strings
            string temp = "";

            for(int i=0; i<grid.size(); i++){
                temp += to_string(grid[i][j]);
                temp += ',';
            }

            colStrings.push_back(temp);
        }

        for(const auto rowString: rowStrings){
            for(const auto colString: colStrings){
                if(rowString == colString) count++; // Find the equal pairs within row strings and column strings
            }
        }

        return count;
    }
};
