//https://leetcode.com/problems/number-of-steps-to-reduce-a-number-in-binary-representation-to-one/description/

class Solution {
public:
    // ex:  s =  "1101"
    //
    //  "1101" -> "1110" -> "111" -> "1000" -> "100" -> "10" -> "1" (6 steps)
    //
    int numSteps(string s) {
        int steps = 0, carry = 0;

        for(int i=s.length()-1, temp; i>=0; i--){   // For last digit to first digit
            temp = (s[i]-'0') + carry;              //      Calculate current value

            if(temp == (s[0]-'0') && i == 0) break; //      IF  is first digit AND no carry -> end the loop

            switch(temp) {
                case 0:                             //      IF  current value is 0
                    carry = 0;                      //          -> carry = 0
                    steps += 1;                     //          -> remove 0 (steps + 1)
                    break;
                case 1:                             //      IF  current value is 1
                    carry = 1;                      //          -> carry = 1
                    steps += 2;                     //          -> carry to next digit AND remove 0 (steps + 2)
                    break;
                case 2:                             //      IF  current value is 2
                    carry = 1;                      //          -> carry = 1
                    steps += 1;                     //          -> remove 0 (steps + 1)
                    break;
            }
        }

        return steps;
    }
};