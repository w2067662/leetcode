//https://leetcode.com/problems/count-pairs-of-similar-strings/description/

class Solution {
public:
    bool contain_same_letters(int i, int j, vector<set<char>>& LetterSet){  //check if two words have same letter set or not
        if(LetterSet[i].size()!=LetterSet[j].size())return false;
        
        for(const auto& letter: LetterSet[i]){
            if(LetterSet[j].find(letter)==LetterSet[j].end())return false;
        }
        
        return true;
    }

    int similarPairs(vector<string>& words) {
        vector<set<char>> LetterSet(words.size());              //storing letter sets of each word
        int ans=0;

        for(int i=0;i<words.size();i++){                        //insert letters of each word into set
            for(int j=0;j<words[i].length();j++){
                LetterSet[i].insert(words[i][j]);
            }
        }

        for(int i=0;i<words.size()-1;i++){
            for(int j=i+1;j<words.size();j++){                  
                if(contain_same_letters(i, j, LetterSet))ans++; //if two words have same letter set, find 1 pair, ans+=1
            }
        }

        return ans;
    }
};
