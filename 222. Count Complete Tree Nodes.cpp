//https://leetcode.com/problems/count-complete-tree-nodes/description/

class Solution {
public:
    int find_max(TreeNode* root, int index){
        //if no child nodes, return current index
        if(!root->left&&!root->right) return index; 
        //if left and right child node exist, find max index from left and right subtree
        else if(root->left&&root->right) return max(find_max(root->left, index*2), find_max(root->right, index*2+1));
        //else find max index from left subtree
        return find_max(root->left, index*2);
    }

    int countNodes(TreeNode* root) {
        if(!root)return 0;          //if no root, return 1
        return find_max(root, 1);   //if have root , start from 1
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
