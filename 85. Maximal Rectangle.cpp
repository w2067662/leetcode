//https://leetcode.com/problems/maximal-rectangle/description/

class Solution {
public:
    //ex:  matrix = [["1","0","1","0","0"],    ->  heights = [1,0,1,0,0]  ;  maxArea = 1
    //               ["1","0","1","1","1"],    ->  heights = [2,0,2,1,1]  ;  maxArea = 3
    //               ["1","1","1","1","1"],    ->  heights = [3,1,3,2,2]  ;  maxArea = 6
    //               ["1","0","0","1","0"]]    ->  heights = [4,0,0,3,0]  ;  maxArea = 6
    //                                                                       maxArea = 6
    int maximalRectangle(vector<vector<char>>& matrix) {
        if(matrix.empty()) return 0;

        vector<int> heights(matrix[0].size()+1, 0);
        int maxArea = 0;

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                heights[j] = (matrix[i][j] == '1') ? heights[j]+1 : 0;         // Update the heights of current row
            }

            stack<int> stack;
            for(int j=0; j<heights.size(); j++){                               // Iterate through the heights
                while(!stack.empty() && heights[j] < heights[stack.top()]){    //    While  stack is not empty AND
                                                                               //           current height < height of stack top
                    int height = heights[stack.top()];                         //           -> Calculate height
                    stack.pop();                                               //           -> pop height of stack top
                    int width = (stack.empty())? j : j-stack.top()-1;          //           -> Calculate width

                    maxArea = max(maxArea, width*height);                      //           Update max area
                }

                stack.push(j);                                                 //    Push current height's index onto stack
            }
        }

        return maxArea;
    }
};
