//https://leetcode.com/problems/adding-spaces-to-a-string/description/

class Solution {
public:
    string addSpaces(string s, vector<int>& spaces) {
        string ans = "";

        for(int i=0, j=0, insertIndex; i<s.length(); i++){
            insertIndex = (j >= spaces.size()) ? INT_MAX : spaces[j]; // IF   j out of range -> set insert index as INT_MAX
                                                                      // ELSE                -> set insert index as spaces[j]

            if(i < insertIndex) ans += s[i];                          // IF   i < insert index -> append s[i] to answer
            else {                                                    // ELSE
                ans += " ";                                           //                       -> append  ' ' to answer
                i--;
                j++;
            }
        }

        return ans;
    }
};
