//https://leetcode.com/problems/count-number-of-distinct-integers-after-reverse-operations/description/

class Solution {
public:
    int reverseInt(int num){    //calculate the reversed integar
        string n = to_string(num);
        int res = 0;

        for(int i=n.length()-1; i>=0; i--){
            res = res * 10 + n[i] - '0';
        }

        return res;
    }

    int countDistinctIntegers(vector<int>& nums) {
        set<int> set;

        for(const auto num: nums){
            set.insert(num);               //insert current number into set
            set.insert(reverseInt(num));   //insert reversed current number into set
        }

        return set.size();
    }
};
