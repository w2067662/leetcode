//https://leetcode.com/problems/sum-root-to-leaf-numbers/description/

class Solution {
public:
    bool isLeaf(TreeNode* root){    // Check is leaf node or not
        return !root->left && !root->right;
    }

    int pathNum(const vector<int> &path){  // Calculate path number
        int sum = 0;
        for(const auto num: path){
            sum = sum*10 + num;
        }
        return sum;
    }

    void traverse(TreeNode* root, vector<int> &path, int &sum){ // Traverse the tree
        if(!root) return;

        path.push_back(root->val);              // Push current number into path

        if(isLeaf(root)) sum += pathNum(path);  // IF   current node is leaf node -> sum up its path number

        traverse(root->left , path, sum);       // Traverse left  subtree
        traverse(root->right, path, sum);       // Traverse right subtree

        path.pop_back();                        // Pop last number from path

        return ;
    }

    int sumNumbers(TreeNode* root) {
        vector<int> path;
        int sum = 0;

        traverse(root, path, sum);  // Traverse the tree

        return sum;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

