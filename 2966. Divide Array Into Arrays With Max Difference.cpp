//https://leetcode.com/problems/divide-array-into-arrays-with-max-difference/description/

class Solution {
public:
    //ex1:  nums = [1,3,4,8,7,9,3,5,1], k = 2
    //     nums' = [1,1,3,3,4,5,7,8,9] (sorted)          ; result = []
    //              ^ ^ ^                       3-1 <= 2 ; result = [{1,1,3}]
    //                    ^ ^ ^                 5-3 <= 2 ; result = [{1,1,3}, {3,4,5}]
    //                          ^ ^ ^           9-7 =< 2 ; result = [{1,1,3}, {3,4,5}, {7,8,9}]
    //
    //                                                     ->  result = [{1,1,3}, {3,4,5}, {7,8,9}]
    //
    //ex2:  nums = [1,3,3,2,7,3], k = 3
    //     nums' = [1,2,3,3,3,7] (sorted)                ; result = []
    //              ^ ^ ^                       3-1 <= 3 ; result = [{1,2,3}]
    //                    ^ ^ ^                 7-3  > 3  (not able to divide array)
    //
    //                                                     ->  result = []
    //
    vector<vector<int>> divideArray(vector<int>& nums, int k) {
        vector<vector<int>> result;

        sort(nums.begin(), nums.end());                                      // Sort numbers in ascending order

        for (int i=0; i<nums.size(); i+=3) {                                 // For i from 0 to nums.size (traverse 3 elements each time)
            if (i+2 < nums.size() && nums[i+2]-nums[i] <= k) {               //      IF current group contains exactly 3 elements AND
                                                                             //         difference of the largest and the smallest <= K
                result.push_back({nums[i], nums[i+1], nums[i+2]});           //         -> push current group into answer
            }
            else return {};                                                  //      IF not able to divide the array 
                                                                             //         -> return empty array
        }

        return result;
    }
}; 
