//https://leetcode.com/problems/double-a-number-represented-as-a-linked-list/description/

class Solution {
public:
    // reverse():

    //    NULL      1 -> 8 -> 9 -> NULL
    //     ^        ^               
    //  prev,next  curr         

    //    NULL      1 -> 8 -> 9 -> NULL
    //     ^        ^    ^          
    //    prev     curr next

    //    NULL  <-  1    8 -> 9 -> NULL
    //     ^        ^    ^          
    //    prev     curr next

    //    NULL  <-  1    8 -> 9 -> NULL
    //              ^    ^          
    //        prev,curr next

    //    NULL  <-  1    8 -> 9 -> NULL
    //              ^    ^          
    //            prev curr,next

    //    NULL  <-  1 <- 8    9 -> NULL

    //    NULL  <-  1 <- 8 <- 9

    ListNode* reverse(ListNode* head) {
        ListNode* prev = NULL, *curr = head, *next = NULL;

        while (curr) {           // WHILE  current node is not NULL
            next = curr->next;   //     Store next node
            curr->next = prev;   //     Reverse current node's pointer
            prev = curr;         //     Move pointers one position ahead
            curr = next;         //     Set next node as current
        }

        return prev;             // New head of the reversed list
    }

    // doubleListFromBehind():

    //      9(* 2 + 0 = 18) -> 9 -> 9  -> NULL     carry = 1
    //      ^
    
    //      8 -> 9(* 2 + 1 = 19) -> 9  -> NULL     carry = 1
    //           ^

    //      8 -> 9 -> 9(* 2 + 1 = 19)  -> NULL     carry = 1
    //                ^

    //      8 -> 9 -> 9 -> 1(new node) -> NULL
    //                     ^

    ListNode* doubleListFromBehind(ListNode* head){
        ListNode* curr = head;
        int carry = 0;

        while (curr) {                              // WHILE  current node is not NULL
            curr->val = curr->val * 2 + carry;      //      Double the value + Carry
            carry = curr->val / 10;                 //      Calculate next node's Carry
            curr->val %= 10;                        //      Calculate current node's value
            if (!curr->next && carry) {             //      IF  current node's next is NULL and Carry is 1
                curr->next = new ListNode(carry);   //          -> Set new node with value 1 as current node's next
                break;                              //          -> End the loop
            }
            curr = curr->next;                      //      Goto next node
        }

        return head;
    }

    ListNode* doubleIt(ListNode* head) {
        return (!head) ? NULL : reverse(doubleListFromBehind(reverse(head)));   // IF    no head -> return NULL
                                                                                // ELSE          -> return list with doubled number
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */