//https://leetcode.com/problems/serialize-and-deserialize-bst/description/

class Codec {
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        vector<int> inorder;
        traverse(root, inorder);                  //travere the tree to get node value's inorder
        return toString(inorder);                 //return inorder vector as string
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        vector<int> inorder = toVectorInt(data);  //convert data from string to inorder vector
        return buildTree(inorder);                //build the tree with inorder vector
    }

    //------------------------
    // supporting functions
    //------------------------
    void traverse(TreeNode* root, vector<int> &inorder){ //traverse the tree
        if(!root)return;

        inorder.push_back(root->val);

        traverse(root->left , inorder);
        traverse(root->right, inorder);
        return;
    }

    string toString(const vector<int> &vec){ //convert vector<int> to string
        string s = "";
        for(int i=0;i<vec.size();i++){
            s += to_string(vec[i]);
            if(i != vec.size()-1) s += ",";
        }
        return s;
    }

    vector<int> toVectorInt(const string &s){ //convert string to vector<int>
        vector<int> vec;
        string temp = "";

        for(const auto ch: s){
            if(ch == ','){
                vec.push_back(stoi(temp));
                temp = "";
            } else temp += ch;
        }
        if(temp != "") vec.push_back(stoi(temp));

        return vec;
    }

    TreeNode* buildTree(vector<int> &inorder){ //build the tree with inorder vector
        TreeNode* root = NULL;

        for(const auto num: inorder){
            if(!root) root = new TreeNode(num);
            else insert(root, num);
        }

        return root;
    }

    void insert(TreeNode* root, const int num){ //insert the values from inorder vector
        if(num > root->val){
            if(root->right) insert(root->right, num);
            else root->right = new TreeNode(num);
        } else {
            if(root->left) insert(root->left, num);
            else root->left = new TreeNode(num);
        }

        return;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

// Your Codec object will be instantiated and called as such:
// Codec* ser = new Codec();
// Codec* deser = new Codec();
// string tree = ser->serialize(root);
// TreeNode* ans = deser->deserialize(tree);
// return ans;
