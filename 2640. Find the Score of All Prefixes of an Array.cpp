//https://leetcode.com/problems/find-the-score-of-all-prefixes-of-an-array/description/

class Solution {
public:
    vector<long long> findPrefixScore(vector<int>& nums) {
        vector<long long> output;
        long long maxNum = 0, curr = 0;

        for(auto& num: nums){                       // For all the numbers in vector
            maxNum = num > maxNum ? num : maxNum;   //      Store the largest number
            num += maxNum;                          //      Add up max number to current number
        }

        for(const auto num: nums){                  // For all the numbers in vector
            curr += num;                            //      Calculate the current value
            output.push_back(curr);                 //      Push the current value into output
        }

        return output;
    }
};
