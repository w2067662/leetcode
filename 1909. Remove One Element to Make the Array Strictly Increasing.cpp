//https://leetcode.com/problems/remove-one-element-to-make-the-array-strictly-increasing/description/

class Solution {
public:
    bool canBeIncreasing(vector<int>& nums1) {
        vector<int> nums2(nums1);   //copy nums1 to nums2
        bool isAscending1 = true, isAscending2 = true;

        for(int i=1; i<nums1.size(); i++){
            if(nums1[i-1] >= nums1[i]){
                nums1.erase(nums1.begin()+i-1);             //erase i-1 from nums1
                nums2.erase(nums2.begin()+i);               //erase i   from nums2
                break;
            } 
        }

        for(int i=1; i<nums1.size(); i++){
            if(nums1[i-1] >= nums1[i])isAscending1 = false; //check nums1 is ascending or not
            if(nums2[i-1] >= nums2[i])isAscending2 = false; //check nums2 is ascending or not
        }

        return isAscending1 || isAscending2;                //check nums1 or nums2 one is ascending
    }
};
