//https://leetcode.com/problems/maximum-difference-by-remapping-a-digit/description/

class Solution {
private:
    typedef char RemapType;
    const RemapType MAX = '9';
    const RemapType MIN = '0';
public:
    int remap(int num, RemapType rtype){    //remap the number to max or to min
        string s = to_string(num);
        int res = 0;
        char ch = ' ';

        for(int i=0; i<s.length(); i++){    //set the first non '9' or non '0' digit as ch
            if(s[i] != rtype){
                ch = s[i];
                break;
            }
        }

        for(int i=0; i<s.length(); i++){    //change the digits with same value to ch to '9' or '0'
            if(s[i] == ch) s[i] = rtype;
        }

        for(int i=0; i<s.length(); i++){    //convert remapped string number to integar
            res = res*10 + s[i]- '0';
        }

        return res;
    }

    int minMaxDifference(int num) {
        return remap(num, MAX) - remap(num, MIN); //calculate the difference of max and min
    }
};
