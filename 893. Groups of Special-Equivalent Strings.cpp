//https://leetcode.com/problems/groups-of-special-equivalent-strings/description/

class Solution {
public:
    string process(string &s){  //process the strings
        string odd = "", even = "";

        for(int i=0; i<s.length(); i++){
            i%2 == 0 ? even += s[i] : odd += s[i];                   //store the even and odd index chars
        }

        sort( odd.begin(),  odd.end());                              //sort the  odd index chars
        sort(even.begin(), even.end());                              //sort the even index chars

        s = "";

        for(int i=0;i<odd.length();i++){                             //process the string to make chars in even and odd index in alphabet order
            s += even[i];
            s +=  odd[i];
        }

        if(even.length() > odd.length()) s += even[even.length()-1]; //append the last char into string

        return s;
    }

    int numSpecialEquivGroups(vector<string>& words) {
        set<string> set;

        for(auto& word: words) set.insert(process(word)); //insert processed words into set

        return set.size();                                //return the amount of groups
    }
};
