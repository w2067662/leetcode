//https://leetcode.com/problems/most-frequent-even-element/description/

class Solution {
public:
    int mostFrequentEven(vector<int>& nums) {
        unordered_map<int, int> umap;
        map<int, vector<int>> map;
        int maxAppearTimes = -1;

        for(const auto num: nums) umap[num]++;                    //store the numbers appear times into umap

        for(const auto it: umap){
            if(it.first%2 == 0){                                  //IF  current nubmer is even
                map[it.second].push_back(it.first);               //    -> store the number and its appear times from umap to map
                maxAppearTimes = max(it.second, maxAppearTimes);
            } 
        }

        for(auto& it: map){
            sort(it.second.begin(), it.second.end());             //sort the vector with same appear times numbers inascending order
        }

        return maxAppearTimes == -1 ? -1 : map[maxAppearTimes][0];//IF  even number exist -> return the smallest even number with max appear times
    }
};
