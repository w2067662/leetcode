//https://leetcode.com/problems/cells-in-a-range-on-an-excel-sheet/description/

class Solution {
public:
    vector<string> cellsInRange(string s) {
        vector<string> ans;
        string temp = "--";

        pair<char, char> col = {s[0], s[3]};           //get the range of column
        pair<char, char> row = {s[1], s[4]};           //get the range of row

        for(char i=col.first; i<=col.second; i++){
            temp[0] = i;
            for(char j=row.first; j<=row.second; j++){
                temp[1] = j;
                ans.push_back(temp);                   //push all the slots within the range into anser
            }
        }

        return ans;
    }
};
