//https://leetcode.com/problems/sort-array-by-parity/description/

class Solution {
public:
    vector<int> sortArrayByParity(vector<int>& nums) {
        vector<int> even;
        vector<int> odd;

        for(const auto num: nums){
            if(num%2 == 0) even.push_back(num); //store even number into vector
            else odd.push_back(num);            //store  odd number into vector
        }

        for(const auto num: odd){
            even.push_back(num);                //merge 2 vectors
        }

        return even;
    }
};
