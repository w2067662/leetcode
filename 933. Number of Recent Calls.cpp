//https://leetcode.com/problems/number-of-recent-calls/description/

class RecentCounter {
private:
    typedef pair<int, int> Range;
    vector<int> requests;
    Range range;
public:
    RecentCounter() {
        requests.clear(); //initial requests
    }
    
    int ping(int t) {
        int count=0;

        requests.push_back(t);  //push t into requests
        range = {t-3000, t};    //set the range = [t-3000, t]

        for(int i=0;i<requests.size();i++){
            if(requests[i]>=range.first && requests[i]<=range.second) count++; //count the requests that are in the range
        }

        return count;
    }
};

/**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter* obj = new RecentCounter();
 * int param_1 = obj->ping(t);
 */
