//https://leetcode.com/problems/number-of-students-unable-to-eat-lunch/description/

class Solution {
public:
    int countStudents(vector<int>& students, vector<int>& sandwiches) {
        int one = 0, zero = 0;

        for(auto student: students){            // Count 0's and 1's students
            switch(student){
                case 0: zero++; break;
                case 1:  one++; break;
            }
        }

        for(int i=0; i<sandwiches.size(); i++){ // IF no student matches current sandwich -> end the loop
            if(sandwiches[i] == 0){
                if(zero > 0)zero--;
                else break;
            }
            if(sandwiches[i] == 1){
                if(one > 0)one--;
                else break;
            }
        }

        return one + zero;                      // Return the amount of lefted students without sandwich
    }
};
