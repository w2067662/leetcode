//https://leetcode.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor/description/

class Solution {
public:

    //    0 -----+---------+--------+---- arr.size-1
    //           i         j        k
    //           |--------||--------|
    //                a         b

    // Tips:
           
    //    0 --------+---------+---------- arr.size-1    (1. Define I and J)
    //           <- i ->   <- j ->

    //    0 ------+---------+------------ arr.size-1    (2. Find possible K)
    //            i         j
    //                      |------+-----|
    //                          <- k ->

    int countTriplets(vector<int>& arr){
        int count = 0;
        
        for(int i=0; i<arr.size()-1; i++){          // Traverse i and j for all possible ranges
            for(int j=i+1; j<arr.size(); j++) {
                int a = 0, b = 0;

                for(int k=i; k<j; k++){
                    a ^= arr[k];                    //      Calculate A
                }
                
                
                for(int k=j; k<arr.size(); k++) {   //      For possible ranges or k
                    b ^= arr[k];                    //          Calculate B
                    
                    if (a == b) {                   //          IF A = B
                        count++;                    //             -> count + 1
                    }
                }
            }
        }
        
        return count;
    }
};