//https://leetcode.com/problems/last-stone-weight/description/

class Solution {
public:
    //ex: stones = [2,7,4,1,8,1]    ('-' represent INT_MAX -> wil be poped out)
    
    // sort stones -> stones = [1,1,2,4,7,8]

    // crash(7,8)  -> stones = [1,1,2,4,-,1] -> stones = [1,1,1,2,4]
    // crash(2,4)  -> stones = [1,1,1,-,2]   -> stones = [1,1,1,2]
    // crash(1,2)  -> stones = [1,1,-,1]     -> stones = [1,1,1]
    // crash(1,1)  -> stones = [1,-,0]       -> stones = [0,1]
    // crash(0,1)  -> stones = [-,1]         -> stones = [1]

    int lastStoneWeight(vector<int>& stones) {
        sort(stones.begin(), stones.end()); // Sort the vector in ascending order

        while(stones.size() > 1){                                                      // WHILE  loop until vector.size == 1
            stones[stones.size()-1] = stones[stones.size()-1]-stones[stones.size()-2]; //    Set last number as the difference of two biggest number
            stones[stones.size()-2] = INT_MAX;                                         //    Set second last number as INT_MAX

            sort(stones.begin(), stones.end());                                        //    Sort the vector again

            stones.pop_back();                                                         //    Pop the last number
        }

        return stones[0];                                                              // Return the only number in vector
    }
};
