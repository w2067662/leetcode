//https://leetcode.com/problems/odd-string-difference/description/

class Solution {
public:
    //words         = ["adc","wzy","abc"]
    //word_process  = ["adc","adc","abc"]
    //map   = [ "adc": {0,1}, "abc":{2} ] 
    //                          ^
    //                        "abc"'s vector size is 1(contain the only different word)
    //answer =  "abc" (words[2])
    string word_process(string s){
        int x = s[0]-'a';
        for(int i=0;i<s.length();i++){
            s[i]-=x;
        }
        return s;
    }

    string oddString(vector<string>& words) {
        map<string, vector<int>> map;                //map = [[string]:[vector<int>(index)],...]
        int answer_index=-1;                         //initial answer index

        for(int i=0;i<words.size();i++){
            map[word_process(words[i])].push_back(i);//store processed words and correspond index into map
        }

        for(const auto& element: map){
            if(element.second.size()==1) answer_index = element.second[0]; //find the only different word
        }

        return words[answer_index];
    }
};
