//https://leetcode.com/problems/minimum-amount-of-time-to-collect-garbage/description/

class House { //define House
public:
    int M, P, G;
    House():M(0), P(0), G(0){}
};

class Solution {
public:
    int garbageCollection(vector<string>& garbage, vector<int>& travel) {
        vector<House*> houses;
        int indexM = 0, indexP = 0, indexG = 0;  //store right most index of M,P,G
        int ans = 0;

        for(int i=0; i<garbage.size(); i++){
            House* curr = new House;

            for(const auto type: garbage[i]){    //count for M,P,G types garbage of current house
                switch(type){
                    case 'M':
                        curr->M++;
                        break;
                    case 'P':
                        curr->P++;
                        break;
                    case 'G':
                        curr->G++;
                        break;
                }
            }

            houses.push_back(curr);

            indexM = curr->M == 0 ? indexM : i;
            indexP = curr->P == 0 ? indexP : i;
            indexG = curr->G == 0 ? indexG : i;
        }

        for(int i=0; i<houses.size(); i++){
            ans += houses[i]->M + houses[i]->P + houses[i]->G; //add up the minutes for collecting the garbages of current house
        }

        for(int i=0; i<indexM; i++) ans += travel[i];          //calculate the minutes to go from house i to house i+1
        for(int i=0; i<indexP; i++) ans += travel[i];
        for(int i=0; i<indexG; i++) ans += travel[i];

        return ans;
    }
};
