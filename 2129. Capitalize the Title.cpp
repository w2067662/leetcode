//https://leetcode.com/problems/capitalize-the-title/description/

class Solution {
public:
    string capitalizeTitle(string title) {
        vector<string> words;
        string temp = "";

        for(const auto ch: title){             //store the words from title into vector
            if(ch == ' '){
                words.push_back(temp);
                temp = "";
            }
            else temp += tolower(ch);
        }
        if(temp != "")words.push_back(temp);

        for(auto& word: words){
            if(word.length()>=3) word[0] = toupper(word[0]); //IF  length of the word >= 3, capitalize the word 
        }

        title = "";                            //clear the title

        for(int i=0;i<words.size();i++){       //append words to title
            title += words[i];
            if(i!=words.size()-1)title += " ";
        }

        return title;
    }
};
