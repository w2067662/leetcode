//https://leetcode.com/problems/find-the-duplicate-number/description/

class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        set<int> set;

        for(const auto num: nums){                      // For all numbers in vector
            if(set.find(num) != set.end()) return num;  //      IF   found duplicate number -> return number
            set.insert(num);                            //      ELSE                        -> insert into set
        }

        return -1;
    }
};
