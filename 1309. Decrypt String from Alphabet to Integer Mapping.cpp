//https://leetcode.com/problems/decrypt-string-from-alphabet-to-integer-mapping/description/

class Solution {
public:
    string toLetters (string s){
        string letters = "";
        if(s[s.length()-1]=='#'){               //if there is '#' in the end
            for(int i=0;i<s.length()-3;i++){
                letters += s[i]-'1'+'a';        //change all numbers to letters
            }
            letters += (s[s.length()-3]-'0') * 10 + (s[s.length()-2]-'1') +'a'; //change '10#'-'26#' to 'j'-'z'
        }else{
            for(int i=0;i<s.length();i++){      //no '#' in the end
                letters += s[i]-'1'+'a';        //change all numbers to letters directly
            }
        }
        return letters;
    }

    string freqAlphabets(string s) {
        string ans  = "";
        string temp = "";

        for(int i=0;i<s.length();i++){
            if(s[i]=='#'){                      //when encounter '#' process "n#" to letter
                ans += toLetters(temp + "#");
                temp = "";                      //reset temp
            }else{
                temp+=s[i];
            }
        }
        if(temp!="")ans += toLetters(temp);     //if no '#' in end and temp is not empty,
                                                //change the numbers in temp to letters
        return ans;
    }
};
