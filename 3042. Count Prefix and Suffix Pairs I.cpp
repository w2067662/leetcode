//https://leetcode.com/problems/count-prefix-and-suffix-pairs-i/description/

class Solution {
public:
    bool isPrefixAndSuffix(string& a, string& b){   // Check A is both prefix and suffix of B
        if(a.length() > b.length()) return false;                // IF A.len > B.len -> FALSE

        return a == b.substr(0, a.length()) &&                   // Check A is prefix of B AND
               a == b.substr(b.length()-a.length(), a.length()); //       A is suffix of B
    }

    int countPrefixSuffixPairs(vector<string>& words) {
        int count = 0;

        for(int i=0; i<words.size()-1; i++){
            for(int j=i+1; j<words.size(); j++){
                if(isPrefixAndSuffix(words[i], words[j])){       // Count pairs (i,j) = {words[i], words[j]}, satisfy the condition:
                                                                 //     words[i] is both prefix and suffix of words[j]
                    count++;                                     //         -> count+1
                }
            }
        }

        return count;
    }
};
