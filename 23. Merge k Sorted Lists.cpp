//https://leetcode.com/problems/merge-k-sorted-lists/description/

//lists = [[1(A)->4(A)->5],
//         [1(B)->3->4(B)],
//         [2->6]
//        ]
//
//map = [ [1: {1(A), 1(B)}],
//        [2: {2}],
//        [3: {3}],
//        [4: {4(A), 4(B)}],
//        [5: {5}],
//        [6: {6}]
//      ]
//
//vec = [1(A),1(B),2,3,4(A),4(B),5,6]
//
//head->1(A)->1(B)->2->3->4(A)->4(B)->5->6->NULL
//      ^
//     new Head

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        map<int, vector<ListNode*>> map;    //[value: {ListNode*, ListNode*, ...}]
        vector<ListNode*> vec;
        ListNode* head = new ListNode;
        ListNode* curr = head;

        //store ListNodes from lists vector to map's structure
        for(int i=0;i<lists.size();i++){
            ListNode* temp = lists[i];
            while(temp){
                if(map.find(temp->val) == map.end()){
                    map.insert({temp->val, {temp} });
                }
                else{
                    map[temp->val].push_back(temp);
                }
                temp = temp->next;
            }
        }

        //store ListNodes from map's structure to vector
        for(const auto& it:map){
            for(int i=0;i<it.second.size();i++){
                vec.push_back(it.second[i]);
                cout << it.second[i]->val <<endl;
            }
        }

        //link all the ListNodes by vector index
        for(int i=0;i<vec.size();i++){
            curr->next = vec[i];
            curr = curr->next;
        }

        return head->next; //return head->next as new head
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
