//https://leetcode.com/problems/check-if-all-1s-are-at-least-length-k-places-away/description/

class Solution {
public:
    bool kLengthApart(vector<int>& nums, int k) {
        vector<int> vec;

        for(int i=0;i<nums.size();i++){
            if(nums[i] == 1) vec.push_back(i);      //store all the index of 1
        }

        for(int i=1;i<vec.size();i++){
            if(vec[i]-vec[i-1]-1 < k) return false; //check the amount of 0 between 1's are larger than K or not
        }

        return true;
    }
};
