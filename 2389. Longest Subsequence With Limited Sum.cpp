//https://leetcode.com/problems/longest-subsequence-with-limited-sum/description/

class Solution {
public:
    vector<int> answerQueries(vector<int>& nums, vector<int>& queries) {
        sort(nums.begin(), nums.end());        //sort the number array in ascending order

        int sum = 0, tempSum, tempIndex;

        for(const auto num: nums)sum+=num;     //calculate the sum of number array

        for(auto& query : queries){
            tempSum = sum, tempIndex = 0;
            for(int i=nums.size()-1;i>=0;i--){ //
                if(tempSum <= query){          //IF   find the max amount of numbers that satisfy sum <= query
                    tempIndex = i+1;           //     -> set the answer of current query as the size of current subarray
                    break;
                }
                tempSum -= nums[i];            //ELSE -> deduct the largest number from current subarray
            }
            query = tempIndex;
        }

        return queries;
    }
};
