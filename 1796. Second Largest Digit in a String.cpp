//https://leetcode.com/problems/second-largest-digit-in-a-string/description/

class Solution {
public:
    int secondHighest(string s) {
        vector<int> vec;
        set<int> set;

        for(const auto ch: s){
            if(isdigit(ch))set.insert(ch-'0');           //insert digit into set
        }

        for(const auto digit: set) vec.push_back(digit); //push digit form set into vector

        sort(vec.begin(), vec.end());                    //sort the vector in ascending

        return vec.size() >= 2 ? vec[vec.size()-2] : -1; //if there exist no second largest digit -> return -1
    }
};
