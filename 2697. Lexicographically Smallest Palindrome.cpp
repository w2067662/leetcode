//https://leetcode.com/problems/lexicographically-smallest-palindrome/description/

class Solution {
public:
    string makeSmallestPalindrome(string s) {
        for(int i=0; i<s.length()/2; i++){                                           //traverse pair[i] = {letter1, letter2}
            s[i]              = s[i] < s[s.length()-i-1] ? s[i] : s[s.length()-i-1]; //change the letter to smaller value of letter1, letter2
            s[s.length()-i-1] = s[i] < s[s.length()-i-1] ? s[i] : s[s.length()-i-1]; //change the letter to smaller value of letter1, letter2
        }

        return s;
    }
};
