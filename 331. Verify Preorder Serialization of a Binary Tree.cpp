//https://leetcode.com/problems/verify-preorder-serialization-of-a-binary-tree/description/

class Solution {
public:
    //ex: preorder = "9,3,4,#,#,1,#,#,#,2,#,6,#,#"
    //        node = [T,T,T,F,F,T,F,F,T,F,T,F,F] (T: node, F: null)
    //                ^                             stack = ['n']
    //                  ^                           stack = ['n','n']
    //                    ^                         stack = ['n','n','n']
    //                      ^                       stack = ['n','n','n','#']
    //                        ^                     stack = ['n','n',('n','#')]              found ('n','#') pattern
    //                                              stack = ['n','n','#']
    //        node = [T,T,T,F,F,T,F,F,T,F,T,F,F]
    //                          ^                   stack = ['n','n','#','n']
    //                            ^                 stack = ['n','n','#','n','#']
    //                              ^               stack = ['n','n','#',('n','#')]          found ('n','#') pattern
    //                                              stack = ['n',('n','#')]                  found ('n','#') pattern
    //                                              stack = ['n','#']
    //                                ^             stack = ['n','#','n']
    //                                  ^           stack = ['n','#','n','#']
    //        node = [T,T,T,F,F,T,F,F,T,F,T,F,F]
    //                                    ^         stack = ['n','#','n','#','n']
    //                                      ^       stack = ['n','#','n','#','n','#']
    //                                        ^     stack = ['n','#','n','#',('n','#')]      found ('n','#') pattern
    //                                              stack = ['n','#',('n','#')]              found ('n','#') pattern
    //                                              stack = [('n','#')]                      found ('n','#') pattern
    //                                              stack = ['#']                            found ('n','#') pattern
    //                                                        ^
    //                                                        stack.size = 1 && stack.top = '#' -> TRUE                               

    vector<bool> preprocess(const string& s){ // Preprocess the preorder string to vector<bool>
        vector<bool> res;
        bool node = false;

        for(const auto ch: s){
            switch(ch){
                case '#': 
                    node = false; 
                    break;
                case ',': 
                    res.push_back(node);
                    break;
                default:
                    node = true;
            }
        }
        res.push_back(node);

        return res;
    }

    bool isValidSerialization(string preorder) {
        stack<char> stack;
        vector<bool> nodes = preprocess(preorder);

        for(const auto node: nodes){
            switch(node){
                case true:                                        // IF current is node
                    stack.push('n');                              //    -> push node into stack
                    break;

                case false:                                       // IF current is NULL
                    while(!stack.empty() && stack.top() == '#'){  //    IF encounter 2 NULL 
                                                                  //       --------------------------------
                                                                  //       (Find ('n','#') pattern and pop)
                                                                  //       --------------------------------
                        stack.pop();                              //         Pop the NULL in left subtree

                        if(stack.empty() || stack.top() == '#'){  //         IF encounter 3 NULL
                            return false;                         //            -> FALSE (not valid)
                        }

                        stack.pop();                              //         Pop the left and right subtree traversed node
                                                                  //       --------------------------------
                    }
                    stack.push('#');                              //    Push NULL into stack
                    break;
            }
        }

        return stack.size() == 1 && stack.top() == '#';           // Check only '#' lefted in stack (valid tree) or not
    }
};
