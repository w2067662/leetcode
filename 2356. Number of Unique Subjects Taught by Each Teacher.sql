#https://leetcode.com/problems/number-of-unique-subjects-taught-by-each-teacher/description/

# Write your MySQL query statement below
SELECT teacher_id, COUNT(distinct subject_id) AS CNT
FROM Teacher
GROUP BY teacher_id
