//https://leetcode.com/problems/divide-a-string-into-groups-of-size-k/description/

class Solution {
public:
    vector<string> divideString(string s, int k, char fill) {
        vector<string> ans;
        int filled = (k - s.length()%k) == k ? 0 : k - s.length()%k; //calculate the amount of char needed to be filled 

        for(int i=0;i<filled;i++) s += fill;                         //filled the chars from behind

        for(int i=0;i+k-1<s.length();i+=k){
            ans.push_back(s.substr(i,k));                            //splite into parts and push into answer
        }

        return ans;
    }
};
