//https://leetcode.com/problems/check-array-formation-through-concatenation/description/

class Solution {
public:
    bool canFormArray(vector<int>& arr, vector<vector<int>>& pieces) {
        map<int, int> map;              //map = {[number]: [index]}

        for(int i=0;i<arr.size();i++){  //store all the numbers from arr into map
            map[arr[i]] = i;
        }

        for(const auto piece: pieces){
            for(int i=0; i<piece.size(); i++){
                if(map.find(piece[i]) == map.end()) return false;   //check the pieces exist in map or not
            }

            for(int i=1; i<piece.size(); i++){
                if(map[piece[i]]-map[piece[i-1]] != 1) return false;//check the pieves are valid to form array or not
            }
        }

        return true;
    }
};
