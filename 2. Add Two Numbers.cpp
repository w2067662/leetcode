//https://leetcode.com/problems/add-two-numbers/description/

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        
        //check if list is NULL
        if(!l1)return l2;
        else if(!l2) return l1;
        
        //create new_head and new_end for new list
        ListNode* new_head= NULL;
        ListNode* new_end = NULL;
        
        //temp value for 18%10="8"
        //carry for 18/10="1"
        //ex:[9]+[9]=[8,1]
        int carry=0;
        int temp_val=0;
        int len=0;
        
        //do the loop, if two list has value(not NULL) in same index, add it
        //the carry should be updated in each cycle
        //ex:["1,1,1"]+["1,1,1",1,1,1]=["2,2,2"]
        while(l1&&l2){  
            temp_val=l1->val + l2->val + carry;
            if(temp_val>=10){
                temp_val%=10;
                carry=1;
            }
            else carry=0;
            
            ListNode *temp = new ListNode(temp_val);
            if(new_head==NULL){
                new_head = temp; 
                new_end = temp;
            }//set new_head and new_end
            len++;
            
            if(len==1&&!l1->next&&!l2->next){
                l1=l1->next;
                l2=l2->next;
                continue;
            }
            else new_end->next=temp;
            
            l1=l1->next;
            l2=l2->next;
            new_end=new_end->next;
        }
        
        //assign lefted value from longer list to new list
        //ex:[1,1,1]+[1,1,1,1,1,1]=[2,2,2,"1,1,1"]
        if(l1){
            while(l1!=NULL){
                temp_val=l1->val + carry;
                if(temp_val>=10){
                    temp_val%=10;
                    carry=1;
                }
                else carry=0;
                
                ListNode *temp = new ListNode(temp_val);
                new_end->next=temp; 
                l1=l1->next;
                new_end=new_end->next;
            }
        }
        else if(l2){
            while(l2!=NULL){
                temp_val=l2->val + carry;
                if(temp_val>=10){
                    temp_val%=10;
                    carry=1;
                }
                else carry=0;
                
                ListNode *temp = new ListNode(temp_val);
                new_end->next=temp;
                l2=l2->next;
                new_end=new_end->next;
            }
        }
        
        
        //make end node for "1"
        //ex:[9,9]+[9,9]=[8,9,"1"] 
        if(carry==1){
            ListNode *temp = new ListNode(carry);
            new_end->next=temp;
            new_end=new_end->next;
            carry=0;
        }
       
        return new_head;
        
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
