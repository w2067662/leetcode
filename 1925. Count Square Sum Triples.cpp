//https://leetcode.com/problems/count-square-sum-triples/description/

class Solution {
public:
    int countTriples(int n) {
        int ans = 0;
        for(int a=1; a<=n-1; a++){
            for(int b=1; b<=n-1; b++){
                for(int c=1; c<=n; c++){
                    if(pow(a, 2) + pow(b, 2) == pow(c, 2)) ans++; //if a^2 + b^2 = c^2, answer+1
                }
            }
        }

        return ans;
    }
};
