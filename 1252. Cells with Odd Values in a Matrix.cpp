//https://leetcode.com/problems/cells-with-odd-values-in-a-matrix/description/

class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>>& indices) {
        vector<vector<int>> matrix (m, vector<int>(n, 0));
        int count=0;
        for(int i=0;i<indices.size();i++){      //traverse the element in indices
            for(int col=0;col<m;col++){         //whole line that vertical pass through element +1
                matrix[col][indices[i][1]]++;
            }
            for(int row=0;row<n;row++){
                matrix[indices[i][0]][row]++;   //whole line that horizontal pass through element +1
            }
        }
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){//count for odd numbers in matrix
                if(matrix[i][j]%2==1)count++;
            }
        }
        return count;
    }
};
