//https://leetcode.com/problems/reorder-list/description/

class Solution {
public:
    ListNode* middle(ListNode* head){           // Get the middle node of current list
        ListNode *fast = head, *slow = head;

        while(fast->next && fast->next->next){  // While fast has next and next next node
            fast = fast->next->next;            //      Fast goto next next
            slow = slow->next;                  //      Slow goto next
        }

        return slow;
    }

    ListNode* reverse(ListNode* head){
        if(!head) return NULL;

        ListNode *node1 = head, *node2 = head;  // Set node1, node2 as head
        ListNode *temp = NULL;
    
        while(node2->next){                     // While node2 has next
            temp = node2->next;                 //      Set temp as node2's next
            node2->next = node2->next->next;    //      Set node2's next as node2's next next
            temp->next = node1;                 //      Set temp's next as node1
            node1 = temp;                       //      Set node1 as temp
        }

        head->next = NULL;                      // Reversed list tail points to NULL
        
        return node1;
    }

    void merge(ListNode* a, ListNode* b){       // Merge list A with list B (A.Length >= B.Length)

        while(a && b){                          // While A AND B is not NULL
            ListNode* aNext = a->next;          //      Mark A's next node as aNext
            ListNode* bNext = b->next;          //      Mark B's next node as bNext

            a->next = b;                        // A points to B
            b->next = aNext;                    // B points to A's next

            a = aNext;                          // A goto next
            b = bNext;                          // B goto next
        }
    }

    void reorderList(ListNode* head) {
        ListNode *list1 = head, *list2 = middle(head);  // Split list into list1 and list2

        list2 = reverse(list2);                         // Reverse list2

        merge(list1, list2);                            // Merge list1 and list2
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */