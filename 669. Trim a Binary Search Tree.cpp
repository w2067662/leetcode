//https://leetcode.com/problems/trim-a-binary-search-tree/description/

class Solution {
public:
    TreeNode* trimBST(TreeNode* root, int low, int high) {
        if(!root) return NULL;
        
        if(root->val < low){                            // If current node's value is smaller than the range (low, high)       
            return trimBST(root->right, low, high);     //    -> Trim the right subtree 
        }

        if(root->val > high){                           // If current node's value is larger than the range (low, high)
            return trimBST(root->left, low, high);      //    -> Trim the left subtree
        }
                                                        // If current node's value is in the range (low, high)
        return new TreeNode(                            //    -> return trimmed subtree      
            root->val,
            trimBST(root->left , low, high),
            trimBST(root->right, low, high)
        );
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
