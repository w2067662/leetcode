//https://leetcode.com/problems/find-the-losers-of-the-circular-game/description/

class Solution {
public:
    vector<int> circularGameLosers(int n, int k) {
        vector<int> score (n, 0);
        vector<int> ans;
        int current = 0;
        int time = 1;

        score[0]++;

        while(true){                             //playing the game
            current = (current + k*time)%n;      //pass the ball to next player
            score[current]++;                    //add up score for the current player who got the ball
            time++;                              //passing times increase in each turn
            if(score[current] >= 2)break;        //if someone's score >=2, end the game
        }

        for(int i=0; i<score.size(); i++){
            if(score[i] == 0) ans.push_back(i+1);//push the losers into answer
        }

        sort(ans.begin(), ans.end());            //sort the losers inascending order

        return ans;
    }
};
