//https://leetcode.com/problems/sum-of-all-odd-length-subarrays/description/

class Solution {
public:
    int sumOddLengthSubarrays(vector<int>& arr) {
        int sum=0;
        for(int odd=1;odd<=arr.size();odd+=2){      //odd = 1 -> 3 -> 5 ...
            for(int i=0;i<arr.size()-odd+1;i++){    //from i to len(arr)-odd+1
                for(int j=i;j<i+odd;j++){           //add [odd] numbers
                    sum += arr[j];
                }
            }
        }
        return sum;
    }
};
