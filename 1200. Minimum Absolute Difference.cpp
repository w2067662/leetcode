//https://leetcode.com/problems/minimum-absolute-difference/description/

class Solution {
public:
    vector<vector<int>> minimumAbsDifference(vector<int>& arr) {
        sort(arr.begin(), arr.end());
        vector<vector<int>>ans;
        int min = INT_MAX;

        for(int i=0;i<arr.size()-1;i++){
            min = arr[i+1]-arr[i]<min ? arr[i+1]-arr[i] : min;            //find min absolute difference
        }

        for(int i=0;i<arr.size()-1;i++){
            if(arr[i+1]-arr[i] == min) ans.push_back({arr[i], arr[i+1]}); //find all pairs with min absolute difference
        }

        return ans;
    }
};
