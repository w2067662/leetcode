#https://leetcode.com/problems/user-activity-for-the-past-30-days-i/description/

# Write your MySQL query statement below
SELECT activity_date AS day, 
COUNT(distinct user_id) AS active_users
FROM Activity
GROUP BY day
HAVING "2019-06-28"<=activity_date AND activity_date<="2019-07-27"
