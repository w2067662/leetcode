//https://leetcode.com/problems/rank-transform-of-an-array/description/

class Solution {
public:
    vector<int> arrayRankTransform(vector<int>& arr) {
        vector<int>ans (arr.size(), 0);   //create a vector<int> as same size as score for storing answers
        map<int, vector<int>>map;
        
        for(int i=0;i<arr.size();i++){
            if(map.find(arr[i])==map.end()){
                vector<int> vec;
                vec.push_back(i);
                map.insert({arr[i], vec});
            }
            else{
                 map[arr[i]].push_back(i);  //map = [{score, [index1,index2,...]}...]
            }
        } 

        int rank = 1;    //set rank at the first
        for(auto element: map){  //start from the lowest number to highest number
            for(int i=0;i<element.second.size();i++){ //ex1: arr=[100,100,100] -> ans=[1,1,1]
                ans[element.second[i]]= rank;         //ex2: arr=[50,40,60]    -> ans=[3,1,2] 
            }
            rank++; //to next higher score
        }

        return ans;
    }
};
