//https://leetcode.com/problems/number-of-wonderful-substrings/description/

class Solution {
public:
    // Tips: 
    //      A bit in the mask is 1 if the corresponding character's frequency is odd, and 0 if even.

    // ex1: word = "aabb"
    //                                 ; mask  =  0000                 
    //                                                                 ; map = [ 0000  :1 ]    ; ans = 0
    //              ^      'a' -> 0001 ; mask  =  0000 ^ 0001  =  0001
    //                                                                 ; map = [ 0000  :1, 
    //                                                                           0001  :1 ]    ; ans = 1(+1) ("a")
    //               ^     'a' -> 0001 ; mask  =  0001 ^ 0001  =  0000 
    //                                                                 ; map = [ 0000  :2,
    //                                                                           0001  :2 ]    ; ans = 3(+2) ("aa", "_a")
    //                ^    'b' -> 0010 ; mask  =  0000 ^ 0010  =  0010
    //                                                                 ; map = [ 0000  :2,
    //                                                                           0001  :2,
    //                                                                           0010  :2 ]    ; ans = 5(+2) ("aab", "__b")
    //                 ^   'b' -> 0010 ; mask  =  0010 ^ 0010  =  0000
    //                                                                 ; map = [ 0000  :4,
    //                                                                           0001  :3,
    //                                                                           0010  :3 ]    ; ans = 9(+4) ("aabb", "_abb", "__bb", "___b")

    long long wonderfulSubstrings(string word) {
        map<long long, long long> map = {{0, 1}};    // Set default map with mask (0000, empty string) = 1
        long long ans = 0, mask = 0;

        for(int i=0; i<word.size(); i++) {           // FOR  chars within word 
            mask = mask ^ (1 << (word[i]-'a'));      //      Calculate current mask
            ans += map[mask];                        //      Add up the frequency of current mask
           
            for(char ch='a'; ch<='j'; ch++){         //      FOR  char from 'a' to 'j' (consist of first ten lowercase English letters)
                ans += map[mask ^ (1 << (ch-'a'))];  //           -> Add up the frequency of current mask with current char appears ODD number of times
            }

            map[mask]++;                             //      Frequency of current mask + 1
        }
        
        return ans;
    }
};
