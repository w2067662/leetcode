//https://leetcode.com/problems/insert-delete-getrandom-o1/description/

class RandomizedSet {
private:
    unordered_set<int> set;
public:
    RandomizedSet() {
        this->set.clear();  // Clear the set
    }
    
    bool insert(int val) {
        if(this->set.find(val) != this->set.end()) return false; // IF current val is already in the set -> False

        set.insert(val);                                         // Insert the value into set
        return true;
    }
    
    bool remove(int val) {
        if(this->set.find(val) == this->set.end()) return false; // IF current val is not in the set -> False

        set.erase(val);                                          // Erase the value from the set
        return true;
    }
    
    int getRandom() {
        return *next(set.begin(), rand() % set.size()); // next([start pointer], [advance index]) -> next() return the iterator of a container                                               
                                                        // rand() % set.size()                    -> return a value from 0 to set.size()
    }
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */
