//https://leetcode.com/problems/pascals-triangle/description/

class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> ans;

        for(int i=0;i<numRows;i++){                             //i start from 0 to numRows-1
            vector<int> temp;
            for(int j=0;j<i+1;j++){                             //j start from 0 to numRows-1
                if(j == 0 || j == i)temp.push_back(1);          //if current slot is the first or last element in current row, store 1
                else temp.push_back(ans[i-1][j-1]+ans[i-1][j]); //if current slot is not first or last element in current row, store answer[current_row-1][current_slot-1] + answer[current_row-1][current_slot]
            }
            ans.push_back(temp);
        }

        return ans;
    }
};
