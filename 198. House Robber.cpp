//https://leetcode.com/problems/house-robber/description/

class Solution {
public:
    //ex1:  nums = [5]                                  answer = 5

    //ex2:  nums = [8,7]                                answer = max(8,7) = 8

    //ex3:  nums = [5,10,9]                             answer = max(10, 5+9) = 14

    //ex4:  nums = [2,7, 9, 3, 1, 4, 4, 5, 9, 8, 4]
    //              *    ^                              9 + 2 = 11
    //             [2,7,11, 3, 1, 4, 4, 5, 9, 8, 4]
    //              * *     ^                           3 + max(2,7)   = 10    
    //             [2,7,11,10, 1, 4, 4, 5, 9, 8, 4]
    //                *  *     ^                        1 + max(7,11)  = 12 
    //             [2,7,11,10,12, 4, 4, 5, 9, 8, 4]
    //                   *  *     ^                     4 + max(11,10) = 15 
    //             [2,7,11,10,12,15, 4, 5, 9, 8, 4]
    //                      *  *     ^                  4 + max(10,12) = 16 
    //             [2,7,11,10,12,15,16, 5, 9, 8, 4]
    //                         *  *     ^               5 + max(12,15) = 20
    //             [2,7,11,10,12,15,16,20, 9, 8, 4]
    //                            *  *     ^            9 + max(15,16) = 25
    //             [2,7,11,10,12,15,16,20,25, 8, 4]
    //                               *  *     ^         8 + max(16,20) = 28
    //             [2,7,11,10,12,15,16,20,25,28, 4]
    //                                  *  *     ^      4 + max(20,25) = 29
    //
    //             [2,7,11,10,12,15,16,20,25,28,29]
    //                                        *  *      answer = max(28,29) = 29
    //
    int rob(vector<int>& nums) {
        switch(nums.size()){
            case 1: return nums[0];                                     // (Example 1) nums.size = 1
            case 2: return max(nums[0], nums[1]);                       // (Example 2) nums.size = 2
            case 3: return max(nums[1], nums[0]+nums[2]);               // (Example 3) nums.size = 3
        }
                                                                        // (Example 4) nums.size > 3

        for(int i=2; i<nums.size();i++){                                // For i to nums.size
            nums[i] += (i == 2)? nums[i-2] : max(nums[i-3], nums[i-2]); //      IF i  = 2  ->  nums[i] += nums[i-2]
                                                                        //      IF i != 2  ->  nums[i] += max(nums[i-3], nums[i-2])
        }

        return max(nums[nums.size()-2], nums[nums.size()-1]);           // Return the max(second last value, last value) as answer
    }
};
