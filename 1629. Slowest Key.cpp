//https://leetcode.com/problems/slowest-key/description/

class Solution {
public:
    char slowestKey(vector<int>& releaseTimes, string keysPressed) {
        unordered_map<char, int> umap;                          //umap = {[char]: [duration]}
        map<int, vector<char>> map;                             //map = {[duration]: vector{[char1], [char2], ..}}
        int maxDuration = 0;
        int pressStart = 0;

        for(int i=0, duration;i<releaseTimes.size();i++){       //store the max duration of each key
            duration = releaseTimes[i]-pressStart;
            umap[keysPressed[i]] = max(duration, umap[keysPressed[i]]);
            pressStart = releaseTimes[i];
        }

        for(const auto it:umap){                                //store from umap to map
            map[it.second].push_back(it.first);
            maxDuration = max(it.second, maxDuration);
        }

        sort(map[maxDuration].begin(), map[maxDuration].end()); //sort the keys with same duration lexicographically

        return map[maxDuration][map[maxDuration].size()-1];     //return the key with largest duration
    }
};
