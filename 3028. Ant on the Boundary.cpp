//https://leetcode.com/problems/ant-on-the-boundary/description/

class Solution {
public:
    int returnToBoundaryCount(vector<int>& nums) {
        int count = 0, x = 0;

        for(const auto num: nums){
            x += num;               // Calculate current position

            if(x == 0) count++;     // IF current position is on the boundary (0)
                                    //    -> count+1
        }

        return count;
    }
};
