//https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/description/

class Solution {
public:
    int minimumSum(int num) {
        string s = to_string(num);       //convert integar into string
        vector<int> vec;
        int countZero = 0;

        for(int i=0;i<s.length();i++){
            vec.push_back(s[i]-'0');     //push digit into vec
            if(s[i] == '0') countZero++; //count for 0
        }

        sort(vec.begin(), vec.end());    //sort the digits in vector in ascending order

        switch(countZero){
            case 0: return vec[0]*10 + vec[1]*10 + vec[2] + vec[3]; //ex: num = 1423 -> vec = {1,2,3,4} ; answer = 10 + 20 + 3 + 4 =37
            case 1: return vec[1]*10 + vec[2] + vec[3];             //ex: num = 1320 -> vec = {0,1,2,3} ; answer = 10 + 2 + 3 = 15
            case 2: return vec[2] + vec[3];                         //ex: num = 8500 -> vec = {0,0,5,8} ; answer = 5 + 8 = 13
            case 3: return vec[3];                                  //ex: num = 3000 -> vec = {0,0,0,3} ; answer = 3
        }

        return -1;
    }
};
