//https://leetcode.com/problems/maximum-enemy-forts-that-can-be-captured/description/

class Solution {
public:
    int maxForts(int index, vector<int>& forts){    //find max forts that can be captured
        int left  = 0, right = 0;
        bool leftFound = false, rightFound = false; 
       
        for(int i=index-1; i>=0;i--){               //finding forts from left
            if(forts[i] == 0) left++;
            else if (forts[i] == 1)break;
            else if (forts[i] == -1){               //only when -1 (no forts) is encountered, the 0's (enemy) are count 
                leftFound = true;
                break;
            }
        }

        for(int i=index+1; i<forts.size();i++){     //finding forts from right
            if(forts[i] == 0) right++;
            else if (forts[i] == 1)break;
            else if (forts[i] == -1){               //only when -1 (no forts) is encountered, the 0's (enemy) are count 
                rightFound = true;
                break;
            }
        }

        left  = leftFound  ? left  : 0;
        right = rightFound ? right : 0;

        return max(left, right);
    }

    int captureForts(vector<int>& forts) {
        int ans = 0;

        for(int i=0, temp; i<forts.size(); i++){
            if(forts[i] == 1){                   //if current is 1
                temp = maxForts(i, forts);       //find the max forts that can be captured 
                ans = max(temp, ans);            //store the max forts
            }
        }

        return ans;
    }
};
