//https://leetcode.com/problems/count-of-matches-in-tournament/description/

class Solution {
public:
    int numberOfMatches(int n) {
        int ans = 0;
        int matches, lefted;

        while(n>1){
            matches = n/2;          //matches = teams / 2
            lefted = n%2;           //lefted = teams % 2
            ans += matches;         //add up matches in the turn to answer
            n = matches + lefted;   //advance teams = matches + lefted
        }

        return ans;
    }
};
