//https://leetcode.com/problems/power-of-two/description/

class Solution {
public:
    //1 - 1 = 0 = 0b0        =>  1 & 0 = 0b1    & 0b0    = 0
    //2 - 1 = 1 = 0b1        =>  2 & 1 = 0b10   & 0b1    = 0
    //4 - 1 = 3 = 0b11       =>  4 & 3 = 0b100  & 0b11   = 0
    //8 - 1 = 7 = 0b111      =>  8 & 7 = 0b1000 & 0b111  = 0
    bool isPowerOfTwo(int n) {
        return n>0 && !(n & (n-1));
    }
};
