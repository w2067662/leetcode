//https://leetcode.com/problems/maximum-number-of-words-you-can-type/description/

class Solution {
public:
    int canBeTypedWords(string text, string brokenLetters) {
        vector<string> words;
        set<char> set;
        string temp = "";
        bool canBeTyped;
        int ans = 0;

        for(int i=0; i<brokenLetters.length(); i++){    //store the broken letters into map
            set.insert(brokenLetters[i]);
        }

        for(int i=0; i<text.length(); i++){             //store the words into vector
            if(text[i] == ' '){
                words.push_back(temp);
                temp = "";
            }else {
                temp += text[i];
            }
        }
        if(temp != "") words.push_back(temp);

        for(int i=0;i<words.size();i++){                //check current word can be typed or not
            canBeTyped = true;
            for(int j=0;j<words[i].length();j++){
                if(set.find(words[i][j]) != set.end()){
                    canBeTyped = false;
                    break;
                }
            }
            if(canBeTyped)ans++;
        }

        return ans;
    }
};
