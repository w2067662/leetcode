//https://leetcode.com/problems/maximum-element-after-decreasing-and-rearranging/description/

class Solution {
public:
    int maximumElementAfterDecrementingAndRearranging(vector<int>& arr) {
        int prev = 0, maxNum = 0;

        sort(arr.begin(), arr.end());                  //sort the array in ascending order

        for(int i=0;i<arr.size();i++){
            if(abs(arr[i]-prev) > 1 ) arr[i] = prev+1; //IF  abs(arr[i] - arr[i - 1]) <= 1 -> arr[i] decrease to previous+1
            maxNum = max(maxNum, arr[i]);              //store the max number
            prev = arr[i];                             //set current number as previous number
        }

        return maxNum;
    }
};
