//https://leetcode.com/problems/categorize-box-according-to-criteria/description/

class Solution {
public:
    string categorizeBox(int length, int width, int height, int mass) {
        bool isBulky = false;
        bool isHeavy = false;

        if(length>=10000 || width>=10000 || height>=1000 || length*width*height >= pow(10, 9)) isBulky = true; //check Builky
        if(mass >= 100) isHeavy = true;                                                                        //check Heavy

        if(isBulky && isHeavy) return "Both";                                                                  //return correspond answer
        else if (!isBulky && !isHeavy) return "Neither";
        else if (isBulky && !isHeavy) return "Bulky";
        return "Heavy";
    }
};
