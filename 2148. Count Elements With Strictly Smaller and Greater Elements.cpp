//https://leetcode.com/problems/count-elements-with-strictly-smaller-and-greater-elements/description/

class Solution {
public:
    int countElements(vector<int>& nums) {
        map<int, int> map;
        int minNum = INT_MAX, maxNum = INT_MIN;
        int ans = 0;

        for(const auto num: nums){
            map[num]++;                   //store the appear times of number into map

            minNum = min(minNum, num);    //store the min number in vector
            maxNum = max(maxNum, num);    //store the max number in vector
        }

        for(const auto it: map){
            if(it.first != minNum && it.first != maxNum) ans += it.second; //IF   current number is not the min or max number
        }                                                                  //     -> current number has 1 strictly smaller number and 1 strictly bigger number

        return map.size() >= 3 ? ans : 0; //IF   map.size < 3 -> no number has 1 strictly smaller number and 1 strictly bigger number -> answer = 0
    }
};
