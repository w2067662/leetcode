//https://leetcode.com/problems/string-to-integer-atoi/

class Solution {
public:
    int makeINT(string str, bool negative){ //catch overflow
        int ans=0;
        for(int i=0;i<str.length();i++){
            //get signed digit
            int digit=str[i]-'0';
            if(negative) digit*=-1;
            //return if bigger than INT_MAX or smaller than INT_MIN
            if(!negative && (ans> INT_MAX/10  || (ans==INT_MAX/10 && digit>(INT_MAX%10))))return INT_MAX;
            if( negative && (ans< INT_MIN/10  || (ans==INT_MIN/10 && digit<(INT_MIN%10))))return INT_MIN;
            //add digit to ans;
            ans=ans*10+digit;
        }
        return ans;
    }
    int myAtoi(string s) {
        string str="";
        bool negative=false;
        bool added=false;
        for(int i=0;i<s.length();i++){
            if(s[i]>='0'&&s[i]<='9'){   //if '0'~'9', append to string
                if(!added)added=true;
                str+=s[i];
            }
            else if(s[i]=='-'||s[i]=='+'){  //if '-'or'+'
                if(added)break; //***two sign will be 0, ex: "+-", "-+"***
                else{
                    added=true;
                    if(s[i]=='-')negative=true;
                }
            }
            else if(islower(s[i])||isupper(s[i])){  //if 'a'~'z'or'A'~'Z', then 0
                break;
            } 
            else if(s[i]==' ' && added) break;  //if ' ', and encountered sign or digit already, then end it, ex:" -1  1"
            else if(s[i]=='.') break;   //no contain float point
        }
        return makeINT(str, negative);  //make valid INT answer
    }
};
