//https://leetcode.com/problems/count-largest-group/description/

class Solution {
public:
    int digitSum(int num){  //count for digit sum
        int res = 0;
        while(num>0){
            res += num%10;
            num/=10;
        }
        return res;
    }

    int countLargestGroup(int n) {
        map<int, vector<int>> map;  //map = {[digitSum]: vector{[num1],[num2],...}}
        int maxSize = 0;
        int ans = 0;

        for(int i=1; i<=n; i++){
            map[digitSum(i)].push_back(i);  //store numbers into map
        }

        for(const auto it:map){
            maxSize = it.second.size() > maxSize ? it.second.size() : maxSize;  //get largest size of vector
        }
        
        for(const auto it:map){
            if(it.second.size() == maxSize)ans++; //if current element's vector size = maxSize, answer+1
        }

        return ans;
    }
};
