//https://leetcode.com/problems/apply-transform-over-each-element-in-array/description/

/**
 * @param {number[]} arr
 * @param {Function} fn
 * @return {number[]}
 */
var map = function(arr, fn) {
    var ans=[];
    for(var i=0; i<arr.length; i++){
        ans[i]=fn(arr[i], i);
    }
    return ans;
};
