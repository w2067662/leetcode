//https://leetcode.com/problems/spiral-matrix-iv/description/

class Solution {
public:
    bool isValid(vector<vector<int>> &matrix, int i, int j){ //check current position is valid or not
        return 0 <= i && i < matrix.size() && 0 <= j && j < matrix[i].size();
    }

    pair<int, int> nextStep(int orientation, pair<int, int> &curr){ //calculate next step
        switch(orientation){
            case 0: return {curr.first+0, curr.second+1};
            case 1: return {curr.first+1, curr.second+0};
            case 2: return {curr.first+0, curr.second-1};
            case 3: return {curr.first-1, curr.second+0};
        }

        return curr;
    }

    vector<vector<int>> spiralMatrix(int m, int n, ListNode* head) {
        vector<vector<int>> matrix (m, vector<int> (n, -1));        //create M x N size matrix
        pair<int, int> curr = {0, 0}, next;
        int orientation = 0;                                        //right: 0, down: 1, left: 2, up: 3 

        while(true){
            matrix[curr.first][curr.second] = head->val;            //store current node value into current position

            if(!head->next) break;                                  //IF  no next node -> break

            next = nextStep(orientation, curr);                     //calculate next step
            if(!isValid(matrix, next.first, next.second)){          //IF  next step not valid
                orientation = orientation+1 > 3 ? 0 : orientation+1;//    -> change orientation
            }

            next = nextStep(orientation, curr);                     //calculate next step
            if(matrix[next.first][next.second] != -1){              //IF  next step is not empty
                orientation = orientation+1 > 3 ? 0 : orientation+1;//    -> change orientation
            }

            curr = nextStep(orientation, curr);                     //set next step's position as current position

            head = head->next;                                      //goto next node
        }

        return matrix;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
