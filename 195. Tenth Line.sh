#https://leetcode.com/problems/tenth-line/description/

awk 'NR == 10' file.txt #onli output the 10th line in file.txt

#NR : row
#NF : words amount

#ex: this:is:test -> NR = 1 ; NF = 3
#    this:is:test -> NR = 2 ; NF = 3
