//https://leetcode.com/problems/check-if-a-parentheses-string-can-be-valid/description/

class Solution {
private:
    bool isLocked(const char& ch){
        return ch == '1';
    }
    
public:
    // TIPS: Same solution as problem 678. Consider unlocked braces as '*'.
    bool canBeValid(string s, string locked) {
        if(s.length()%2) return false;  // IF s.length is not even -> FALSE (invalid to pair up all braces)

        stack<int> star, stack;

        for(int i=0; i<s.length(); i++){
            if(!isLocked(locked[i])) s[i] = '*';       // Mark all the unlocked braces as '*'
        }

        for(int i=0; i<s.length(); i++){               // For i from 0 to s.length
            switch(s[i]){
                case '(': stack.push(i); break;        //       IF current char is '(' -> push into stack
                case '*': star.push(i); break;         //       IF current char is '*' -> push into star
                case ')':                              //       IF current char is ')'
                    if(!stack.empty()) stack.pop();    //          IF   stack is not empty -> pop 1 '(''s index from stack (pair up)
                    else if(!star.empty()) star.pop(); //          IF   star  is not empty -> pop 1 '*''s index from stack (pair up)
                    else return false;                 //          ELSE unable to pair with current ')'
                                                       //               -> return FALSE
                    break;
            }
        }

        while( !stack.empty() && !star.empty() &&      // While stack is not empty AND start is not empty
                stack.top() < star.top() ){            //       stack's top index < star's top index
            star.pop();                                //       -> Pop top index from star  (pair up '(' with '*')
            stack.pop();                               //       -> Pop top index from stack
        }

        return stack.empty();   // IF    all braces are paired -> stack is empty (True)
                                // ELSE  exist braces unpaired -> stack is not empty (FALSE)
    }
};