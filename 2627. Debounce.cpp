//https://leetcode.com/problems/debounce/description/

/**
 * @param {Function} fn
 * @param {number} t milliseconds
 * @return {Function}
 */
var debounce = function(fn, t) {
    let timer;

    return function(...args) {
        clearTimeout(timer);                       //IF within T time, next debounce is called -> stop current debounce
        timer = setTimeout(()=>fn(...args), t);    //IF after T time, not debounce called -> call the function
    }
};

/**
 * const log = debounce(console.log, 100);
 * log('Hello'); // cancelled
 * log('Hello'); // cancelled
 * log('Hello'); // Logged at t=100ms
 */
