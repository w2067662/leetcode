//https://leetcode.com/problems/deepest-leaves-sum/description/

class Solution {
public:
    bool isLeaf(TreeNode* root){ //check current root is leaf node or not
        return !root->left && !root->right;
    }

    void traverse(TreeNode* root, int level, map<int, vector<int>, greater<int>> &map){
        if(!root) return;

        if(isLeaf(root)) map[level].push_back(root->val); //IF  is leaf node -> store current node value into map[level]

        traverse(root->left , level+1, map);
        traverse(root->right, level+1, map);
        return;
    }

    int deepestLeavesSum(TreeNode* root) {
        map<int, vector<int>, greater<int>> map;
        int sum = 0;

        traverse(root, 0, map);                              //traverse the tree to find leaf nodes

        for(const auto num: map.begin()->second) sum += num; //sum up the deepest leaf nodes

        return sum;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
