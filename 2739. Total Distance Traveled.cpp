//https://leetcode.com/problems/total-distance-traveled/description/

class Solution {
public:
    int distanceTraveled(int mainTank, int additionalTank) {
        int spending, injected;
        int ans = 0;

        while(mainTank>0){                                  //IF    no fuels end the loop
            spending = mainTank >= 5 ? 5 : mainTank;        //IF    mainTank have more than 5 fuels, spend 5 fuels
                                                            //ELSE  spend all lefted fuels
            if(additionalTank>0){                           //IF    additionalTank has fuels
                injected = spending == 5 ? 1 : 0;           //      IF    mainTank spend 5 fuels -> injected  1 fuel to mainTank
                                                            //      ELSE                         -> injected no fuel to mainTank
                mainTank = mainTank - spending + injected;  //      calculate lefted fuels
                additionalTank -= injected;                 //      deduct injected fuel from additionalTank
            }else{
                mainTank -= spending;                       //IF    additionalTank has no fuels  -> mainTank spend fuels (no injection)
            }
            ans += spending*10;                             //add up travel distance
        }

        return ans;
    }
};
