//https://leetcode.com/problems/check-if-the-number-is-fascinating/description/

class Solution {
public:
    bool isFascinating(int n) {
        map<int, int> map;                                          //map = {[digit]: [appear times]}

        string s = to_string(n) + to_string(2*n) + to_string(3*n);  //concat numbers into string

        for(int i=0; i<s.length(); i++){
            map[s[i]-'0']++;                                        //store digits into map
        }

        if(map.size() != 9) return false;                           //if map size != 0, not containing all the digits from 1 to 9 exactly once
                                                                    //      -> FALSE
        for(int i=1; i<=9; i++){
            if(map.find(i) == map.end())return false;               //check map contain digits from 1 to 9
            if(map[i] != 1)return false;                            //check digit from 1 to 9 appear exablty once
        }

        return true;
    }
};
