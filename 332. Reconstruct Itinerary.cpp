//https://leetcode.com/problems/reconstruct-itinerary/description/

class Solution {
public:
    //ex: tickets = [["JFK","KUL"],["JFK","NRT"],["NRT","JFK"]]
    //graph = ["JFK": vector{"NRT","KUL"}
    //         "NRT": vector{"JFK"}]

    //current = "JFK"       ; route = []                            ; stack = ["JFK", "KUL"]
    //current = "KUL"(end)  ; route = ["KUL"]                       ; stack = ["JFK"]
    //current = "JFK"       ; route = ["KUL"]                       ; stack = ["JFK", "NRT"]
    //current = "NRT"       ; route = ["KUL"]                       ; stack = ["JFK", "NRT", "JFK"]
    //current = "JFK"(end)  ; route = ["KUL", "JFK"]                ; stack = ["JFK", "NRT"]
    //current = "NRT"(end)  ; route = ["KUL", "JFK", "NRT"]         ; stack = ["JFK"]
    //current = "JFK"(end)  ; route = ["KUL", "JFK", "NRT", "JFK"]  ; stack = [] (empty) -> end while loop

    //answer = reverse(route) = ["JFK", "NRT", "JFK", "KUL"]
    vector<string> findItinerary(vector<vector<string>>& tickets) {
        map<string, vector<string>> graph;  //drawing graph
        vector<string> stack = {"JFK"};     //start at "JKF"
        vector<string> route;
        string curr;

        for(int i=0; i<tickets.size(); i++){
            graph[tickets[i][0]].push_back(tickets[i][1]); //mapping the pointers
        }

        for(auto& it:graph){
            sort(it.second.rbegin(), it.second.rend());    //sort the vector in descending order (DFS)
        }                                                  //find the end first

        while(!stack.empty()){
            curr = stack.back();                           //get the top from stack
            if(graph.find(curr) != graph.end() && !graph[curr].empty()){ //if current is not the end
                stack.push_back(graph[curr].back());                     //     push current into stack
                graph[curr].pop_back();                                  //     pop out current from graph(visited)
            }else{                                                       //if current is the end
                route.push_back(stack.back());                           //     push the end node into route
                stack.pop_back();                                        //     pop out the end node from stack
            }
        }

        reverse(route.begin(), route.end());               //reverse route
        return route;
    }
};
