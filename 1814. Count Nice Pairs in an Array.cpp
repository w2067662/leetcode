//https://leetcode.com/problems/count-nice-pairs-in-an-array/description/

class Solution {
public:
    //ex: nums = [13,10,35,24,76]
    //             ^                13-31 = -18  ; map = [-18:1]
    //                ^             10- 1 =   9  ; map = [-18:1, 9:1]
    //                   ^          35-53 = -18  ; map = [-18:2, 9:1]
    //                      ^       24-42 = -18  ; map = [-18:3, 9:1]
    //                         ^    76-67 =   9  ; map = [-18:3, 9:2]
    //     map = [-18:3, 9:2]
    //              ^               count = 0 + 3*2/2 = 3
    //                   ^          count = 3 + 2*2/2 = 4
    //   count = 4
    //
    int reverse(int n){ //reverse the number
        int res = 0;
        while (n > 0) {
            res = res*10 + n%10;
            n /= 10;
        }
        return res;
    }

    int countNicePairs(vector<int>& nums) {
        map<int, int> map; 
        long long count = 0;

        for(const auto num: nums){          //store all the number's reverse difference into map
            map[num-reverse(num)]++;
        }

        for(const auto it: map){
            count += it.second>=2 ? (long long)it.second*(it.second-1)/2 : 0; //count possible pair in map
        }

        return count % (int)(pow(10, 9)+7); //mod the count by 10^9+7;
    }
};
