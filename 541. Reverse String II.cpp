//https://leetcode.com/problems/reverse-string-ii/description/

class Solution {
public:
    string reverse(string s, int left, int right){      //reverse string from left ot right
        for(int i=left, j=right; i<=left + (right-left)/2;i++, j--){
            swap(s[i], s[j]);
        }
        return s;
    }
    //ex1: s = "ab|ca|bc|de|fg|h"; k=2 -> ans = "ba|ca|cb|de|gf|h"
    //ex2: s = "abca|bcde|fgh"   ; k=4 -> ans = "acba|bcde|hgf"
    string reverseStr(string s, int k) {
        bool doReverse =true;
        if(k>s.length())return reverse(s, 0, s.length()-1); //ex: s = "abc"; k=4 -> ans = "cba"

        for(int i=0;i<s.length();i+=k){
            if(i+k-1<s.length() && doReverse){      //if doReverse and i~i+k-1 in the range of s, reverse from i to i+k-1
                s = reverse(s, i, i+k-1);
                doReverse =false;
            }
            else if(i+k-1>=s.length() && doReverse){//if doReverse and i~i+k-1 out of range of s, reverse from i to s.length()-1
                s = reverse(s, i, s.length()-1);
                doReverse =false;
            }
            else doReverse =true;
        }
        return s;
    }
};
