//https://leetcode.com/problems/lemonade-change/description/

class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        map<int, int> coins;

        for(const auto bill: bills){
            switch(bill){
                case 5:                             //if recieve 5  -> no need to change
                    coins[5]++;
                    break;
                case 10:                            //if recieve 10 -> change for coin 5 x1
                    if(coins[5]<=0)return false;    //              -> not able to change coins
                    coins[10]++;
                    coins[5]--;
                    break;
                case 20:                            //if recieve 20 
                    if(coins[10]>=1 && coins[5]>=1){//              -> change for coin 10 x1 + coin 5 x1
                        coins[10]--;
                        coins[5]--;
                    }else if(coins[5]>=3){          //              -> change for coin 5 x3
                        coins[5]-=3;
                    }else return false;             //              -> not able to change coins
                    break;
            }
        }

        return true;
    }
};
