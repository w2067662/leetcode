//https://leetcode.com/problems/defuse-the-bomb/description/

class Solution {
public:
    vector<int> decrypt(vector<int>& code, int k) {
        vector<int> ans;
        int times = abs(k-0);   //ex: k = -2 -> times = 2 (need to add up 2 elements)
        int startIndex;

        for(int i=0, startIndex, sum; i<code.size(); i++){
            sum = 0;
            startIndex = k >= 0 ? i+1 : i + (code.size() - times%code.size());  //get the start index 
            for(int j=0; j<times; j++){                                         //from start index,
                sum += code[(startIndex+j)%code.size()];                        //add up [times] elements
            }
            ans.push_back(sum);                                                 //push the sum of [times] elements into answer
        }

        return ans;
    }
};
