//https://leetcode.com/problems/even-odd-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, int level, vector<vector<int>> &levelList){ // Traverse the tree and get the level list
        if(!root) return;

        if(level >= levelList.size()) levelList.push_back({root->val});
        else levelList[level].push_back(root->val);

        traverse(root->left , level+1, levelList);  // Traverse  left subtree
        traverse(root->right, level+1, levelList);  // Traverse right subtree
        return;
    }

    bool isEvenOddTree(TreeNode* root) {
        vector<vector<int>> levelList;
        int curr;

        traverse(root, 0, levelList);

        for(int i=0; i<levelList.size(); i++){          // Traverse the level list by level
            switch(i%2){                                // IF  current level is 
                case 0:                                 //     EVEN
                    curr = INT_MIN;
                    for(const auto num: levelList[i]){
                        if(num%2 == 0) return false;    //    -> check all numbers in current list is odd or not
                        if(curr >= num) return false;   //    -> check list is strictly increasing
                        curr = num;
                    }
                    break;
                case 1:                                 //     ODD
                    curr = INT_MAX;
                    for(const auto num: levelList[i]){
                        if(num%2 != 0) return false;    //    -> check all numbers in current list is even or not
                        if(curr <= num) return false;   //    -> check list is strictly decreasing
                        curr = num;
                    }
                    break;
            }
        }

        return true;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
