//https://leetcode.com/problems/sum-of-left-leaves/description/

class Solution {
public:
    int leftLeafVal(TreeNode* root){
        if(!root) return 0;                                 // IF    no root                     -> 0
        if(!root->left) return 0;                           // IF    not exist left child        -> 0
        if(root->left->left || root->left->right) return 0; // IF    left child is not leaf node -> 0
        return root->left->val;                             // ELSE                              -> left child's value
    }

    int sumOfLeftLeaves(TreeNode* root) {
        return root ?                                                                             // IF    has root
            (leftLeafVal(root) + sumOfLeftLeaves(root->left) + sumOfLeftLeaves(root->right)) : 0; //       -> Sum up left node's value and traverse child nodes
                                                                                                  // ELSE  -> 0
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */