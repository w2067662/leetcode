//https://leetcode.com/problems/determine-the-winner-of-a-bowling-game/description/

class Solution {
public:
    int score(vector<int>& player){                        //count for player's score
        int score = 0;
        bool strike;
        for(int i=0; i<player.size(); i++){
            strike = false;
            if(i-1 >= 0){                                  //check previous 2 hits contain strike (10 point)
                if(player[i-1] == 10) strike = true;
            }
            if(i-2 >= 0){
                if(player[i-2] == 10) strike = true;
            }
            score += strike ? 2*player[i] : player[i];     //IF     previous 2 hits contain strike -> add 2N points
        }                                                  //ELSE                                  -> add  N points

        return score;
    }

    int isWinner(vector<int>& player1, vector<int>& player2) {
        if(score(player1) == score(player2)) return 0;     //draw
        else if(score(player1) > score(player2)) return 1; //player1 wins
        else return 2;                                     //player2 wins
    }
};
