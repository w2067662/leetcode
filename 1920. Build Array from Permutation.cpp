//https://leetcode.com/problems/build-array-from-permutation/description/

class Solution {
public:
    vector<int> buildArray(vector<int>& nums) {
        vector<int> ans;

        for(int i=0; i<nums.size(); i++){
            ans.push_back(nums[nums[i]]);   //push nums[nums[i]] into answer
        }

        return ans;
    }
};
