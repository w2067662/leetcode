//https://leetcode.com/problems/valid-boomerang/description/

class Solution {
public:
    bool isBoomerang(vector<vector<int>>& p) {
        return (p[0][0] - p[1][0]) * (p[0][1] - p[2][1]) 
            != (p[0][0] - p[2][0]) * (p[0][1] - p[1][1]);
    }
};

//    x0 - x1       y0 - y1
//   ---------  =  --------- (on the same line)
//    x0 - x2       y0 - y2

//
//=> (x0 - x1) x (y0 - y2) = (x0 - x2) x (y0 - y1)
//
