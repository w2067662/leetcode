//https://leetcode.com/problems/image-smoother/description/

class Solution {
private:
    int Imax;
    int Jmax;
public:
    bool valid(int i, int j){   //check position valid
        return i>=0 && j>=0 && i<=Imax && j<=Jmax;
    }

    int smooth_corner(vector<vector<int>>& img, int i, int j){
        int sum=0;
        int size=0;
        int x_start, y_start, x_end, y_end;
        // set start position and end position
        if(i==0 && j==0){               //-------
            x_start=0;   x_end=1;       //| S .
            y_start=0;   y_end=1;       //| . E
        }
        else if(i==0 && j==Jmax){       //-------
            x_start=0;   x_end=1;       //  S . |
            y_start=j-1; y_end=j;       //  . E |
        }
        else if(i==Imax && j==0){       //| S . 
            x_start=i-1; x_end=i;       //| . E 
            y_start=0;   y_end=1;       //-------
        }
        else if(i==Imax && j==Jmax){    //  S . |
            x_start=i-1; x_end=i;       //  . E |
            y_start=j-1; y_end=j;       //-------
        }
        // smoother from start position to end position
        for(int x=x_start;x<=x_end;x++){
            for(int y=y_start;y<=y_end;y++){
                if(valid(x, y)){
                    sum+=img[x][y];
                    size++;
                }
            }
        }
        return sum/size;
    }

    int smooth_edge(vector<vector<int>>& img, int i, int j){
        int sum=0;
        int size=0;
        int x_start, y_start, x_end, y_end;
        // set start position and end position
        if(i==0){                       //------
            x_start=0;   x_end=1;       // S . .
            y_start=j-1; y_end=j+1;     // . . E
        }
        else if(j==0){                  //|S .
            x_start=i-1; x_end=i+1;     //|. .
            y_start=0;   y_end=1;       //|. E
        }
        else if(i==Imax){               // S . .
            x_start=i-1; x_end=i;       // . . E
            y_start=j-1; y_end=j+1;     //-------
        }
        else if(j==Jmax){               //  S .|
            x_start=i-1; x_end=i+1;     //  . .|
            y_start=j-1; y_end=j;       //  . E|
        }
        // smoother from start position to end position
        for(int x=x_start;x<=x_end;x++){
            for(int y=y_start;y<=y_end;y++){
                if(valid(x, y)){
                    sum+=img[x][y];
                    size++;
                }
            }
        }
        return sum/size;
    }

    int smooth_middle(vector<vector<int>>& img, int i, int j){
        int sum=0;
        int size=0;                     
        for(int x=i-1;x<=i+1;x++){      // - - - - -
            for(int y=j-1;y<=j+1;y++){  // - S . . -
                if(valid(x, y)){        // - . . . -
                    sum+=img[x][y];     // - . . E -
                    size++;             // - - - - -
                }
            }
        }
        return sum/size;
    }
    
    vector<vector<int>> imageSmoother(vector<vector<int>>& img) {
        // get Imax and Jmax
        Imax=img.size()-1;
        Jmax=img[0].size()-1;
        // create a same size 2D array ANS 
        vector<vector<int>> ans (img.size(), vector<int>(img[0].size()));

        for(int i=0;i<=Imax;i++){
            for(int j=0;j<=Jmax;j++){
                if(i==0 || i==Imax || j==0 || j==Jmax){
                    if((i==0 && j==0) || (i==0 && j==Jmax) ||  (i==Imax && j==0) || (i==Imax && j==Jmax)){
                        ans[i][j] = smooth_corner(img, i, j);   //is corner, do smooth_corner()
                    }
                    else ans[i][j] = smooth_edge(img, i, j);    //is edge, do smooth_edge()
                }
                else ans[i][j] = smooth_middle(img, i, j);      //is middle, do smooth_middle()
            }
        }

        return ans;
    }
};
