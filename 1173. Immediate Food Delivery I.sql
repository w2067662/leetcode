#https://leetcode.com/problems/immediate-food-delivery-i/description/

# Write your MySQL query statement below
SELECT ROUND(AVG(order_date = customer_pref_delivery_date)*100, 2)
AS immediate_percentage
FROM Delivery

# AVG(order_date = customer_pref_delivery_date) -> immediate/scheduled (in this case is 0.3333..)
# ROUND(column_name, decimals(N digits after '.') )
