//https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/description/

class Solution {
public:
    string minRemoveToMakeValid(string s) {
        stack<int> stack;
        string ans = "";
        int braces = 0;

        for(int i=0; i<s.length(); i++){    // For i from 0 to s.length
            switch(s[i]){
                case '(':                   //      IF  encounter '('
                    braces++;               //          -> braces + 1
                    stack.push(i);          //          -> Push current index into stack
                    break;
                case ')':                   //      IF  encounter ')'
                    if(braces == 0){        //          IF no left brace to pair
                        s[i] = ' ';         //             -> Mark current char as ' '
                    } else {                //          IF exist left brace to pair
                        braces--;           //             -> braces - 1
                        stack.pop();        //             -> Pop 1 left brace's index from stack
                    }
                    break;
            }
        }

        while(!stack.empty()){              // While stack is not empty (unpaired left braces)
            s[stack.top()] = ' ';           //      Mark '(' as ' '
            stack.pop();                    //      Pop current index from stack
        }

        for(const auto& ch: s){             // For all chars within string
            if(ch != ' ') ans += ch;        //      IF current char is not ' '
                                            //         -> Append to answer
        }

        return ans;
    }
};