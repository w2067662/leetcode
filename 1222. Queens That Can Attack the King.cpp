//https://leetcode.com/problems/queens-that-can-attack-the-king/description/

class Solution {
public:
    //ex:
    //      K . . Q . . Q
    //            ^     ^
    //            |     Not attackable
    //         attackable (no other queens in between)
    //
    set<pair<int, int>> attackable(vector<vector<char>>& board, vector<int>& king){ // Find all the attackable queens
        set<pair<int, int>> attackableQueens;

        for(int x=king[0]; x>=0; x--){                // Up
            if(board[x][king[1]] == 'Q') {
                attackableQueens.insert({x, king[1]});
                break;
            }
        }

        for(int x=king[0]; x<8; x++){                 // Down
            if(board[x][king[1]] == 'Q') {
                attackableQueens.insert({x, king[1]});
                break;
            }
        }

        for(int y=king[1]; y>=0; y--){                // Left
            if(board[king[0]][y] == 'Q'){
                attackableQueens.insert({king[0], y});
                break;
            }
        }

        for(int y=king[1]; y<8; y++){                 // Right
            if(board[king[0]][y] == 'Q'){
                attackableQueens.insert({king[0], y});
                break;
            }
        }

        for(int x=king[0], y=king[1]; x>=0 && y>=0; x--, y--){   // Left-Top
            if(board[x][y] == 'Q'){
                attackableQueens.insert({x, y});
                break;
            }
        }

        for(int x=king[0], y=king[1]; x>=0 && y<8; x--, y++){   // Right-Top
            if(board[x][y] == 'Q'){
                attackableQueens.insert({x, y});
                break;
            }
        }

        for(int x=king[0], y=king[1]; x<8 && y>=0; x++, y--){   // Left-Down
            if(board[x][y] == 'Q'){
                attackableQueens.insert({x, y});
                break;
            }
        }

        for(int x=king[0], y=king[1]; x<8 && y<8; x++, y++){   // Right-Down
            if(board[x][y] == 'Q'){
                attackableQueens.insert({x, y});
                break;
            }
        }

        return attackableQueens;
    }

    vector<vector<int>> queensAttacktheKing(vector<vector<int>>& queens, vector<int>& king) {
        vector<vector<int>> ans;
        vector<vector<char>> board(8, vector<char>(8, '.'));                            // Create and initialize the 8 x 8 board with '.'

        board[king[0]][king[1]] = 'K';                                                  // Assign King's position

        for(const auto queen: queens){
            board[queen[0]][queen[1]] = 'Q';                                            // Assign Queens' positions
        }

        set<pair<int, int>> attackableQueens = attackable(board, king);                 // Get all the attackable queens

        for(const auto queen: queens){
            if(attackableQueens.find({queen[0], queen[1]}) != attackableQueens.end()){
                ans.push_back(queen);                                                   // Push the attackable queens into answer in sequence
            }
        }

        return ans;
    }
};
