//https://leetcode.com/problems/longest-palindromic-substring/description/

class Solution {
public:
    //ex:  s = "babad"
    //          ^   ^      -> not Palindrome  ;  answer = ""
    //          ^  ^       -> not Palindrome  ;  answer = ""
    //          ^ ^        -> is  Palindrome  ;  answer = "bab"
    //           ^  ^      -> not Palindrome  ;  answer = "bab"
    // [END] -> no next substring has length larger than current answer
    //
    bool isPalindrome(const string &s, int left , int right){ //check the string is palindrome or not
        while(left <= right){
            if(s[left] != s[right]) return false;
            left++;
            right--;
        }
        return true;
    }

    string longestPalindrome(string s) {
        string ans="";

        for(int i=0; i<s.length(); i++){                           //left  range of substring
            for(int j=s.length()-1; j>=i; j--){                    //right range of substring
                if(j-i+1 > ans.length() && isPalindrome(s, i, j)){ //IF   current substring has longer length and is Palindrome
                    ans = s.substr(i, j-i+1);                      //     -> set as answer
                }
            }
        }
        
        return ans;
    }
};
