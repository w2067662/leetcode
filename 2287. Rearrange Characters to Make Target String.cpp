//https://leetcode.com/problems/rearrange-characters-to-make-target-string/description/

class Solution {
public:
    typedef map<char, int> CharContainer;
    int rearrangeCharacters(string s, string target) {
        CharContainer c1, c2;
        int ans = INT_MAX, temp;

        for(const auto ch: s)      c1[ch]++; //store the appear times of letters from string into c1 map
        for(const auto ch: target) c2[ch]++; //store the appear times of letters from target into c2 map

        for(const auto it: c2){
            temp = c1[it.first]/it.second;   //calculate the most possible times to rearrange the target word 
            ans = min(ans, temp);
        }

        return ans;
    }
};
