//https://leetcode.com/problems/remove-sub-folders-from-the-filesystem/description/

class FileSystem {
private:
    class Folder{ //define Folder
        public:
            string name;
            map<string, Folder*> folders; //with multiple folders
            Folder(){};
            Folder(string n):name(n){};
    };
    
    Folder* root;
public:
    FileSystem() {
        this->root = new Folder; //initialize with creating new root
    }
    
    void insert(string path) {
        insertFolder(this->root, toVector(path), 0);
    }
    
    bool isSubfolder(string path) {
        return searchSuperfolder(this->root, toVector(path), 0);
    }

    //--------------------------
    //   Supporting functions
    //--------------------------
    vector<string> toVector(const string &path){ //convert string to vector<string>
        vector<string> vec;
        string temp = "";

        for(const auto ch: path){
            if(ch == '/' && temp != ""){
                vec.push_back(temp);
                temp = "";
            }
            temp += ch;
        }
        if(temp != "")vec.push_back(temp);

        return vec;
    }

    void insertFolder(Folder* root, const vector<string> &path, int index){
        if(index >= path.size()){
            root->folders["$"] = new Folder("$");                   //set the folder end as '$'
            return;
        } 

        if(root->folders.find(path[index]) == root->folders.end()){ //IF   no folder's value match current path
            root->folders[path[index]] = new Folder(path[index]);   //     -> create a new folde with current char path
        }
        insertFolder(root->folders[path[index]], path, index+1);    //insert next folder
    }

    bool searchSuperfolder(Folder* root, const vector<string> &path, int index){
        if(index >= path.size()) return false;                      //IF   search the folder until the end -> is not subfolder

        if(root->folders.find("$") != root->folders.end()) return true; //IF   found the super folder -> is subfolder

        if(root->folders.find(path[index]) == root->folders.end()) return false;
        return searchSuperfolder(root->folders[path[index]], path, index+1);
    }
};

class Solution {
private:
    
public:
    vector<string> removeSubfolders(vector<string>& folder) {
        vector<string> folders;
        FileSystem fileSystem;

        for(const auto path: folder){
            fileSystem.insert(path);            //insert all the folders
        }

        for(const auto path: folder){
            if(!fileSystem.isSubfolder(path)) { //IF  current folder is not subfolder
                folders.push_back(path);        //    -> push into answer
            }
        }

        return folders;
    }
};
