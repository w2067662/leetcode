#https://leetcode.com/problems/get-the-size-of-a-dataframe/description/

import pandas as pd

def getDataframeSize(players: pd.DataFrame) -> List[int]:
    return list(players.shape)

#.shape : returns the amount of columns and rows
#list() : make the element into list
    
