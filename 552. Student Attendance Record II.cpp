//https://leetcode.com/problems/student-attendance-record-ii/description/

class Solution {
private:
    const long long MOD = 1e9 + 7;

public:
    // Possible Status:
    //                         A          L          P
    //     s0:  0A 0L  ->  1A 0L(s3)  0A 1L(s1)  0A 0L(s0)  -> s0' = s0 + s1 + s2
    //     s1:  0A 1L  ->  1A 0L(s3)  0A 2L(s2)  0A 0L(s0)  -> s1' = s0
    //     s2:  0A 2L  ->  1A 0L(s3)      X      0A 0L(s0)  -> s2' = s1
    //     s3:  1A 0L  ->      X      1A 1L(s4)  1A 0L(s3)  -> s3' = s0 + s1 + s2 + s3 + s4 + s5
    //     s4:  1A 1L  ->      X      1A 2L(s5)  1A 0L(s3)  -> s4' = s3
    //     s5:  1A 2L  ->      X          X      1A 0L(s3)  -> s5' = s4

    int checkRecord(int n) {
        vector<long long> temp = {0,0,0,0,0,0}, dp = {1,0,0,0,0,0}; // Default day 1: 0A 0L

        for(int i=0; i<n; i++){
            for(int j=0; j<dp.size(); j++){
                temp[j] = dp[j];    // Copy current status from DP to temp
            }

            dp[0] = (temp[0] + temp[1] + temp[2]) % MOD;    // Calculate next possible status
            dp[1] = (temp[0]) % MOD;
            dp[2] = (temp[1]) % MOD;
            dp[3] = (temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]) % MOD;
            dp[4] = (temp[3]) % MOD;
            dp[5] = (temp[4]) % MOD; 
        }

        return (dp[0] + dp[1] + dp[2] + dp[3] + dp[4] + dp[5]) % MOD; // Sum up possible attendance records
    }
};