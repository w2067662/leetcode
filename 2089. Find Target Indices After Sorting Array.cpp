//https://leetcode.com/problems/find-target-indices-after-sorting-array/description/

class Solution {
public:
    vector<int> targetIndices(vector<int>& nums, int target) {
        map<int, vector<int>> map;

        sort(nums.begin(), nums.end());                             //sort the vector in ascending order

        for(int i=0;i<nums.size();i++) map[nums[i]].push_back(i);   //push current index of current number into map

        return map[target];                                         //return the index vector of target number 
    }
};
