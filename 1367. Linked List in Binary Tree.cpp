//https://leetcode.com/problems/linked-list-in-binary-tree/description/

class Solution {
public:
    bool check(TreeNode* root, ListNode* head){
        if(!head) return true;                   //(!head && !root) || (!head && root)
        if(!root) return false;                  //( head && !root)
        if(root->val != head->val) return false; //( head &&  root)
        return check(root->left, head->next) || check(root->right, head->next); //check next value of list node in left and right subtree
    }

    void traverse(TreeNode* root, ListNode* head, bool &exist){ //traverse the tree to find subpath
        if(!root) return;

        if(check(root, head)){ //IF  subpath is found
            exist = true;      //    -> is exist 
            return;
        }

        traverse(root->left , head, exist); //traverse left  subtree
        traverse(root->right, head, exist); //traverse right subtree
        return;
    }

    bool isSubPath(ListNode* head, TreeNode* root) {
        bool exist = false;
        traverse(root, head, exist); //traverse the tree to find subpath
        return exist;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
