//https://leetcode.com/problems/goat-latin/description/

class Solution {
public:
    bool isvowel(char ch) { 
        return (ch == 'a') || (ch == 'e') ||(ch == 'i') ||(ch == 'o') ||(ch == 'u'); 
    }

    string process_words(string word, int index){
        string s="";
        if(isvowel(tolower(word[0]))){                  //if first letter is vowel
            for(int i=0;i<word.size();i++) s+=word[i];  //copy the word
        }   
        else{                                           //if first letter is not vowel
            for(int i=1;i<word.size();i++) s+=word[i];  //copy the word from second letter to the end
            s+=word[0];                                 //append first letter to the back of the word
        }
        s+="ma";                                        //append "ma"
        for(int i=0;i<index;i++)s+="a";                 //append 'a' with n times, n = index

        return s;
    }

    string toGoatLatin(string sentence) {
        vector<string> words;
        string temp="";
        string ans="";

        for(int i=0;i<sentence.length();i++){           //traverse letters in the sentence
            if(sentence[i]==' ') {                      //if sentence[i] is ' ' -> find a word
                words.push_back(temp);                  //push to vector's back
                temp="";                                //reset temp string to ""
            }
            else temp+=sentence[i];                     //if sentence[i] is not ' ' -> append letter
        }
        words.push_back(temp);

        for(int i=0, index=1; i<words.size();i++, index++){                     //get words from vector
            if(i!=words.size()-1) ans += process_words(words[i], index) + " ";  //process the words, 
            else ans += process_words(words[i], index);                         //and append to answer string
        }

        return ans;
    }
};
