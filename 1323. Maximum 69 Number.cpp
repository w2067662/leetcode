//https://leetcode.com/problems/maximum-69-number/description/

class Solution {
public:
    int maximum69Number (int num) {
        bool digitChanged = false;
        int ans=0;
        string s = to_string(num);  //use string to find first '6'
        
        for(int i=0;i<s.length();i++){      //start from left to right
            if(s[i]=='6' && !digitChanged){ //change first found '6' to '9'(to make changed number the biggest)
                s[i]='9';
                digitChanged = true;
            }
            ans = ans*10 + s[i] -'0';
        }
        return ans;
    }
};
