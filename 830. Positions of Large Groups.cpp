//https://leetcode.com/problems/positions-of-large-groups/description/

class Solution {
public:
    vector<vector<int>> largeGroupPositions(string s) {
        vector<pair<int, int>> vec;
        vector<vector<int>> ans;
        int range;
        char ch = s[0];         //set ch as first letter of string

        vec.push_back({0, 0});  //start finding group's range

        for(int i=0; i<s.size();i++){
            if(s[i] == ch) vec[vec.size()-1].second = i; //IF  s[i] =  s[i-1] -> update group range [start, end+1]
            else vec.push_back({i, i});                  //IF  s[i] != s[i-1] -> reset [start, end] as [i, i] (first element in next group)

            ch = s[i];                                   //store s[i-1] for next letter s[i] to compare with
        }

        for(int i=0;i<vec.size();i++){
            range = abs(vec[i].second-vec[i].first)+1;
            if(range >= 3) ans.push_back({vec[i].first, vec[i].second}); //if group range >= 3, is large group -> push range [start, end] into answer
        }

        return ans;
    }
};
