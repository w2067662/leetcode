//https://leetcode.com/problems/count-prefixes-of-a-given-string/description/

class Solution {
public:
    int countPrefixes(vector<string>& words, string s) {
        int count = 0;
        for(const auto word: words){
            if(s.length() >= word.length()){
                if(word == s.substr(0, word.length()))count++; //IF   current word is the prefix of string -> count+1
            }
        }
        return count;
    }
};
