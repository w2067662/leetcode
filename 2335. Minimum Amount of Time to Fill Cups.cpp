//https://leetcode.com/problems/minimum-amount-of-time-to-fill-cups/description/

class Solution {
public:
    int fillCups(vector<int>& amount) {
        int ans = 0;

        while(amount[0]>0 || amount[1]>0 || amount[2]>0){ //IF  no cup with value > 0 -> end the loop
            sort(amount.begin(), amount.end());           //sort the cups in ascending order

            amount[1]--;                                  //the cup with second largest value deduct 1 (fill the cup)
            amount[2]--;                                  //the cup with        largest value deduct 1 (fill the cup)

            ans++;                                        //count for operation times
        }

        return ans;
    }
};
