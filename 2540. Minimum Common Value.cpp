//https://leetcode.com/problems/minimum-common-value/description/

class Solution {
public:
    int getCommon(vector<int>& nums1, vector<int>& nums2) {
        set<int> set;
        vector<int> vec;

        for(int i=0;i<nums1.size();i++){
            set.insert(nums1[i]);                                       //store the numbers into set
        }

        for(int i=0;i<nums2.size();i++){                                
            if(set.find(nums2[i]) != set.end()) vec.push_back(nums2[i]);//store the common numbers into vector
        }

        sort(vec.begin(), vec.end());                                   //sort the vector in ascending order

        return vec.size() ? vec[0] : -1;                                //if no possible common nubmer, return -1
    }
};
