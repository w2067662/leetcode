//https://leetcode.com/problems/range-sum-of-bst/description/

class Solution {
public:
    int rangeSumBST(TreeNode* root, int low, int high) {
        if(!root)return 0;                                               //return if no root
        if(root->val<low)return rangeSumBST(root->right, low, high);     //if current node value is smaller than range, search right tree
        else if(root->val>high)return rangeSumBST(root->left, low, high);//if current node value is bigger  than range, search left  tree
        return root->val + rangeSumBST(root->left, low, high) + rangeSumBST(root->right, low, high);//if current node value is in the range, return the sum of current node value and left tree's sum and right tree's sum
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
