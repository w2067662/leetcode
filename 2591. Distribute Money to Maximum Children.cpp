//https://leetcode.com/problems/distribute-money-to-maximum-children/description/

class Solution {
public:
    int distMoney(int money, int children) {
        if(money < children) return -1;       //IF  money is not enough to distribute to all children -> return -1

        int ans = 0;

        money -= children;                    //distribute at least 1 dollar to each children

        for(int i=0;i<children-1;i++){        //try to distribute 7 dollars to each children
            if(money >= 7){
                money -= 7;
                ans++;
            }else break;                      //IF  money is not enough to distribute 7 dollars -> end the loop
        }

        if(money == 7)return ans+1;           //IF  still lefted 7 dollars -> ditrbute 7 dollars to last child -> return answer+1
        else if(money == 3)return ans == children-1 ? ans-1 : ans; //IF    [children-1] children get 8 dollars and only lefted 1 child gets 4 (1+3) dollars 
                                                                   //      -> distribute 1 dollar from 4 to another child to make it 9 dollars -> return answer-1
                                                                   //IF    children-N (N>1) get 8 dollars and lefted 3 dollars -> return answer
        else return ans;                                           //IF    lefted not 7 or 3 dollars -> return answer
    }
};
