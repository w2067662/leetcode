//https://leetcode.com/problems/combination-sum-iv/description/

class Solution {
public:
    //ex: nums = [1,2,3], target = 4
    //
    //possible answer for [ target = 4 ]   = all combinations of 1 + [ target = 3 ] 
    //                                     + all combinations of 2 + [ target = 2 ]
    //                                     + all combinations of 3 + [ target = 1 ]
    //Dynamic programming formula -> DP[4] = DP[4] + DP[4 - numbers smaller than sum]
    //
    //DP  = [ 1, 1, 2, 4, 7]
    //sum =      1  2  3  4
    //           ^              (1)                 possible answer = 1
    //              ^           (1,1)               possible answer = 1(+1) = 2
    //                 ^        (1,2); (2,1)        possible answer = 2(+2) = 4
    //                    ^     (1,3); (2,2); (3,1) possible answer = 4(+3) = 7
    int combinationSum4(vector<int>& nums, int target) {
        vector<unsigned> DP(target+1, 0);                     //there contains negative integars
        DP[0] = 1;
        for(int sum=1; sum<=target; sum++){                   //sum start from 1 to target
            for(int i=0; i<nums.size(); i++){                 //i start from 0 to vector's end
                if(sum>=nums[i]) DP[sum] += DP[sum-nums[i]];  //DP[sum] = DP[sum] + DP[sum - numbers smaller than sum]
            }
        }
        return DP[target];
    }
};
