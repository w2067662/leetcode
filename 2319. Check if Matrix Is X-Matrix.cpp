//https://leetcode.com/problems/check-if-matrix-is-x-matrix/description/

class Solution {
public:
    bool checkXMatrix(vector<vector<int>>& grid) {
        for(int i=0;i<grid.size();i++){
            for(int j=0;j<grid[0].size();j++){
                if(i == j || i+j == grid.size()-1){
                    if(grid[i][j] == 0) return false; //check the     X in matrix should be non-zero number
                }else{
                    if(grid[i][j] != 0) return false; //check the non-X in matrix should be     zero number
                }
            }
        }

        return true;
    }
};
