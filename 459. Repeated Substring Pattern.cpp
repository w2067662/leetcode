//https://leetcode.com/problems/repeated-substring-pattern/

class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        string temp= s+s;                                   //ex: s = "grf" , temp = "grfgrf"
        return temp.substr(1,temp.size()-2).find(s) != -1;  //temp.substr = "rfgr" != temp.substr.find("grf")-> false
    }
};
