//https://leetcode.com/problems/minimum-time-to-make-rope-colorful/description/

class Solution {
public:
    //ex:  colors = "aabaa", neededTime = [1,2,3,4,1]
    //     ranges = [{0,1}, {2,2}, {3,4}]                                                      cost = 0
    //                 ^                    0 ~ 1 >= 2  ;  neededTime = [1,2,3,4,1]
    //                                                                   ^ ^        (sorted)
    //                                                                   +                     cost = 1 (+1)
    //                        ^             2 ~ 2 <  2  ;  neededTime = [1,2,3,4,1]
    //                               ^      3 ~ 4 >= 2  ;  neededTime = [1,2,3,1,4]
    //                                                                         ^ ^  (sorted)
    //                                                                         +               cost = 2 (+1)
    //
    int minCost(string colors, vector<int>& neededTime) {
        vector<pair<int, int>> ranges;
        pair<int, int> curr = {0, 0};
        int cost = 0;

        for(int i=1; i<colors.length(); i++){
            if(colors[i] == colors[i-1]) curr.second = i;  //IF   found continuous color -> expand the right range
            else {                                         //ELSE 
                ranges.push_back(curr);                    //     -> push the range into vector
                curr = {i, i};                             //     -> set current range as {i, i}
            }
        }
        ranges.push_back(curr);                            //push the last range into vector

        for(const auto range: ranges){
            if(range.second-range.first+1 >= 2){                                         //IF  current range has more than 2 continuouse color
                sort(neededTime.begin()+range.first, neededTime.begin()+range.second+1); //    -> sort the neededTime from left range to right range of current range in ascending order
                for(int i=range.first; i<range.second; i++) cost += neededTime[i];       //    -> add up the smaller N-1 neededTimes to cost
            }
        }

        return cost;
    }
};
