//https://leetcode.com/problems/kth-smallest-element-in-a-bst/description/

class Solution {
public:
    void traverse(TreeNode* root, int &k, int &ans){ //use inorder traversal to traverse the BST from smallest number to largest number
        if(!root) return;

        traverse(root->left , k, ans); //traverse left  subtree

        k--;                           //for each traversed node, K-1
        if(k == 0){                    //IF   K = 0 -> current node is the Kth smallest node
            ans = root->val;           //           -> store current node value into answer
            return;
        }

        traverse(root->right, k, ans); //traverse right subtree

        return;
    }
    
    int kthSmallest(TreeNode* root, int k) {
        int ans = -1;

        traverse(root, k, ans); //traverse the tree to find kth smallest number
        
        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
