//https://leetcode.com/problems/remove-nth-node-from-end-of-list/description/

class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if(head==NULL)return NULL;
        ListNode *list=head;
        
        int len=0;
        while (list){       //count for length
            len++;
            list=list->next;
        }
        
        list = head;
        
        if(len==n) {        //if length = N, remove first element in the list
            list=list->next;//  goto next
            return list;    //  return next as new head
        }
        
        while(list){
            if(len==n+1){   //if arrive N-1 th node from end of the list, remove next node
                if(list->next!=NULL && list->next->next!=NULL){ //if next next node is not NULL
                    list->next=list->next->next;                //  set current node's next to next next node
                    break;
                }
                else if(list->next->next==NULL){                //if next next node is NULL
                    list->next=NULL;                            //  set current node's next to NULL
                    break;
                }
            }
            list=list->next;//goto next
            len--;
            if(len==0)break;
        }
        
        list = head;
        return list;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
