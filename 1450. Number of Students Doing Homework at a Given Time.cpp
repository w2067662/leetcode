//https://leetcode.com/problems/number-of-students-doing-homework-at-a-given-time/description/

class Solution {
public:
    int busyStudent(vector<int>& startTime, vector<int>& endTime, int queryTime) {
        int ans = 0;
        for(int i=0; i<startTime.size();i++){
            if(startTime[i] <= queryTime && queryTime <= endTime[i])ans++;  //if current student is doing homeword at queryTime, answer+1
        }

        return ans;
    }
};
