//https://leetcode.com/problems/base-7/description/

class Solution {
public:
    string reverseString(string s){     //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    string intToBase7String(bool positive, int num){    //change integer to base7 string
        string s="";
        while(num>0){
            s+=num%7 + '0';
            num/=7;
        }
        if(!positive)s+='-';        //if positive add '-' at the end
        return reverseString(s);    //reverse the answer
    }
    string convertToBase7(int num) {
        if(num==0)return "0";
        return (num>=0)? intToBase7String(true, num) : intToBase7String(false, 0-num);
    }
};
