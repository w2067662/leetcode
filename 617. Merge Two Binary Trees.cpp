//https://leetcode.com/problems/merge-two-binary-trees/description/

class Solution {
public:
    void merge(TreeNode* root1, TreeNode* root2) {   //merge in recurrsion
        root1->val+=root2->val;   //add up tree nodes value

        if(root1->left && root2->left)merge(root1->left, root2->left);    //if both nodes have left, merge left
        else if(!root1->left && root2->left)root1->left=root2->left;      //if only root2 has  left, root2's left become root1's left

        if(root1->right && root2->right)merge(root1->right, root2->right);//if both nodes have right, merge right
        else if(!root1->right && root2->right)root1->right=root2->right;  //if only root2 has  right, root2's left become root1's right

        return;
    }
    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
       if(!root1 && !root2)return NULL;         //return NULL if both are NULL
       else if( root1 && !root2)return root1;   //return root1 if root2 is NULL
       else if(!root1 &&  root2)return root2;   //return root2 if root1 is NULL
       else merge(root1, root2);                //do merge if both are not NULL

       return root1;                            //merge to tree1 and return tree1's root
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
