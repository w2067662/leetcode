//https://leetcode.com/problems/long-pressed-name/description/

class Solution {
public:
    //ex: name = "alex", typed = "aaleex"
    //"alex"   -> vector1 = {"a", "l", "e", "x"}
    //"aaleex" -> vector2 = {"aa", "l", "ee", "x"}
    //compare("a", "aa") -> true
    //compare("l", "l")  -> true
    //compare("e", "ee") -> true
    //compare("x", "x")  -> true
    //answer = TRUE

    vector<string> splitString(string name){ //split the word by consecutie same letters
        vector<string>vec;
        string temp="";
        temp += name[0];

        for(int i=1;i<name.length();i++){
            if(name[i]!=name[i-1]){
                vec.push_back(temp);
                temp=name[i];
            }else{
                temp+=name[i];
            }
        }
        if(temp!="") vec.push_back(temp);

        return vec;
    }

    bool compareVector(vector<string> a, vector<string> b){ //compare the vector
        if(a.size()!=b.size())return false;

        for(int i=0;i<a.size();i++){
            if(a[i].length()>b[i].length() || a[i][0]!=b[i][0])return false;
        }

        return true;
    }

    bool isLongPressedName(string name, string typed) {
        return compareVector(splitString(name), splitString(typed));
    }
};
