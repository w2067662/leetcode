//https://leetcode.com/problems/minimum-length-of-string-after-deleting-similar-ends/description/

class Solution {
public:
    //ex1:  s = "aabccabba"
    //   vec = ["aa","b","cc","a","bb","a"]
    //           ^                      ^     'a'  = 'a'
    //                ^            ^          'b'  = 'b'
    //                    ^    ^              'c' != 'a'  (break)
    //
    //   "cc" + "a" -> minLen = 3

    //ex2:  s = "bbabbbccbccbbabbb"
    //   vec = ["bb","a","bbb","cc","b","cc",bb","a","bbb"]
    //           ^                                    ^     'b'  = 'b'
    //                ^                           ^         'a'  = 'a'
    //                    ^                  ^              'b'  = 'b'
    //                          ^        ^                  'c'  = 'c'
    //
    //   "b" -> only 1 lefted but size = 1 -> minLen = 1

    vector<string> continuousLetters(const string& s){ // Convert the string to vector that contains 
                                                       //   continuous letters as substring
        vector<string> vec;
        string temp = "";
        char prev = s[0];

        for(const auto& ch: s){
            if(ch != prev){
                vec.push_back(temp);
                temp = "";
            }
                
            temp += ch;
            prev = ch;
        }
        if(temp != "") vec.push_back(temp);

        return vec;
    }

    int minimumLength(string s) {
        vector<string> vec = continuousLetters(s);                          // Convert string to continuous letters
        int left, right, minLen = 0;

        for(left = 0, right = vec.size()-1; left < right; left++, right--){ // Compare the first letter of left substring and right substring
            if(vec[left][0] != vec[right][0]) break;                        //      IF first letter not equal -> break
        }

        for(int i = left; i <= right; i++){                                 // Calculate the total length of unremovable substring in middle
            minLen += vec[i].length();
        }

        return left == right && vec[left].size() != 1 ? 0 : minLen;         // IF   only lefted 1 substring with length != 1 
                                                                            //      -> answer = 0
                                                                            // ELSE -> answer = minLen
    }
};
