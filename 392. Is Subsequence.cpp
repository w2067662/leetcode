//https://leetcode.com/problems/is-subsequence/description/

class Solution {
public:
    bool isSubsequence(string s, string t) {
        int j=0;
        for(int i=0; i<t.length(); i++){
            if (t[i] == s[j]) j++;       //search the letters in S in order from T
        }
        return s.size() == j;            //check if all the letters in S are found in order from T
    }
};
