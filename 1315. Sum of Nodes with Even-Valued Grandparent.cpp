//https://leetcode.com/problems/sum-of-nodes-with-even-valued-grandparent/description/

class Solution {
public:
    int sumUpGrandChildren(TreeNode* root){ //sum up grand children
        int sum = 0;
        if(root->left){
            if(root->left->left ) sum += root->left->left->val;
            if(root->left->right) sum += root->left->right->val;
        }
        if(root->right){
            if(root->right->left ) sum += root->right->left->val;
            if(root->right->right) sum += root->right->right->val;
        }
        return sum;
    }

    void traverse(TreeNode* root, int &sum){
        if(!root) return;

        if(root->val%2 == 0) sum += sumUpGrandChildren(root); //IF  current node's value is even
                                                              //    -> sum up its grand children
        traverse(root->left , sum); //traverse left  subtree
        traverse(root->right, sum); //traverse right subtree
        return;
    }

    int sumEvenGrandparent(TreeNode* root) {
        int sum = 0;

        traverse(root, sum); //traverse the tree

        return sum;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
