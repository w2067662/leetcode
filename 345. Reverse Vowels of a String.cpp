//https://leetcode.com/problems/reverse-vowels-of-a-string/description/

class Solution {
public:
    //ex: s = "hEllo" -> findVowels = "Eo" -> reversedVowel = "oE"
    //      ->"hollE"

    bool isVowel(char ch){  //check for lower case or uppercase vowels
        switch(ch){
            case 'a': case 'e': case 'i': case 'o':  case 'u':
            case 'A': case 'E': case 'I': case 'O':  case 'U':
                return true;
        }
        return false;
    }
    string reverseString(string s){ //reverse vowel strings
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
    string findVowels(string s){    //find vowels from original strings
        string res = "";
        for(int i=0;i<s.length();i++){
            if(isVowel(s[i])) res += s[i];
        }
        return res;
    }
    string reverseVowels(string s) {
        string reversedVowel = reverseString(findVowels(s));
        for(int i=0, j=0;i<s.length();i++){ //swap original vowels to reversed vowels
            if(isVowel(s[i])){
                s[i] = reversedVowel[j];
                j++;
            } 
        }
        return s;
    }
};
