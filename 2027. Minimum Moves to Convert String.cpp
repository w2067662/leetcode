//https://leetcode.com/problems/minimum-moves-to-convert-string/description/

class Solution {
public:
    int minimumMoves(string s) {
        int ans=0;
        for(int i=0;i<s.length();){
            if(s[i] == 'X'){        //IF  encounter 'X'
                i+=3;               //    ->  move exactly 3 char
                ans++;              //    ->  answer+1
            } else i++;             //IF  encounter 'O'
        }                           //    ->  goto next char (no moves)

        return ans;
    }
};
