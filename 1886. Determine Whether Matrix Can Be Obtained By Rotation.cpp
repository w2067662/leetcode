//https://leetcode.com/problems/determine-whether-matrix-can-be-obtained-by-rotation/description/

class Solution {
public:
    void rotate(vector<vector<int>>& mat){                                      //rotate matrix
        vector<vector<int>> rotated (mat.size(), vector<int>(mat[0].size(), 0));

        int x = 0, y = rotated[0].size()-1;

        for(int i=0;i<mat.size();i++){
            for(int j=0;j<mat[i].size();j++){
                rotated[x][y] = mat[i][j];
                x++;
                if(x >= rotated.size()){
                    x=0;
                    y--;
                }
            }
        }

        mat = rotated;
    }

    bool compare(vector<vector<int>>& mat, vector<vector<int>>& target){        //compare matrix
        for(int i=0;i<mat.size();i++){
            for(int j=0;j<mat[i].size();j++){
                if(mat[i][j] != target[i][j])return false;
            }
        }
        return true;
    }

    bool findRotation(vector<vector<int>>& mat, vector<vector<int>>& target) {
        for(int i=0;i<4;i++){                                                   //do rotation for at most 4 times
            if(compare(mat, target)) return true;                               //check matrixs are the same
            rotate(mat);                                                        //do the rotation
        }

        return false;
    }
};
