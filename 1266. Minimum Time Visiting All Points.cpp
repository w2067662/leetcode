//https://leetcode.com/problems/minimum-time-visiting-all-points/description/

class Solution {
public:
    int minTimeToVisitAllPoints(vector<vector<int>>& points) {
        if(points.size() == 1)return 0;     //only one point -> times = 0
        int times=0;

        int x = points[0][0];               //set x = first point's x
        int y = points[0][1];               //set y = first point's y

        for(int i=1, distanceX, distanceY, smallerDis;i<points.size();i++){ //ex: [1,1] -> [3,4]
            distanceX = abs(points[i][0]-x);                                //distanceX = |3-1| = 2
            distanceY = abs(points[i][1]-y);                                //distanceY = |4-1| = 3

            times += distanceX + distanceY - min(distanceX, distanceY);     //walk up-right 2 times and walk up 1 time (total 3 times)

            x = points[i][0];               //set x = next point's x
            y = points[i][1];               //set y = next point's y
        }
        return times;
    }
};
