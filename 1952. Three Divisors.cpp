//https://leetcode.com/problems/three-divisors/description/

class Solution {
public:
    bool isThree(int n) {
        int count = 0;
        for(int i=1; i<=n; i++){          //i start from 1 to N
            if(n%i == 0)count++;
            if(count>3)return false;
        }

        return count == 3 ? true : false; //check numbers has exactly 3 divisors
    }
};
