//https://leetcode.com/problems/eliminate-maximum-number-of-monsters/description/

class Solution {
public:
    //ex1:  dist = [1,3,4], speed = [1,1,1]   ->   minutes = [1,3,4]
    //   minutes = [1,3,4]          ;  eliminate = 0
    //              ^        1 >  0 ;  eliminate = 1
    //                ^      3 >  1 ;  eliminate = 2
    //                  ^    4 >  2 ;  eliminate = 3  -> answer = 3
    //
    //ex2:  dist = [3,2,4], speed = [5,3,2]   ->   minutes = [1,1,2]
    //   minutes = [1,1,2]          ;  eliminate = 0
    //              ^        1 >  0 ;  eliminate = 1
    //                ^      1 <= 1 ;  eliminate = 1 (end the game) -> answer = 1
    //
    //explain:  
    //      Count the all the monsters' arrive minutes. Each time should have
    //  exactly 1 monster arrive. If there exist more than 1 monsters arrive
    //  at the same time (exist minutes[i] = minutes[j], i < j), then end the 
    //  game.
    //
    int eliminateMaximum(vector<int>& dist, vector<int>& speed) {
        vector<int> minutes;
        int eliminate = 0;

        for(int i=0;i<dist.size();i++){
            minutes.push_back(ceil(dist[i] / (double)speed[i])); //count the arrive minutes of all the monsters
        }

        sort(minutes.begin(), minutes.end());                    //sort the minutes vector in ascending order

        for(const auto minute: minutes){
            if(eliminate >= minute) break;                       //IF   eliminate >= minute -> not able to eliminate more than 1 monster at the same time -> end the game
            eliminate++;                                         //ELSE -> eliminate 1 monster
        }

        return eliminate;
    }
};
