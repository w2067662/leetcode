//https://leetcode.com/problems/evaluate-reverse-polish-notation/description/

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> stack;
        int left, right;

        for (const auto& token : tokens) {                                       // Traverse all the tokens in vector
            if (isdigit(token[0]) || (token.size() > 1 && isdigit(token[1]))) {  //     IF token is number
                stack.push(stoi(token));                                         //        -> push number into stack
            } else {                                                             //     IF token is operator
                right = stack.top();                                             //        -> get top element as right operand
                stack.pop();
                left  = stack.top();                                             //        -> get second top element as left perand
                stack.pop();

                if (token == "+") stack.push(left + right);                      //         IF token = "+" -> Adding
                else if (token == "-") stack.push(left - right);                 //         IF token = "-" -> Deducting
                else if (token == "*") stack.push(left * right);                 //         IF token = "*" -> Multiplying
                else if (token == "/") stack.push(left / right);                 //         IF token = "/" -> Dividing
            }
        }

        return stack.top();                                                      // Return the remain stack top as result
    }
};
