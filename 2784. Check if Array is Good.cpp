//https://leetcode.com/problems/check-if-array-is-good/description/

class Solution {
public:
    bool checkBase(vector<int>& nums){  //check Base N valid (Base N = [1, 2,..., N-1, N, N])
        if(nums[nums.size()-1]+1 != nums.size())return false; //check Base size = last element's value+1

        for(int i=0; i<nums.size()-1;i++){
            if(nums[i] != i+1) return false;
        }

        return nums[nums.size()-1] == nums[nums.size()-2]; //check last number equals to second last number
    }

    bool isGood(vector<int>& nums) {
        sort(nums.begin(), nums.end()); //sort the vector in ascending order
        return checkBase(nums); 
    }
};
