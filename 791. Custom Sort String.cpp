//https://leetcode.com/problems/custom-sort-string/description/

class Solution {
public:
    string customSortString(string order, string s) {
        map<char, int> map;

        for(char ch='a'; ch<='z'; ch++){
            map[ch] = 27;                                           // Set chars 'a' from 'z' with value 27
        }

        for(int i=0; i<order.length(); i++){
            map[order[i]] = i;                                      // Set chars in order from given string
        }                                                           //  smaller value (left), larger value (right)

        sort(s.begin(), s.end(), [&](const auto& a, const auto& b){
            return map[a] < map[b];                                 // Sort the string chars following the order
        });

        return s;
    }
};