//https://leetcode.com/problems/make-array-zero-by-subtracting-equal-amounts/description/

class Solution {
public:
    int minimumOperations(vector<int>& nums) {
        sort(nums.begin(), nums.end()); //sort the numbers in ascending order

        int last = 0, ans = 0;

        for(const auto num:nums){
            if(last < num) ans++;       //IF   number is strictly increasing -> answer+1
            last = num;
        }

        return ans;
    }
};
