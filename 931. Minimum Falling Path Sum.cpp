//https://leetcode.com/problems/minimum-falling-path-sum/description/

class Solution {
public:
    //ex: matrix = [[2,1,3],
    //              [6,5,4],
    //              [7,8,9]]
    //
    //   matrix' = [[2,1,3],
    //              [7,6,5],
    //              [13,14,14]]
    //
    bool isValid(vector<vector<int>>& matrix, int i, int j){ //check current position is valid or not
        return 0 <= i && i < matrix.size() && 0 <= j && j < matrix[i].size();
    }

    int minFallingPathSum(vector<vector<int>>& matrix) {
        int ans = INT_MAX;

        for(int i=1; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                int minPathSum = INT_MAX;

                for(int range = -1; range <= 1; range++){
                    if(isValid(matrix, i-1, j+range)) minPathSum = min(minPathSum, matrix[i-1][j+range]); //get the min path sum of current row
                }

                matrix[i][j] += minPathSum;                                                               //add up the min path sum
            }
        }

        for(int i=0; i<matrix[matrix.size()-1].size(); i++) ans = min(ans, matrix[matrix.size()-1][i]);   //find the min path sum from last row

        return ans;
    }
};
