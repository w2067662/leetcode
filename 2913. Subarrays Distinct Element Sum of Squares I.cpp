//https://leetcode.com/problems/subarrays-distinct-element-sum-of-squares-i/description/

class Solution {
public:
    int sumCounts(vector<int>& nums) {
        vector<int> distinct;
        set<int> set;
        int ans = 0;

        for(int i=1; i<=nums.size(); i++){        //i (length) start from 1 to number vector's size
            for(int j=0; j+i-1<nums.size();j++){  //get all the subarrays with size i
                set.clear();
                for(int k=j; k<=j+i-1; k++){      //traverse all the number in current subarray
                    set.insert(nums[k]);          //  -> insert current number into set
                }
                
                distinct.push_back(set.size());   //push amounts of distinct number of current subarray into distinct vector
            }
        }

        for(const auto val: distinct){
            ans += pow(val, 2);                   //calculate the sum of squares of all the value in distinct vector
        }

        return ans;
    }
};
