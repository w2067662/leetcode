//https://leetcode.com/problems/intersection-of-two-arrays/description/

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> set1, set2;
        vector<int> interSection;

        for(const auto& num: nums1){
            set1.insert(num);                   // Insert numbers from nums1 into set1
        }

        for(const auto& num: nums2){
            set2.insert(num);                   // Insert numbers from nums2 into set2
        }

        for(const auto& a: set1){
            for(const auto& b: set2){
                if(a == b){                     // IF found intersection numbers
                    interSection.push_back(a);  //    -> insert into answer
                }
            }
        }

        return interSection;
    }
};