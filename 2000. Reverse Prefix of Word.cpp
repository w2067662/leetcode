//https://leetcode.com/problems/reverse-prefix-of-word/description/

class Solution {
public:
    void reverse(string& s, int left, int right) { // Reverse string from index [left] to index [right]
        while (left < right) {                     //   WHILE  left < right
            swap(s[left++], s[right--]);           //          -> Swap left and right
                                                   //          -> Left  goto next (+1)
                                                   //          -> Right goto next (-1)
        }
    }

    string reversePrefix(string word, char ch) {
        for(int i=0; i<word.size(); i++){          // Traverse the chars within word
            if(word[i] == ch){                     //   IF current char is target
                reverse(word, 0, i);               //      -> Reverse the prefix of the word
                break;
            }
        }

        return word;
    }
};
