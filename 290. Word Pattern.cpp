//https://leetcode.com/problems/word-pattern/description/

class Solution {
public:
    bool checkValidPattern(vector<char>& letters, vector<string>& words){
        if(letters.size() != words.size()) return false;

        map<char, string> L2W;
        map<string, char> W2L;

        for(int i=0; i<letters.size(); i++){
            if(L2W.find(letters[i]) == L2W.end() && W2L.find(words[i]) == W2L.end()){       //IF   current letter and current word is not mapped
                L2W[letters[i]] = words[i];                                                 //      -> map current letter and current word (bijection)
                W2L[words[i]] = letters[i];
            } else if(L2W.find(letters[i]) != L2W.end() && W2L.find(words[i]) != W2L.end()){//IF   current letter and current word are both mapped
                if(L2W[letters[i]] != words[i] || W2L[words[i]] != letters[i]) return false;//      -> check current letter and current word are mapped to each other or not 
            } else return false;                                                            //IF   current letter or current word, 1 is mapped 
        }                                                                                   //      -> FALSE

        return true;
    }

    bool wordPattern(string pattern, string s) {
        vector<char> letters;
        vector<string> words;
        string temp ="";

        for(const auto ch: pattern){             //store the letters into vector from pattern
            letters.push_back(ch);
        }

        for(const auto ch: s){                   //store the words into vector form sentence
            if(ch == ' '){
                words.push_back(temp);
                temp = "";
            }else temp +=ch;
        }
        if(temp != "") words.push_back(temp);

        return checkValidPattern(letters, words);//check the pattern is valid
    }
};
