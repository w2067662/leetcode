//https://leetcode.com/problems/complex-number-multiplication/description/

class Solution {
public:
    pair<int, int> getRealAndImaginary(string num){ //convert complex number string to real number and imaginary number
        int real, imaginary;
        int temp = 0;
        bool isNegative = false;

        for(const auto ch: num){
            switch(ch){
                case '+':
                    real = isNegative ? -temp : temp;
                    temp = 0;
                    isNegative = false;
                    break;
                case '-':
                    isNegative = true;
                    break;
                case 'i':
                    imaginary = isNegative ? -temp : temp;
                    temp = 0;
                    isNegative = false;
                    break;
                default:
                    if('0' <= ch && ch <= '9') temp = temp*10 + ch - '0';
                    break;
            }
        }

        return {real, imaginary};
    }

    string complexNumberMultiply(string num1, string num2) {
        int real, imaginary;
        int R1, R2, I1, I2;
        pair<int, int> pair;

        pair = getRealAndImaginary(num1);
        R1 = pair.first, I1 = pair.second;
        pair = getRealAndImaginary(num2);
        R2 = pair.first, I2 = pair.second;

        real      = R1*R2 - I1*I2; //calculate multipled real      number
        imaginary = R1*I2 + R2*I1; //calculate multipled imaginary number

        return to_string(real) + "+" + to_string(imaginary) + "i";
    }
};
