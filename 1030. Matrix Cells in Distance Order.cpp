//https://leetcode.com/problems/matrix-cells-in-distance-order/description/

class Solution {
private:
    int Imax;
    int Jmax;
public:
    bool isValid(int i, int j){             //check next step is valid
        return i>=0 && i<=Imax && j>=0 && j<=Jmax;
    }

    vector<vector<int>> allCellsDistOrder(int rows, int cols, int rCenter, int cCenter) {
        vector<vector<bool>> visited (rows, vector<bool>(cols, false));
        vector<vector<int>> ans;
        queue<pair<int, int>> queue;
        pair<int, int> curr;
        vector<int> steps={1,0,-1,0,1};
        Imax = rows-1;
        Jmax = cols-1;

        queue.push({rCenter, cCenter});     //push center position into queue
        visited[rCenter][cCenter] = true;   //mark center position visited

        while(!queue.empty()){      //if queue not empty
            curr = queue.front();   //get the front element from queue
            queue.pop();            //pop the front element

            for(int i=0;i<4;i++){   //next step of current position(up, down, left, right)
                if(isValid(curr.first+steps[i], curr.second+steps[i+1]) &&       //if next step is valid AND
                    !visited[curr.first+steps[i]][curr.second+steps[i+1]]){      //not visited
                    queue.push({curr.first+steps[i], curr.second+steps[i+1]});   //push next step into queue
                    visited[curr.first+steps[i]][curr.second+steps[i+1]] = true; //mark visited
                }
            }
            ans.push_back({curr.first, curr.second}); //push current position into vector
        }

        return ans;
    }
};
