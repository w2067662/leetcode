//https://leetcode.com/problems/most-frequent-number-following-key-in-an-array/description/

class Solution {
public:
    int mostFrequent(vector<int>& nums, int key) {
        map<int, int> map;
        vector<pair<int, int>> target;

        for(int i=0; i<=nums.size()-2;i++){
            if(nums[i] == key)map[nums[i+1]]++;     //if current number = key -> count for target's (next number) appear times
        }

        for(const auto it:map){
            target.push_back({it.second, it.first});//store the appear times of targets from map into vector
        }

        sort(target.begin(), target.end());         //sort the target in ascending order

        return target[target.size()-1].second;      //return the target that appears the most frequently
    }
};
