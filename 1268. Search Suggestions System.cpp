//https://leetcode.com/problems/search-suggestions-system/description/

class Trie {
private:
    class TrieNode{ //define TrieNode
        public:
            char ch;
            map<char, TrieNode*> children; //with multiple children node
            TrieNode(){};
            TrieNode(char c):ch(c){};
    };
    
    TrieNode* root;
public:
    Trie() {
        this->root = new TrieNode; //initialize with creating new root
    }
    
    void insert(const string &word) {
        insertWord(this->root, word, 0);
    }
 
    vector<string> suggest(const string &word){  //get the suggested words
        vector<string> words;
        suggestWords(this->root, word, 0, words);
        return words;
    }
    //--------------------------
    //   Supporting functions
    //--------------------------
    void insertWord(TrieNode* root, const string& word, int index){
        if(index >= word.length()){
            root->children['$'] = new TrieNode('$');                  //set the string end as '$'
            return;
        } 

        if(root->children.find(word[index]) == root->children.end()){ //IF   no child node's value match current char
            root->children[word[index]] = new TrieNode(word[index]);  //     -> create a new child node with current char value
        }
        insertWord(root->children[word[index]], word, index+1);       //insert next char
    }

    void suggestWords(TrieNode* root, const string& word, int index, vector<string> &words){ //find suggested words that contain current word as prefix
        if(index >= word.length()){                                          //IF  the end of prefix word TrieNode is found
            suggestFirstThree(root, word.substr(0, word.length()-1), words); //    -> push the first three suggested words into vector
            return;
        }       

        if(root->children.find(word[index]) == root->children.end()) return;
        suggestWords(root->children[word[index]], word, index+1, words);
    }

    void suggestFirstThree(TrieNode* root, string word, vector<string> &words){
        if(words.size() >= 3) return;                                  //IF   3 suggested words are found -> end the function

        if(root->ch == '$'){                                           //IF   found the end of a word in trie (with '$' as the of a word)
            words.push_back(word);                                     //     -> push the word into vector
        }

        for(const auto child: root->children){
            suggestFirstThree(child.second, word + root->ch , words);  //traverse next TrieNode lexicographically 
        }
    }
};

class Solution {
public:
    vector<vector<string>> suggestedProducts(vector<string>& products, string searchWord) {
        vector<vector<string>> ans;
        Trie trie;

        for(const auto product: products) trie.insert(product);  //insert products into trie

        for(int i=1; i<=searchWord.length(); i++){
            ans.push_back(trie.suggest(searchWord.substr(0,i))); //get the first three suggest words from trie for each typed letter
        }

        return ans;
    }
};
