//https://leetcode.com/problems/percentage-of-letter-in-string/description/

class Solution {
public:
    int percentageLetter(string s, char letter) {
        map<char, int> map;

        for(const auto ch: s) map[ch]++;  //store the char's appear times into map

        return map[letter]*100/s.length();//calculate the percentage of letters appear times
    }
};
