//https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/

class Solution {
private:
    vector<string> DigitToLetter;
public:
    void initialize(){  //store the correspond letters to digit map
        DigitToLetter.push_back("");        //digit = 0
        DigitToLetter.push_back("");        //digit = 1
        DigitToLetter.push_back("abc");     //digit = 2
        DigitToLetter.push_back("def");     //digit = 3
        DigitToLetter.push_back("ghi");     //digit = 4
        DigitToLetter.push_back("jkl");     //digit = 5
        DigitToLetter.push_back("mno");     //digit = 6
        DigitToLetter.push_back("pqrs");    //digit = 7
        DigitToLetter.push_back("tuv");     //digit = 8
        DigitToLetter.push_back("wxyz");    //digit = 9
    }

    vector<string> letterCombinations(string digits) {
        if(digits == "") return {};

        initialize();   //initialize DigitToLetter

        vector<string> ans; //for answer return

        //count for answer strings, and then create N empty string
        int howManyStrings = 1; 
        for(int i=0;i<digits.length();i++){
            howManyStrings = howManyStrings * DigitToLetter[digits[i]-'0'].length();  
        }
        for(int i=0;i<howManyStrings;i++){
            ans.push_back("");
        }
            //***Solution concept***
            //ex: digits ="5678"
            //5 -> "jkl"  -> each letter have to repeat 3 x 4 x 3 = 36 times
            //6 -> "mno"  -> 3 letters
            //7 -> "pqrs" -> 4 letters
            //8 -> "tuv"  -> 3 letters
            //ans = ["j"...(36 "j")..."j", "k"...(36 "k")..."k","l"...(36 "l")..."l"]

            //6 -> "mno"  -> each letter have to repeat 4 x 3 = 12 times
            //7 -> "pqrs" -> 4 letters
            //8 -> "tuv"  -> 3 letters
            //ans = ["jm"...(12 "jm")..."jm","jn"...(12 "jn")..."jn","jo"...(12 "jo")..."jo","km"...(12 "km")..."km"]

            //7 -> "pqrs" -> each letter have to repeat 3 times
            //8 -> "tuv"  -> 3 letters
            //ans = ["jmp"...(3 "jmp")..."jmp","jmq"...(3 "jmq")..."jmq","jmr"...(3 "jmr")..."jmr","jms"...(3 "jms")..."jms"]

            //8 -> "tuv"  -> each letter have to repeat 1 time
            //ans = ["jmpt","jmpu","jmpv","jmqt","jmqu","jmqv","jmrt","jmru","jmrv"...]

        for(int i=0;i<digits.length();i++){ //start from first digit
            //count for repeat times of each letter in  DigitToLetter map
            int count=1;
            if(i!=digits.length()-1){   //condition for last digit, last digit will always repeat 1 time
                for(int j=i+1;j<digits.length();j++){
                    count = count*DigitToLetter[digits[j]-'0'].length(); 
                }
            } 

            int NthLetter = 0;
            int repeatCount = 0;
            for(int n=0;n<ans.size();n++){
                ans[n]+=DigitToLetter[digits[i]-'0'][NthLetter];    //append the letter to answer strings

                repeatCount++;  //***count for each repeat***
                
                if(repeatCount >= count){   //if repeat is bigger than count, than reset repeatCount to 0
                    NthLetter++;
                    repeatCount = 0;
                } 
                if(NthLetter >=DigitToLetter[digits[i]-'0'].length()){  //repeat the letter cycle of each digit
                    NthLetter=0;
                } 
            }
        }
        return ans;
    }
};
