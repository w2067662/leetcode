//https://leetcode.com/problems/two-city-scheduling/description/

class Solution {
public:
    // ex:   costs = [[10,20],[30,200],[400,50],[30,20]]
    
    //   diffs  = [. 10,170,-350,-10]
    //   diffs' = [-350,-10,  10,170] (sorted)
    //
    //   minCost  = 10+30+400+30 = 470 
    //   minCost' = 470-350-10 = 110
    
    int twoCitySchedCost(vector<vector<int>>& costs) {
        vector<int> diffs;
        int minCost = 0;

        for(const auto cost: costs){
            minCost += cost[0];                    // Sum up min cost
            diffs.push_back(cost[1]-cost[0]);      // Push difference of each cost pair into vector
        }

        sort(diffs.begin(), diffs.end());          // Sort the differences in ascending order

        for(int i=0; i<costs.size()/2; i++){
            minCost += diffs[i];                   // Deduct the min cost by smaller values
        }

        return minCost;
    }
};
