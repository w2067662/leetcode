//https://leetcode.com/problems/maximum-or/description/

class Solution {
public:
    //ex:  nums = [2,1,3,4,7,2,7] ; k = 2
    //
    //     left = [2,3,3,7,7,7,7]
    //    right = [7,7,7,7,7,7,7]
    //                                                        ; answer = 0
    //             ^               2 | 2 * pow(2,2) | 7 = 15  ; answer = max(0,15)  = 15
    //               ^             3 | 1 * pow(2,2) | 7 =  7  ; answer = max(15,7)  = 15
    //                 ^           3 | 3 * pow(2,2) | 7 = 15  ; answer = max(15,15) = 15
    //                   ^         7 | 4 * pow(2,2) | 7 = 23  ; answer = max(15,23) = 23
    //                     ^       7 | 7 * pow(2,2) | 7 = 31  ; answer = max(23,31) = 31
    //                       ^     7 | 2 * pow(2,2) | 7 = 15  ; answer = max(31,15) = 31
    //                         ^   7 | 7 * pow(2,2) | 7 = 31  ; answer = max(31,31) = 31
    //
    long long maximumOr(vector<int>& nums, int k) {
        vector<long long> left(nums.size(), 0), right(nums.size(), 0);
        long long ans = 0, temp = 0;

        for(int i=1; i<nums.size(); i++){
            left[i] = nums[i-1] | left[i-1];            // Calculate left  OR 
        }

        for(int i=nums.size()-2; i>=0; i--){
            right[i] = nums[i+1] | right[i+1];          // Calculate right OR 
        }

        for(int i = 0; i < nums.size(); i++) {
            long long num = nums[i];
            num *= pow(2, k);                           // Multiply current number by 2^k
            ans = max(ans, (left[i] | num | right[i])); // Calculate the result and store the max OR
        }

        return ans;
    }
};
