//https://leetcode.com/problems/diameter-of-binary-tree/description/

class Solution {
public:
    int depth(TreeNode* root){  // Get depth of tree
        return !root ? 0 : max(depth(root->left), depth(root->right)) + 1; 
    }

    void recurrence(TreeNode* root, int& ans){ // Recursion to get max diameter
        if(!root) return;

        ans = max(ans, depth(root->left) + depth(root->right));

        recurrence(root->left , ans);
        recurrence(root->right, ans);
        return;
    }

    int diameterOfBinaryTree(TreeNode* root) {
        int ans = 0;
        recurrence(root, ans); // Recursion to get the answer
        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
