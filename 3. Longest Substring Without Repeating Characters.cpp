//https://leetcode.com/problems/longest-substring-without-repeating-characters/description/

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.size();
        int ans = 0;
        unordered_map<int, int> newIndex;
        int i = 0;
        
        for (int j=0; j<n; ++j) {
            if(newIndex.count(s[j]) > 0 ){   // if s[j] was seen before, update left pointer  
                i = max(i, newIndex[s[j]] + 1);                
            }
                      
          ans = max(ans, j - i + 1);      
          newIndex[s[j]] = j; //always keep track of the new index of s[j];
        }
        return ans;
    }
};
