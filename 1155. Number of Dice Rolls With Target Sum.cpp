class Solution {
private:
    const static int MOD = 1e9 + 7; //define Modulo

public:
    int numRollsToTarget(int n, int k, int target) {
        int dp[n+1][target+1];       //setup danamic programing matrix
        memset(dp, 0, sizeof(dp));   //for large memory spaces of danamic programing matrix
        dp[0][0] = 1;

        for(int i = 1; i <= n; i++){                                                   //for 1 ~ N dices
            for(int j = 1; j <= target; j++){                                          //for 1 ~ target
                for(int currFace = 1; currFace <= k; currFace++){                      //for 1 ~ current faces
                    if(currFace <= j){
                        dp[i][j] = (dp[i][j] % MOD + dp[i-1][j-currFace] % MOD) % MOD; //current slot = current slot + slot[dice-1][currTarget-currentFace]
                    }
                }
            }
        }

        return dp[n][target];
    }
};
