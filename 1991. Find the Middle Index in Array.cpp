//https://leetcode.com/problems/find-the-middle-index-in-array/description/

class Solution {
public:
    int findMiddleIndex(vector<int>& nums) {
        int leftSum = 0, rightSum = 0;

        for(const auto num: nums) rightSum += num; //sum up rightSum

        for(int i=0;i<nums.size();i++){
            if(i != 0)leftSum  += nums[i-1];       //calculate leftSum and rightSum
            rightSum -= nums[i];

            if(leftSum == rightSum) return i;      //IF   leftSum = rightSum, current index is the answer
        }

        return -1;                                 //no possible answer found
    }
};
