//https://leetcode.com/problems/two-sum-iv-input-is-a-bst/description/

class Solution {
private:
    vector<int> numbers;
public:
    void traverse(TreeNode* root){
        if(!root)return;
        numbers.push_back(root->val);
        traverse(root->left );
        traverse(root->right);
        return;
    }
    bool findTarget(TreeNode* root, int k) {
        traverse(root);                                  //store all the nodes' value into vector
        for(int i=0;i<numbers.size()-1;i++){             //find two elements sum = k
            for(int j=i+1;j<numbers.size();j++){
                if(numbers[i]+numbers[j]==k)return true;
            }
        }
        return false;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
