//https://leetcode.com/problems/sum-of-unique-elements/description/

class Solution {
public:
    int sumOfUnique(vector<int>& nums) {
        map<int, int> map;  //map = {[number]: [appear times]}
        int ans = 0;
        
        for(auto num: nums) map[num]++;         //store the number into map

        for(auto it: map) {
            if(it.second == 1) ans += it.first; //add up the unique numbers
        }

        return ans;
    }
};
