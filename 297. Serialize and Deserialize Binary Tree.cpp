//https://leetcode.com/problems/serialize-and-deserialize-binary-tree/description/

class Codec {
private:
    //----------------------
    // Supporting Functions
    //----------------------
    void toPreorderString(TreeNode* root, string &s){ //convert the tree to preorder string
        if(root){
            s += to_string(root->val);
            s += ',';
            toPreorderString(root->left , s);
            toPreorderString(root->right, s);
        } else {
            s += 'N';                                 //represent NULL as 'N'
        }
        return;
    }
    
    vector<int> toPreorderVector(string data){        //convert preorder string to vector
        vector<int> preorder;
        string temp = "";

        for(const auto ch: data){
            switch(ch){
                case ',':
                    preorder.push_back(stoi(temp));
                    temp = "";
                    break;
                case 'N':
                    preorder.push_back(INT_MIN);      //represent 'N' (NULL) as INT_MIN
                    break;
                default:
                    temp += ch;
            }
        }

        return preorder;
    }

    TreeNode* buildTree(vector<int> &preorder, int &curr){  //build the tree by preorder data
        if(curr >= preorder.size()) return NULL;
        if(preorder[curr] == INT_MIN) return NULL;          //IF value of current node is INT_MIN -> NULL

        TreeNode* root = new TreeNode(preorder[curr]);
        root->left  = buildTree(preorder, ++curr);
        root->right = buildTree(preorder, ++curr);
        return root;
    }

public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string data = "";
        toPreorderString(root, data);
        return data;
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        vector<int> preorder = toPreorderVector(data);
        int curr = 0;
        return buildTree(preorder, curr);
    }

};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

// Your Codec object will be instantiated and called as such:
// Codec ser, deser;
// TreeNode* ans = deser.deserialize(ser.serialize(root));
