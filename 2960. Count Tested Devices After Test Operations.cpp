//https://leetcode.com/problems/count-tested-devices-after-test-operations/description/

class Solution {
public:
    int countTestedDevices(vector<int>& batteryPercentages) {
        int testedDevices = 0, decrease = 0;
        
        for(int i=0; i<batteryPercentages.size(); i++){
            batteryPercentages[i] -= decrease;          //deduct current decrease

            if(batteryPercentages[i] > 0){              //IF current batter percentages > 0
                testedDevices++;                        //   -> tested devices +1
                decrease++;                             //   -> decrease +1
            }
        }

        return testedDevices;
    }
};
