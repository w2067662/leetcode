//https://leetcode.com/problems/smallest-missing-integer-greater-than-sequential-prefix-sum/description/

class Solution {
public:
    int missingInteger(vector<int>& nums) {
        int sum = nums[0];

        for(int i=1; i<nums.size(); i++){
            if(nums[i]-nums[i-1] == 1) sum += nums[i]; // Get the sequential prefix sum
            else break;
        }

        sort(nums.begin(), nums.end());                // Sort the vector in ascending order

        for(const auto num: nums){
            if(sum == num) sum++;                      // IF  value of sum exist in vector -> sum+1
        }

        return sum;
    }
};
