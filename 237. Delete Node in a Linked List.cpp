//https://leetcode.com/problems/delete-node-in-a-linked-list/description/

class Solution {
public:
    //ex:
    //      -> 0 -> 1 -> 2 -> 3 ->
    //              ^
    //          delete node

    //      -> 0 -> 2 -> 2 -> 3 ->  (Step1: Set value of next node as value of current node)
    

    //      -> 0 -> 2    2 -> 3 ->  (Step2: Set next next node as current's next)
    //              |_________^

    //      -> 0 -> 2 -> 3 ->       (Result)

    void deleteNode(ListNode* node) {
        node->val  = node->next->val;   // Set value of next node as value of current node 
        node->next = node->next->next;  // Set next next node as current's next
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */