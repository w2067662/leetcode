//https://leetcode.com/problems/snail-traversal/description/

/**
 * @param {number} rowsCount
 * @param {number} colsCount
 * @return {Array<Array<number>>}
 */
Array.prototype.snail = function(rowsCount, colsCount) {
    if (rowsCount * colsCount !== this.length) return [];                            //IF array size != matrix size 
                                                                                     //   -> return empty array

    let matrix = Array.from({ length: rowsCount }, () => []);                        //initialize matrix with M rows

    let currRow = 0;
    let down = true;

    for (const num of this) {                                                        //for the number in the array
        down ? matrix[currRow].push(num) : matrix[rowsCount - currRow - 1].push(num);//push the number into current row

        currRow++;                                                                   //goto next row
        if (currRow >= rowsCount) {                                                  //IF  current row is out of range
            currRow = 0;                                                             //    -> reset current row
            down = !down;                                                            //    -> go opposite way
        }
    }

    return matrix;
};

/**
 * const arr = [1,2,3,4];
 * arr.snail(1,4); // [[1,2,3,4]]
 */
