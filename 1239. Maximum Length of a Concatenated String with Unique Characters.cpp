//https://leetcode.com/problems/maximum-length-of-a-concatenated-string-with-unique-characters/description/

class Solution {
public:
    //ex:  arr = ["un","iq","ue"]
    //             ^                str = "un"      ;  maxLen = 2
    //             ^    ^           str = "uniq"    ;  maxLen = 4
    //             ^    ^    ^      str = "unique"  ;  maxLen = 4  (contain 2 'u')
    //                  ^           str = "iq"      ;  maxLen = 4
    //                  ^    ^      str = "ique"    ;  maxLen = 4
    //                       ^      str = "ue"      ;  maxLen = 4
    //
    int findMaxLength(vector<string>& arr, string s, int index){
        set<char> set(s.begin(), s.end());                        // Store current string into set

        if(s.length() != set.size()) return 0;                    // IF current string exist duplicate char
                                                                  //    -> return 0

        int ans = s.length();

        for(int i=index+1; i<arr.size(); i++){                    // For current index+1 to arr.size
            ans = max(ans, findMaxLength(arr, s + arr[i], i));    //    Get the max length of concatenation strings by recursion
        }

        return ans;
    }

    int maxLength(vector<string>& arr) {
        return findMaxLength(arr, "", 0);                         // Find the max length of concatenation formed by string array with unique chars
    }
};
