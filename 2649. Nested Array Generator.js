//https://leetcode.com/problems/nested-array-generator/description/

/*
Syntax:
    yield* [expression]  ->  [expression] (Optional): An iterable object.
                       ->  Returns the value returned by that iterator when it's closed (when done is true).

Example:

function* g1() {
  yield 2;
  yield 3;
  yield 4;
}

function* g2() {
  yield 1;
  yield* g1();
  yield 5;
}

var iterator = g2();

console.log(iterator.next()); // { value: 1, done: false }
console.log(iterator.next()); // { value: 2, done: false }
console.log(iterator.next()); // { value: 3, done: false }
console.log(iterator.next()); // { value: 4, done: false }
console.log(iterator.next()); // { value: 5, done: false }
console.log(iterator.next()); // { value: undefined, done: true }
*/

/**
 * @param {Array} arr
 * @return {Generator}
 */
var inorderTraversal = function*(arr) {
    for (const element of arr) {                                      // Traverse the elements in the array
        if (Array.isArray(element)) yield* inorderTraversal(element); // IF element is a array -> do recursion to current elemet
        else yield element;                                           // ELSE                  -> return the value
    }
};

/**
 * const gen = inorderTraversal([1, [2, 3]]);
 * gen.next().value; // 1
 * gen.next().value; // 2
 * gen.next().value; // 3
 */
