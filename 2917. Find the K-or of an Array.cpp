//https://leetcode.com/problems/find-the-k-or-of-an-array/description/

class Solution {
public:
    int findKOr(vector<int>& nums, int k) {
        int ans = 0, count;

        for(int i=0; i<32; i++){              //i start from 0 to 31 -> 2^0 ~ 2^31
            count = 0;

            for(const auto x: nums){
                if((1<<i) & x) count++;       //IF  (2^i & x) == 2^i -> count+1
            }

            if(count >= k) ans += pow(2, i);  //IF  current bits is set in at least k elements of the array
        }                                     //    -> answer + 2^i

        return ans;
    }
};
