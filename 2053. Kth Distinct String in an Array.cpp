//https://leetcode.com/problems/kth-distinct-string-in-an-array/description/

class Solution {
public:
    string kthDistinct(vector<string>& arr, int k) {
        vector<pair<int, string>> vec;               //vec = { {[index1], [string1]}, {[index2], [string2]}, ... }
        map<string, vector<int>> map;                //map = { [string]: vector{[index1], [index2], ...} }

        for(int i=0; i<arr.size();i++){
            map[arr[i]].push_back(i);                //store string into map
        }

        for(const auto it: map){
            if(it.second.size()  == 1) vec.push_back({it.second[0], it.first}); //store the distinct string into vector
        }

        sort(vec.begin(), vec.end());                //sort the vector by index in ascending order

        return vec.size() < k ? "" : vec[k-1].second;//return the K th distinct string OR return empty string if not exist
    }
};
