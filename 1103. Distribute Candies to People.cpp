//https://leetcode.com/problems/distribute-candies-to-people/description/

class Solution {
public:
    vector<int> distributeCandies(int candies, int num_people) {
        vector<int> ans(num_people, 0);
        int currentCandies=1;
        int currentIndex=0;

        while(candies>0){   //end when all candies are distributed
            if(candies >= currentCandies){      //lefted candies is more than current distribute candies
                ans[currentIndex]+=currentCandies;
                candies-=currentCandies;
            }
            else{
                ans[currentIndex]+=candies;
                candies-=candies;
            }
            currentCandies++;
            currentIndex++;
            currentIndex = currentIndex >= num_people? 0:currentIndex; //if current index is bigger than num_people, set index to 0
        }

        return ans;
    }
};
