//https://leetcode.com/problems/uncommon-words-from-two-sentences/description/

class Solution {
public:
    vector<string> getWords(string s){  //get the words from sentence
        vector<string> words;
        string temp="";

        for(int i=0;i<s.length();i++){
            if(s[i]==' '){
                words.push_back(temp);
                temp="";
            }else temp+=s[i];
        }
        words.push_back(temp);

        return words;
    }

    vector<string> uncommonFromSentences(string s1, string s2) {
        map<string, int> map;
        vector<string> ans;

        for(int i=0;i<getWords(s1).size();i++) map[getWords(s1)[i]]++;  //insert { [word]:[appear times] } into map

        for(int i=0;i<getWords(s2).size();i++) map[getWords(s2)[i]]++;  //insert { [word]:[appear times] } into map

        for(const auto it:map){
            if(it.second<2) ans.push_back(it.first);    //find uncommon words and store into answers
        }

        return ans;
    }
};
