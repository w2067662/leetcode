//https://leetcode.com/problems/minimum-absolute-difference-in-bst/description/

class Solution {
public:
    void recurrsion(TreeNode* root, vector<int>& vec){  //do recurrsion to traverse the tree
        if(!root)return;
        vec.push_back(root->val);                       //store all the nodes value from tree into vec
        recurrsion(root->left ,  vec);  
        recurrsion(root->right,  vec);
        return;
    }

    int getMinimumDifference(TreeNode* root) {
        if(!root)return 0;                               //return 0, if no root

        int min = INT_MAX;                               //set min as int max
        vector<int> vec;                                 //vector for nodes value

        recurrsion(root, vec);                           //do recurrsion and store all the nodes value into vec
        sort(vec.begin(), vec.end());                    //ascending sorting vector

        for(int i=0, j=i+1;i<vec.size()-1;i++, j++){
            if((vec[j]-vec[i])<min) min = vec[j]-vec[i]; //find min absolute difference
        }

        return min;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
