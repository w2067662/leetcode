//https://leetcode.com/problems/seat-reservation-manager/description/

class SeatManager {
private:
    priority_queue<int, vector<int>, greater<int>> pq; //sorting in ascending order

public:
    SeatManager(int n) {
        for(int i=1; i<=n; i++) pq.push(i); //initialize priority queue
    }
    
    int reserve() {
        int seatNumber = pq.top();          //get the top of priority queue as reserved seat number
        pq.pop();                           //pop the top element from priority queue
        return seatNumber;
    }
    
    void unreserve(int seatNumber) {
        pq.push(seatNumber);                //push seat unreserved seat number into priority queue
    }
};

/**
 * Your SeatManager object will be instantiated and called as such:
 * SeatManager* obj = new SeatManager(n);
 * int param_1 = obj->reserve();
 * obj->unreserve(seatNumber);
 */
