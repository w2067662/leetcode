//https://leetcode.com/problems/minimum-difference-between-highest-and-lowest-of-k-scores/description/

class Solution {
public:
    int minimumDifference(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end());       //sort the vector in ascending order

        int ans = INT_MAX;
        int temp;

        for(int i=0; i+k-1<nums.size(); i++){ //get K numbers as current group
            temp = nums[i+k-1] - nums[i];     //calculate the difference of current group 
            ans = min(ans, temp);             //store the min difference
        }

        return ans;
    }
};
