//https://leetcode.com/problems/find-the-town-judge/description/

class Solution {
public:
    //ex1: n = 2, trust = [[1,2]]
    //              1  2
    //trust_map = [[F, T    1
    //              F, F]]  2
    //answer = 2 (row 2 are ALL 'F', and column 2 (without row 2) are ALL 'T') 
    //---------------------------------------------
    //ex2: n = 3, trust = [[1,3],[2,3]]
    //              1  2  3
    //trust_map = [[F, F, T    1
    //              F, F, T    2
    //              F, F, F]]  3
    //answer = 3 (row 3 are ALL 'F', and column 3 (without row 3) are ALL 'T')
    //---------------------------------------------
    int findJudge(int n, vector<vector<int>>& trust) {
        vector<vector<bool>> trust_map (n, vector<bool>(n, false)); //create trust map with initial false
        
        for(int i=0;i<trust.size();i++){                            //draw trust map
            trust_map[trust[i][0]-1][trust[i][1]-1]=true;
        }

        for(int i=0;i<trust_map.size();i++){
            bool  isTownJudge=true;
            for(int j=0;j<trust_map[i].size();j++){                 //check row values are all false
                if(trust_map[i][j]){
                    isTownJudge=false;
                    break;
                }
            }
            for(int k=0;k<trust_map.size();k++){                    //check column [i] values without current row are all true
                if(k!=i && !trust_map[k][i]){
                    isTownJudge=false;
                    break;
                }
            }
            if (isTownJudge) return i+1;                            //found town judge
        }

        return -1;                                                  //town judge not found
    }
};
