//https://leetcode.com/problems/split-the-array/description/

class Solution {
public:
    bool isPossibleToSplit(vector<int>& nums) {
        map<int, int> map;

         for(const auto& num: nums){
             map[num]++;                     // Store frequency of numbers into map
         }

         for(const auto it: map){
             if(it.second > 2) return false; // If frequency of current number > 2
                                             //    -> FALSE
         }

         return nums.size()%2 == 0;          // Check the amount of numbers is even or not 
                                             //  (able to split equally into 2 vectors)
    }
};
