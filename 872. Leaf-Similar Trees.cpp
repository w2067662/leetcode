//https://leetcode.com/problems/leaf-similar-trees/description/

class Solution {
public:
    bool isLeaf(TreeNode* root){                        //check is leaf node or not
        return !root->left && !root->right;
    }

    void traverse(TreeNode* root, vector<int>& vec){    //travese the tree
        if(!root)return;
        if(isLeaf(root)){                               //store root value if root is leaf node
            vec.push_back(root->val);
            return;
        }
        traverse(root->left , vec);
        traverse(root->right, vec);
        return;
    }

    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        vector<int> root_one_leaf;
        vector<int> root_two_leaf;

        traverse(root1, root_one_leaf);                 //traverse tree1
        traverse(root2, root_two_leaf);                 //traverse tree2

        if(root_one_leaf.size()!=root_two_leaf.size())return false; //check same amount of leaves

        for(int i=0;i<root_one_leaf.size();i++){                
            if(root_one_leaf[i]!=root_two_leaf[i])return false; //check similar leaf sequence or not
        }
        return true;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
