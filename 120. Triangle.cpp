//https://leetcode.com/problems/triangle/description/

class Solution {
public:
    //triangle =  [[2],
    //            [3,4],
    //            [6,5,7],
    //            [4,1,8,3]]

    //solution =  [[2],
    //            [5,6],
    //            [11,10,13],
    //            [15,*11,18,16]] <- buttom
    //ans = 11

    int minimumTotal(vector<vector<int>>& triangle) {
        if(triangle.size() == 0) return 0;              //return 0 , if no triangle
        if(triangle.size() == 1) return triangle[0][0]; //return the only number , if triangle only have 1 level
    
        for(int i=1;i<triangle.size();i++){ //traverse all the levels in triangle, level should be bigger than 1
            for(int j=0;j<triangle[i].size();j++){  //traverse all the numbers in each level, add up the min from last level
                if(j == 0){
                    triangle[i][j] += triangle[i-1][j]; //if encounter first number in level, only add up triangle[i-1][j]
                }
                else if (j == (triangle[i].size()-1)){  //if encounter last number in level, only add up triangle[i-1][j-1]
                    triangle[i][j] += triangle[i-1][j-1];
                }
                else{   //if encounter middle numbers in level, add up the smaller one (triangle[i-1][j-1] or triangle[i-1][j])
                    triangle[i][j] += min(triangle[i-1][j-1], triangle[i-1][j]);
                }
            }
        }

        int ans = triangle[triangle.size()-1][0]; //set answers as the first number of the last level in triangle
        for(int i=0;i<triangle[triangle.size()-1].size();i++){
            ans = min(ans, triangle[triangle.size()-1][i]); //find the min sum from the buttom of trangle
        }
        return ans;
    }
};
