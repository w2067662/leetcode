//https://leetcode.com/problems/reverse-linked-list/description/

class Solution {
public:
    //ex:  head = [1,2,3,4,5]

    //     1 -> 2 -> 3 -> 4 -> 5
    //     ^
    // node1,node2

    //        (tmp)
    //     1 -> 2 -> 3 -> 4 -> 5
    //     ^
    // node1,node2

    //      _________
    //    (|   tmp   |)
    //     1    2 -> 3 -> 4 -> 5
    //     ^
    // node1,node2

    //      _________
    //     |   tmp   |
    //     1(<-)2    3 -> 4 -> 5
    //     ^
    // node1,node2

    //      _________
    //     |   tmp   |
    //     1 <- 2    3 -> 4 -> 5
    //     ^    ^
    //   node2 (node1)
    
    //    tmp
    //     2 -> 1 -> 3 -> 4 -> 5
    //     ^    ^
    //  node1  node2

    // ...

    //    tmp
    //     5 -> 4 -> 3 -> 2 -> 1
    //     ^                   ^
    //  node1                node2


    ListNode* reverseList(ListNode* head) {
        if(!head) return NULL;
    
        ListNode *node1 = head, *node2 = head;  // Set node1, node2 as head
        ListNode *temp = NULL;
    
        while(node2->next){                     // While node2 has next
            temp = node2->next;                 //      Set temp as node2's next
            node2->next = node2->next->next;    //      Set node2's next as node2's next next
            temp->next = node1;                 //      Set temp's next as node1
            node1 = temp;                       //      Set node1 as temp
        }
        
        return node1;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

