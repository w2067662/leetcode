//https://leetcode.com/problems/distribute-elements-into-two-arrays-i/description/

class Solution {
public:
    vector<int> resultArray(vector<int>& nums) {
        vector<int> arr1, arr2;

        for(int i=0; i<nums.size(); i++){                        // For i from 0 to nums.size
            switch(i){
                case 0:                                          //     IF i = 0
                    arr1.push_back(nums[i]);                     //         -> Append nums[i] to array1
                    break;
                case 1:                                          //     IF i = 1
                    arr2.push_back(nums[i]);                     //         -> Append nums[i] to array2
                    break;
                default:                                         //     IF i > 1
                    arr1[arr1.size()-1] > arr2[arr2.size()-1] ?  //         IF last number of array1 > last number of array2
                        arr1.push_back(nums[i]) :                //             -> Append nums[i] to array1
                        arr2.push_back(nums[i]);                 //     ELSE    -> Append nums[i] to array2
            }
        }

        arr1.insert(arr1.end(), arr2.begin(), arr2.end());       // Append array2 to array1

        return arr1;
    }
};
