//https://leetcode.com/problems/backspace-string-compare/description/

class Solution {
public:
    string result(string s){    //process the string with backward and return the result
        string res = "";

        for(int i=1; i<s.length(); i++){
            if(s[i] == '#'){                //IF  encounter '#' -> do backward
                for(int j=i-1; j>=0; j--){  //    j start from i-1 to 0
                    if(s[j] != '#'){        //    IF  s[j] is not '#'
                        s[j] = '#';         //        -> backward current letter (mark as '#')
                        break;
                    }
                }
            }
        }

        for(int i=0; i<s.length(); i++){
            if(islower(s[i]))res += s[i];   //append non '#' letters to return string 
        }

        return res;
    }

    bool backspaceCompare(string s, string t) {
        return result(s) == result(t);
    }
};
