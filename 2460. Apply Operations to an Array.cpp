//https://leetcode.com/problems/apply-operations-to-an-array/description/

class Solution {
public:
    vector<int> applyOperations(vector<int>& nums) {

        for(int i=0;i<nums.size()-1;i++){              
            if(nums[i] == nums[i+1] && nums[i]!=0){     //find adjacent numbers that have same value
                nums[i]*=2;                             //[1,*2,*2,1,1,0] -> [1,*4,*0,1,1,0]
                nums[i+1]=0;
            }
        }

        for(int i=0;i<nums.size();i++){
            if(nums[i]==0){                             //shift left non-zero numbers
                for(int j=i+1;j<nums.size();j++){       //[1,4,0,*2,0,0] ->  [1,4,*2,0,0,0]
                    if(nums[j]!=0){
                         swap(nums[i], nums[j]);
                         break;
                    }
                }
            }
        }
        return nums;
    }
};
