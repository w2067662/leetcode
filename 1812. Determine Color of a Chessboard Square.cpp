//https://leetcode.com/problems/determine-color-of-a-chessboard-square/description/

class Solution {
public:
    bool squareIsWhite(string coordinates) {
        return (coordinates[0]-'a')%2 == 0 && (coordinates[1]-'0')%2 == 0 || //a, c, e, g with even number
               (coordinates[0]-'a')%2 != 0 && (coordinates[1]-'0')%2 != 0 ;  //b, d, f, h with  odd number
    }
};
