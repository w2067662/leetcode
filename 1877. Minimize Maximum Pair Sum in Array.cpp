//https://leetcode.com/problems/minimize-maximum-pair-sum-in-array/description/

class Solution {
public:
    int minPairSum(vector<int>& nums) {
        int maxPairSum = 0;

        sort(nums.begin(), nums.end());                                  //sort the vector in ascending order

        for(int i=0; i<nums.size()/2; i++){
            maxPairSum = max(maxPairSum, nums[i]+nums[nums.size()-i-1]); //store the minimized max pair sum
        }

        return maxPairSum;
    }
};
