//https://leetcode.com/problems/minimum-one-bit-operations-to-make-integers-zero/description/

class Solution {
public:
    //
    //  1 -> 0 (1)                                                      1 bit  -> 2^1-1 = 1
    //  10 -> 11 -> 01 -> 00 (3)                                        2 bits -> 2^2-1 = 3
    //  100 -> 101 -> 111 -> 110 -> 010 -> ... -> 000 (4) + (3) = (7)   3 bits -> 2^3-1 = 7
    //                                                                  ...
    //                                                                  N bits -> 2^N-1
    //
    //  111: 100 -> 110 -> 111
    //       (7) -3 (4) +1 (5)
    //
    //ex: n = 89
    //
    //        7654321
    //  89 -> 1011001
    //
    //        1000000 -> 0   takes 2^7-1 = 127 steps
    //        0010000 -> 0   takes 2^5-1 =  31 steps
    //        0001000 -> 0   takes 2^4-1 =  15 steps
    //        0000001 -> 0   takes 2^1-1 =   1 step
    //
    //                ->   (2^7-1) - (2^5-1) + (2^4-1) - (2^1-1)
    //                   = 127 - 31 + 15 -1
    //                   = 110
    //
    int minimumOneBitOperations(int n) {
        bitset<32> bits(n);
        string bitString = bits.to_string();

        int ans = 0, alpha = 1;

        for(int i=0; i<bitString.length(); i++){
            if(bitString[i] == '1'){
                ans += alpha * (pow(2, bitString.length()-i) - 1);
                alpha = -alpha;
            }
        }

        return ans;
    }
};
