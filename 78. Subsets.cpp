//https://leetcode.com/problems/subsets/description/

class Solution {
public:
    void findSubsets(vector<int>& arr, vector<vector<int>>& subsets, vector<int>& temp, int index){
        subsets.push_back(temp);                    // Push current subset to subsets

        for(int i=index; i<arr.size(); i++){        // For i from index to arr.size
            temp.push_back(arr[i]);                 //      Push arr[i] into current subset
            findSubsets(arr, subsets, temp, i+1);   //      Find subset in next recursion
            temp.pop_back();                        //      Pop  arr[i] from current subset
        }
    }

    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> subsets;
        vector<int> temp;

        findSubsets(nums, subsets, temp, 0); // Helper function to find subsets

        return subsets;
    }
};
