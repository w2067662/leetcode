//https://leetcode.com/problems/binary-watch/description/

class Solution {
private:
    const int Hour =12;     //for only 12 hours
    const int Minute = 60;  //for 60 minutes
public:
    string IntToString(int num){    //change int to string
        if(num ==0)return "0";  //return "0", if number is 0

        string res="";
        while(num>0){       //add number as char to string
            res+=(num%10)+'0';
            num/=10;
        }

        for(int i=0;i<res.length()/2;i++){  //reverse string
            swap(res[i], res[res.length()-i-1]);
        }
        return res;
    }
    vector<string> readBinaryWatch(int turnedOn) {
        vector<string> ans;
        string temp = "";
        bitset<4> hour;     //"000000" for <8,4,2,1>
        bitset<6> minute;   //"0000"   for <32,16,8,4,2,1>
        for(int h=0;h<Hour;h++){
            for(int m=0;m<Minute;m++){
                hour =h;    //store into bitset and count for 1's
                minute =m;  //store into bitset and count for 1's
                if(hour.count()+minute.count() ==  turnedOn){
                    if(m<10) temp = IntToString(h) + ":0" + IntToString(m); //add 0
                    else temp = IntToString(h) + ":" + IntToString(m);
                    ans.push_back(temp);
                }
            }
        }
        return ans;
    }
};
