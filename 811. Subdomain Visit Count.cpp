//https://leetcode.com/problems/subdomain-visit-count/description/

class Solution {
public:
    int getTimes(const string &s){ //get the times from the string
        int times = 0;

        for(const auto ch: s){
            if(!isdigit(ch)) return times;
            times = times*10 + ch - '0';
        }

        return times;
    }

    vector<string> getSubDomains(const string &domain){ //get the domains from the string
        vector<string> subDomains;
        vector<string> words;
        string temp = "";
        bool start = false;

        for(const auto ch: domain){                 
            if(ch == ' '){                              //IF encounter space (' ')
                start = true;                           //   -> start collecting the words
                continue;
            } 
            if(start){
                if(ch == '.'){
                    words.push_back(temp);              //store the words that are splited by '.'
                    temp = "";
                }
                else temp += ch;
            }
        }
        if(temp != "") words.push_back(temp);

        for(int i=0; i<words.size(); i++){
            temp = "";
            for(int j=i; j<words.size(); j++){
                temp += words[j];
                if(j != words.size()-1) temp += '.';
            }
            subDomains.push_back(temp);                 //store the subDomains
        }

        return subDomains;
    }

    vector<string> subdomainVisits(vector<string>& cpdomains) {
        map<string, int> map;
        vector<string> ans;

        for(const auto s: cpdomains){
            int time = getTimes(s);                                       //get the times from current domain's string
            vector<string> subDomains = getSubDomains(s);                 //get the subDomains from current domain

            for(const auto subDomain: subDomains) map[subDomain] += time; //add up times of each subDomain
        }

        for(const auto it: map){
            ans.push_back(to_string(it.second) + " " + it.first);         //reconstruct the subDomains with times
        }

        return ans;
    }
};
