//https://leetcode.com/problems/reformat-the-string/description/

class Solution {
public:
    string reformat(string s) {
        vector<char> *longerVecPtr;
        vector<char> *shorterVecPtr;
        vector<char>letters;
        vector<char>digits;
        string ans="";

        for(int i=0;i<s.length();i++){
            if(islower(s[i])){
                letters.push_back(s[i]);    //store letters into vector
            } else if (isdigit(s[i])){
                digits.push_back(s[i]);     //store digits into vector
            }
        }
        
        int letterSize = letters.size();
        int digitSize = digits.size();
        if(abs(letterSize-digitSize)>1) return "";  //if not able to reformat, then return empty string

        longerVecPtr  = letters.size() > digits.size() ? &letters : &digits;    //get longer  size vector
        shorterVecPtr = letters.size() <= digits.size() ? &letters : &digits;   //get shorter size vector

        for(int i=0;i<shorterVecPtr->size();i++){
            ans += longerVecPtr->at(i);                                         //start with longer size vector
            ans += shorterVecPtr->at(i);                                        //then  with shorter size vector
        }

        if(abs(letterSize-digitSize)==1) ans += longerVecPtr->at(longerVecPtr->size()-1); //if longerVec.size() = shorterVec.size()+1, append the last element from longerVec to answer

        return ans;
    }
};
