//https://leetcode.com/problems/make-three-strings-equal/description/

class Solution {
public:
    int findMinimumOperations(string s1, string s2, string s3) {
        int equalCount = -1;

        int minLen = min({s1.length(), s2.length(), s3.length()}); //get the min length from 3 strings

        for(int i=0; i<minLen; i++){
            if(s1[i] == s2[i] && s2[i] == s3[i]) equalCount++;     //count the equals from left to right
            else break;
        }

        return equalCount == -1 ? -1 : (s1.length()-1-equalCount) + (s2.length()-1-equalCount) + (s3.length()-1-equalCount); //return min operations
    }
};
