//https://leetcode.com/problems/rearrange-array-elements-by-sign/description/

class Solution {
public:
    vector<int> rearrangeArray(vector<int>& nums) {
        vector<int> positive, negative, ans;

        for(const auto num: nums){
            num < 0 ? negative.push_back(num) : positive.push_back(num); // Store numbers into positive and negative vectors
        }

        for(int i=0; i<positive.size(); i++){
            ans.push_back(positive[i]);                                  // Rearrange the numbers and push into answer
            ans.push_back(negative[i]);
        }

        return ans;
    }
};
