//https://leetcode.com/problems/keep-multiplying-found-values-by-two/description/

class Solution {
public:
    int findFinalValue(vector<int>& nums, int original) {
        set<int>set;

        for(const auto num: nums)set.insert(num); //insert numbers into set

        while(set.find(original) != set.end()){   //find number until next number is not found
            original *= 2;                        //IF   current number exist in set -> next number = current number * 2
        }

        return original;
    }
};
