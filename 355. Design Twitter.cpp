//https://leetcode.com/problems/design-twitter/description/

class User { //define User
public:
    int ID;
    vector<int> Follows;
    User(int id):ID(id){}
};

class Post{ //define Post
public:
    int ID;
    int Recent;
    Post(int id, int recent):ID(id), Recent(recent){}
};

bool sortByRecent(Post* a, Post* b){ //define sorting function (sort posts by recent)
    return a->Recent > b->Recent;
}

class Twitter {
private:
    int Recent;
    map<int, User*> Users;
    map<int, vector<Post*>> Posts;

    void creatUser(int userID){ //create a new User and store into Users
        User* newUser = new User(userID);
        Users[userID] = newUser;
    }

    void createPost(int userID, int postID){    //create a new post and store into Posts
        Post* newPost = new Post(postID, Recent++);
        Posts[userID].push_back(newPost);
    }
public:
    Twitter() {
        Recent = 0; //initialize recent as 0, for each time creating a new post, increase by 1
    }
    
    void postTweet(int userID, int tweetID) {
        if(Users.find(userID) == Users.end()) creatUser(userID); //IF current user not exist -> create new User

        createPost(userID, tweetID);                             //create new tweet
    }
    
    vector<int> getNewsFeed(int userID) {
        if(Users.find(userID) == Users.end()) return {};                       //IF current user not exist -> return empty vector

        vector<Post*> posts;
        vector<int> newsFeeds;

        for(const auto post: Posts[userID]) posts.push_back(post);             //push all self posts into vector

        for(const auto followID: Users[userID]->Follows){                      //push all following user's posts into vector
            for(const auto post: Posts[followID]){
                posts.push_back(post);
            }
        }

        sort(posts.begin(), posts.end(), sortByRecent);                        //sort the posts by recent

        if(posts.size() > 10){                                                 //get the 10 most recent post
            for(int i=0;i<10;i++) newsFeeds.push_back(posts[i]->ID);
        } else {
            for(int i=0;i<posts.size();i++) newsFeeds.push_back(posts[i]->ID);
        }

        return newsFeeds;
    }
    
    void follow(int followerID, int followeeID) {
        if(Users.find(followerID) == Users.end()) creatUser(followerID); //IF current follower not exist -> create new user
        if(Users.find(followeeID) == Users.end()) creatUser(followeeID); //IF current followee not exist -> create new user

        for(const auto followID: Users[followerID]->Follows){
            if(followID == followeeID) return;                           //IF already followed -> end the function
        }

        Users[followerID]->Follows.push_back(followeeID);                //follow the user
    }
    
    void unfollow(int followerID, int followeeID) {
        for(int i=0;i<Users[followerID]->Follows.size();i++){
            if(Users[followerID]->Follows[i] == followeeID){                            //find the followee
                Users[followerID]->Follows.erase(Users[followerID]->Follows.begin()+i); //unfollow the followee
            }
        }
    }
};

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter* obj = new Twitter();
 * obj->postTweet(userId,tweetId);
 * vector<int> param_2 = obj->getNewsFeed(userId);
 * obj->follow(followerId,followeeId);
 * obj->unfollow(followerId,followeeId);
 */
