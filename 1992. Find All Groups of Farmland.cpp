//https://leetcode.com/problems/find-all-groups-of-farmland/description/

class Solution {
public:
    bool isValid(const vector<vector<int>>& grid, const int& m, const int& n){ // Check current position is valid
        return 0 <= m && m < grid.size() && 0 <= n && n < grid[0].size();
    }

    void DFS(vector<vector<int>>& grid, int m, int n, int& xMax, int& yMax){   // DFS to mark "1" -> "0" AND find left bottom point
        if(!isValid(grid, m, n)) return;
        if(!grid[m][n]) return;
        
        grid[m][n] = 0;

        xMax = max(xMax, m);
        yMax = max(yMax, n);
        
        DFS(grid, m+1, n, xMax, yMax);
        DFS(grid, m-1, n, xMax, yMax);
        DFS(grid, m, n+1, xMax, yMax);
        DFS(grid, m, n-1, xMax, yMax);
    }

    vector<vector<int>> findFarmland(vector<vector<int>>& land) {
        vector<vector<int>> ans;
        
        for(int m=0; m<land.size(); m++){                // Traverse the grid
            for(int n=0; n<land[0].size(); n++){
                if(land[m][n]){                          //    IF current position is land ('1')
                    int xMax = 0, yMax = 0;
                    DFS(land, m, n, xMax, yMax);         //       -> DFS to mark current land as '0' AND store the max (x1, y2) position
                    ans.push_back({m, n, xMax, yMax});   //       -> Push the farm land range (x0, y0, x1, y1) into answer
                }
            }
        }
        
        return ans;
    }
};
