//https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/description/

class Solution {
public:
    TreeNode* buildTree(vector<int> &sorted, int left, int right){ //build the tree
        if(left > right) return NULL;

        int mid = left + (right-left)/2;      //get the middle value from the sorted list

        return new TreeNode(                  //create a new node and return to parent
            sorted[mid],
            buildTree(sorted,  left, mid-1),  //build left  subtree
            buildTree(sorted, mid+1, right)   //build right subtree
        );
    }

    TreeNode* sortedListToBST(ListNode* head) {
        vector<int> sorted;

        while(head){
            sorted.push_back(head->val);      //push value of current list node into sorted vector
            head = head->next;                //goto next list node
        }

        return buildTree(sorted, 0, sorted.size()-1); //build the tree with sorted vector
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
