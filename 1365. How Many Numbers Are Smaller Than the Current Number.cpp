//https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/description/

class Solution {
public:
    vector<int> smallerNumbersThanCurrent(vector<int>& nums) {
        map<int, int>map;
        int total=0;

        for(int i=0;i<nums.size();i++){
            map[nums[i]]++;                 //store {[number]:[appear times]} into map
        }

        for(int i=0;i<nums.size();i++){     //traverse vector
            total=0;
            for(const auto it:map){         
                if(it.first==nums[i])break; //if current element in map = current number, then end the loop
                else total+=it.second;      //count total_smaller_than_current_number
            }
            nums[i]=total;                  //change current number to  total_smaller_than_current_number
        }
        return nums;
    }
};
