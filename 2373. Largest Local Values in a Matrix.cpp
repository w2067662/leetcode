//https://leetcode.com/problems/largest-local-values-in-a-matrix/description/

class Solution {
public:
    int largest(vector<vector<int>>& grid, int x1, int y1, int x2, int y2){  // Find largest number in the range (x1-x2, y1-y2)
        int res = 0;
        for(int i=x1; i<=x2; i++){
            for(int j=y1; j<=y2; j++){
                res = max(res, grid[i][j]);
            }
        }
        return res;
    }

    vector<vector<int>> largestLocal(vector<vector<int>>& grid) {
        vector<vector<int>> matrix (grid.size()-2, vector<int>(grid[0].size()-2, 0));

        for(int i=0; i<matrix.size(); i++){
            for(int j=0; j<matrix[i].size(); j++){
                matrix[i][j] = largest(grid, i, j, i+2, j+2);                // Calculate the largest number from current slot's 3x3 matrix
            }
        }

        return matrix;
    }
};
