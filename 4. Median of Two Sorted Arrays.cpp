//https://leetcode.com/problems/median-of-two-sorted-arrays/description/

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        vector<double> ans;
        
        for(auto num:nums1) ans.push_back(num); //merge two vector
        for(auto num:nums2) ans.push_back(num);

        sort(ans.begin(), ans.end());           //sort in ascending order

        return ans.size()%2 == 0 ? (ans[ans.size()/2-1] + ans[ans.size()/2])/2 : ans[ans.size()/2]; //calculate Median
    }
};
