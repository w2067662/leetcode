//https://leetcode.com/problems/minimum-number-of-operations-to-move-all-balls-to-each-box/description/

class Solution {
public:
    vector<int> minOperations(string boxes) {
        vector<int> ans;

        for(int i=0; i<boxes.length(); i++){                // For i from 0 to boxes.length
            int operation = 0;

            for(int j=0; j<boxes.length(); j++){            //      For j from 0 to boxes.length
                if(boxes[j] == '1') operation += abs(i-j);  //          IF current char is '1'
                                                            //             -> add up the distances between i and j
            }

            ans.push_back(operation);                       //      Push the sum of distances into answer
        }

        return ans;
    }
};
