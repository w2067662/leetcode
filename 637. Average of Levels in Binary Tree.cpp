//https://leetcode.com/problems/average-of-levels-in-binary-tree/description/

class Solution {
public:
    vector<double> sum;
    vector<int> count;

    void traverse(TreeNode* root, int level) {
        if(!root)return;                      //return if no root

        if(level>=sum.size()){                //if find next level
            sum.push_back(double(root->val)); //push value AND
            count.push_back(1);               //node count to vector
        }
        else{                                 //if current level exist
            sum[level]+=double(root->val);    //add up value AND
            count[level]++;                   //node count in vector
        }

        traverse(root->left ,level+1);        //recurrsion traverse root->left  (next level)
        traverse(root->right,level+1);        //recurrsion traverse root->right (next level)
        return;
    }

    vector<double> averageOfLevels(TreeNode* root) {
        traverse(root, 0);                    //traverse the tree
        for(int i=0;i<sum.size();i++){        
            sum[i]/=double(count[i]);         //get values' average = (double)(sum / count(nodes))
        }   
        return sum;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
