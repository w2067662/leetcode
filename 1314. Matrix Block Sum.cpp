//https://leetcode.com/problems/matrix-block-sum/description/

class Solution {
public:
    bool isValid(vector<vector<int>>& mat, int x, int y){ // Check current position is valid in the range of matrix
        return 0 <= x && x < mat.size() && 0 <= y && y < mat[0].size();
    }

    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
        vector<vector<int>>ans (mat.size(), vector<int>(mat[0].size(), 0));

        for(int i=0; i<mat.size(); i++){            // Traverse the answer matrix
            for(int j=0; j<mat[i].size(); j++){

                for(int x=i-k; x<=i+k; x++){        //      Traverse the original matrix
                    for(int y=j-k; y<=j+k; y++){
                        if(isValid(mat, x, y)){     //          IF current position (x, y) -> (i-k <= x <= i+k) AND (j-k <= y <= j+k) is valid
                            ans[i][j] += mat[x][y]; //             -> sum up the value
                        }
                    }
                }
            }
        }

        return ans;
    }
};
