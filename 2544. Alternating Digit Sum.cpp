//https://leetcode.com/problems/alternating-digit-sum/description/

class Solution {
public:
    int alternateDigitSum(int n) {
        int ans = 0;
        string s = to_string(n);                        //convert integar to number string

        for(int i=0;i<s.length();i++){
            ans += i%2 == 0 ? (s[i]-'0') : -(s[i]-'0'); //IF  odd  index -> deduct 
        }                                               //IF  even index -> add

        return ans;
    }
};
