//https://leetcode.com/problems/flatten-binary-tree-to-linked-list/description/

class Solution {
private:
    vector<TreeNode*> vec;
public:
    void traverse(TreeNode* root) {      //traverse the tree and store nodes into vector
        if(!root)return;

        vec.push_back(root);

        traverse(root->left );
        traverse(root->right);

        return;
    }

    void flatten(TreeNode* root) {
        if(!root)return;

        traverse(root);                  //traverse the tree and store all nodes

        for(int i=0;i<vec.size()-1;i++){ 
            vec[i]->left  = NULL;        //set node's left  child as NULL
            vec[i]->right = vec[i+1];    //set node's right child as next node in vector
        }
        vec[vec.size()-1]->left  = NULL;
        vec[vec.size()-1]->right = NULL;

        return;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
