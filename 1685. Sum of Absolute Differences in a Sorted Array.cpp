//https://leetcode.com/problems/sum-of-absolute-differences-in-a-sorted-array/description/

class Solution {
public:
    //ex:   nums = [1,4,6,8,10]
    //                                               absDiffSum = |1-1|+|4-1|+|6-1|+|8-1|+|10-1| = 24
    //              l r r r r   diff =  4-1 = 3   ;  absDiffSum = 24 + (1-4)*3 = 15
    //              l l r r r   diff =  6-4 = 2   ;  absDiffSum = 15 + (2-3)*2 = 13
    //              l l l r r   diff =  8-6 = 2   ;  absDiffSum = 13 + (3-2)*2 = 15
    //              l l l l r   diff = 10-8 = 2   ;  absDiffSum = 15 + (4-1)*2 = 21
    //
    vector<int> getSumAbsoluteDifferences(vector<int>& nums) {
        vector<int> ans;
        int absDiffSum = 0, diff;

        for(int i=0;i<nums.size();i++){
            absDiffSum += abs(nums[i]-nums[0]);   //sum up absolute differences
        }

        ans.push_back(absDiffSum);                //push first absolute differences sum into answer

        for(int i=1;i<nums.size();i++){
            diff = nums[i]-nums[i-1];
            absDiffSum += (2*i-nums.size())*diff; //calculate next absolute differences sum
            
            ans.push_back(absDiffSum);            //push into answer
        }

        return ans;
    }
};
