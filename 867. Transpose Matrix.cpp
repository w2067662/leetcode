//https://leetcode.com/problems/transpose-matrix/description/

class Solution {
public:
    vector<vector<int>> transpose(vector<vector<int>>& matrix) {
        //create answer vector<vector<int>> for transpose matrix
        vector<vector<int>> ans (matrix[0].size(), vector<int>(matrix.size())); 

        int x=0, y=0;
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix[i].size();j++){
                ans[x][y]=matrix[i][j]; //store current number from matrix to answer
                x++;                    //goto next x in answer
                if(x>=ans.size()){      //if x oversize
                    x=0;                //  set x as 0
                    y++;                //  goto next y in answer
                }
            }
        }
        return ans;
    }
};
