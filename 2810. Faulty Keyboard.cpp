//https://leetcode.com/problems/faulty-keyboard/description/

class Solution {
public:
    void reverse(string& s){            //reverse string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
    }
    string finalString(string s) {
        string ans="";

        for(int i=0;i<s.length();i++){  
            if(s[i]=='i') reverse(ans); //if encounter 'i', reverse answer
            else ans+=s[i];             //if not 'i' , append current letter to answer
        }

        return ans;
    }
};
