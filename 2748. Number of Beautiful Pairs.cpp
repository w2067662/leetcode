//https://leetcode.com/problems/number-of-beautiful-pairs/description/

class Solution {
public:
    int GCD(int a, int b){ //count for greatest common divisor
        int res;
        while( b!=0 ){
            res = b;
            b = a%b;
            a = res;
        }
        return res;
    }

    int countBeautifulPairs(vector<int>& nums) {
        int ans = 0;
        string a = "";
        string b = "";

        for(int i=0; i<nums.size()-1;i++){
            for(int j=i+1; j<nums.size();j++){
                a = to_string(nums[i]); //convert nums[i] to number string
                b = to_string(nums[j]); //convert nums[j] to number string
                if (GCD(a[0]-'0', b[b.length()-1]-'0') == 1) ans++; //if GCD of nums[i]'s first digit and nums[j]'s last digit = 1 -> is BeautifulPairs -> answer+1
            }
        }
        return ans;
    }
};
