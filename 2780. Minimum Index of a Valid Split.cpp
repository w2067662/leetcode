//https://leetcode.com/problems/minimum-index-of-a-valid-split/description/

class Solution {
public:
    int minimumIndex(vector<int>& nums) {
        map<int, int> map1, map2;

        for(int i=0; i<nums.size(); i++) map2[nums[i]]++;       //store all the number's frequency into map2

        for(int i=0; i<nums.size()-1; i++){
            map1[nums[i]]++;                                    //calculate left  frequency of current number
            map2[nums[i]]--;                                    //calculate right frequency of current number

            if(map1[nums[i]] * 2 > i+1 &&                       //IF  current number is the dominent number of both left and right
               map2[nums[i]] * 2 > nums.size()-i-1) return i;   //    -> return current number's index as answer
        }

        return -1;
    }
};
