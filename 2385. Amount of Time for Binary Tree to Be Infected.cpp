//https://leetcode.com/problems/amount-of-time-for-binary-tree-to-be-infected/description/

class Solution {
public:
    void traverse(TreeNode* root, map<int, vector<int>> &adjacentList){ //traverse the tree
        if(!root) return;

        if(root->left){                                          //IF left node exist
            adjacentList[root->val].push_back(root->left->val);  //   -> store current node and left node's value into adjacentList
            adjacentList[root->left->val].push_back(root->val);
            traverse(root->left , adjacentList);                 //   -> traverse left subtree
        }

        if(root->right){                                         //IF right node exist
            adjacentList[root->val].push_back(root->right->val); //   -> store current node and right node's value into adjacentList
            adjacentList[root->right->val].push_back(root->val);
            traverse(root->right, adjacentList);                 //   -> traverse right subtree
        }   

        return;
    }

    void BFS(map<int, vector<int>> &adjacentList, set<int> &visited, int curr, int currTime, int &time){
        visited.insert(curr);                                       //mark current node as visited

        time = max(time, currTime);                                 //store the max time for answer

        for(const auto next: adjacentList[curr]){                   //traverse all the next node
            if(visited.find(next) == visited.end()){                //IF not visited
                BFS(adjacentList, visited, next, currTime+1, time); //   -> goto next node (infection)
            }
        }

        return;
    }

    int amountOfTime(TreeNode* root, int start) {
        map<int, vector<int>> adjacentList;
        set<int> visited;
        int time = 0;

        traverse(root, adjacentList);               //traverse the tree -> convert to adjacentList

        BFS(adjacentList, visited, start, 0, time); //use BFS to simulate the infection to get the cost time

        return time;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
