//https://leetcode.com/problems/ugly-number/description/

class Solution {
public:
    bool isUgly(int n) {
        if(n == 0) return false;    //***if n equals to 0, then return false

        int uglyFactor[3] = {2,3,5};    //store the ugly factors

        for(int i=0;i<3;i++){
            while(n%uglyFactor[i] == 0){    //keep divide 2 or 3 or 5, until number n don't have factors of 2,3,5 
                n/=uglyFactor[i];
            }
        }

        return n==1;
    }
};
