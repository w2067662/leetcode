//https://leetcode.com/problems/partition-array-for-maximum-sum/description/

class Solution {
public:
    //ex: arr = [1,15,7,9,2,5,10]  ;  k = 3 (j from 1 to K)
    //                                                                       DP = [0,0,0,0,0,0,0]
    //           ^                   i = 0, j = 1, i-j+1 (index) = 0
    //				     maxNum = max(0, 1) = 1
    //				     maxSum = max(0, 0 + 1*1) = 1      
    //								          -> DP = [1,0,0,0,0,0,0]
    //		[1,15,7,9,2,5,10]
    //		    ^                i = 1, j = 1, i-j+1 (index) = 1
    //				     maxNum = max( 0,15) = 15
    //				     DP[i-j] = DP[0] = 1
    //				     maxSum = max( 0, 0 + 15*1) = 15

    //		 ^  ^                i = 1, j = 2, i-j+1 (index) = 0
    //				     maxNum = max(15, 1) = 15
    //				     DP[i-j] = DP[-1] -> 0
    //				     maxSum = max(15, 0 + 15*2) = 30
    //								          -> DP = [1,30,0,0,0,0,0]
    //		[1,15,7,9,2,5,10]
    //		      ^              i = 2, j = 1, i-j+1 (index) = 2
    //				     maxNum = max( 0, 7) = 7
    //				     DP[i-j] = DP[1] = 30
    //				     maxSum = max( 0, 30 + 7*1) = 37

    //		    ^ ^              i = 2, j = 2, i-j+1 (index) = 1
    //				     maxNum = max( 7,15) = 15
    //				     DP[i-j] = DP[0] = 1	
    //				     maxSum = max( 7, 1 + 15*2) = 31

    //		 ^  ^ ^              i = 2, j = 3, i-j+1 (index) = 0
    //				     maxNum = max(15, 1) = 15
    //				     DP[i-j] = DP[-1] -> 0	
    //				     maxSum = max(31, 0 + 15*3) = 45
    //								          -> DP = [1,30,45,0,0,0,0]
    //		[1,15,7,9,2,5,10]
    //		        ^            i = 3, j = 1, i-j+1 (index) = 3
    //				     maxNum = max( 0, 9) = 9
    //				     DP[i-j] = DP[2] = 45
    //				     maxSum = max( 0, 45 + 9*1) = 54

    //		      ^ ^            i = 3, j = 2, i-j+1 (index) = 2
    //				     maxNum = max( 9, 7) = 9
    //				     DP[i-j] = DP[1] = 30	
    //				     maxSum = max(54, 30 + 9*2) = 54

    //		    ^ ^ ^            i = 3, j = 3, i-j+1 (index) = 1
    //				     maxNum = max( 9, 15) = 15
    //				     DP[i-j] = DP[0] = 1	
    //				     maxSum = max(54, 1 + 15*3) = 54
    //								          -> DP = [1,30,45,54,0,0,0]
    //		[1,15,7,9,2,5,10]
    //		          ^          i = 4, j = 1, i-j+1 (index) = 4
    //				     maxNum = max( 0, 2) = 2
    //				     DP[i-j] = DP[3] = 54
    //				     maxSum = max( 0, 54 + 2*1) = 56

    //		        ^ ^          i = 4, j = 2, i-j+1 (index) = 3
    //				     maxNum = max( 2, 9) = 9
    //				     DP[i-j] = DP[2] = 45	
    //				     maxSum = max(56, 45 + 9*2) = 63

    //		      ^ ^ ^          i = 4, j = 3, i-j+1 (index) = 2
    //				     maxNum = max( 9, 7) = 9
    //				     DP[i-j] = DP[1] = 30	
    //				     maxSum = max(63, 30 + 9*3) = 63
    //								          -> DP = [1,30,45,54,63,0,0]
    //		[1,15,7,9,2,5,10]
    //		            ^        i = 5, j = 1, i-j+1 (index) = 5
    //				     maxNum = max( 0, 5) = 5
    //				     DP[i-j] = DP[4] = 63
    //				     maxSum = max( 0, 63 + 5*1) = 68

    //		          ^ ^        i = 5, j = 2, i-j+1 (index) = 4
    //				     maxNum = max( 5, 2) = 5
    //				     DP[i-j] = DP[3] = 54	
    //				     maxSum = max(68, 54 + 5*2) = 68

    //		        ^ ^ ^        i = 5, j = 3, i-j+1 (index) = 3
    //				     maxNum = max( 5, 9) = 9
    //				     DP[i-j] = DP[2] = 45	
    //				     maxSum = max(68, 45 + 9*3) = 72
    //								          -> DP = [1,30,45,54,63,72,0]
    //		[1,15,7,9,2,5,10]
    //		               ^     i = 6, j = 1, i-j+1 (index) = 6
    //				     maxNum = max( 0,10) = 10
    //				     DP[i-j] = DP[5] = 72
    //				     maxSum = max( 0, 72 + 10*1) = 82

    //		            ^  ^     i = 6, j = 2, i-j+1 (index) = 5
    //				     maxNum = max(10, 5) = 10
    //				     DP[i-j] = DP[4] = 63	
    //				     maxSum = max(82, 63 + 10*2) = 83

    //		          ^ ^  ^     i = 6, j = 3, i-j+1 (index) = 4
    //				     maxNum = max(10, 2) = 10
    //				     DP[i-j] = DP[3] = 54	
    //				     maxSum = max(83, 54 + 10*3) = 84
    //								          -> DP = [1,30,45,54,63,72,84]
    //	DP = [1,30,45,54,63,72,84]
    //				^  max sum after partition

    int maxSumAfterPartitioning(vector<int>& arr, int k) {
        vector<int> DP(arr.size(), 0);

        for (int i=0, maxNum, maxSum; i<arr.size(); i++) {                   // For i from 0 to arr.size
            maxNum = maxSum = 0;

            for (int j=1; j<=k && i-j+1 >= 0; j++) {			     //		For j from 1 to K AND i-j+1 (index) should be valid
                maxNum = max(maxNum, arr[i-j+1]);			     //			Get the max number
                maxSum = max(maxSum, (i >= j ? DP[i-j] : 0) + maxNum * j);   // 		Get the max sum
                                                                             //    			(if index within array     -> compare with (DP[i-j] + maxNum * j)
                                                                             //     			 if index not within array -> compare with (maxNum * j)
            }

            DP[i] = maxSum;						     //		Store max sum to Dynamic Programming vector
        }

        return DP[arr.size()-1];					     // Return the last maxSum in DP as the answer
    }
};
