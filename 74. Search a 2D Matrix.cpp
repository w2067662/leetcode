//https://leetcode.com/problems/search-a-2d-matrix/description/

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        for(int i=0; i<matrix.size(); i++){
            if(matrix[i][0] <= target && target <= matrix[i][matrix[i].size()-1]){  //IF  target is in the range of current row
                for(int j=0; j<matrix[i].size(); j++){                              //    -> search target in current row
                    if(target == matrix[i][j]) return true;
                    if(target < matrix[i][j]) return false;                         //    IF   encounter bigger number in current row -> FLASE (no possible answer)
                }
            }
        }
        return false;
    }
};
