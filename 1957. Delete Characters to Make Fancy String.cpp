//https://leetcode.com/problems/delete-characters-to-make-fancy-string/description/

class Solution {
public:
    string makeFancyString(string s) {
        vector<string> vec;
        string temp ="";
        string ans = "";

        temp += s[0];

        for(int i=1;i<s.length();i++){           //split the string into different substring with consecutive letters
            if(s[i] != s[i-1]){
                vec.push_back(temp);
                temp = "";
            }
            temp += s[i];
        }
        if(temp != "") vec.push_back(temp);

        for(int i=0;i<vec.size();i++){
            for(int j=0;j<vec[i].length();j++){  //append atmost 2 same consecutive letters to answer
                if(j>=2) break;
                ans += vec[i][j];
            }
        }

        return ans;
    }
};
