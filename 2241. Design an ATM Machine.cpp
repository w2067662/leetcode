//https://leetcode.com/problems/design-an-atm-machine/description/

class ATM {
private:
    const int denomination = 5; //5 denominations
    long long balance;  //use long long type to catch overflow
    vector<int> dollars;
    vector<long long> banknotes; //use long long type to catch overflow
public:
    ATM() { //initialize
        balance=0;
        int dollar[5] ={20, 50, 100, 200, 500};
        for(int i=0;i<denomination;i++){
            dollars.push_back(dollar[i]);
            banknotes.push_back(0);
        }
    }
    
    void deposit(vector<int> banknotesCount) {
        for(int i=0;i<denomination;i++) banknotes[i]+=banknotesCount[i];
        updateBalance();    //update balance
    }

    void updateBalance(){   //self-added function for updating balance
        balance=0;
        for(int i=0;i<denomination;i++) balance+=(long long)(dollars[i]*banknotes[i]);  //aware of overflow
    }
    
    vector<long long> withdraw(int amount) {    //***use vector<long long> instead of vector<int>***
        vector<long long> withdraw; //for output answers, in [0,0,0,0,0] format
        vector<long long> bankNotes_temp;   //for checking bankNotes is valid

        if(balance<amount) return {-1}; //if balance not enough to withdraw then return -1

        for(int i=0;i<denomination;i++){    // initialize withdraw and bankNotes_temp vector
            withdraw.push_back(0);
            bankNotes_temp.push_back(banknotes[i]);
        }
        
        for(int i=denomination-1;i>=0;i--){ //start from biggest denomination, 500->200->100->50->20
            //ex1:
            //         count                          dollars   = [20,50,100,200,500]
            //                           amount = 600; banknotes = [ 0, 1,  0,  1,  1]; withdraw = [0,0,0,0,0]
            //600/500 = 1; 1>=1($500) -> amount = 100; banknotes = [ 0, 1,  0,  1,  0]; withdraw = [0,0,0,0,1]
            //100/200 = 0; 0< 1($200) -> amount = 100; banknotes = [ 0, 1,  0,  1,  0]; withdraw = [0,0,0,0,1]
            //100/100 = 1; 1>=0($100) -> amount = 100; banknotes = [ 0, 1,  0,  1,  0]; withdraw = [0,0,0,0,1]
            //100/ 50 = 2; 2>=1($ 50) -> amount =  50; banknotes = [ 0, 0,  0,  1,  0]; withdraw = [0,1,0,0,1]
            // 50/ 20 = 2; 2>=0($ 20) -> amount =  50; banknotes = [ 0, 0,  0,  1,  0]; withdraw = [0,1,0,0,1]
            //                           amount =  50 != 0 -> withdraw not possible -> return {-1}

            //ex2:
            //         count                          dollars   = [20,50,100,200,500]
            //                           amount = 900; banknotes = [ 1, 5,  8,  3,  2]; withdraw = [0,0,0,0,0]
            //900/500 = 1; 1< 2($500) -> amount = 400; banknotes = [ 1, 5,  8,  3,  1]; withdraw = [0,0,0,0,1]
            //400/200 = 2; 2< 3($200) -> amount =   0; banknotes = [ 1, 5,  8,  1,  1]; withdraw = [0,0,0,2,1]
            //                           amount =   0 == 0 -> withdraw possible -> return withdraw = [0,0,0,2,1]

            long long count = amount/dollars[i];    
            if(count>=bankNotes_temp[i]){
                amount-=bankNotes_temp[i]*dollars[i];
                withdraw[i]+=bankNotes_temp[i];
                bankNotes_temp[i]=0;
            }
            else {
                amount-=count*dollars[i];
                withdraw[i]+=count;
                bankNotes_temp[i]-=count;
            }
        }
        if(amount==0){ //if withdraw is possible, no denomination errors
            for(int i=0;i<denomination;i++) banknotes[i]-=withdraw[i]; //update banknotes
            updateBalance();    //update balance
            return withdraw;    //return the withdraw output
        }
        else return {-1};
    }
};

/**
 * Your ATM object will be instantiated and called as such:
 * ATM* obj = new ATM();
 * obj->deposit(banknotesCount);
 * vector<int> param_2 = obj->withdraw(amount);
 */
