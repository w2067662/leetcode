//https://leetcode.com/problems/battleships-in-a-board/description/

class Solution {
public:
    // [tips]
    // horizontally: 
    //         X X X <- body of the ship (continue)
    //         ^ 
    //         count the head of the ship as 1 battle ship
    //
    // vertically:
    //         X <- count the head of the ship as 1 battle ship
    //         X
    //         X <- body of the ship (continue)
    //
    int countBattleships(vector<vector<char>>& board) {
        int count=0;
        
        for (int i=0; i<board.size(); i++) {
            for (int j=0; j<board[i].size(); j++) {
                if (board[i][j] == '.') continue;            //IF neither head or body -> continue
                if (i > 0 && board[i-1][j] == 'X') continue; //IF is body vertically   -> continue
                if (j > 0 && board[i][j-1] == 'X') continue; //IF is body horizontally -> continue

                count++;                                     //count the head of the ship as 1 battle ship
            }
        }
        
        return count;
    }
};
