//https://leetcode.com/problems/univalued-binary-tree/description/

class Solution {
public:
    void traverse(TreeNode* root,  set<int>& set){
        if(!root)return;
        set.insert(root->val);
        traverse(root->left ,  set);
        traverse(root->right,  set);
        return;
    }

    bool isUnivalTree(TreeNode* root) {
        set<int> set;
        traverse(root, set);
        return set.size()==1;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
