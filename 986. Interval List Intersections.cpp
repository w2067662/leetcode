//https://leetcode.com/problems/interval-list-intersections/description/

class Solution {
public:
    vector<vector<int>> intervalIntersection(vector<vector<int>>& firstList, vector<vector<int>>& secondList) {
        vector<vector<int>> intersection;
        int i=0, j=0, left, right;

        while(i < firstList.size() && j < secondList.size()){
            left  = max(firstList[i][0], secondList[j][0]);          //get the intersection's left  range
            right = min(firstList[i][1], secondList[j][1]);          //get the intersection's right range

            if(left <= right) intersection.push_back({left, right}); //IF  left range <= right range
                                                                     //    -> find 1 interscetion and push into answer

            right == firstList[i][1] ? i++ : j++;                    //goto next range of firstList or secondList
        }

        return intersection;
    }
};
