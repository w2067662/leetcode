//https://leetcode.com/problems/find-the-maximum-divisibility-score/description/

class Solution {
public:
    int maxDivScore(vector<int>& nums, vector<int>& divisors) {
        map<int, vector<int>> score;                 //map = {[score]: vector{[divisor1], [divisor2], ...} }

        for(int i=0, count;i<divisors.size();i++){   //count for the score of each divisors
            count = 0;
            for(int j=0 ;j<nums.size();j++){
                if(nums[j]%divisors[i] == 0)count++;
            }
            score[count].push_back(divisors[i]);
        }

        auto it = score.rbegin();                    //get the vector of largest score from map
        sort(it->second.begin(), it->second.end());  //sort the vector in ascending order

        return it->second[0];                        //and return the divisor with min value of the vector
    }
};
