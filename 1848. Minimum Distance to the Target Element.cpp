//https://leetcode.com/problems/minimum-distance-to-the-target-element/description/

class Solution {
public:
    int getMinDistance(vector<int>& nums, int target, int start) {
        int leftDis  = INT_MAX;
        int rightDis = INT_MAX;

        for(int i=start; i<nums.size();i++){  //find target from right
            if(nums[i] == target){
                rightDis = abs(i-start);
                break;
            } 
        }

        for(int i=start; i>=0;i--){           //find target from left
            if(nums[i] == target){
                leftDis = abs(i-start);
                break;
            } 
        }

        return min(leftDis, rightDis);        //return min distance
    }
};
