//https://leetcode.com/problems/maximum-length-substring-with-two-occurrences/description/

class Solution {
private:
    static const int AtMostOccurence = 2;   // At most 2 occurance for each char
public:
    //ex: nums = [ b, c, b, b, b, c, b, a], k = 2
    //                                              frequency = {}              ;  maxLen = 0
    //            LR                                frequency = {b:1}           ;  maxLen = 1
    //            L   R                             frequency = {b:1,c:1}       ;  maxLen = 2
    //            L      R                          frequency = {b:2,c:1}       ;  maxLen = 3
    //            L         R                       frequency = {b:3,c:1}       ;  maxLen = 3
    //                                                             ^                            (3 > 2) -> LEFT+1
    //                L     R                       frequency = {b:2,c:1}       ;  maxLen = 3
    //                L        R                    frequency = {b:3,c:1}       ;  maxLen = 3
    //                                                             ^                            (3 > 2) -> LEFT+1
    //                   L     R                    frequency = {b:3}           ;  maxLen = 3
    //                                                             ^                            (3 > 2) -> LEFT+1
    //                      L  R                    frequency = {b:2}           ;  maxLen = 3
    //                      L     R                 frequency = {b:2,c:1}       ;  maxLen = 3
    //                      L        R              frequency = {b:3,c:1}       ;  maxLen = 3
    //                                                             ^                            (3 > 2) -> LEFT+1
    //                         L     R              frequency = {b:2,c:1}       ;  maxLen = 3
    //                         L        R           frequency = {a:1,b:2,c:1}   ;  maxLen = 4
    //                                                                          -> maxLen = 4
    int maximumLengthSubstring(string s) {
        map<char, int> frequency;
        int maxLen = 0;

        for(int left=0, right=0; right<s.length(); ){                      // For RIGHT < nums.size
            frequency[s[right]]++;                                         //      s[RIGHT]'s frequency + 1

            while(frequency[s[right]] > AtMostOccurence && left < right){  //      WHILE s[RIGHT]'s frequency > K AND
                                                                           //            LEFT < RIGHT
                frequency[s[left]]--;                                      //            -> s[LEFT]'s frequency - 1
                left++;                                                    //            -> Goto next LEFT
            }

            maxLen = max(maxLen, right-left+1);                            //      Store the max length
            
            right++;                                                       //      Goto next RIGHT
        }

        return maxLen;
    }
};