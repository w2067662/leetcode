//https://leetcode.com/problems/minimum-changes-to-make-alternating-binary-string/description/

class Solution {
public:
    int minOperations(string s) {
        int even0 = 0, even1 = 0;
        int  odd0 = 0,  odd1 = 0;

        for(int i=0;i<s.length();i++){
            if(i%2==0) s[i] == '0' ? even0+=1 : even1+=1; //count for 0 and 1 of chars in even index
            else       s[i] == '0' ?  odd0+=1 :  odd1+=1; //count for 0 and 1 of chars in  odd index
        }

        return min(even0+odd1, even1+odd0);               //return the min operation
    }
};
