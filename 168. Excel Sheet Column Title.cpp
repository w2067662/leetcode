//https://leetcode.com/problems/excel-sheet-column-title/description/

class Solution {
public:
    string reverseString(string s){ //reverse the answer string
        for(int i=0;i<s.length()/2;i++){
            swap(s[i], s[s.length()-i-1]);
        }
        return s;
    }
        //ex: columnNumber = 701
        //                   701-1 = 700 ; 700 >= 26 -> 700 mod 26 = 24 -> ans = "Y" -> 700 / 26= 26
        //                    26-1 =  25 ;  25 <  26 ->  25 mod 26 = 25 -> ans = "YZ"
        // reverse(ans) = "ZY"
    string convertToTitle(int columnNumber) {
        string ans ="";
        columnNumber-=1;
        while(columnNumber>=26){
            ans += columnNumber%26 + 'A';
            columnNumber = columnNumber/26 -1;
        }
        ans += columnNumber%26 + 'A';

        return reverseString(ans);  //reverse the answer string
    }
};
