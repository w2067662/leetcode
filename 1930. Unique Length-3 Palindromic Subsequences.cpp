//https://leetcode.com/problems/unique-length-3-palindromic-subsequences/description/

class Solution {
public:
    //ex:  s = "bbcbaba"                                                                ;  count = 0
    //          ^                s[left]  = 'b' (not visited) -> mark left 'b' visited
    //          ^    ^      find s[right] = 'b'
    //          ^****^    insert s[mid]                                                 ;  count = 3 (+3)
    //           ^               s[left]  = 'b' (visited) 
    //            ^              s[left]  = 'c' (not visited) -> mark left 'c' visited
    //            ^         find s[right] = 'c' (not exist)
    //             ^             s[left]  = 'b' (visited) 
    //              ^            s[left]  = 'a' (not visited) -> mark left 'a' visited
    //              ^ ^     find s[right] = 'a'
    //              ^*^   insert s[mid]                                                 ;  count = 4 (+1)
    //
    int countPalindromicSubsequence(string s) {
        set<char> visitedLeftCh;
        set<char> visitedMidCh;
        int count = 0;

        for(int left=0; left<s.length()-2; left++){
            if(visitedLeftCh.find(s[left]) == visitedLeftCh.end()){  //IF  left char not visited
                visitedLeftCh.insert(s[left]);                       //    -> mark as visited
                visitedMidCh.clear(); 

                for(int right=s.length()-1; right>=left+2; right--){
                    if(s[right] == s[left]){                         //    -> find right char same to left
                        for(int mid=left+1; mid<=right-1; mid++){
                            visitedMidCh.insert(s[mid]);             //    -> insert all the mid char between left and right into set
                        }
                        break;
                    }
                }

                count += visitedMidCh.size();                        //    -> add up count
            }
        }

        return count;
    }
};
