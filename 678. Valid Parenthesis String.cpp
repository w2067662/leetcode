//https://leetcode.com/problems/valid-parenthesis-string/description/

class Solution {
public:
    bool checkValidString(string s) {
        stack<int> star, stack;

        for(int i=0; i<s.length(); i++){               // For i from 0 to s.length
            switch(s[i]){
                case '(': stack.push(i); break;        //       IF current char is '(' -> push into stack
                case '*': star.push(i); break;         //       IF current char is '*' -> push into star
                case ')':                              //       IF current char is ')'
                    if(!stack.empty()) stack.pop();    //          IF   stack is not empty -> pop 1 '(''s index from stack (pair up)
                    else if(!star.empty()) star.pop(); //          IF   star  is not empty -> pop 1 '*''s index from stack (pair up)
                    else return false;                 //          ELSE unable to pair with current ')'
                                                       //               -> return FALSE
                    break;
            }
        }

        while( !stack.empty() && !star.empty() &&      // While stack is not empty AND start is not empty
                stack.top() < star.top() ){            //       stack's top index < star's top index
            star.pop();                                //       -> Pop top index from star  (pair up '(' with '*')
            stack.pop();                               //       -> Pop top index from stack
        }

        return stack.empty();   // IF    all braces are paired -> stack is empty (True)
                                // ELSE  exist braces unpaired -> stack is not empty (FALSE)
    }
};