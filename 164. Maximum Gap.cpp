//https://leetcode.com/problems/maximum-gap/description/

class Solution {
public:
    int maximumGap(vector<int>& nums) {
        int maxGap = 0;

        sort(nums.begin(), nums.end());        //sort the vector in ascending order

        for(int i=1, diff;i<nums.size();i++){
            diff = nums[i]-nums[i-1];          //calculate the difference of consecutive numbers
            maxGap = max(maxGap, diff);        //store the largest difference as maxGap
        }

        return maxGap;
    }
};
