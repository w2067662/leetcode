//https://leetcode.com/problems/find-the-sum-of-encrypted-integers/description/

class Solution {
public:
    int encrypt(int n){                                 // Encrypt the number
        int count = 0, maxDigit = 0, encryptedNum = 0;
        string s = to_string(n);                        // Convert integer to number string

        for(const auto& ch: s){                         // For each char in number string
            count++;                                    //      Count for digits
            maxDigit = max(maxDigit, ch-'0');           //      Store the max digit
        }

        for(int i=0; i<count; i++){
            encryptedNum = encryptedNum*10 + maxDigit;  // Encrypt the integer by max digit
        }

        return encryptedNum;
    }

    int sumOfEncryptedInt(vector<int>& nums) {
        int sum = 0;

        for(const auto& num: nums){
            sum += encrypt(num);                        // Sum up the encrypted numbers
        }

        return sum;
    }
};