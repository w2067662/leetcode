//https://leetcode.com/problems/minimum-depth-of-binary-tree/description/

class Solution {
private:
    int ans;
public:
    void reccurance(TreeNode* root, int depth){ //traverse all the node to find leaf nodes
        if(!root)return;
        if(!root->left && !root->right){    //if is leaf node,
            if(depth <ans) ans=depth;   //check for depth
        }
        else{
            reccurance(root->left , depth+1);
            reccurance(root->right, depth+1);
        }
        return;
    }
    int minDepth(TreeNode* root) {
        if(!root) return 0;
        
        ans = INT_MAX;
        reccurance(root, 1);
        return ans;
    }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
