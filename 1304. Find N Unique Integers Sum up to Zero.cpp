//https://leetcode.com/problems/find-n-unique-integers-sum-up-to-zero/description/

class Solution {
public:
    vector<int> sumZero(int n) {
        vector<int> ans;

        for(int i=1; i<=n/2; i++){    // FOR i from 1 to n/2
            ans.push_back(i);         //    Push   index into answer
            ans.push_back(0-i);       //    Push  -index into answer
        }

        if(n%2 !=0){                  // IF N is odd   
            ans.push_back(0);         //    Push 0 into answer
        }

        return ans;
    }
};
