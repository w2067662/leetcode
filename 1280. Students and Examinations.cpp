#https://leetcode.com/problems/students-and-examinations/description/

# Write your MySQL query statement below
SELECT s.student_id, s.student_name, sb.subject_name, 
COUNT(e.subject_name) AS attended_exams
FROM Students AS s
JOIN Subjects AS sb
LEFT JOIN Examinations AS e
ON s.student_id = e.student_id AND sb.subject_name = e.subject_name
GROUP BY s.student_id, sb.subject_name 
ORDER BY s.student_id, sb.subject_name 

#syntax should follow this rule: WHERE -> GROUP BY -> HAVING -> ORDER BY
