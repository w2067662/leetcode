//https://leetcode.com/problems/three-consecutive-odds/description/

class Solution {
public:
    bool threeConsecutiveOdds(vector<int>& arr) {
        int count = 0;

        for(int i=0; i<arr.size(); i++){              // FOR  all numbers within array
            count = (arr[i]%2 != 0) ? count + 1 : 0;  //      IF current number is  odd -> count + 1
                                                      //      IF current number is even -> count = 0

            if(count == 3) return true;               // IF found 3 consecutive odds -> TRUE
        }

        return false;   // No 3 consecutive odds -> FALSE
    }
};
