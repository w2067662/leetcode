//https://leetcode.com/problems/set-mismatch/description/

class Solution {
public:
    vector<int> findErrorNums(vector<int>& nums) {
        int duplicate=-1;
        int missed=1;

        sort(nums.begin(), nums.end()); //sort the vector

        if(nums[0]==missed)missed++;
        for(int i=1;i<nums.size();i++){ //find duplicate and missed
            if(nums[i]==nums[i-1])duplicate=nums[i];
            if(nums[i]==missed)missed++;
        }

        return {duplicate, missed};
    }
};
