//https://leetcode.com/problems/binary-tree-pruning/description/

class Solution {
public:
    bool containsOne(TreeNode* root){ //check current subtree has any node contains 1
        return !root ? false :  containsOne(root->left) || containsOne(root->right) || root->val == 1;
    }

    TreeNode* pruneTree(TreeNode* root) {
        if(!root) return NULL;

        if(!containsOne(root)) return NULL;

        root->left  = pruneTree(root->left);  //prune left  subtree
        root->right = pruneTree(root->right); //prune right subtree
        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
