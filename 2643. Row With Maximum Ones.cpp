//https://leetcode.com/problems/row-with-maximum-ones/description/

class Solution {
public:
    vector<int> rowAndMaximumOnes(vector<vector<int>>& mat) {
        int row = 0;
        int maxCount = 0;

        for(int i=0, count; i<mat.size(); i++){
            count = 0;
            for(int j=0; j<mat[i].size(); j++){             //sum up row values
                count += mat[i][j];
            }
            row = count > maxCount ? i : row;               //get the row with largest row sum
            maxCount = count > maxCount ? count : maxCount; //get the largest row sum
        }

        return {row, maxCount};
    }
};
