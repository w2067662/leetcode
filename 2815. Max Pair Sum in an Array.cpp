//https://leetcode.com/problems/max-pair-sum-in-an-array/description/

class Solution {
public:
    int maxDigit(int num){              //get number's max Digit
        string digits = to_string(num);
        int maxDigit = -1;
        for(int i=0;i<digits.length();i++){
            maxDigit = digits[i]-'0' > maxDigit ? digits[i]-'0' : maxDigit;
        }
        return maxDigit;
    }

    int maxSum(vector<int>& nums) {
        map<int, vector<int>>map;   //map = {[max digit]: vector{[num1], [num2],...} }
        int tempSum = -1;
        int max=-1;

        for(int i=0;i<nums.size();i++){
            map[maxDigit(nums[i])].push_back(nums[i]); //insert all numbers form vector to map
        }

        for(auto it:map){
            if(it.second.size()>=2){                      //if the numbers in vector with same max digit >= 2
                sort(it.second.begin(), it.second.end()); //sort the vector in ascending order
                                                          
                tempSum = it.second[it.second.size()-1] + it.second[it.second.size()-2];
                max = tempSum > max ? tempSum : max;      //check the sum of last number ans second last number is bigger than max or not
            }
        }

        return max;
    }
};
