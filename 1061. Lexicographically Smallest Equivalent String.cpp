//https://leetcode.com/problems/lexicographically-smallest-equivalent-string/description/

class Solution {
    struct DisjointSet {
        public:
        map<char, char> map;
        DisjointSet(){
            for(char ch='a'; ch<='z'; ch++) map[ch] = ch;          // Initialize disjoint set
        }

        char find(char ch){
            return map[ch] == ch ? ch : (map[ch] = find(map[ch])); // IF   current char match to itself 
                                                                   //      -> return current char
        }                                                          // ELSE 
                                                                   //      -> find and match current char to the smallest equivalent char and return

        void merge(char a, char b){
            map[find(a)] = map[find(b)] = min(find(a), find(b));   // Get the smallest equivalent char from A and B, and re-match the smallest equivalent char of A and B to it
        }
    };

public:
    string smallestEquivalentString(string s1, string s2, string baseStr) {
        DisjointSet dijointSet;

        for(int i=0; i<s1.length(); i++){
            dijointSet.merge(s1[i], s2[i]);            // Merge same index chars of string1 and string2
        }

        for(int i=0; i<baseStr.length(); i++){
            baseStr[i] = dijointSet.find(baseStr[i]);  // Find the smallest equivalent char of current char
        }

        return baseStr;
    }
};
