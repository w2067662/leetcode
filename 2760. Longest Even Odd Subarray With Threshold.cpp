
//https://leetcode.com/problems/longest-even-odd-subarray-with-threshold/description/

class Solution {
public:
    int longestAlternatingSubarray(vector<int>& nums, int threshold) {
        int maxLen = 0;
        bool start = false;
        int count = 0;

        for(int i=0; i<nums.size(); i++){
            if(nums[i]%2 == 0 && !start && nums[i] <= threshold){         //IF  current number is even 
                                                                          //AND smaller than threshold 
                                                                          //AND not started yet
                start = true;                                             //        -> set start as true  
            }

            if(i!=0 && start){                                            //IF  not first element and started
                if(nums[i-1] % 2 == nums[i] % 2 || nums[i] > threshold){  //    IF current number not valid
                    if(nums[i]%2 == 0 && nums[i] <= threshold) count = 1; //         IF  current number can be start number -> count +1   
                    else {                                                //         IF  current number can not be start number
                        start = false;                                    //                -> set start as FALSE
                        count = 0;                                        //                -> reset count as 0
                    }
                } else count++;                                           //    IF current number is valid    -> count+1
            } else if (start) count++;                                    //IF  is first element and started  -> count+1

            maxLen = max(count, maxLen);
        }
        return maxLen;
    }
};
