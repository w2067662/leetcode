//https://leetcode.com/problems/second-minimum-node-in-a-binary-tree/

class Solution {
public:
    set<int> set;   //store duplicate node values

    void traverse(TreeNode* root){  //traverse the tree
        if(!root)return;    

        set.insert(root->val);      //insert into set
        
        traverse(root->left );
        traverse(root->right);
        return;
    }

    int findSecondMinimumValue(TreeNode* root) {
        traverse(root);
            
        if(set.size()<2)return -1;  //if all the nodes are same value (no second min value), return -1
        auto it =set.begin();       //*it = first min value
        it++;                       //*it = second min value
        int ans = *it;

        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
