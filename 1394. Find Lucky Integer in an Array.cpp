//https://leetcode.com/problems/find-lucky-integer-in-an-array/description/

class Solution {
public:
    int findLucky(vector<int>& arr) {
        map<int, int>map;
        int ans=0;

        for(int i=0;i<arr.size();i++){
            map[arr[i]]++;
        }

        for(const auto it:map){
            if(it.first == it.second)ans=it.first;
        }

        return ans==0 ? -1 : ans; //if no possible answer return -1
    }
};
