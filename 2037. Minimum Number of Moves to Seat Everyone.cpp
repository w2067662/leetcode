//https://leetcode.com/problems/minimum-number-of-moves-to-seat-everyone/description/

class Solution {
public:
    int minMovesToSeat(vector<int>& seats, vector<int>& students) {
        int ans = 0;

        sort(seats.begin(), seats.end());       // Sort the seats inascending order
        sort(students.begin(), students.end()); // Sort the students inascending order

        for(int i=0; i<seats.size(); i++){
            ans += abs(seats[i]-students[i]);   // Calculate the difference between seats and students
        }
        return ans;
    }
};
