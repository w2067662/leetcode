//https://leetcode.com/problems/the-employee-that-worked-on-the-longest-task/description/

class Solution {
public:
    int hardestWorker(int n, vector<vector<int>>& logs) {
        map<int, vector<int>> map;
        int maxTime = 0, currTime = 0, duration;

        for(const auto log: logs){
            duration = log[1]-currTime;                //calculate the duration
            map[duration].push_back(log[0]);           //push current worker and its working duration into map
            maxTime =max(maxTime, duration);           //store the max duration
            currTime = log[1];
        }

        sort(map[maxTime].begin(), map[maxTime].end());//sort the vector of max duration in ascending order

        return map[maxTime][0];                        //return the min number worker with max working times
    }
};
