//https://leetcode.com/problems/maximum-matching-of-players-with-trainers/description/

class Solution {
public:
    //ex:  players = [4,4,7,9] ; trainers = [8,2,5,8,10]
    //
    //     playersMap  = {{4:2}, {7:1}, {9:1}}
    //     trainersMap = {{2:1}, {5:1}, {8:2}, {10:1}}
    //
    //     player = 4 ; trainer = 2  -> 4  > 2   => next trainer              => matches = 0
    //     player = 4 ; trainer = 5  -> 4 <= 5   => minFreq = min(2, 1) = 1
    //                                           => {4:2} -> {4:1(-1)}
    //                                           => {5:1} -> {5:0(-1)}
    //                                                                        => matches = 1(+1)
    //     player = 4 ; trainer = 8  -> 4 <= 8   => minFreq = min(1, 2) = 1
    //                                           => {4:1} -> {4:0(-1)}
    //                                           => {8:2} -> {8:1(-1)}
    //                                                                        => matches = 2(+1)
    //     player = 7 ; trainer = 8  -> 7 <= 8   => minFreq = min(1, 1) = 1
    //                                           => {7:1} -> {7:0(-1)}
    //                                           => {8:1} -> {8:0(-1)}
    //                                                                        => matches = 3(+1)
    //     player = 9 ; trainer = 10 -> 9 <= 10  => minFreq = min(1, 1) = 1
    //                                           =>  {9:1} ->  {9:0(-1)}
    //                                           => {10:1} -> {10:0(-1)}
    //                                                                        => matches = 4(+1)
    //
    int matchPlayersAndTrainers(vector<int>& players, vector<int>& trainers) {
        map<int, int> playersMap, trainersMap;
        int matches = 0;

        for(const auto& player: players) playersMap[player]++;          // Store {[Player]: [Frequency]} into playersMap
        for (const auto& trainer: trainers) trainersMap[trainer]++;     // Store {[Trainer]: [Frequency]} into trainersMap

        auto p = playersMap.begin();                                    // Set iterator p at playersMap's begin

        for (auto& t : trainersMap) {                                               // For all the trainers in trainersMap
            if (p == playersMap.end()) break;                                       //      IF no player -> end the loop

            while (p->first <= t.first && p != playersMap.end() && t.second > 0) {  //      WHILE  exist player AND
                                                                                    //             current trainer's frequency > 0 AND
                                                                                    //             player < trainer (can be matched)
                int minFreq = min(p->second, t.second);                             //             -> get the min frequency from player and trainer

                t.second -= minFreq;                                                //             -> deduct player and trainer until one is 0
                p->second -= minFreq;
                matches += minFreq;                                                 //             -> add up matches

                if(p->second == 0) p++;                                             //             IF current player's frequency is 0
                                                                                    //                -> go to next player
            }
        }

        return matches;
    }
};
