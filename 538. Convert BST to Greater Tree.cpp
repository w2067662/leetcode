//https://leetcode.com/problems/convert-bst-to-greater-tree/description/

class Solution {
public:
    void traverse(TreeNode* root, int &currVal){
        if(!root) return;

        traverse(root->right, currVal); //traverse right subtree

        currVal += root->val;     //add up current value with root value
        root->val = currVal;      //set root value as current value

        traverse(root->left , currVal); //traverse left  subtree

        return;
    }

    TreeNode* convertBST(TreeNode* root) {
        int startVal = 0;
        traverse(root, startVal); //traverse the tree and convert to greater tree
        return root;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
