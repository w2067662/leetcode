//https://leetcode.com/problems/check-distances-between-same-letters/description/

class Solution {
public:
    bool checkDistances(string s, vector<int>& distance) {
        map<char, vector<int>> map;

        for(int i=0;i<s.length();i++) map[s[i]].push_back(i);                     //store char's index into map

        for(const auto it: map){
            if(it.second[1]-it.second[0]-1 != distance[it.first-'a'])return false;//check distance of current char is well-spaced or not
        }

        return true;
    }
};
