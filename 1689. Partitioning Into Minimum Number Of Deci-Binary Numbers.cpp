//https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/description/

class Solution {
public:
    int minPartitions(string n) {
        int partition = 0;

        for(const auto ch: n){
            partition = max(partition, ch-'0');  // Get the largest number char as answer
        }

        return partition;
    }
};
