//https://leetcode.com/problems/k-items-with-the-maximum-sum/description/

class Solution {
public:
    int kItemsWithMaximumSum(int numOnes, int numZeros, int numNegOnes, int k) {
        int ans = 0;
        
        // Take from numOnes and Add ones
        int takeOnes = min(k, numOnes);
        ans += takeOnes;
        k -= takeOnes;

        // Take from numZeros and Add zeros
        int takeZeros = min(k, numZeros);
        k -= takeZeros;

        // Take from numNegOnes and Add negative ones
        int takeNegOnes = min(k, numNegOnes);
        ans -= takeNegOnes;

        return ans;
    }
};
