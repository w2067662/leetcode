//https://leetcode.com/problems/divisor-game/description/

class Solution {
public:
    //ex:
    //                1 2 3 4 5 6 7 8 10
    //n = 2           A                     Alice is possible to win     -> TRUE
    //n = 3           A B                   Alice is not possible to win -> FALSE
    //n = 4           A B A                 Alice is possible to win     -> TRUE
    //n = 5           A B A B               Alice is not possible to win -> FALSE
    //  ...
    //n = [even number]                     Alice is possible to win     -> TRUE
    //n = [odd  number]                     Alice is not possible to win -> FALSE
    bool divisorGame(int n) {
        return n%2 == 0;
    }
};
