//https://leetcode.com/problems/minimum-deletions-to-make-character-frequencies-unique/description/

class Solution {
public:
    //ex: s = "aaabbbcc"
    //[step1] store into unordered map                                                       -> umap = {'a':3, 'b':3, 'c':2}
    //[step2] store in container's structure                                                 -> container = {2:vector['c'], 3:vector['a', 'b']}
    //[step3] store first element from container's vector to map                             -> map  = {2:'c', 3:'a'}
    //[step4] delete non-first element's appear times from container to find possible answer -> map  = {1:'b' 2:'c', 3:'a'} ('b's appear times deduct from 3 to 1)
    typedef map<int, vector<char>> Container;
    int minDeletions(string s) {
        unordered_map<char, int> umap;
        map<int, char> map;
        Container container;
        int ans = 0;
        //[step1]
        for(int i=0; i<s.length(); i++){
            umap[s[i]]++;
        }
        //[step2]
        for(const auto it:umap){
            container[it.second].push_back(it.first);
        }
        //[step3]
        for(const auto it:container){
            map[it.first] = it.second[0];
        }
        //[step4]
        for(auto it=container.rbegin(); it!=container.rend(); it++){  //traverse the all vectors in container by key (appear times) 
            for(int i=1; i<it->second.size(); i++){                   //traverse non-first element in the vector
                for(int j=it->first; j>0 ; j--){                      //from current appear times to 0 (deletion)
                    if(map.find(j)==map.end()){                       //find a empty key (appear times) for current element in vector
                        map[j] = it->second[i];                       //pair this empty key with current element
                        break;
                    }
                    ans++;                                            //count for deletions
                }
            }
        }
        
        return ans;
    }
};
