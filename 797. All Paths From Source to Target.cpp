//https://leetcode.com/problems/all-paths-from-source-to-target/description/

class Solution {
public:
    void DFS (map<int, vector<int>> &adjacentList, vector<vector<int>> &paths, vector<int> &temp, int curr, const int end){
        temp.push_back(curr);                          //push current node to the vector's back

        if(curr == end){                               //IF  current node is the end
            paths.push_back(temp);                     //    -> store current path
            temp.pop_back();                           //    -> pop current node from vector's back
            return;                                    //    -> return
        }

        for(const auto next: adjacentList[curr]){
            DFS(adjacentList, paths, temp, next, end); //DFS next node
        }

        temp.pop_back();                               //pop current node from vector's back

        return;                                        //return to previous node
    }

    vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
        map<int, vector<int>> adjacentList;
        vector<vector<int>> paths;
        vector<int> temp;

        for(int i=0; i<graph.size(); i++){
            for(const auto node: graph[i]) adjacentList[i].push_back(node); //build the adjacent list
        }

        DFS(adjacentList, paths, temp, 0, graph.size()-1);                  //use DFS to find all paths

        return paths;
    }
};
