//https://leetcode.com/problems/group-by/description/

/**
 * @param {Function} fn
 * @return {Object}
 */
Array.prototype.groupBy = function(fn) {
    const res = {};

    for(let item of this){
        const key = fn(item);         //get the key of item by function
        if(!res[key]) res[key] = [];  //IF  no current key in res -> create new list
        res[key].push(item);          //push into res[key] list
    }

    return res;
};

/**
 * [1,2,3].groupBy(String) // {"1":[1],"2":[2],"3":[3]}
 */
