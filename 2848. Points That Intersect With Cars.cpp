//https://leetcode.com/problems/points-that-intersect-with-cars/description/

class Solution {
public:
    int numberOfPoints(vector<vector<int>>& nums) {
        set<int> set;

        for(const auto range: nums){                //traverse all the ranges in vector
            for(int i=range[0]; i<=range[1]; i++){  //insert all the numbers from range = (left, right) into set
                set.insert(i);
            }
        }

        return set.size();
    }
};
