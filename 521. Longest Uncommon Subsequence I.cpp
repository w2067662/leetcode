//https://leetcode.com/problems/longest-uncommon-subsequence-i/description/

class Solution {
public:
    int findLUSlength(string a, string b) {
        if(a==b)return -1;  //same string -> no longest uncommon subsequence
        return(a.size()>b.size())?a.size():b.size();    //return the longer string
    }
};
