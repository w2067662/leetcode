//https://leetcode.com/problems/integer-break/description/

class Solution {
public:
    //ex: 
    //n = 2  -> 1 x 1     = 1      answer = n-1
    //n = 3  -> 1 x 2     = 2      answer = n-1
    //n = 4  ->(4)        = 4      lefted = 1  ; count = 1
    //n = 5  -> 3 x(2)    = 6      lefted = 2  ; count = 1
    //n = 6  -> 3 x 3     = 9      lefted = 0  ; count = 2
    //n = 7  -> 3 x(4)    = 12     lefted = 1  ; count = 2
    //n = 8  -> 3 x 3 x(2)= 18     lefted = 2  ; count = 2
    //n = 9  -> 3 x 3 x 3 = 27     lefted = 0  ; count = 3
    //  ...
    int integerBreak(int n) {
        if(n <= 3)return n-1;

        vector<int> vec;

        int count = n/3, lefted = n%3;            //calculate count and lefted
        int product = 1;

        for(int i=0;i<count;i++) vec.push_back(3);//push [count] 3's into vector

        if(lefted > 1) vec.push_back(lefted);     //IF   lefted = 2   -> push 2 into vector
        else vec[vec.size()-1] += lefted;         //IF   lefted = 0,1 -> add to last number in vector

        for(const auto num: vec) product *= num;  //calculate product

        return product;
    }
};
