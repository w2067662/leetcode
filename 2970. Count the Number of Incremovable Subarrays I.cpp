//https://leetcode.com/problems/count-the-number-of-incremovable-subarrays-i/description/

class Solution {
public:
    bool valid(const vector<int> &arr){ //check array is strictly ascending
        for(int i=1; i<arr.size(); i++){
            if(arr[i] <= arr[i-1]) return false;
        }
        return true;
    }

    int incremovableSubarrayCount(vector<int>& nums) {
        int count = 0;

        for(int left=0; left<nums.size(); left++){                               //traverse window's left  from 0    to N
            for(int right=left; right<nums.size(); right++){                     //traverse window's right from left to N
                vector<int> temp;

                for(int i=0; i<left; i++) temp.push_back(nums[i]);               //push the left  exclusive numbers into temp 
                for(int i=right+1; i<nums.size(); i++) temp.push_back(nums[i]);  //push the right exclusive numbers into temp

                if(valid(temp)) count++;                                         //IF  temp is is strictly ascending -> count+1
            }
        }

        return count;
    }
};
