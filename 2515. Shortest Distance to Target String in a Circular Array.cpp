//https://leetcode.com/problems/shortest-distance-to-target-string-in-a-circular-array/description/

class Solution {
public:
    int closetTarget(vector<string>& words, string target, int startIndex) {
        int size = words.size();
        int right = INT_MAX;
        int left = INT_MAX;
        int ans;

        for(int i=0; i<size; i++) words.push_back(words[i]); //append the vector itself for 2 times
        for(int i=0; i<size; i++) words.push_back(words[i]);
        
        startIndex += size;                                  //set start index to subarray in the middle

        for(int i=startIndex; i<words.size(); i++){          //search target from right
            if(words[i] == target){
                right = abs(i-startIndex);
                break;
            }
        }

        for(int i=startIndex; i>=0; i--){                    //search target from left
            if(words[i] == target){
                left = abs(i-startIndex);
                break;
            }
        }

        ans = min(left, right);                              //get the min path from left and right

        return ans >= size ? -1 : ans;                       //if found no answers, return -1
    }
};
