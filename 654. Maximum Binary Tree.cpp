//https://leetcode.com/problems/maximum-binary-tree/description/

class Solution {
public:
    int getMaxNumberIndex(const vector<int>& nums, int left, int right){ //get the max number's index within the range = {left, right}
        int maxNumIndex = 0, maxNum = -1;

        for(int i=left; i<=right; i++){
            if(nums[i] > maxNum){
                maxNum = nums[i];
                maxNumIndex = i;
            }
        }

        return maxNumIndex;
    }

    TreeNode* buildTree(const vector<int>& nums, int left, int right){ //build the tree
        if(left > right) return NULL;

        int pivot = getMaxNumberIndex(nums, left, right);   //get the max number's index as pivot

        return new TreeNode(                                //create a new node with
            nums[pivot],                                    //      -> value of pivot index number in vector
            buildTree(nums,    left, pivot-1),              //      -> left  subtree built by  left subarray
            buildTree(nums, pivot+1,   right)               //      -> right subtree built by right subarray
        );
    }

    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        return buildTree(nums, 0, nums.size()-1);  //build the tree with number vector
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
