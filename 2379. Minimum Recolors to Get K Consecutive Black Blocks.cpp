//https://leetcode.com/problems/minimum-recolors-to-get-k-consecutive-black-blocks/description/

class Solution {
public:
    int minimumRecolors(string blocks, int k) {
        int ans = INT_MAX, neededRecolor;

        for(int i=0; i+k-1<blocks.length();i++){
            neededRecolor = 0;
            for(int j=i;j<=i+k-1 ;j++){                     //count for blocks that need to be recolored
                neededRecolor += blocks[j] == 'B' ? 0 : 1;
            }
            ans = min(ans, neededRecolor);                  //store the min amount of blocks needed to be recolored
        }

        return ans;
    }
};
