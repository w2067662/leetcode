//https://leetcode.com/problems/check-if-number-has-equal-digit-count-and-digit-value/description/

class Solution {
public:
    bool digitCount(string num) {
        map<int, int> map;

        for(const auto ch: num) map[ch-'0']++;   //count for the appear times of digits

        for(int i=0;i<num.length();i++){
            if(map[i] != num[i]-'0')return false;//check if number has equal digit count and digit value or not
        }

        return true;
    }
};
