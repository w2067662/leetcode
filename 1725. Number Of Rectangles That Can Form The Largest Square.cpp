//https://leetcode.com/problems/number-of-rectangles-that-can-form-the-largest-square/description/

class Solution {
public:
    int countGoodRectangles(vector<vector<int>>& rectangles) {
        int maxLength = 0;
        int count = 0;

        for(int i=0, length; i<rectangles.size();i++){
            length = min(rectangles[i][0], rectangles[i][1]);
            maxLength = length > maxLength ? length : maxLength;             //find max length
        }

        for(int i=0; i<rectangles.size();i++){
            if(min(rectangles[i][0], rectangles[i][1]) == maxLength) count++;//if possible square length from current rectangle = max length, count+1
        }

        return count;
    }
};
