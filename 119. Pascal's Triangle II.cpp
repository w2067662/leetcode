//https://leetcode.com/problems/pascals-triangle-ii/description/

class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> ans;

        for(int i=0; i<=rowIndex; i++){     //i start from 0 to current row index
            vector<int> temp (i+1, 1);      //create new vector for current row
            
            for(int j=1; j<i; j++){
                temp[j] = ans[j-1] + ans[j];//calculate the number in the middle of the row
            }

            ans = temp;                     //set current row as answer
        }

        return ans;
    }
};
