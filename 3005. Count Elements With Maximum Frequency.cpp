//https://leetcode.com/problems/count-elements-with-maximum-frequency/description/

class Solution {
public:
    int maxFrequencyElements(vector<int>& nums) {
        map<int, int> map, frequencies;

        for(const auto num: nums){
            map[num]++;                // Store the frequency of numbers into map {number: frequency}
        }

        for(const auto it: map){
            frequencies[it.second]++;  // Store the appear times of same frequency {frequency: appear times}
        }

        return frequencies.rbegin()->first * frequencies.rbegin()->second; // Answer = frequency x appear times
    }
};
