//https://leetcode.com/problems/first-unique-character-in-a-string/description/

class Solution {
public:
    int firstUniqChar(string s) {
        map<char, int> map;

        for(const auto ch: s){
            map[ch]++;                    // Store the frequency of chars into map
        }

        for(int i=0; i<s.length(); i++){  // Traverse the string
            if(map[s[i]] == 1) return i;  //    IF current char is unique -> return index
        }

        return -1;                        // IF no unique char -> return -1
    }
};
