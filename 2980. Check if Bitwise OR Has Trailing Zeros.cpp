//https://leetcode.com/problems/check-if-bitwise-or-has-trailing-zeros/description/

class Solution {
public:
    bool hasTrailingZeros(vector<int>& nums) {
        int count = 0;

        for(const auto& num: nums){
            if(num%2 == 0) count++;  //count for the amount of even numbers
        }

        return count >= 2;           //exist more than 2 even number -> able to get a trailing zeros number by OR
    }
};
