//https://leetcode.com/problems/type-of-triangle-ii/description/

class Solution {
public:
    string triangleType(vector<int>& nums) {
        sort(nums.begin(), nums.end());                 // Sort the edge's length in ascending order

        if(nums[0]+nums[1] <= nums[2]){                 // IF edges are not able to form a triangle
            return "none";                              //    -> none
        }

        if(nums[0] == nums[1] && nums[1] == nums[2]){   // IF 3 edges are equal
            return "equilateral";                       //    -> equilateral
        }

        if(nums[0] == nums[1] || nums[1] == nums[2]){   // IF 2 edges are equal
            return "isosceles";                         //    -> isosceles
        }

        return "scalene";                               // IF no edges are equal
                                                        //    -> scalene
    }
};
