//https://leetcode.com/problems/climbing-stairs/description/

class Solution {
public:
    int climbStairs(int n) {
        vector<int> ans = {0,1,2};
        
        for(int i=3; i<=n; i++){
            ans.push_back(ans[i-1]+ans[i-2]);   // Arr[i] = arr[i-1] + arr[i-2]
        }
        
        return ans[n];
    }
};
