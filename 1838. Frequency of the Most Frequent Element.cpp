//https://leetcode.com/problems/frequency-of-the-most-frequent-element/description/

class Solution {
public:
    //ex:  nums = [ 1 , 4 , 8 , 13] ;  k = 5
    //             lr                         left = 0, right = 0 ; windowSum =  1 ; overSize = 0  < k ; window = [1]      ; maxFreq = 1
    //             l    r                     left = 0, right = 1 ; windowSum =  5 ; overSize = 2  < k ; window = [1,4]    ; maxFreq = 2
    //             l        r                 left = 0, right = 2 ; windowSum = 13 ; overSize = 11 > k ; window = [1,4,8]  (shrink window)
    //                  l   r                 left = 1, right = 2 ; windowSum = 12 ; overSize = 4  < k ; window = [4,8]    ; maxFreq = 2
    //                  l        r            left = 1, right = 3 ; windowSum = 25 ; overSize = 14 > k ; window = [4,8,13] (shrink window)
    //                      l    r            left = 2, right = 3 ; windowSum = 21 ; overSize = 5  = k ; window = [8,13]   ; maxFreq = 2
    //  maxFreq = 2
    //
    int maxFrequency(vector<int>& nums, int k) {
        int maxFreq = 1 , left = 0;
        long windowSum = 0, overSize;

        sort(nums.begin(), nums.end());                                     //sort the vector in ascending order

        for(int right =0 ; right<nums.size(); right++){   
            windowSum += nums[right];                                       //add the right number into window

            overSize = (long)(right-left+1) * nums[right]  - windowSum;     //calculate the oversize
                                                                            //oversize = size of window * target number (the right most number in window) - current window sum
            
            while(overSize > k){                                            //WHILE  it's oversized (not possible to make all numbers in window same as target number)
                windowSum -= nums[left++];                                  //       -> shrink the window from left
                overSize = (long)(right-left+1) * nums[right]  - windowSum; //       -> recalculate oversize
            }

            maxFreq = max(maxFreq, right-left+1);                           //store the max frequency
        } 

        return maxFreq;
    }
};
