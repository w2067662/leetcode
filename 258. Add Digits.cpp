//https://leetcode.com/problems/add-digits/description/

class Solution {
public:
    //ex: num = 156243 (>=10)
    //    num = 0       ; temp = 156243
    //    num = 3       ; temp = 15624
    //    num = 7       ; temp = 1562
    //    num = 9       ; temp = 156
    //    num = 15      ; temp = 15
    //    num = 20      ; temp = 1
    //    num = 21      ; temp = 0

    //    num = 21     (>=10)
    //    num = 0      ; temp = 21
    //    num = 1      ; temp = 2
    //    num = 3      ; temp = 0
    
    //    num = 3      (< 10) <- answer
    int addDigits(int num) {
        int temp=0;
        while(num>=10){
            temp = num;
            num=0;
            while(temp>=10){    //add up numbers
                num+=temp%10;
                temp/=10;
            }
            num+=temp%10;
        }
        return num;
    }
};
