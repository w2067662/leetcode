//https://leetcode.com/problems/path-sum-ii/description/

class Solution {
public:
    bool isLeaf(TreeNode* root){ //check current node is leaf node or not
        return !root->left && !root->right;
    }

    int sum(vector<int> &path){ //calculate the path sum
        int sum = 0;
        for(const auto num: path) sum += num;
        return sum; 
    }

    void traverse(TreeNode* root, const int &targetSum, vector<int> &path, vector<vector<int>> &ans){
        if(!root) return;

        path.push_back(root->val);                                      //push current value into path
        traverse(root->left , targetSum, path, ans);                    //traverse left  subtree

        if(isLeaf(root) && sum(path) == targetSum) ans.push_back(path); //IF  current node is leaf node and has path sum = tartgetSum
                                                                        //    -> store current path into answer
        traverse(root->right, targetSum, path, ans);                    //traverse right subtree
        path.pop_back();                                                //pop current value from path
        
        return;
    }

    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        vector<vector<int>> ans;
        vector<int> path;

        traverse(root, targetSum, path, ans); //traverse the tree

        return ans;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
