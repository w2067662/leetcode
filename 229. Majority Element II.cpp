//https://leetcode.com/problems/majority-element-ii/description/

class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        map<int, int> map;
        vector<int> ans;
        int N = floor(nums.size()/3);                 //calculate N = ⌊ n/3 ⌋

        for(const auto num: nums) map[num]++;         //store numbers' appear times into map

        for(const auto it: map){
            if(it.second > N) ans.push_back(it.first);//IF  current number's appear times > N -> push into answer
        }

        return ans;
    }
};
