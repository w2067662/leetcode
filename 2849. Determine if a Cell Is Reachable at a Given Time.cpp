//https://leetcode.com/problems/determine-if-a-cell-is-reachable-at-a-given-time/description/

class Solution {
public:
    // .   .    .    .    .    .    .
    // .  >=2  >=2  >=2  >=2  >=2   .  1. Set point S OR point F at position 0
    // .  >=2  >=1  >=1  >=1  >=2   .  2. Calculate the distance between point S AND point F
    // .  >=2  >=1 0,>=2 >=1  >=2   .  3. Check it is reachable at time T
    // .  >=2  >=1  >=1  >=1  >=2   .
    // .  >=2  >=2  >=2  >=2  >=2   .
    // .   .    .    .    .    .    .
    bool isReachableAtTime(int sx, int sy, int fx, int fy, int t) {
        if(sx == fx && sy == fy) return t != 1;      //IF   S and F are at same position -> check T != 1
        else return t >= max(abs(sx-fx), abs(sy-fy));//ELSE -> check T >= max distance between x-axis and y-axis
    }
};
