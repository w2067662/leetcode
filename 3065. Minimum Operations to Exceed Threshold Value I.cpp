//https://leetcode.com/problems/minimum-operations-to-exceed-threshold-value-i/description/

class Solution {
public:
    int minOperations(vector<int>& nums, int k) {
        int count = 0;

        sort(nums.begin(), nums.end()); // Sort the numbers in ascending order

        for(const auto& num: nums){     // Traverse the numbers from left to right (the smallest to the biggest)
            if(num < k) count++;        //      IF    num < K -> count+1
            else break;                 //      ELSE          -> break
        }

        return count;
    }
};
