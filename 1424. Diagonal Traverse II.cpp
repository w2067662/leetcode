//https://leetcode.com/problems/diagonal-traverse-ii/description/

class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        vector<vector<pair<int, int>>> diagonalMatrix;
        vector<int> diagonalOrder;

        for(int i=0; i<nums.size(); i++){
            for(int j=0; j<nums[i].size(); j++){
                if(i+j >= diagonalMatrix.size()) diagonalMatrix.push_back({{i, j}}); //store the position of numbers into diagonalMatrix
                else diagonalMatrix[i+j].push_back({i, j});
            }
        }

        for(const auto list: diagonalMatrix){
            for(int i=list.size()-1;i>=0;i--){                                       //traverse the list from end to front
                diagonalOrder.push_back(nums[list[i].first][list[i].second]);        //push the numbers into diagonalOrder
            }
        }

        return diagonalOrder;
    }
};
