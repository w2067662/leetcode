//https://leetcode.com/problems/find-center-of-star-graph/description/

class Solution {
public:
    int findCenter(vector<vector<int>>& edges) {
        map<int, vector<int>> graph;                     // Map = {[value of current node]: vector{[accessible node1], [accessible node2],...}

        for(int i=0; i<edges.size(); i++){               // FOR  all edges, edges[i] = {node1, node2}
            graph[edges[i][0]].push_back(edges[i][1]);   //      -> mapping node1 to node2
            graph[edges[i][1]].push_back(edges[i][0]);   //      -> mapping node2 to node1
        }

        for(const auto it:graph){
            if(it.second.size() == edges.size()){
                return it.first;                         // Find the node that connect to all other nodes
            }
        }

        return -1;
    }
};
