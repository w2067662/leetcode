//https://leetcode.com/problems/count-good-nodes-in-binary-tree/description/

class Solution {
public:
    bool isGood(vector<int> &vec, int val){ //check is good node or not
        for(const auto num: vec){
            if(num > val) return false;
        }
        return true;
    }

    void traverse(TreeNode* root, vector<int> &path, int &count){
        if(!root) return;

        if(isGood(path, root->val)) count++;//IF  current node is good node -> count+1

        path.push_back(root->val);          //push current node value into path
        traverse(root->left , path, count); //traverse left  subtree
        traverse(root->right, path, count); //traverse right subtree
        path.pop_back();                    //pop last node value from path
        return;
    }

    int goodNodes(TreeNode* root) {
        vector<int> path;
        int count = 0;

        traverse(root, path, count); //traverse the tree to find good nodes

        return count;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
