//https://leetcode.com/problems/xor-operation-in-an-array/description/

class Solution {
public:
    //ex: n = 5, start = 0
    //0 -> "0"              temp = "0"             i=0, j=0
    //2 -> "10" -> "01"     temp = "0"             i=2, j=1
    //                             "01" = "01"
    //4 -> "100" -> "001"   temp = "01"            i=4, j=2
    //                             "011" = "011"
    //6 -> "110" -> "011"   temp = "011"           i=6, j=3
    //                             "011" = "000"
    //8 -> "1000" -> "0001" temp = "000"
    //                             "0001" = "0001" i=8, j=4 [end]
    //"0001" -> "1000" -> 8 (answer)
    //
    string toBinary(int n){
        if(n==0)return "0";
        string s="";
        while(n>0){
            s+=n%2+'0';
            n/=2;
        }
        return s;
    }

    int toIntegar(string s){
        int res=0;
        for(int i=s.length()-1;i>=0;i--){
            res = res*2 + s[i] - '0';
        }
        return res;
    }

    string XOR(string a, string b){
        string longer = a.length()>b.length()? a : b;
        string shorter = a.length()>b.length()? b : a;

        for(int i=0;i<shorter.length();i++){
            if(shorter[i] == longer[i]) longer[i]='0';
            else longer[i]='1';
        }

        return longer;
     }

    int xorOperation(int n, int start) {
        string temp="";
        for(int i=start, j=0; j<n;i+=2, j++){
            if(temp=="")temp=toBinary(i);
            else temp = XOR(temp, toBinary(i));
        }
        return toIntegar(temp);
    }
};
