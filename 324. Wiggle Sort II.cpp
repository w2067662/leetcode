//https://leetcode.com/problems/wiggle-sort-ii/description/

class Solution {
public:
    //ex:  nums = [1,5,1,1,6,4,7]
    //
    //    nums' = [7,6,5,4,1,1,1] (sorted)
    //
    //   right  = [  7,  6,  5]
    //    left  = [4,  1,  1,  1]
    //
    //   nums'' = [4,7,1,6,1,5,1]
    //
    void wiggleSort(vector<int>& nums) {
        vector<int> left, right;

        sort(nums.begin(), nums.end(), [&](int& a, int& b){      // Sort the vector in descending order
            return a > b;
        });

        right.assign(nums.begin(), nums.begin()+nums.size()/2);  // Assign 0 ~ N/2 to right
        left.assign(nums.begin()+nums.size()/2, nums.end());     // Assign N/2 ~ N to left

        for(int i=0; i<left.size(); i++){
            nums[i*2] = left[i];                                 // Assign the value from left  to nums
        }

        for(int i=0; i<right.size(); i++){
            nums[i*2+1] = right[i];                              // Assign the value from right to nums
        }
    }
};
