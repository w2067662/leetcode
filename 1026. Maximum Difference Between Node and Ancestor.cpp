//https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/description/

class Solution {
public:
    void getMaxDiff(TreeNode* root, const int &val, int &diff){ //traverse the subtree of current root and get max difference
        if(!root) return;

        diff = max(diff, abs(root->val-val)); //calculate and store the difference

        getMaxDiff(root->left , val, diff);
        getMaxDiff(root->right, val, diff);
        return;
    }

    void traverse(TreeNode* root, int &maxDiff){ //traverse the tree to get the max difference
        if(!root) return;

        int diff = 0;
        getMaxDiff(root, root->val, diff); //get the max difference of current node
        maxDiff = max(diff, maxDiff);      //store the max difference

        traverse(root->left , maxDiff);
        traverse(root->right, maxDiff);
        return;
    }

    int maxAncestorDiff(TreeNode* root) {
        int maxDiff = 0;
        traverse(root, maxDiff);
        return maxDiff;
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
