//https://leetcode.com/problems/maximum-product-of-three-numbers/description/

class Solution {
public:
    int maximumProduct(vector<int>& nums) {
        sort(nums.begin(), nums.end());                                            //sort the vector
        
        int choice1 = nums[nums.size()-1]*nums[nums.size()-2]*nums[nums.size()-3]; //the 3 biggest positive numbers product
        int choice2 = nums[nums.size()-1]*nums[0]*nums[1];                         //2 smallest negative numbers AND
                                                                                   //1 biggest positive number product

        return choice1 > choice2 ? choice1 : choice2;                              //return bigger one
    }
};
