//https://leetcode.com/problems/squares-of-a-sorted-array/description/

class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {
        for(auto& num: nums){
            num = pow(num, 2);          // Calculate square of current number
        }

        sort(nums.begin(), nums.end()); // Sort the squares in ascending order

        return nums;
    }
};
