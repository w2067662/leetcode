//https://leetcode.com/problems/maximum-score-after-splitting-a-string/description/

class Solution {
public:
    int maxScore(string s) {
        int maxScore = 0;

        for(int i=1, leftCount, rightCount; i<s.length(); i++){
            leftCount = rightCount = 0;                  //reset leftCount and rightCount

            for(int left=0; left<i; left++){             //count '0's in left  substring
                if(s[left] == '0')leftCount++;
            }

            for(int right=i; right<s.length(); right++){ //count '1's in right substring
                if(s[right] == '1')rightCount++;
            }

            maxScore = leftCount+rightCount > maxScore ? leftCount+rightCount : maxScore; //get max score (score = leftCount + rightCount)
        }

        return maxScore;
    }
};
