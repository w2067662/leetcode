//https://leetcode.com/problems/split-linked-list-in-parts/description/

class Solution {
public:
    vector<ListNode*> splitListToParts(ListNode* head, int k) {
        vector<ListNode*> ans (k); //create vector with size k
        int count = 0;
        ListNode* temp = head;

        while(temp){               //count for nodes in list
            count++;
            temp = temp->next;
        }

        temp = head;                //set temp as head

        for(int i=0, times, avg = count/k, lefted = count%k; i<ans.size();i++, lefted--){
            ListNode* newNode = temp;           //create 'newNode' for storing into vector as head of splited list
            times = lefted > 0 ? avg+1 : avg;   //if there are lefted nodes, times = avg+1, else times = avg

            for(int time=0; time<times ;time++){//temp goto next node for [times] times
                if(time == times-1){            //when temp goto next node for the last time -> break the connection of the list
                    ListNode* toNull = temp;    //create 'toNull' node for pointing to NULL
                    temp = temp->next;          //temp goto next
                    toNull->next=NULL;          //'toNull's next points to NULL
                }
                else temp = temp->next;         //if temp goto next node not for the last time -> goto next node directly
            }

            ans[i] = newNode;                   //store the 'newNode' as head of splited list into vector
        }

        return ans;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
